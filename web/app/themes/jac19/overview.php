<?php
/**
 * Template Name: Overview
 */
?>

<?php if($post->post_content || WP_ENV == 'development') { ?>
<div class="section custom-content-wrap">
  <div class="container sub-main">
    <?php the_content(); ?>
  </div>
</div><!--/.section-->
<?php } ?>

<?php
$query = new WP_Query([
  'post_type' => 'page',
  'post_status' => 'publish',
  'orderby' => 'menu_order',
  'order' => 'ASC',
  'posts_per_page' => -1,
  'post_parent' => get_the_ID()
]);

if ($query->have_posts()) { ?>

  <section class="section-overview section-alternating">
      <?php
      while ($query->have_posts()) {
        global $post;
        $query->the_post();
        if($post->menu_order >= 0) {
          get_template_part('templates/content','overview');
        }
      }
      ?>
  </section>

<?php }
wp_reset_postdata(); ?>
