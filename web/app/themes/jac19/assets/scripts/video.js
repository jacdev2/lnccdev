(function($) {

  $( document ).ready(function() {

    if ($('#video-gallery.postbox').length) { // check if video meta box exists

      $(document)

        .on('click', '.js-video-add-video', function (e) {

          e.preventDefault();

          var _this = $(this);

          showLoader();

          $.ajax({
            type: 'post',
            dataType: 'html',
            url: ajaxurl,
            data: {
              action: 'jac_video_new_video',
              post: _this.data('post')
            },
            success: function (r) {
              hideLoader();
              $('.js-video-wrap').append(r);
            }
          });

        })

        .on('click', '.js-video-delete-video', function (e) {

          e.preventDefault();

          var _this = $(this);

          if (_this.closest('.js-video').hasClass('js-unsaved')) {
            _this.closest('.js-video').hide('slow', function () {
              $(this).remove();
            });
          } else {
            showLoader();
            $.ajax({
              type: 'post',
              dataType: 'json',
              url: ajaxurl,
              data: {
                action: 'jac_video_delete_video',
                post: _this.data('post'),
                order: _this.data('order'),
                url: _this.data('url')
              },
              success: function (r) {
                hideLoader();
                if (r.success) {
                  _this.closest('.js-video').hide('slow', function () {
                    $(this).remove();
                  });
                } else {
                  alert('Error! Please refresh and try again.');
                }
              }
            });

          }

        })

      ; // $(document)

    } // close for check video meta box exists

  });

})(jQuery);
