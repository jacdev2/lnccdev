<?php
/**
 * Review order table
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="shop_table woocommerce-checkout-review-order-table">
	<ul class="cart-totals-row nolist">
		<li class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></li>
		<li class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></li>
	</ul>
	<?php
		do_action( 'woocommerce_review_order_before_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<ul class="cart-totals-row nolist <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
					<li class="product-name">
						<span><?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;'; ?></span>
						<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
						<?php echo WC()->cart->get_item_data( $cart_item ); ?>
					</li>
					<li class="product-total">
						<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
					</li>
				</ul>
				<?php
			}
		}

		do_action( 'woocommerce_review_order_after_cart_contents' );
	?>

		<ul class="cart-totals-row nolist cart-subtotal">
			<li><?php _e( 'Subtotal', 'woocommerce' ); ?></li>
			<li><?php wc_cart_totals_subtotal_html(); ?></li>
		</ul>

		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
			<ul class="cart-totals-row nolist cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
				<li><?php wc_cart_totals_coupon_label( $coupon ); ?></li>
				<li><?php wc_cart_totals_coupon_html( $coupon ); ?></li>
			</ul>
		<?php endforeach; ?>

		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

			<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

		<?php endif; ?>

		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
			<ul class="cart-totals-row nolist fee">
				<li><?php echo esc_html( $fee->name ); ?></li>
				<li><?php wc_cart_totals_fee_html( $fee ); ?></li>
			</ul>
		<?php endforeach; ?>

		<?php if ( wc_tax_enabled() && WC()->cart->tax_display_cart === 'excl' ) : ?>
			<?php if ( get_option( 'woocommerce_tax_total_display' ) === 'itemized' ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
					<ul class="cart-totals-row nolist tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
						<li><?php echo esc_html( $tax->label ); ?></li>
						<li><?php echo wp_kses_post( $tax->formatted_amount ); ?></li>
					</ul>
				<?php endforeach; ?>
			<?php else : ?>
				<ul class="cart-totals-row nolist tax-total">
					<li><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></li>
					<li><?php wc_cart_totals_taxes_total_html(); ?></li>
				</ul>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

		<ul class="cart-totals-row nolist order-total">
			<li><?php _e( 'Total', 'woocommerce' ); ?></li>
			<li><?php wc_cart_totals_order_total_html(); ?></li>
		</ul>

		<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

</div><!--/.shop_table.woocommerce-checkout-review-order-table-->