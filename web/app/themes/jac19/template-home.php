<?php
/**
 * Template Name: Home Template
 */
?>
<?php // while (have_posts()) : the_post(); ?>

<?php // endwhile; ?>

<section class="slider js-height">
<?php

// Check rows exists.
if( have_rows('home_slider') ):

    // Loop through rows.
    while( have_rows('home_slider') ) : the_row();

        // Load sub field value.
        $slider_image = get_sub_field('slider_image');
        $slider_title = get_sub_field('slider_title');
        $slider_subtitle = get_sub_field('slider_subtitle');
        $link_to_open = get_sub_field('link_to_open');
        // Do something...
        ?>

        <article class="slide" data-bg="<?=$slider_image?>" style="z-index:1">
            <div class="content">
                <h3><?=$slider_title?>.</h3>
                <p><?=$slider_subtitle?></p>
                <a href="<?=$link_to_open?>" class="slider-link">More</a>
            </div>
        </article>

        <?php

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;
?>
</section>
    
<nav class="slider-nav"><a class="next" href="#">↓</a></nav>