<?php
/*
The below code is just an example. Modify the markup as needed.
Change the "js-menu-open" class to "js-menu-open-left" if you
want off-canvas to open from the left. Don't forget to add
class "left" to nav.menu-off-canvas as well.
*/
?>
<?php if(is_front_page()){

    $args = [
        'post_type' => 'slide',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => 4,
    ];

    $query = new WP_Query($args);
    $totalpagescount = new WP_Query([
        'posts_per_page' => -1,
        'post_type'      => 'page',
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
        'post_status' => 'publish',
        'post_parent' => 0
    ]);
    $totalparentcount = $totalpagescount->post_count;
    $total = $query->post_count;

    ?>
    <nav class="menu-fixed">
        <div class="bottombar knockout">
            <div class="top-right-icons">
            </div>
        </div>
    </nav><!--(dev) slide dots on left menu bar-->
    <header>
        <div class="side-toggle-menu">
            <span id="toggle_menu">
                <i class="fa fa-bars" aria-hidden="true"></i><br>
                <span class="very-small-text">MENU</span>
            </span>
        </div>

        <div <?php if($total > 1) { echo ' class="js-slider-nav"'; } ?>>
            <?php
            $counter = 1;
            while ($query->have_posts()) {
                $query->the_post();?>
                <?php
                $metaClone = [];
                for($i=1; $i<=$totalparentcount; $i++){
                    $meta = get_post_meta(get_the_ID(),'management_team-'.$i);
                    echo $meta[0];
                    $metaClone[$i] = $meta[0];
                }
                global $wp_query;
                if (in_array($wp_query->post->ID, $metaClone)){
                    echo '<span>slide dot</span>';
                } else{}
                ?>
                <?php
                // get_template_part('templates/content', get_post_type());
            } // while have posts
            ?>
        </div><!--/.js-slick-slides (dev) slide dots on left menu bar-->

        <div class="side-menu">
            <span id="close_button">
                <i class="fa fa-times" aria-hidden="true"></i>
            </span>
                <img src="<?php echo get_template_directory_uri()?>/assets/images/u89.png" class="side-logo">

                <?php

                wp_nav_menu( array(
                    'theme_location' => 'side_menu',
                    'container_class' => 'first-menu' ) );

                wp_nav_menu( array(
                    'theme_location' => 'side_bottom',
                    'container_class' => 'second-menu' ) );

                ?>
        </div>
    </header>

<?php }elseif (is_page('members')) {
    $args = [
        'post_type' => 'slide',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => -1,
    ];

    $query = new WP_Query($args);
    $totalpagescount = new WP_Query([
        'posts_per_page' => -1,
        'post_type'      => 'page',
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
        'post_status' => 'publish',
        'post_parent' => 0
    ]);
    $totalparentcount = $totalpagescount->post_count;
    $total = $query->post_count;

    ?>
    <nav class="menu-fixed">
        <div class="bottombar knockout">
            <div class="top-right-icons">
            </div>
        </div>
    </nav><!--(dev) slide dots on left menu bar-->
    <header>
        <div class="side-toggle-menu">
            <span id="toggle_menu">
                <i class="fa fa-bars" aria-hidden="true"></i><br>
                <span class="very-small-text">MENU</span>
            </span>
        </div>

        <div <?php if($total > 1) { echo ' class="js-slider-nav"'; } ?>>
            <?php
            $counter = 1;
            while ($query->have_posts()) {
                $query->the_post();?>
                <?php
                $metaClone = [];
                for($i=1; $i<=$totalparentcount; $i++){
                    $meta = get_post_meta(get_the_ID(),'management_team-'.$i);
                    echo $meta[0];
                    $metaClone[$i] = $meta[0];
                }
                global $wp_query;
                if (in_array($wp_query->post->ID, $metaClone)){
                    echo '<span>slide dot</span>';
                } else{}
                ?>
                <?php
                // get_template_part('templates/content', get_post_type());
            } // while have posts
            ?>
        </div><!--/.js-slick-slides (dev) slide dots on left menu bar-->

        <div class="side-menu">
            <span id="close_button">
                <i class="fa fa-times" aria-hidden="true"></i>
            </span>
            <img src="<?php echo get_template_directory_uri()?>/assets/images/u89.png" class="side-logo">

            <?php

            wp_nav_menu( array(
                'theme_location' => 'side_menu',
                'container_class' => 'first-menu' ) );

            wp_nav_menu( array(
                'theme_location' => 'side_bottom',
                'container_class' => 'second-menu' ) );

            ?>
        </div>
    </header>

<?php } else { ?>

    <nav class="menu-fixed">
      <div class="bottombar bg-gray knockout">
        <div class="container flex-container">
          <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
            <img src="<?= roots\sage\assets\asset_path('images/logo.png'); ?>" alt="<?php bloginfo('name'); ?>"/>
          </a>

          <ul class="menu-primary">
            <?php
              $ID_page = get_the_ID();
              if (has_nav_menu('primary_navigation')) {
                $nav_items = wp_get_nav_menu_items('primary');
                foreach ($nav_items as $nav_item){?>
                    <li <?php if($nav_item->object_id == $ID_page){?>class="active" <?php }{}?>>
                        <a href="<?php echo $nav_item->url ?>" class="toptitle"> <?php echo $nav_item->title; ?> </a>
                            <?php
                                $ID =  $nav_item->object_id;
                               $pages = get_pages('child_of=' . $ID);

                                if (count($pages) > 0){?>
                                    <div class="container-dropdown">
                                        <div class="flexbox">
                                            <div class="flexone">
                                                <h3><?php echo $nav_item->title; ?> </h3>
                                            </div>
                                            <div class="flextwo">
                                                <?php
                                                    $ID =  $nav_item->object_id;
                                                    get_child_links($ID);
                                                ?>
                                            </div>
                                        </div>
                                    </div><!-- -submenu -->
                                <?php } else {}
                            ?>
                    </li>
                  <?php
                }
              }
            ?>
          </ul>
          <div class="menu-right">
            <ul class="menu-side nolist">
             <span class="menu-search js-popup-search"><i class="fa fa-search"></i></span>
              <li class="menu-open bar-container">
                <span><i></i><i></i><i></i></span>
              </li>
            </ul>
          </div>
          <ul class="menu-utility nolist">
            <li class="menu-open js-menu-open">
              <span><i></i><i></i><i></i></span>
            </li>
          </ul>

        </div><!-- container -->
      </div><!-- bottom-bar -->
    </nav><!--/.sticky-nav-->

<?php } ?>