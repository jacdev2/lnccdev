<?php
  $currentID = get_the_ID();

  $args = [
    'post_type' => 'event',
    'post__not_in' => array( $currentID ),
    'posts_per_page' => 3,
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'meta_key' => '_jac_event_end',
    'meta_query' => [
      'key' => '_jac_event_end',
      'value' => time(),
      'compare' => '>=',
    ]
  ];

  $query = new WP_Query($args);

  if ($query->have_posts()) { ?>
    <div class="tile-sidebar sidebar-archives bg-primary knockout">
      <div class="inner">
        <h3>Upcoming Events</h3>
      </div><!--/.inner-->
      <ul class="archive nolist">


    <?php
      $x = 1;
      while ($query->have_posts()) {
        $query->the_post();
        // get_template_part('templates/content', get_post_type());
    ?>

      <li class="js-accordion<?php if($x == 1) { echo " expanded"; } ?>">
        <h4 class="js-accordion-title"><?php the_title(); ?></h4>
        <ul class="archive-months">
          <li class="inner">
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>">Event Details ></a>
          </li><!--/.inner-->
        </ul>
      </li>

    <?php
        $x++;
      } // if while
    ?>
      </ul><!--/.archive-->
    </div><!--/.tile-sidebar.sidebar-events-->
  <?php wp_reset_postdata();
} // end if have posts
