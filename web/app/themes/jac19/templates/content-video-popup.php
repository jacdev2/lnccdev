<?php
  $classes = [ 'tile', 'tile-'.get_post_type(), 'bg-white' ];

  if(is_post_type('post')) {
    $terms = get_the_category();
  } else {
    $terms = wp_get_post_terms( get_the_ID(), get_post_type() . '_categories' );
  }

  if($terms && !is_post_type('page')) {
    $classes[] = 'has-terms';
  }

  $doc = get_post_meta(get_the_ID(), '_doc', true);
  $linkage_text = get_post_meta(get_the_ID(), '_linkage_text', true);
  $linkage_link = get_post_meta(get_the_ID(), '_linkage_link', true);
  $linkage_option = get_post_meta(get_the_ID(), '_linkage_option', true);

  $event_start = (int)get_post_meta($post->ID, '_jac_event_start', true);

  $feat = get_post_thumbnail_id();
  $secondary = get_post_meta(get_the_ID(), get_post_type() . '_secondary-image_thumbnail_id', true);

  if(!empty($meta['_exp'])) {
    $exp_timestamp = DateTime::createFromFormat('Y-m-d', $meta['_exp'][0]);
    $exp_reformat = $exp_timestamp->format('F j, Y');
  }

  if($secondary) {
    $imageID = $secondary;
  } else {
    $imageID = $feat;
  }

  // Button Text. Force "Download" when Document is uploaded.
  if($doc) {
    $button_text = "Download";
  } elseif($linkage_text) {
    $button_text = $linkage_text;
  } else {
    $button_text = 'Read More';
  }

  // Link URL. Uploaded document overrides linkage_url
  if($doc) {
    $linkage_url = wp_get_attachment_url($doc);
  } elseif($linkage_link) {
    $linkage_url = $linkage_link;
  } elseif(is_post_type('application')) {
    $linkage_url = null;
  } else {
    $linkage_url = get_the_permalink();
  }

  // Open in new tab if document uploader or new window selected
  if($doc) {
    $target = ' target="_blank"';
  } elseif($linkage_option == 'new') {
    $target = ' target="_blank"';
  } else {
    $target = null;
  }

  // Popup if popup selected and document is empty
  if($doc) {
    $popup = null;
  } elseif($linkage_option == 'popup') {
    $popup = ' class="js-popup"';
  } else {
    $popup = null;
  }
  $video_link = get_post_meta($post->ID, '_video_popup_link', true);
  $video_height = get_post_meta($post->ID, '_video_popup_height', true);
  $video_width = get_post_meta($post->ID, '_video_popup_width', true);
?>
<div class="video-popup-checker inner">
    <?php 
        if( $feat){?>
            <img style="margin: 0 auto;" class="image" src="<?= wp_get_attachment_image_src( $feat, 'full')[0] ?>)">
            <?php
        } else{}
        if( $video_link){?>
            <iframe <?php if($video_height || $video_width){?> style="height: <?=$video_height?>px; width: <?=$video_width?>px"<?php } ?> class="js-popup" src="<?= $video_link ?>">
            </iframe><?php
        } else{}
    ?>
</div>