<?php if (isset($_GET['key']) && isset($_GET['login'])) { ?>

<div class="modal modal-login lead-form">

    <div class="titlebar">
        <h2 class="login_title">Reset Your Password</h2>
        <p class="subtitle" style="text-align: center;">Please enter your new password.</p>
    </div><!--/.titlebar-->

    <form action="<?php the_permalink(); ?>" method="post" class="modal-form bg-white login js-jac-account-form" data-action="reset">

        <div class="input-wrap form-row form-row-wide">
            <input type="password" class="input-text" name="password" id="reg_password" />
            <label for="reg_password">Password <span class="required">*</span></label>
        </div>

        <div class="input-wrap form-row form-row-wide">
            <input type="password" class="input-text" name="password2" id="reg_password_confirm" />
            <label for="reg_password_confirm">Confirm <span class="required">*</span></label>
        </div>

        <div class="inner form-row">

            <p class="js-success-message js-error-message"></p>

            <input type="hidden" name="key" value="<?= $_GET['key'] ?>">
            <input type="hidden" name="login" value="<?= $_GET['login'] ?>">

            <button class="secondary" type="submit">Reset</button>

        </div><!--/.inner-->

    </form>
</div><!--/.modal-login-->

<?php } else { ?>

<div class="modal modal-login lead-form">

    <div class="titlebar">
        <h2 class="login_title">Lost Your Password?</h2>
        <p class="subtitle" style="text-align: center;">Please enter your email address.</p>
    </div><!--/.titlebar-->

    <form action="<?php the_permalink(); ?>" method="post" class="modal-form bg-white login js-jac-account-form" data-action="password">

        <div class="input-wrap form-row form-row-wide">
            <input id="login-email" type="email" name="email" />
            <label class="modal-field login-email" for="login-email">Email</label>
        </div>

        <div class="inner form-row">

            <p class="js-success-message js-error-message"></p>

            <button class="secondary" type="submit">Reset</button>

            <p class="login-forgot">
                Remembered your password?
                <a href="<?php the_permalink(); ?>">Click Here</a>
            </p>

        </div><!--/.inner-->

    </form>
</div><!--/.modal-login-->

<?php }
