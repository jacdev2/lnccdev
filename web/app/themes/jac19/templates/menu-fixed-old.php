<?php
/*
The below code is just an example. Modify the markup as needed.
Change the "js-menu-open" class to "js-menu-open-left" if you
want off-canvas to open from the left. Don't forget to add
class "left" to nav.menu-off-canvas as well.
*/
?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/styles/style.css">

<script src='<?php echo get_template_directory_uri()?>/assets/scripts/script.js'></script>

<header>
    <div class="side-toggle-menu">
        <span id="toggle_menu">
            <i class="fa fa-bars" aria-hidden="true"></i><br>
            <span class="very-small-text">MENU</span>
        </span>
    </div>
    <div class="side-menu">
        <span id="close_button">
            <i class="fa fa-times" aria-hidden="true"></i>
        </span>
        <img src="<?php echo get_template_directory_uri()?>/assets/images/u89.png" class="side-logo">

        <?php
        
        wp_nav_menu( array( 
            'theme_location' => 'side_menu', 
            'container_class' => 'first-menu' ) ); 

        wp_nav_menu( array( 
            'theme_location' => 'side_bottom', 
            'container_class' => 'second-menu' ) ); 

        ?>
        
    </div>
</header>