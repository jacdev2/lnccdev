<?php
  $classes = [ 'tile-search', 'bg-gray-lighter', 'tile-lift'];

  if(is_post_type('post')) {
    $terms = get_the_category();
  } else {
    $terms = wp_get_post_terms( get_the_ID(), get_post_type() . '_categories' );
  }

  $doc = get_post_meta(get_the_ID(), '_doc', true);
  $linkage_text = get_post_meta(get_the_ID(), '_linkage_text', true);
  $linkage_link = get_post_meta(get_the_ID(), '_linkage_link', true);
  $linkage_option = get_post_meta(get_the_ID(), '_linkage_option', true);

  $event_start = (int)get_post_meta($post->ID, '_jac_event_start', true);

  $feat = get_post_thumbnail_id();
  $secondary = get_post_meta(get_the_ID(), get_post_type() . '_secondary-image_thumbnail_id', true);

  if(!empty($meta['_exp'])) {
    $exp_timestamp = DateTime::createFromFormat('Y-m-d', $meta['_exp'][0]);
    $exp_reformat = $exp_timestamp->format('F j, Y');
  }

  if($secondary) {
    $imageID = $secondary;
  } else {
    $imageID = $feat;
  }

  // Button Text. Force "Download" when Document is uploaded.
  if($doc) {
    $button_text = "Download";
  } elseif($linkage_text) {
    $button_text = $linkage_text;
  } else {
    $button_text = 'Read More';
  }

  // Link URL. Uploaded document overrides linkage_url
  if($doc) {
    $linkage_url = wp_get_attachment_url($doc);
  } elseif($linkage_link) {
    $linkage_url = $linkage_link;
  } elseif(is_post_type('application')) {
    $linkage_url = null;
  } else {
    $linkage_url = get_the_permalink();
  }

  // Open in new tab if document uploader or new window selected
  if($doc) {
    $target = ' target="_blank"';
  } elseif($linkage_option == 'new') {
    $target = ' target="_blank"';
  } else {
    $target = null;
  }

  // Popup if popup selected and document is empty
  if($doc) {
    $popup = null;
  } elseif($linkage_option == 'popup') {
    $popup = ' class="js-popup"';
  } else {
    $popup = null;
  }

if($linkage_url){ ?>
  <a href="<?= $linkage_url ?>" <?= $target ?> <?php post_class($classes); ?>>
<?php }else{ ?>
  <article <?php post_class($classes); ?>>
<?php } ?>

  <header class="tile-image">
    <?php if($event_start) { ?>
    <div class="timestamp bg-primary knockout">
      <p title="<?= get_the_time('M j, Y'); ?>"><?= date('M j, Y', $event_start); ?></p>
    </div><!--/.timestamp-->
  <?php } elseif(get_post_type() == 'post') { ?>
      <div class="timestamp bg-primary knockout">
        <p title="<?= get_the_time('M j, Y'); ?>"><?php the_time('M j, Y'); ?></p>
      </div><!--/.timestamp-->
    <?php } ?>
    <?php JAC\Get\article_srcset( $imageID ); ?>
  </header>

  <div class="entry-summary">
    <div class="inner">
      <?php
        the_title('<h3 class="entry-title">','</h3>');
        if(get_post_type() == 'team_member'){
          the_subtitle('<p class="title">','</p>');
        }
      ?>
    </div><!--/.inner-->
  </div><!--/.entry-summary-->
<?php
if($linkage_url){ ?>
  </a>
<?php }else{ ?>
  </article>
<?php }
