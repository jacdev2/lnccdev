<?php
$args = [
  'post_type' => 'event',
  'post_status' => 'publish',
  'orderby' => 'meta_value_num',
  'order' => 'ASC',
  'meta_key' => '_jac_event_end',
  'posts_per_page' => -1,
  'meta_query' => [
    [
      'key' => '_jac_event_end',
      'value' => time(),
      'compare' => '>=',
    ]
  ]
];

$query = new WP_Query($args);

if ( $query->have_posts() ) { ?>
  <div class="section section-events bg-gray-lighter">
    <div class="section titlebar blurb">
      <h2>Upcoming Events</h2>
    </div><!--/.titlebar-->
    <div class="container">
      <div class="tile-wrap tile-wrap-event">
    <?php
    while ($query->have_posts()) {
      $query->the_post();
      get_template_part('templates/content', get_post_type());
    } // while have posts
    ?>
    </div><!--/.tile-wrap-event-->
  </div><!--/.container-->
</div><!--/.section-events-->
  <?php
} // if have posts
wp_reset_postdata();
?>

<div class="section bg-gray-lightest">
  <div class="titlebar blurb">
    <h2>Past Events</h2>
  </div><!--/.blurb-->
</div><!--/.titlebar-->
