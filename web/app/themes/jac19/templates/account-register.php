<div class="modal modal-login lead-form">
    <h2 class="login_title">Create an Account</h2>
    <p class="subtitle" style="text-align: center;">Please enter your email address and password.</p>

    <form action="<?php the_permalink(); ?>" method="post" class="modal-form bg-white login js-jac-account-form" data-action="register">

        <div class="input-wrap form-row form-row-wide">
            <input type="email" class="input-text" name="email" id="reg_email" />
            <label for="reg_email" style="font-size:12px;">Email address <span class="required" style="font-size:14px;">*</span></label>
        </div>

        <div class="input-wrap form-row form-row-wide">
            <input type="password" class="input-text" name="password" id="reg_password" />
            <label for="reg_password" style="font-size:12px;">Password <span class="required">*</span></label>
        </div>

        <div class="input-wrap form-row form-row-wide">
            <input type="password" class="input-text" name="password2" id="reg_password_confirm" />
            <label for="reg_password_confirm" style="font-size:12px;">Confirm <span class="required">*</span></label>
        </div>

        <div class="inner form-row">
            <p class="js-success-message js-error-message"></p>

            <button class="secondary" type="submit">Register</button>

            <p class="login-forgot">
                Already have an account?
                <a href="<?php the_permalink(); ?>">Click Here</a>
            </p>
        </div><!--/.inner-->

    </form>
</div><!--/.modal-login-->
