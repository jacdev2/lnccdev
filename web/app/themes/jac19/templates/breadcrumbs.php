<?php if ( function_exists('yoast_breadcrumb') ) { ?>
  <div class="yoast-breadcrumb bg-primary knockout">
    <div class="container">
      <?php
      add_filter('wpseo_breadcrumb_links', function ($crumbs) {
        $new = [];
        foreach ($crumbs as $crumb) {
          if (isset($crumb['ptarchive'])) {

            global $jac_post_overrides;
            if (isset($jac_post_overrides)) {
              foreach ($jac_post_overrides as $jac_post_override) {
                if ($crumb['ptarchive'] == $jac_post_override[1]) {
                  $cid = get_id_by_path($jac_post_override[0]);
                  $pid = wp_get_post_parent_id($cid);
                  if (!empty($pid))
                    $new[] = [
                      'id' => $pid
                    ];
                  if (!empty($cid))
                    $new[] = [
                      'id' => $cid
                    ];
                }
              }
            } else {
              $new[] = $crumb;
            }

          } else {
            $new[] = $crumb;
          }
        }
        return $new;
      });
      yoast_breadcrumb('<p class="breadcrumbs">','</p>'); ?>
    </div><!--/.container-->
  </div><!--/.pagination-wrap-->
<?php } ?>
