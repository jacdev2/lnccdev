<?php if(is_user_logged_in()){ ?>
	<nav class="woocommerce-MyAccount-navigation">
		<div class="section-split">
			<div class="split-col">
				<ul class="custom-account-nav">
					<li <?php if(is_page( 'account-dashboard' )){ echo "class='is-active'"; } ?>>
						<a href="<?= bloginfo('url') ?>/account-dashboard"  title="View your account dashboard">
							<i class="far fa-user"></i>
							<h3>Dashboard</h3>
						</a>
					</li>
					<li <?php if(is_wc_endpoint_url( 'orders' )){ echo "class='is-active'"; } ?>>
						<a href="<?= bloginfo('url') ?>/account/orders"  title="View your orders">
							<i class="far fa-truck"></i>
							<h3>Orders</h3>
						</a>
					</li>
					<li <?php if(is_wc_endpoint_url( 'edit-address' )){ echo "class='is-active'"; } ?>>
						<a href="<?= bloginfo('url') ?>/account/edit-address" title="View/edit your addresses">
							<i class="far fa-location-arrow"></i>
							<h3>Addresses</h3>
						</a>
					</li>
                    <li <?php if(is_wc_endpoint_url( 'edit-account' )){ echo "class='is-active'"; } ?>>
                        <a href="<?= bloginfo('url') ?>/account/edit-account" title="View your account details">
                            <i class="far fa-address-card"></i>
                            <h3>My&nbsp;Details</h3>
                        </a>
                    </li>
				</ul>
			</div><!-- split-col -->

			<div class="split-col align-right-no-mobile">
				<a href="<?= wp_logout_url(); ?>" title="Log out of your account" class="button primary">Logout</a>
			</div><!-- split-col -->

	</nav>

<?php }else{ ?>

	<nav class="woocommerce-MyAccount-navigation">
		<div class="section-split">
			<div class="split-col">
				<ul class="custom-account-nav">
					<li <?php if(is_page( 'account-dashboard' )){ echo "class='is-active'"; } ?>>
						<a href="<?= bloginfo('url') ?>/account-dashboard"  title="View your account dashboard">
							<i class="far fa-user"></i>
							<h3>Account</h3>
						</a>
					</li>
				</ul>
			</div><!-- split-col -->

			<div class="split-col align-right-no-mobile">
				<?php if(!is_page('account-dashboard')){ ?>
					<a href="<?= bloginfo('url') ?>/account-dashboard" title="Login" class="button primary">Login/Register</a>
				<?php } ?>
			</div><!-- split-col -->

	</nav>
<?php } ?>
