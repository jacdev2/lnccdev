<?php
/**
 * Template Name: Members Template
 */

$first_section = get_field('first_section');
$second_section = get_field('second_section');
$third_section = get_field('third_section');
$fourth_section = get_field('fourth_section');
?>

<div class="rest-body">
    <section class="header-section">
        <div class="grid-x">
            <div class="medium-6 cell">
                <a href="home.html"><img src="<?php echo get_template_directory_uri()?>/assets/images/u263.png" class="header-logo"></a>
            </div>
            <div class="medium-6 cell"></div>
        </div>
    </section>
    <div class="below-header">
        <span></span><span></span><span></span><span></span><span></span>
    </div>
    <div class="breadcrumb">
        <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> 
        <a href="#">membership</a> > 
        <a href="#">overview</a>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="medium-6 cell">
                <div class="heading-style"><span><?php echo $first_section['title']?></span></div>
                <p class="member-para"><?php echo $first_section['description']?></p>
                <a href="<?php echo $first_section['button_link']?>" class="green-btn"><?php echo $first_section['button_text']?></a>
            </div>
            <div class="medium-6 cell">
                <div class="grid-x">
                    <div class="medium-4 cell">
                        <div class="icon-box">
                            <img src="<?php echo $first_section['benefit_icon_1']['icon_image']?>">
                            <p><?php echo $first_section['benefit_icon_1']['icon_text']?></p>
                        </div>
                    </div>
                    <div class="medium-4 cell">
                        <div class="icon-box">
                            <img src="<?php echo $first_section['benefit_icon_2']['icon_image']?>">
                            <p><?php echo $first_section['benefit_icon_2']['icon_text']?></p>
                        </div>
                    </div>
                    <div class="medium-4 cell">
                        <div class="icon-box">
                            <img src="<?php echo $first_section['benefit_icon_3']['icon_image']?>">
                            <p><?php echo $first_section['benefit_icon_3']['icon_text']?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blank-space-35"></div>
        
        <div class="grid-x">
            <div class="medium-4 cell">
                <div class="points-list">
                    <?php
                    foreach ($second_section['list'] as $value) { 
                    ?>
                        <p>
                            <?php echo $value['list_item']?>
                        </p>
                    <?php
                    }
                    ?>
                    <a href="<?php echo $second_section['view_all_link']?>">View All</a>
                </div>
            </div>
            <div class="medium-8 cell">
                <ul class="members-services">
                    <?php
                    foreach ($second_section['benefit_list'] as $value) { 
                    ?>
                    <li>
                        <img src="<?php echo $value['benefit_image']?>" alt="">
                        <p><?php echo $value['benefit_text']?></p>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        
    </div>
    <hr>
    
    <div class="grid-container">
        <div class="grid-x">
        <?php
        foreach ($third_section as $value) { ?>
            <div class="medium-3 cell">
                <div class="flip-card bg-blue">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <img src="<?php echo $value['companys_logo']?>">
                            <h2><?php echo $value['company_name']?></h2>
                            <p><?php echo $value['description']?></p>
                            <div class="card_btn">
                                <a href="<?php echo $value['join_now_link']?>" class="join-btn">Join now</a>
                                <a href="<?php echo $value['view_button_link']?>" class="mem-btn">View Member</a>
                            </div>
                        </div>
                        <div class="flip-card-back">
                            <h3><?php echo $value['company_name']?></h3> 
                            <p><?php echo $value['backside_offer_text']?></p> 
                            <span>valid until: <?php echo $value['offer_validity']?></span>
                            <div class="btn-img">
                                <p class="btn-back-img"><img class="back-img" src="<?php echo $value['companys_logo']?>"></p>
                                <p class="btn-back-btn"><a href="<?php echo $value['view_offer_link']?>" class="view-btn">View</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
            
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x">

        <?php
        foreach ($fourth_section as $value) { ?>
            <div class="medium-3 cell">
                <div class="flip-card bg-green">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <img src="<?php echo $value['companys_logo']?>">
                            <h2><?php echo $value['company_name']?></h2>
                            <p><?php echo $value['description']?></p>
                            <div class="card_btn">
                                <a href="<?php echo $value['join_now_link']?>" class="join-btn">Join now</a>
                                <a href="<?php echo $value['view_button_link']?>" class="mem-btn">View Member</a>
                            </div>
                        </div>
                        <div class="flip-card-back">
                            <h3><?php echo $value['company_name']?></h3> 
                            <p><?php echo $value['backside_offer_text']?></p> 
                            <span>valid until: <?php echo $value['offer_validity']?></span>
                            <div class="btn-img">
                                <p class="btn-back-img"><img class="back-img" src="<?php echo $value['companys_logo']?>"></p>
                                <p class="btn-back-btn"><a href="<?php echo $value['view_offer_link']?>" class="view-btn">View</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

            <!-- <div class="medium-3 cell">
                <div class="flip-card bg-green">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <img src="<?php echo get_template_directory_uri()?>/assets/images/LeGrow.png">
                            <h2>LEGROW’S TRAVEL</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus</p>
                            <div class="card_btn">
                                <a href="#" class="join-btn">Join now</a>
                                <a href="#" class="mem-btn">View Member</a>
                            </div>
                        </div>
                        <div class="flip-card-back">
                            <h3>LEGROW’S TRAVEL</h3> 
                            <p>$50.00 off a vacation package booking </p> 
                            <span>valid until: July 2022</span>
                            <div class="btn-img">
                                <p class="btn-back-img"><img class="back-img" src="<?php echo get_template_directory_uri()?>/assets/images/LeGrow.png"></p>
                                <p class="btn-back-btn"><a href="#" class="view-btn">View</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            
        </div>
    </div>
    <div class="grid-container">
        <div class="get-update-bg">
            <div class="grid-x">
                <div class="medium-3 cell">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/arrow.png">
                </div>
                <div class="medium-9 cell">
                    <div class="update-bg">
                        <h2>GET THE LATEST UPDATES</h2>
                        <span>Offers, Promotions & Updates</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor.</p>
                        <form class="site-search single-block-form" method="get" action="/search">
                            <input type="text" name="" id="search-site" placeholder="Please enter your email address" class="form-control sbf__input">
                            <input type="submit" value="Search" class="button btn-submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>