<?php

namespace JAC\Taxonomies;

add_action('init', function () {

  // register_taxonomy('project_cat', ['project'], [
  //   'label' => 'Categories',
  //   'public' => true,
  //   'show_in_nav_menus' => false,
  //   'show_tagcloud' => false,
  //   'show_admin_column' => false,
  //   'hierarchical' => true,
  //   'rewrite' => [
  //     'slug' => 'project-categories',
  //     'with_front' => false
  //   ],
  // ]);

});

add_action( 'create_category', __NAMESPACE__ . '\\save_custom_tax' );
add_action( 'edit_category', __NAMESPACE__ . '\\save_custom_tax' );
function save_custom_tax ($term_id) {

  if (isset($_REQUEST['ct'])) {
    foreach ($_REQUEST['ct'] as $field => $value) {
      if (!empty($value)) {
        add_term_meta($term_id, $field, $value, true) or update_term_meta($term_id, $field, $value);
      } else {
        delete_term_meta($term_id, $field);
      }
    }
  }
}

add_action('category_add_form_fields', function ($taxonomy) {
  ?>
  <div class="form-field term-subtitle-wrap">
    <label for="tag-subtitle">Subtitle</label>
    <input name="ct[subtitle]" id="tag-subtitle" type="text">
    <p></p>
  </div>
<?php });

add_action('category_edit_form', function ( $tag, $taxonomy ) { ?>
  <table class="form-table">
    <tr class="form-field term-subtitle-wrap">
      <th scope="row">
        <label for="subtitle">Subtitle</label>
      </th>
      <td>
        <input name="ct[subtitle]" id="subtitle" type="text" value="<?= get_term_meta($tag->term_id, 'subtitle', true); ?>">
        <p class="description"></p>
      </td>
    </tr>
  </table>
<?php }, 10, 2 );
