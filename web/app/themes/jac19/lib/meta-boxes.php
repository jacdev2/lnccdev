<?php

namespace JAC\MetaBoxes;

function meta_fields( $fields = [ ['Please Build Your Field Array'] ], $set = null ) {
  global $post;

  foreach ( $fields as $field ) {
    if(isset($field[0])) {
      $label = $field[0];
      $slug = preg_replace('/[^A-Za-z0-9\-]/', '', ( str_replace(' ', '-', $field[0] ) ) );
    } else {
      $label = '';
      $slug = '';
    }

    if(isset($field[1])) { $name = $field[1]; } else { $name = ''; }
    if(isset($field[2])) { $type = $field[2]; } else { $type = 'text'; }
    if(isset($field[3])) { $class = $field[3]; } else { $class = ''; }
//    if(isset($field[4])) { $placeholder = $field[4]; } else { $placeholder = $label; }

  ?>

    <div class="input-wrap <?= $type ?>-wrap <?= $class ?> <?= $set ?>">
      <label for="jac-meta-<?= $slug ?>"><?= $label ?></label>
      <input
        id="jac-meta-<?= $slug ?>"
        type="<?= $type ?>"
        name="cf[<?= $name ?>]"
        value="<?= get_post_meta($post->ID, $name, true); ?>"
      />
    </div><!--/.input-wrap <?= $type ?>-wrap-->

  <?php
  } // end foreach

} //

add_action('save_post', function ($post_id) {
  $editor_id = WYSIWYG_EDITOR_ID;
  $meta_key = WYSIWYG_META_KEY;

  if(isset($_REQUEST[$editor_id]))
      update_post_meta($_REQUEST['post_ID'], WYSIWYG_META_KEY, $_REQUEST[$editor_id]);
      
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }

  if ( isset( $_REQUEST['cf'] ) ) {
    foreach ($_REQUEST['cf'] as $field => $value) {

      //fix human
      switch ($field) {
        case '_linkage_link':
          if (!empty( $value ) && !isset(parse_url($value)['scheme'])) $value = 'http://' . $value;
          break;
      }

      if (!empty($value)) {
        add_post_meta($post_id, $field, trim($value), true) or
        update_post_meta($post_id, $field, trim($value));
      } else {
        delete_post_meta($post_id, $field);
      }
    }
  }

  if ( isset( $_REQUEST['cfm'] ) ) {
    foreach ($_REQUEST['cfm'] as $field => $values) {

      $old = get_post_meta($post_id, $field) ? get_post_meta($post_id, $field) : [];
      if (!empty($old)) {
        //remove old
        foreach ($old as $item) {
          if (in_array($item, $values)) continue;
          delete_post_meta($post_id, $field, $item);
        }
      }

      if (count($values) == 1 && empty($values[0])){
        delete_post_meta($post_id, $field);
        continue;
      }

      //add new
      foreach ($values as $item) {
        if (in_array($item, $old)) continue;
        if (empty($item)) continue;
        add_post_meta($post_id, $field, $item);
      }

    }
  }

});

define('WYSIWYG_META_BOX_ID', 'my-editor');
define('WYSIWYG_EDITOR_ID', 'myeditor'); //Important for CSS that this is different
define('WYSIWYG_META_KEY', 'extra-content');

add_action('add_meta_boxes', function ($post_type, $post){
    if (post_type_supports($post_type, 'wysiwyg')) {
        add_meta_box( 'wysiwyg', 'Secondary Editor',__NAMESPACE__ . '\\wysiwyg_render_meta_box', $post_type, 'normal', 'default');
}}, 10, 2);

function wysiwyg_render_meta_box(){

    global $post;

    $meta_box_id = WYSIWYG_META_BOX_ID;
    $editor_id = WYSIWYG_EDITOR_ID;

    //Add CSS & jQuery goodness to make this work like the original WYSIWYG
    echo "
                <style type='text/css'>
                        #$meta_box_id #edButtonHTML, #$meta_box_id #edButtonPreview {background-color: #F1F1F1; border-color: #DFDFDF #DFDFDF #CCC; color: #999;}
                        #$editor_id{width:100%;}
                        #$meta_box_id #editorcontainer{background:#fff !important;}
                </style>

                <script type='text/javascript'>
                        jQuery(function($){
                                $('#$meta_box_id #editor-toolbar > a').click(function(){
                                        $('#$meta_box_id #editor-toolbar > a').removeClass('active');
                                        $(this).addClass('active');
                                });

                                if($('#$meta_box_id #edButtonPreview').hasClass('active')){
                                        $('#$meta_box_id #ed_toolbar').hide();
                                }

                                $('#$meta_box_id #edButtonPreview').click(function(){
                                        $('#$meta_box_id #ed_toolbar').hide();
                                });

                                $('#$meta_box_id #edButtonHTML').click(function(){
                                        $('#$meta_box_id #ed_toolbar').show();
                                });
				//Tell the uploader to insert content into the correct WYSIWYG editor
				$('#media-buttons a').bind('click', function(){
					var customEditor = $(this).parents('#$meta_box_id');
					if(customEditor.length > 0){
						edCanvas = document.getElementById('$editor_id');
					}
					else{
						edCanvas = document.getElementById('content');
					}
				});
                        });
                </script>
        ";

    //Create The Editor
    $content = get_post_meta($post->ID, WYSIWYG_META_KEY, true);
    wp_editor($content, $editor_id);

    //Clear The Room!
    echo "<div style='clear:both; display:block;'></div>";
}

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'disable-yoast')) {
    remove_meta_box( 'wpseo_meta', $post_type, 'normal' );
    $wpseo_titles = get_option('wpseo_titles');
    if (isset($wpseo_titles['hideeditbox-' . $post_type]) && $wpseo_titles['hideeditbox-' . $post_type] == false) {
      $wpseo_titles['hideeditbox-' . $post_type] = true;
      $wpseo_titles['noindex-' . $post_type] = true;
      update_option('wpseo_titles', $wpseo_titles);
    }

    add_action('admin_head', function ($post) { ?>
      <style>
        .misc-pub-section.yoast-seo-score {
          display: none !important;
        }
      </style>
      <?php
    });
  }

}, 11, 2);

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'linkage')) {
    add_meta_box('linkage', 'Link Target', __NAMESPACE__ . '\\meta_box_linkage', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_linkage($post) { ?>
  <div class="jac-metabox">

    <div class="jac-meta-fields">
      <?php

        $fields = [
          //['Label','id','value','type','class']
          [ 'Link URL',    '_linkage_link', 'text', 'half' ],
          [ 'Button Text', '_linkage_text', 'text', 'third' ],
        ];

        foreach($fields as $field) {

          if(get_post_meta($post->ID, $field[1])) {
            $value = get_post_meta($post->ID, $field[1], true);
          } else {
            $value = null;
          }
      ?>
        <div class="input-wrap <?= $field[2] ?>-wrap <?= $field[3] ?>">
          <label for="<?= $field[1] ?>"><?= $field[0] ?></label>
          <input
            id="<?= $field[1] ?>"
            name="cf[<?= $field[1] ?>]"
            value="<?= $value ?>"
            type="<?= $field[2] ?>"
          />
        </div><!--/.<?= $field[2] ?>-->
      <?php } ?>
    </div><!--/.jac-meta-fields-->

    <p>Open Link in:</p>

    <ul class="jac-metabox-checkboxes">

      <?php
        $options = [
          [ 'Same Tab/Window',                              'same'  ],
          [ 'New Tab/Window <small>rel="external"</small>', 'new'   ],
          [ 'Video Popup <small>class="js-popup"</small>',  'popup' ],
        ];

        $option_value = get_post_meta($post->ID, '_linkage_option', true);

        foreach($options as $option) { ?>

          <li>
            <input
              type="radio"
              id="link-option-<?= $option[1] ?>"
              name="cf[_linkage_option]"
              value="<?= $option[1] ?>"
              <?php if( ($option[1] == $option_value ) || ( empty($option_value) && $option[1] == 'same' ) ) { ?>
                checked="checked"
              <?php } ?>
            />
            <label for="link-option-<?= $option[1] ?>"><?= $option[0] ?></label>
          </li>

        <?php } ?>

    </ul><!--/.jac-metabox-checkboxes-->
  </div><!--/.jac-metabox-->
<?php }

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'seclinkage')) {
    add_meta_box('secondarylinkage', 'Secondary Link Target', __NAMESPACE__ . '\\meta_box_secondary_linkage', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_secondary_linkage($post) { ?>
  <div class="jac-metabox">

    <div class="jac-meta-fields">
      <?php

        $fields = [
          //['Label','id','value','type','class']
          [ 'Secondary Link URL',    '_sec_linkage_link', 'text', 'half' ],
          [ 'Secondary Button Text', '_sec_linkage_text', 'text', 'third' ],
        ];

        foreach($fields as $field) {

          if(get_post_meta($post->ID, $field[1])) {
            $value = get_post_meta($post->ID, $field[1], true);
          } else {
            $value = null;
          }
      ?>
        <div class="input-wrap <?= $field[2] ?>-wrap <?= $field[3] ?>">
          <label for="<?= $field[1] ?>"><?= $field[0] ?></label>
          <input
            id="<?= $field[1] ?>"
            name="cf[<?= $field[1] ?>]"
            value="<?= $value ?>"
            type="<?= $field[2] ?>"
          />
        </div><!--/.<?= $field[2] ?>-->
      <?php } ?>
    </div><!--/.jac-meta-fields-->

    <p>Open Link in:</p>

    <ul class="jac-metabox-checkboxes">

      <?php
        $options = [
          [ 'Same Tab/Window',                              'same'  ],
          [ 'New Tab/Window <small>rel="external"</small>', 'new'   ],
          [ 'Video Popup <small>class="js-popup"</small>',  'popup' ],
        ];

        $option_value = get_post_meta($post->ID, '_sec_linkage_option', true);

        foreach($options as $option) { ?>

          <li>
            <input
              type="radio"
              id="sec_link-option-<?= $option[1] ?>"
              name="cf[_sec_linkage_option]"
              value="<?= $option[1] ?>"
              <?php if( ($option[1] == $option_value ) || ( empty($option_value) && $option[1] == 'same' ) ) { ?>
                checked="checked"
              <?php } ?>
            />
            <label for="sec_link-option-<?= $option[1] ?>"><?= $option[0] ?></label>
          </li>

        <?php } ?>

    </ul><!--/.jac-metabox-checkboxes-->
  </div><!--/.jac-metabox-->
<?php }

function details_social_channel($post) {
  $icons = [
    'facebook-f', 'facebook-official', 'facebook-square',
    'twitter', 'twitter-squre',
    'linkedin', 'linkedin-square',
    'instagram',
    'google-plus', 'google-plus-circle',
    'youtube', 'youtube-play', 'youtube-square',
    'vimeo', 'vimeo-square',
    'pinterest', 'pinterest-p', 'pinterest-square',
    'tripadvisor',
    'behance', 'behance-square',
    'bookmark', 'cloud', 'cog', 'comment',
    'envelope', 'envelope-o', 'envelope-square',
    'external-link', 'chain', 'rss'
  ];
?>
  <div class="jac-metabox">
    <div class="jac-meta-fields">

      <div class="input-wrap select-wrap quarter">
        <label for="social-icon">Icon</label>
        <select id="social-icon" name="cf[_social_icon]">
          <option value="">Select Icon</option>
          <?php
            foreach($icons as $icon) {
              $name = ucwords(str_replace("-", " ", preg_replace('/\\.[^.\\s]{3,4}$/', '', $icon) ));
          ?>
            <option value="<?= $icon ?>"<?php if (get_post_meta($post->ID, '_social_icon', true) == $icon) echo ' selected'; ?>><?= $name ?></option>

          <?php } ?>
        </select>
      </div><!--/.select-wrap-->

      <?php
        $fields = [
          //['Label','id','value','type','class', 'note']
          [ 'Handle',     '_social_handle', 'text', 'quarter', 'Optional' ],
          [ 'Profile URL', '_social_url',    'url',  'half',   'Full URL: Include http:// or mailto:' ],
        ];
        meta_fields($fields);
      ?>

    </div><!--/.jac-meta-fields-->
  </div><!--/.jac-metabox-->
<?php } // details_social_channel

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'cta-selector')) {
    add_meta_box('cta_selector', 'Call to Action Selector', __NAMESPACE__ . '\\meta_box_cta_selector', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_cta_selector($post) {
  ?>
    <div class="jac-metabox">
      <ul class="jac-metabox-checkboxes">

        <?php
          $ctas = get_posts([
            'posts_per_page' => -1,
            'post_type'      => 'call_to_action',
            'orderby'        => 'menu_order',
            'order'          => 'ASC',
          ]);
          $cf = get_post_custom();
  ?>
            <li>
              <input
                type="radio"
                id="cta-clear"
                name="cf[_cta]"
                value=""
                <?php if( !get_post_meta($post->ID, '_cta', true) ) { ?>
                  checked="checked"
                <?php } ?>
              />
              <label for="cta-clear"><strong>Clear Selection</strong><small>Display Featured CTA</small></label>
            </li>

            <li>
              <input
                type="radio"
                id="cta-none"
                name="cf[_cta]"
                value="none"
                <?php if( get_post_meta($post->ID, '_cta', true) == 'none' ) { ?>
                  checked="checked"
                <?php } ?>
              />
              <label for="cta-none"><strong>No CTA</strong><small>Disable Featured CTA</small></label>
            </li>
  <?php
          foreach($ctas as $option) {
  ?>
            <li>
              <input
                type="radio"
                id="cta-<?= $option->ID ?>"
                name="cf[_cta]"
                value="<?= $option->ID ?>"
                <?php if($option->ID == get_post_meta($post->ID, '_cta', true)) { ?>
                  checked="checked"
                <?php } ?>
              />
              <label for="cta-<?= $option->ID ?>"><strong><?= $option->post_title ?></strong><small><?= get_the_date('Y/m/d',$option->ID) ?></small></label>
            </li>
  <?php
          }
  ?>
      </ul><!--/.jac-metabox-checkboxes-->
    </div><!--/.jac-metabox-->
  <?php
} // cta selector

function details_call_to_action($post) {
  $cta_ID = $post->ID;
  ?>
    <div class="jac-metabox">
      <p>Below is a list of posts this <strong>Call to Action</strong> has been applied to. If nothing has been listed, this call to action has not been applied to any post.</p>

  <?php
    $postTypes = get_post_types( ['public' => true], 'objects' );
    // echo '<pre>',print_r($postTypes),'</pre>';

    foreach ($postTypes as $postType) {
      $slug = $postType->name;
  //    $singular_name = $postType->labels->singular_name;
      $name = $postType->labels->name;
      // echo '<pre>',print_r($postType),'</pre>';

      $query = get_posts([
        'post_type'   => $slug,
        'orderby'   => 'date',
        'order'     => 'DESC',
        'posts_per_page' => -1,
        'meta_query' => [
          [
            'key' => '_cta',
            'value' => $cta_ID,
          ]
        ],
      ]);

      if ( !empty($query) ) { ?>
      <hr/>
      <h4><?= $name ?> (<?= count($query) ?>)</h4>
      <ul class="jac-metabox-checkboxes">
      <?php foreach ($query as $item) { ?>
        <li><a href="<?= get_edit_post_link( $item->ID); ?>"><i class="fa fa-edit"></i></a> <?= $item->post_title; ?></li>
      <?php } ?>
      </ul>
  <?php
      }
    } // foreach post type
  ?>
    </div><!--/.jac-metabox-->
  <?php
} // cta pages

add_action('post_submitbox_misc_actions', function($post) { ?>
  <div class="jac-meta-fields jac-meta-expiry">
    <div class="input-wrap full misc-pub-section curtime">
      <label for="js-exp-date">Expiry Date:</label>
      <input
        type="date"
        name="cf[_exp]"
        autocomplete="off"
        placeholder="Date"
        id="js-exp-date"
        value="<?= get_post_meta($post->ID, '_exp', true) ?>"
      />
      <p><small>This post will unpublish after this date.</small></p>
    </div><!--/.input-wrap-->
  </div>
<?php });

add_action( 'check_post_exp', function() {
  foreach (get_posts([
    'post_type' => array_merge(get_post_types([
      '_builtin' => false
    ]), ['post'=>'post', 'page'=>'page']), //get all post types
    'posts_per_page' => -1
  ]) as $p) {
    $exp = get_post_meta($p->ID, '_exp', true);
    if ( !empty($exp) && (strtotime($exp) < time())) {
      wp_update_post([
        'ID' => $p->ID,
        'post_status' => 'draft'
      ]);
//      wp_trash_post($p->ID);
    }
  }
});

if( !wp_next_scheduled( 'check_post_exp' ) ) {
  wp_schedule_event( time(), 'daily', 'check_post_exp' );
}


add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'video-popup')) {
    add_meta_box('video_popup', 'Video Popup', __NAMESPACE__ . '\\meta_box_video_popup', $post_type, 'side', 'default');
  }

}, 10, 2);

function meta_box_video_popup() {
  global $post;
  ?>
  <div class="jac-metabox">
    <div class="jac-meta-fields jac-meta-location-info">
      <div class="input-wrap">
        <label for="jac-meta-video-popup-url">Video Link</label>
        <input id="jac-meta-video-popup-url" type="url" name="cf[_video]" value="<?= get_post_meta($post->ID, '_video', true); ?>" />
        <h3>Supports:</h3>
        <ul>
          <li><a href="https://youtube.com" target="_blank"><i class="fab fab-youtube"></i> Youtube</a></li>
          <li><a href="https://vimeo.com" target="_blank"><i class="fab fab-vimeo"></i> Vimeo</a></li>
        </ul>
      </div>
    </div><!--/.jac-meta-video-popup-->
  </div><!--/.jac-metabox-->
<?php }

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'location-info')) {
    add_meta_box('location_info', 'Location', __NAMESPACE__ . '\\meta_box_location_info', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_location_info() {
  global $post;
  ?>
    <div class="jac-metabox">
      <div class="jac-meta-fields jac-meta-location-info">
        <?php
          $fields = [
            ['Address Line 1',   '_location_info_address1',   'text',  ''  ],
            ['Address Line 2',   '_location_info_address2',   'text',  ''  ],
            ['City / Town',      '_location_info_city',       'text',  ''  ],
            ['Province / State', '_location_info_province',   'text',  ''  ],
            ['Country',          '_location_info_country',    'text',  ''  ],
            ['Postal / ZIP',     '_location_info_postal',     'text',  ''  ],
          ];
          meta_fields($fields);
        ?>
      </div><!--/.jac-meta-fields-->
    </div><!--/.jac-metabox-->
  <?php
}

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'hours')) {
    add_meta_box('hours', 'Hours of Operation', __NAMESPACE__ . '\\meta_box_hours', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_hours() {
  global $post;
  ?>
    <div class="jac-metabox">
      <div class="jac-meta-fields jac-meta-location-info">
        <?php
          $fields = [
            ['Weekdays',      '_hours_weekdays',   'text',  'half',  'Hidden if left empty'],
            ['Weekends',      '_hours_weekends',   'text',  'half',  'Hidden if left empty'],
            ['Monday',        '_hours_monday',     'text',  'half',  'Hidden if left empty'],
            ['Tuesday',       '_hours_tuesday',    'text',  'half',  'Hidden if left empty'],
            ['Wednesday',     '_hours_wednesday',  'text',  'half',  'Hidden if left empty'],
            ['Thursday',      '_hours_thursday',   'text',  'half',  'Hidden if left empty'],
            ['Friday',        '_hours_friday',     'text',  'half',  'Hidden if left empty'],
            ['Saturday',      '_hours_saturday',   'text',  'half',  'Hidden if left empty'],
            ['Sunday',        '_hours_sunday',     'text',  'half',  'Hidden if left empty'],
            ['Custom Alert',  '_hours_alert',      'text',  'half',  'Custom Alert'],
          ];

          meta_fields($fields);
  ?>
      </div><!--/.jac-meta-fields-->
    </div><!--/.jac-metabox-->
  <?php
}

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'contact-info')) {
    add_meta_box('contact_info', 'Contact Info', __NAMESPACE__ . '\\meta_box_contact_info', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_contact_info() {
  global $post;
  ?>
    <div class="jac-metabox">
      <div class="jac-meta-fields jac-meta-contact-info">
  <?php
          $fields = [
            [ 'Telephone',     '_contact_info_tel',       'tel',    'half'   ],
            [ 'Extension',     '_contact_info_tel_ext',   'text',   'half'   ],
            [ 'Cellular',      '_contact_info_cell',      'tel',    'third'  ],
            [ 'Toll Free',     '_contact_info_tollfree',  'tel',    'third'  ],
            [ 'Facsimile',     '_contact_info_fax',       'tel',    'third'  ],
            [ 'Email Address', '_contact_info_email',     'email',   'half'  ],
          ];

          meta_fields($fields);
  ?>
      </div><!--/.jac-meta-fields-->
    </div><!--/.jac-metabox-->
  <?php
}

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'social-channels')) {
    add_meta_box('social_channels', 'Social Channels', __NAMESPACE__ . '\\meta_box_social_channels', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_social_channels() {
  global $post;
  ?>
    <div class="jac-metabox">
      <div class="jac-meta-fields jac-meta-social-channels">
  <?php
          $fields = [
            'Facebook',
            'Twitter',
            'LinkedIn',
            'Instagram',
            'Google-Plus',
            'YouTube',
            'Pinterest',
            'TripAdvisor',
            'Link',
          ];

          foreach ( $fields as $field ) {
            if(isset($field)) {
              $label = $field;
              $slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', ( str_replace(' ', '-', $field ) ) ));
            } else {
              $label = '';
              $slug = '';
            }
  ?>
            <div class="input-wrap half">
              <label for="jac-meta-social-channel-<?= $slug ?>">
                <i class="fa fa-<?= $slug ?>"></i>
                <?= $label ?> URL
              </label>
              <input
                id="jac-meta-social-channel-<?= $slug ?>"
                type="url"
                name="cf[_social_channel_<?= $slug ?>]"
                value="<?= get_post_meta($post->ID, '_social_channel_'.$slug, true); ?>"
              />
            </div><!--/.input-wrap url-wrap-->
  <?php
          }
  ?>
      </div><!--/.jac-meta-fields-->
    </div><!--/.jac-metabox-->
  <?php
}

add_action('add_meta_boxes', function ($post_type, $post) {

  if ( post_type_supports($post_type, 'featured-toggle') && current_theme_supports('featured-posts')) {
    add_meta_box('featured_toggle', 'Featured', __NAMESPACE__ . '\\meta_box_featured_toggle', $post_type, 'side', 'default');
  }

}, 0, 2);

function meta_box_featured_toggle($post) {
  $label = get_post_type_object($post->post_type)->labels->singular_name;
  ?>
    <div class="jac-metabox">
      <ul class="jac-metabox-checkboxes">

        <li class="full">
          <input type="hidden" name="cf[_featured]" value=""/>
          <input
            type="checkbox"
            id="featured-toggle"
            name="cf[_featured]"
            value="1"
            <?php if(1 == get_post_meta($post->ID, '_featured', true)) { ?>
              checked="checked"
            <?php } ?>
          />
          <label for="featured-toggle"><strong>Feature this <?= $label ?></strong></label>
        </li>

      </ul><!--/.jac-metabox-checkboxes-->

  <?php
      $featured_posts = get_posts([
        'post_type' => get_post_type(),
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => -1,
        'meta_query' => [
          [
            'key' => '_featured',
            'value' => 1
          ]
        ],
      ]);

      if ( !empty($featured_posts) ) {
  ?>
        <hr/>
        <h4>The following posts are also featured:</h4>
        <ul>
  <?php
        foreach ($featured_posts as $item) {
          if($item->ID != get_the_ID()) {
  ?>
            <li><a href="<?= get_edit_post_link($item->ID); ?>" class="fa fa-edit"></a> <?= $item->post_title ?></li>
  <?php
          } // if ID doesn't match
        } // while posts
  ?>
        </ul>
  <?php
      } // if posts
  ?>
    </div><!--/.jac-metabox-->
  <?php 
} // featured toggle

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'icon-selector')) {
    add_meta_box('icon_selector', 'Icon Selector', __NAMESPACE__ . '\\meta_box_icon_selector', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_icon_selector($post) {

  $icons = [
    'glass', 'music', 'search', 'envelope-o', 'heart', 'star', 'star-o', 'user', 'film', 'th-large', 'th', 'th-list', 'check', 'times', 'search-plus', 'search-minus', 'power-off', 'signal', 'cog', 'trash-o', 'home', 'file-o', 'clock-o', 'road', 'download', 'arrow-circle-o-down', 'arrow-circle-o-up', 'inbox', 'play-circle-o', 'repeat', 'refresh', 'list-alt', 'lock', 'flag', 'headphones', 'volume-off', 'volume-down', 'volume-up', 'qrcode', 'barcode', 'tag', 'tags', 'book', 'bookmark', 'print', 'camera', 'font', 'bold', 'italic', 'text-height', 'text-width', 'align-left', 'align-center', 'align-right', 'align-justify', 'list', 'outdent', 'indent', 'video-camera', 'pencil', 'map-marker', 'adjust', 'tint', 'pencil-square-o', 'share-square-o', 'check-square-o', 'arrows', 'step-backward', 'fast-backward', 'backward', 'play', 'pause', 'stop', 'forward', 'fast-forward', 'step-forward', 'eject', 'chevron-left', 'chevron-right', 'plus-circle', 'minus-circle', 'times-circle', 'check-circle', 'question-circle', 'info-circle', 'crosshairs', 'times-circle-o', 'check-circle-o', 'ban', 'arrow-left', 'arrow-right', 'arrow-up', 'arrow-down', 'share', 'expand', 'compress', 'plus', 'minus', 'asterisk', 'exclamation-circle', 'gift', 'leaf', 'fire', 'eye', 'eye-slash', 'exclamation-triangle', 'plane', 'calendar', 'random', 'comment', 'magnet', 'chevron-up', 'chevron-down', 'retweet', 'shopping-cart', 'folder', 'folder-open', 'arrows-v', 'arrows-h', 'bar-chart', 'twitter-square', 'facebook-square', 'camera-retro', 'key', 'cogs', 'comments', 'thumbs-o-up', 'thumbs-o-down', 'star-half', 'heart-o', 'sign-out', 'linkedin-square', 'thumb-tack', 'external-link', 'sign-in', 'trophy', 'github-square', 'upload', 'lemon-o', 'phone', 'square-o', 'bookmark-o', 'phone-square', 'twitter', 'facebook', 'github', 'unlock', 'credit-card', 'rss', 'hdd-o', 'bullhorn', 'bell', 'certificate', 'hand-o-right', 'hand-o-left', 'hand-o-up', 'hand-o-down', 'arrow-circle-left', 'arrow-circle-right', 'arrow-circle-up', 'arrow-circle-down', 'globe', 'wrench', 'tasks', 'filter', 'briefcase', 'arrows-alt', 'users', 'link', 'cloud', 'flask', 'scissors', 'files-o', 'paperclip', 'floppy-o', 'square', 'bars', 'list-ul', 'list-ol', 'strikethrough', 'underline', 'table', 'magic', 'truck', 'pinterest', 'pinterest-square', 'google-plus-square', 'google-plus', 'money', 'caret-down', 'caret-up', 'caret-left', 'caret-right', 'columns', 'sort', 'sort-desc', 'sort-asc', 'envelope', 'linkedin', 'undo', 'gavel', 'tachometer', 'comment-o', 'comments-o', 'bolt', 'sitemap', 'umbrella', 'clipboard', 'lightbulb-o', 'exchange', 'cloud-download', 'cloud-upload', 'user-md', 'stethoscope', 'suitcase', 'bell-o', 'coffee', 'cutlery', 'file-text-o', 'building-o', 'hospital-o', 'ambulance', 'medkit', 'fighter-jet', 'beer', 'h-square', 'plus-square', 'angle-double-left', 'angle-double-right', 'angle-double-up', 'angle-double-down', 'angle-left', 'angle-right', 'angle-up', 'angle-down', 'desktop', 'laptop', 'tablet', 'mobile', 'circle-o', 'quote-left', 'quote-right', 'spinner', 'circle', 'reply', 'github-alt', 'folder-o', 'folder-open-o', 'smile-o', 'frown-o', 'meh-o', 'gamepad', 'keyboard-o', 'flag-o', 'flag-checkered', 'terminal', 'code', 'star-half-o', 'location-arrow', 'crop', 'code-fork ', 'chain-broken', 'question', 'info', 'exclamation', 'superscript', 'subscript', 'eraser', 'puzzle-piece', 'microphone', 'microphone-slash', 'shield', 'calendar-o', 'fire-extinguisher', 'rocket', 'maxcdn', 'chevron-circle-left', 'chevron-circle-right', 'chevron-circle-up', 'chevron-circle-down', 'html5', 'css3', 'anchor', 'unlock-alt', 'bullseye', 'ellipsis-h', 'ellipsis-v', 'rss-square', 'play-circle', 'ticket', 'minus-square', 'minus-square-o', 'level-up', 'level-down', 'check-square', 'pencil-square', 'external-link-square', 'share-square', 'compass', 'caret-square-o-down', 'caret-square-o-up', 'caret-square-o-right', 'eur', 'gbp', 'usd', 'inr', 'jpy', 'rub', 'krw', 'btc', 'file', 'file-text', 'sort-alpha-asc', 'sort-alpha-desc', 'sort-amount-asc', 'sort-amount-desc', 'sort-numeric-asc', 'sort-numeric-desc', 'thumbs-up', 'thumbs-down', 'youtube-square', 'youtube', 'xing', 'xing-square', 'youtube-play', 'dropbox', 'stack-overflow', 'instagram', 'flickr', 'adn', 'bitbucket', 'bitbucket-square', 'tumblr', 'tumblr-square', 'long-arrow-down', 'long-arrow-up', 'long-arrow-left', 'long-arrow-right', 'apple', 'windows', 'android', 'linux', 'dribbble', 'skype', 'foursquare', 'trello', 'female', 'male', 'gratipay', 'sun-o', 'moon-o', 'archive', 'bug', 'vk', 'weibo', 'renren', 'pagelines', 'stack-exchange', 'arrow-circle-o-right', 'arrow-circle-o-left', 'caret-square-o-left', 'dot-circle-o', 'wheelchair', 'vimeo-square', 'try', 'plus-square-o', 'space-shuttle', 'slack', 'envelope-square', 'wordpress', 'openid', 'university', 'graduation-cap', 'yahoo', 'google', 'reddit', 'reddit-square', 'stumbleupon-circle', 'stumbleupon', 'delicious', 'digg', 'pied-piper-pp', 'pied-piper-alt', 'drupal', 'joomla', 'language', 'fax', 'building', 'child', 'paw', 'spoon', 'cube', 'cubes', 'behance', 'behance-square', 'steam', 'steam-square', 'recycle', 'car', 'taxi', 'tree', 'spotify', 'deviantart', 'soundcloud', 'database', 'file-pdf-o', 'file-word-o', 'file-excel-o', 'file-powerpoint-o', 'file-image-o', 'file-archive-o', 'file-audio-o', 'file-video-o', 'file-code-o', 'vine', 'codepen', 'jsfiddle', 'life-ring', 'circle-o-notch', 'rebel', 'empire', 'git-square', 'git', 'hacker-news', 'tencent-weibo', 'qq', 'weixin', 'paper-plane', 'paper-plane-o', 'history', 'circle-thin', 'header', 'paragraph', 'sliders', 'share-alt', 'share-alt-square', 'bomb', 'futbol-o', 'tty', 'binoculars', 'plug', 'slideshare', 'twitch', 'yelp', 'newspaper-o', 'wifi', 'calculator', 'paypal', 'google-wallet', 'cc-visa', 'cc-mastercard', 'cc-discover', 'cc-amex', 'cc-paypal', 'cc-stripe', 'bell-slash', 'bell-slash-o', 'trash', 'copyright', 'at', 'eyedropper', 'paint-brush', 'birthday-cake', 'area-chart', 'pie-chart', 'line-chart', 'lastfm', 'lastfm-square', 'toggle-off', 'toggle-on', 'bicycle', 'bus', 'ioxhost', 'angellist', 'cc', 'ils', 'meanpath', 'buysellads', 'connectdevelop', 'dashcube', 'forumbee', 'leanpub', 'sellsy', 'shirtsinbulk', 'simplybuilt', 'skyatlas', 'cart-plus', 'cart-arrow-down', 'diamond', 'ship', 'user-secret', 'motorcycle', 'street-view', 'heartbeat', 'venus', 'mars', 'mercury', 'transgender', 'transgender-alt', 'venus-double', 'mars-double', 'venus-mars', 'mars-stroke', 'mars-stroke-v', 'mars-stroke-h', 'neuter', 'genderless', 'facebook-official', 'pinterest-p', 'whatsapp', 'server', 'user-plus', 'user-times', 'bed', 'viacoin', 'train', 'subway', 'medium', 'y-combinator', 'optin-monster', 'opencart', 'expeditedssl', 'battery-full', 'battery-three-quarters', 'battery-half', 'battery-quarter', 'battery-empty', 'mouse-pointer', 'i-cursor', 'object-group', 'object-ungroup', 'sticky-note', 'sticky-note-o', 'cc-jcb', 'cc-diners-club', 'clone', 'balance-scale', 'hourglass-o', 'hourglass-start', 'hourglass-half', 'hourglass-end', 'hourglass', 'hand-rock-o', 'hand-paper-o', 'hand-scissors-o', 'hand-lizard-o', 'hand-spock-o', 'hand-pointer-o', 'hand-peace-o', 'trademark', 'registered', 'creative-commons', 'gg', 'gg-circle', 'tripadvisor', 'odnoklassniki', 'odnoklassniki-square', 'get-pocket', 'wikipedia-w', 'chrome', 'firefox', 'opera', 'internet-explorer', 'television', 'contao', '500px', 'amazon', 'calendar-plus-o', 'calendar-minus-o', 'calendar-times-o', 'calendar-check-o', 'industry', 'map-pin', 'map-signs', 'map-o', 'map', 'commenting', 'commenting-o', 'houzz', 'vimeo', 'black-tie', 'fonticons', 'reddit-alien', 'edge', 'credit-card-alt', 'codiepie', 'modx', 'fort-awesome', 'usb', 'product-hunt', 'mixcloud', 'scribd', 'pause-circle', 'pause-circle-o', 'stop-circle', 'stop-circle-o', 'shopping-bag', 'shopping-basket', 'hashtag', 'bluetooth', 'bluetooth-b', 'percent', 'gitlab', 'wpbeginner', 'wpforms', 'envira', 'universal-access', 'wheelchair-alt', 'question-circle-o', 'blind', 'audio-description', 'volume-control-phone', 'braille', 'assistive-listening-systems', 'american-sign-language-interpreting', 'deaf', 'glide', 'glide-g', 'sign-language', 'low-vision', 'viadeo', 'viadeo-square', 'snapchat', 'snapchat-ghost', 'snapchat-square', 'pied-piper', 'first-order', 'yoast', 'themeisle', 'google-plus-official', 'font-awesome'
  ];

  $cf = get_post_custom();
  ?>
  <div class="jac-metabox">

  <?php /*
    <div class="jac-meta-fields">
      <div class="input-wrap">
        <label for="do-filter"></label>
        <input
          type="search"
          name="filter-icons"
          class="do-filter"
        />
      </div><!--/.input-wrap-->
    </div><!--/.jac-meta-fields-->
  */ ?>

  <div class="jac-icon-selector">

    <div class="input-wrap radio-wrap">
      <input
        id="icon-null"
        type="radio"
        name="cf[_icon]"
        value=""
        <?php
          if('' == get_post_meta($post->ID, '_icon', true)) {
            echo 'checked="checked"';
          }
        ?>
      />
      <label for="icon-null">
        <i class="fa fa-times"></i>
        <span>no icon</span>
      </label>
    </div>

  <?php

    if( !empty(get_post_meta($post->ID, '_icon', true))) {

      $element = get_post_meta($post->ID, '_icon', true);
      if ( in_array($element,$icons ) ) {

        //find the key
        $key = array_search($element, $icons);
        //delete the element
        unset($icons[$key]);
        //prepend to the front of the array
        array_unshift($icons,$element);

      }
    }

    foreach ( $icons as $icon ) {
  ?>
      <div class="input-wrap radio-wrap">
        <input
          id="icon-<?= $icon; ?>"
          type="radio"
          name="cf[_icon]"
          value="<?= $icon; ?>"
          <?php
            if($icon == get_post_meta($post->ID, '_icon', true)) {
              echo 'checked="checked"';
            }
          ?>
        />
        <label for="icon-<?= $icon; ?>">
          <i class="fa fa-<?= $icon; ?>"></i>
          <span><?= $icon ?></span>
        </label>
      </div>
  <?php
    }
  ?>
    </div><!--/.jac-icon-selector-->
  </div><!--/.jac-metabox-->
  <?php
} // meta_box_icon_selector

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'color-controls')) {
    add_meta_box('color_controls', 'Color Controls', __NAMESPACE__ . '\\meta_box_color_controls', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_color_controls($post) { ?>
  <div class="jac-metabox">
    <p>Background Colour</p>

      <?php
        $optionSets = [
          [
            [ 'Primary',          'primary'           ],
            [ 'Primary Light',    'primary-light'     ],
            [ 'Primary Dark',     'primary-dark'      ],
          ],
          [
            [ 'Secondary',          'secondary'       ],
            [ 'Secondary Light',    'secondary-light' ],
            [ 'Secondary Dark',     'secondary-dark'  ],
          ],
          [
            [ 'Tertiary',          'tertiary'         ],
            [ 'Tertiary Light',    'tertiary-light'   ],
            [ 'Tertiary Dark',     'tertiary-dark'    ],
          ],
          [
            [ 'White',        null       ],
            [ 'Lighter Gray', 'gray-lighter'  ],
            [ 'Light Gray',   'gray-light'    ],
            [ 'Gray',         'gray'          ],
            [ 'Dark Gray',    'gray-dark'     ],
            [ 'Darker Gray',  'gray-darker'   ],
            [ 'Black',        'black'         ],
          ],
        ];

        foreach($optionSets as $optionSet) { ?>

          <ul class="jac-metabox-checkboxes">

          <?php foreach($optionSet as $option) { ?>
          <li>
            <input
              type="radio"
              id="bgcolor-option-<?= $option[1] ?>"
              name="cf[_color_controls_background]"
              value="<?= $option[1] ?>"
              <?php if($option[1] == get_post_meta($post->ID, '_color_controls_background', true)) { ?>
                checked="checked"
              <?php } ?>
            />
            <label for="bgcolor-option-<?= $option[1] ?>"><?= $option[0] ?></label>
          </li>

          <?php } ?>

          </ul><!--/.jac-metabox-checkboxes-->
          <hr/>

        <?php } ?>

    <h4>Text Colour Adjustment</h4>
    <p>this is to maintain contrast between text and background.</p>
    <ul class="jac-metabox-checkboxes">

      <?php
        $options = [
          [ 'Light Background',        null       ],
          [ 'Dark Background',         'knockout'      ],
        ];

        foreach($options as $option) { ?>

          <li>
            <input
              type="radio"
              id="color-option-<?= $option[1] ?>"
              name="cf[_color_controls_text]"
              value="<?= $option[1] ?>"
              <?php if($option[1] == get_post_meta($post->ID, '_color_controls_text', true)) { ?>
                checked="checked"
              <?php } ?>
            />
            <label for="color-option-<?= $option[1] ?>"><?= $option[0] ?></label>
          </li>

        <?php } ?>

    </ul><!--/.jac-metabox-checkboxes-->


  </div><!--/.jac-metabox-->
  <?php 
}

add_action('add_meta_boxes', function ($post_type, $post) {

  if (post_type_supports($post_type, 'doc-uploader')) {
    add_meta_box('doc-uploader', 'Document Uploader', __NAMESPACE__ . '\\meta_box_doc_upload', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_doc_upload($post) {
  wp_enqueue_media();
  $your_doc_id = get_post_meta($post->ID, '_doc', true);
  $your_doc_src = wp_get_attachment_url( $your_doc_id );
  $you_have_doc = !empty($your_doc_src);
  ?>
  <div class="js-doc-uploader">
    <div class="js-img-container">
      <?php if ( $you_have_doc ) { ?>
        <p><?= $your_doc_src ?></p>
      <?php } ?>
    </div>
    <div class="js-input-container"></div>

    <p class="jac-metabox-add hide-if-no-js">
      <a href="<?= esc_url( get_upload_iframe_src( null, $post->ID ) ) ?>" data-post="<?= $post->ID ?>" class="js-add-link"<?php if ( $you_have_doc ) { echo ' style="display: none"'; } ?>><i class="fa fa-plus-circle"></i> Upload</a>
      <a href="#" data-post="<?= $post->ID ?>" class="js-delete-link"<?php if ( !$you_have_doc ) { echo ' style="display: none"'; } ?>><i class="fa fa-minus-circle"></i> Remove</a>
    </p>
  </div>

  <?php 
}

add_action('add_meta_boxes', function ($post_type, $post) {

  $template = get_post_meta($post->post_parent, '_wp_page_template', true);

  if (is_post_type('page') && is_child() && ( $template == 'overview.php' ) && $post->ID !== get_option('page_on_front')) {
    add_meta_box('overview-controls', 'Overview Controls', __NAMESPACE__ . '\\meta_box_overview_controls', $post_type, 'normal', 'default');
  }

}, 10, 2);

function meta_box_overview_controls($post) {
  ?>
  <div class="jac-metabox">
    <ul class="jac-metabox-checkboxes">

      <li class="full">
        <input type="hidden" name="cf[_redirect_single]" value=""/>
        <input
          type="checkbox"
          id="overview-redirect-single"
          name="cf[_redirect_single]"
          value="1"
          <?php if(1 == get_post_meta($post->ID, '_redirect_single', true)) { ?>
            checked="checked"
          <?php } ?>
        />
        <label for="overview-redirect-single">Hide button link and redirect to parent.</label>
      </li>

    </ul><!--/.jac-metabox-checkboxes-->
  </div>
  <?php
} // meta_box_overview_controls

add_action('add_meta_boxes', function ($post_type, $post) {

    if ( post_type_supports($post_type, 'video_popup_link')) {
        add_meta_box('video_popup_link', 'Video Popup Link', __NAMESPACE__ . '\\meta_box_video_popup_link', $post_type, 'normal', 'default');
    }

}, 10, 2);

function meta_box_video_popup_link($post){?>

    <div class="jac-metabox">
        <p>
            <input type="text" name="cf[_video_popup_link]" style="width: 100%;" value="<?= get_post_meta($post->ID, '_video_popup_link', true) ?>">
            <input type="text" placeholder="Video Width (px)" name="cf[_video_popup_width]" style="width: 49.5%;" value="<?= get_post_meta($post->ID, '_video_popup_width', true) ?>">
            <input type="text" placeholder="Video Height (px)" name="cf[_video_popup_height]" style="width: 49.5%;" value="<?= get_post_meta($post->ID, '_video_popup_height', true) ?>">
        </p>
    </div>

<?php }