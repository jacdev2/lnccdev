<?php

namespace JAC\PostTypes;
use JAC\MetaBoxes;

// register_post_type should only be invoked through the 'init' action
add_action('init', function() {
  // call register() here

  register(
    [
      'single' => 'Social Channel',
      'plural' => 'Social Channels',
    ],
    [
      'menu_icon' => 'dashicons-share',
      'menu_position' => 2,
      'exclude_from_search' => true,
      'supports' => [ 'title', 'page-attributes', 'disable-yoast' ]
    ]
  );

  register(
    [
      'single' => 'slide',
      'plural' => 'slides',
    ],
    [
      'taxonomies' => ['Categories'],
      'menu_icon' => 'dashicons-slides',
      'menu_position' => 15,
      'exclude_from_search' => true,
      'supports' => ['title', 'subtitle', 'excerpt', 'thumbnail', 'page-attributes', 'linkage', 'disable-yoast']
    ]
  );

  register(
    [
      'single' => 'Event',
      'plural' => 'Events',
    ],
    [
      'taxonomies' => ['Categories'],
      'menu_icon' => 'dashicons-calendar-alt',
      'menu_position' => 30,
      'exclude_from_search' => false,
      'has_archive' => true,
      'rewrite' => 'the-latest/events',
      'supports' => ['title', 'subtitle', 'excerpt', 'editor', 'thumbnail', 'page-attributes', 'cta-selector', 'location-info', 'event-time', 'video-popup']
    ]
  );

  register(
    [
      'single' => 'Call To Action',
      'plural' => 'Calls To Action',
    ],
    [
      'menu_icon' => 'dashicons-admin-comments',
      'menu_position' => 40,
      'exclude_from_search' => true,
      'supports' => ['title', 'media-gallery','excerpt', 'thumbnail', 'page-attributes', 'linkage', 'featured-toggle', 'color-controls', 'disable-yoast' ]
    ]
  );

  register(
    [
        'single' => 'Notification Bar',
        'plural' => 'Notification Bar',
    ],
    [
        'menu_icon' => 'dashicons-slides',
        'menu_position' => 30,
        'exclude_from_search' => true,
        'has_archive' => false,
        'supports' => ['title','linkage','page-attributes']
    ]
  );

  register(
    [
        'single' => 'Video Popup',
        'plural' => 'Video Popup',
    ],
    [
        'menu_icon' => 'dashicons-slides',
        'menu_position' => 30,
        'exclude_from_search' => true,
        'has_archive' => false,
        'supports' => ['title','editor','video_popup_link','thumbnail','page-attributes']
    ]
  );

});

//Secondary Editor - comment out if not needed
add_post_type_support('page','wysiwyg'); 

if (class_exists('\MultiPostThumbnails')) {

  $MultiPostThumbnails = [
    [ 'Secondary Image',  'page'    ],    
    // [ 'Secondary Image',  'slide'    ],    
    // [ 'Tertiary Image',  'page'    ],    
    // [ 'Quad Image',  'page'    ],
  ];

  foreach( $MultiPostThumbnails as $MultiPostThumbnail ) {
    $slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', (str_replace(' ', '-', $MultiPostThumbnail[0]))));
    new \MultiPostThumbnails(
      array(
        'label' => $MultiPostThumbnail[0],
        'id' => $slug,
        'post_type' => $MultiPostThumbnail[1]
      )
    );

  } // foreach $MultiPostThumbnail

} // if class_exists('\MultiPostThumbnails')

/**
 *
 * register post type
 *
 * @param array $params TODO
 * @param array $args TODO
 * @return object|\WP_Error The registered post type object, or an error object.
 */
function register($params = [], $args = []) {

  // default post type is singular
  // can not contain capital letters or spaces
  $params['post_type'] = isset($params['post_type']) ? $params['post_type'] : strtolower(str_replace(' ', '_', $params['single']));

  // http://codex.wordpress.org/Function_Reference/register_post_type#Reserved_Post_Types
  $reserved = ['post', 'page', 'attachment', 'revision', 'nav_menu_item', 'action', 'author', 'order', 'theme'];
  if (in_array($params['post_type'], $reserved)) {
    _doing_it_wrong( __FUNCTION__, 'You can not register reserved post type.', null );
    return new \WP_Error( 'post_type_reserved', 'You can not register reserved post type.' );
  }

  $singulars = [
    'singular_name' => '%s',
    'name_admin_bar' => '%s',
    'add_new_item' => 'Add New %s',
    'new_item' => 'New %s',
    'edit_item' => 'Edit %s',
    'view_item' => 'View %s',
  ];

  $plural = [
    'name' => '%s',
    'menu_name' => '%s',
    'add_new' => 'Add New',
    'all_items' => 'All %s',
    'search_items' => 'Search %s',
    'parent_item_colon' => 'Parent %s:',
    'not_found' => 'No %s found.',
    'not_found_in_trash' => 'No %s found in Trash.',
  ];

  $labels = [];

  foreach ($singulars as $key => $label) {
    $labels[$key] = sprintf($label, ucfirst($params['single']));
  }
  foreach ($plural as $key => $label) {
    $labels[$key] = sprintf($label, ucfirst($params['plural']));
  }

  $default_args = [
    'labels'             => $labels,
    'public'             => true,
    'exclude_from_search'=> false,
    'show_ui'            => true,
    'show_in_nav_menus'  => false,
    'show_in_menu'       => true,
    'show_in_admin_bar'  => false,
    'query_var'          => false,
    /**
     * 5 - below Posts
     * 10 - below Media
     * 15 - below Links
     * 20 - below Pages
     * 25 - below comments
     * 60 - below first separator
     * 65 - below Plugins
     * 70 - below Users
     * 75 - below Tools
     * 80 - below Settings
     * 100 - below second separator
     */
    'menu_position'        => 25,
    'menu_icon'            => null,
    'rewrite'              => false,
    'capability_type'      => 'post',
    'has_archive'          => false, //this will create url rewrite
    'hierarchical'         => false,
    'supports'             => ['title', 'subtitle', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'linkage'],
    'taxonomies'           => [],
    'register_meta_box_cb' => function ($post) {
      // create function details_{post_type} at meta-boxes.php will automatic support it.
      if (function_exists('\\JAC\\MetaBoxes\\details_' . $post->post_type)) {
        add_meta_box('details', ucfirst( get_post_type_object( get_post_type() )->labels->singular_name ) . ' Details', '\\JAC\\MetaBoxes\\details_' . $post->post_type);
      }
    }
  ];

  // taxonomies need be array
  if (isset($args['taxonomies']) && is_string($args['taxonomies'])) {
    $args['taxonomies'] = [$args['taxonomies']];
  }

  $args = wp_parse_args($args, $default_args);

  if ($args['rewrite'] != false) {
    $args['rewrite'] = [
      'slug' => $args['rewrite'],
      'with_front' => false,
      'feeds' => false,
      'pages' => false,
    ];
    if ($args['has_archive']) $args['rewrite']['pages'] = true; // archive page need pagination
  }

  foreach ($args['taxonomies'] as $taxonomy) {
    // TODO: more robust taxonomy registration
    $taxonomy_name = $params['post_type'] . '_' . strtolower(str_replace(' ', '_', $taxonomy));
    if (!taxonomy_exists($taxonomy_name)) {
      register_taxonomy( $taxonomy_name, $params['post_type'], [
        'label'             => $taxonomy,
        'show_in_nav_menus' => false,
        'show_admin_column' => true,
        'show_tagcloud'     => false,
        'hierarchical'      => true,
      ]);
    };
  }

  if ($args['hierarchical'] == true) $args['query_var'] = true; // hierarchical need query_var

  add_action('manage_' . $params['post_type'] . '_posts_custom_column', 'JAC\\Admin\\manage_custom_column', 10, 2);

  if (in_array('page-attributes', $args['supports'])) {
    add_filter('manage_' . $params['post_type'] . '_posts_columns', function ($posts_columns) {
      $posts_columns['order'] = 'Order';
      return $posts_columns;
    });
  }

  if (in_array('thumbnail', $args['supports'])) {
    add_filter('manage_' . $params['post_type'] . '_posts_columns', function ($posts_columns) {
      return array_merge(array_splice( $posts_columns, 0, 1 ), ['image' => ''], $posts_columns);
    });
  }

  return register_post_type( $params['post_type'], $args );

}

/**
 * order by menu order if post type support page-attributes
 */
add_action('pre_get_posts', function ($query) {
  if (post_type_supports($query->get('post_type'), 'page-attributes')) {
    $order = (isset($_GET['order'])) ? $_GET['order'] : 'ASC';
    $query->set('orderby', 'menu_order');
    $query->set('order', $order);
  }
}, 1);
