<?php

global $jac_post_overrides;
// used in templates/post-conditions.php and templates/pagination.php
$jac_post_overrides = [
  // ['path/to/page','post_type','taxonomy', 'hide_empty'],
  ['the-latest/events','event','event_categories'],
];
