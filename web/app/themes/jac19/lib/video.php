<?php

namespace JAC\Video;

add_action('add_meta_boxes', function ($post_type, $post) {

  if ( post_type_supports( $post_type, 'media-gallery' ) && current_theme_supports( 'media-gallery' ) ) {
    add_meta_box('video-gallery', 'Video Gallery', __NAMESPACE__ . '\\video_gallery_meta_box', $post_type,  'normal', 'high');
  }

}, 10, 2);

function video_gallery_meta_box($post) {
  ?>
  <div class="jac-metabox meta-gallery-picker js-video-wrap">
    <?php
    $videos = get_post_meta($post->ID, '_jac_videos');
//    if (empty($videos))
//      $videos = [
//        ['url' => 'http://www.youtube.com', 'order' => 20],
//        ['url' => 'http://www.vimeo.com', 'order' => 10],
//      ];

    usort($videos, function ($a, $b) {
      return $a['order'] - $b['order'];
    });

    foreach ($videos as $video) {
      display_video($post->ID, $video);
    }
    ?>
  </div>
  <p class="jac-metabox-add hide-if-no-js">
    <a href="#" data-post="<?= $post->ID ?>" class="js-video-add-video"><i class="fa fa-plus-circle"></i> Add Video</a>
  </p>
<?php }


new VideoAjax();
class VideoAjax {

  public function __construct() {

    $functions = get_class_methods($this);

    foreach ($functions as $function) {
      if ($function[0] == '_') continue;
      add_action( 'wp_ajax_jac_video_' . $function, [$this, $function] );
    }

  }

  private function _exit() {
    if (defined('DOING_AJAX') && DOING_AJAX) wp_die();
  }

  public function new_video() {
    $post_id = isset($_POST['post']) ? $_POST['post'] : null;

    display_video($post_id, ['url' => '', 'order' => 0]);
    $this->_exit();
  }

  public function delete_video() {
    $post_id = isset($_POST['post']) ? $_POST['post'] : null;
    $order = isset($_POST['order']) ? $_POST['order'] : null;
    $url = isset($_POST['url']) ? $_POST['url'] : null;

    if (!is_null($post_id) && !is_null($order) && !is_null($url)) {
      delete_post_meta($post_id, '_jac_videos', ['url' => $url, 'order' => (int)$order]);
      wp_send_json_success();
    }

    wp_send_json_error();

  }

}

function display_video($post_id, $video) {
  $uid = uniqid();
  ?>
  <ul class="meta-gallery-row meta-video-row js-video<?php if (defined('DOING_AJAX') && DOING_AJAX) { echo ' js-unsaved';} ?>">
    <li class="meta-gallery-thumbnail">
      <a class="meta-gallery-delete js-video-delete-video" data-post="<?= $post_id ?>" data-order="<?= $video['order'] ?>" data-url="<?= $video['url'] ?>"><i class="fa fa-times-circle"></i></a>
    </li><!--/.meta-gallery-thumbnail-->
    <li class="meta-gallery-order">
      <input class="text" name="js-video-item[<?= $uid ?>][order]" placeholder="Order" value="<?= $video['order'] ?>">
    </li><!--/.meta-gallery-order-->
    <li class="meta-gallery-details">
      <input class="url" name="js-video-item[<?= $uid ?>][url]" placeholder="URL" value="<?= $video['url'] ?>">
    </li><!--/.meta-gallery-details-->
  </ul>
<?php }

add_action('save_post', function ($post_id) {

  $videos = isset($_POST['js-video-item']) ? $_POST['js-video-item'] : [];

  if ($videos) {
    delete_post_meta($post_id, '_jac_videos');

    foreach ($videos as $uid => $video) {
      if (empty($video['url'])) continue;
      add_post_meta($post_id, '_jac_videos', ['url' => $video['url'], 'order' => (int)$video['order']]);
    }
  }

});

// get video and sort it then return url array
function get_videos($post = 0) {
  if (!empty($post)) {
    $post = get_post( $post );
  } else {
    global $post;
  }

  $videos = get_post_meta($post->ID, '_jac_videos');

  usort($videos, function ($a, $b) {
    return $a['order'] - $b['order'];
  });

  $result = [];

  foreach ($videos as $video) {
    $result[] = $video['url'];
  }

  return $result;

}
