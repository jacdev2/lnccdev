<?php

namespace JAC\Admin\Menu;

/**
 * add admin menu
 */
add_action( 'admin_menu', function() {
  add_menu_page('Company', 'Company', 'edit_pages', 'jac-info-contacts', __NAMESPACE__ . '\\contacts_page', 'dashicons-admin-home', 2);
  add_submenu_page('jac-info-contacts', 'Settings', 'Settings', 'edit_pages', 'jac-info-settings', __NAMESPACE__ . '\\settings_page');

  if( current_theme_supports('featured-posts') ) {
    add_submenu_page('jac-info-contacts', 'Featured Posts', 'Featured Posts', 'edit_pages', 'jac-info-featured-posts', __NAMESPACE__ . '\\featured_posts');
  }

});

/**
 * save option value
 */
add_action('save_ov', function ($prefix) {
  if ( isset( $_POST[$prefix] ) ) {
    if ($prefix == 'sc') {
      // TODO: need more test
      preg_match("/^https?:\/\/(www\.)?twitter\.com\/(#!\/)?@?(?<name>[^\/]+)(\/\w+)*$/", $_POST[$prefix]['company_social_twitter'], $twitter);
      if (isset($twitter['name'])) $_POST[$prefix]['company_social_twitter'] = $twitter['name'];
    }
    foreach ( $_POST[$prefix] as $slug => $value ) {

      if (!empty($value)) {
        add_option($slug, $value) or update_option($slug, $value);
      } else {
        delete_option($slug);
      }
    }
    ?>
    <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible">
      <p><strong>Settings saved.</strong></p>
    </div>
    <?php
  }
});

/**
 * contacts page
 */
function contacts_page() {
  do_action('save_ov', 'cc');
  ?>
  <div class="wrap">
     <form method="post" action="<?= admin_url('admin.php?page=jac-info-contacts'); ?>" novalidate="novalidate">
      <?php
      $contacts = [
        'Brand' => [
          [
            'label' => 'Brand Colour',
            'name' => 'brand_colour',
          ],
          [
            'label' => 'Company',
            'name' => 'company',
          ],
        ]
      ];
      $info = [
        'Company Information' => [
          [
            'label' => 'Address Line 1',
            'name' => 'company_one_address1',
            'placeholder' => 'Address Line 1',
          ],
          [
            'label' => 'Address Line 2',
            'name' => 'company_one_address2',
            'placeholder' => 'Address Line 2',
          ],
          [
            'label' => 'City',
            'name' => 'company_one_city',
            'placeholder' => 'City',
          ],
          [
            'label' => 'Province',
            'name' => 'company_one_province',
            'placeholder' => 'Province',
          ],
          [
            'label' => 'Country',
            'name' => 'company_one_country',
            'placeholder' => 'Country',
          ],
          [
            'label' => 'Postal',
            'name' => 'company_one_postal',
            'placeholder' => 'Postal',
          ],
          [
            'label' => 'Phone',
            'name' => 'company_one_phone',
            'placeholder' => 'Phone',
          ],
          [
            'label' => 'Toll Free',
            'name' => 'company_one_tollfree',
            'placeholder' => 'Toll Free',
          ],
          [
            'label' => 'Fax',
            'name' => 'company_one_fax',
            'placeholder' => 'Fax',
          ],
          [
            'label' => 'Email',
            'name' => 'company_one_email',
            'placeholder' => 'Email Address',
          ],
        ],
        'Department One' => [
           [
             'label' => 'Department Name',
             'name' => 'department_name_one',
             'placeholder' => 'Department Name',
             'desc'=> 'Hidden if left empty'
           ],
           [
             'label' => 'Custom Alert',
             'name' => 'department_one_alert',
             'placeholder' => 'Department One Custom Alert',
             'desc'=> 'Hidden if left empty'
           ],
           [
             'label' => 'Department One Time One',
             'name' => 'department_one_time_one',
              'placeholder' => 'Department One Hours 1',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Label One',
             'name' => 'department_one_label_one',
             'placeholder' => 'Department One Label 1',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Time Two',
             'name' => 'department_one_time_two',
              'placeholder' => 'Department One Hours 2',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Label Two',
             'name' => 'department_one_label_two',
             'placeholder' => 'Department One Label 2',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Time Three',
             'name' => 'department_one_time_three',
              'placeholder' => 'Department One Hours 3',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Label Three',
             'name' => 'department_one_label_three',
             'placeholder' => 'Department One Label 3',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Time Four',
             'name' => 'department_one_time_four',
              'placeholder' => 'Department One Hours 4',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Label Four',
             'name' => 'department_one_label_four',
             'placeholder' => 'Department One Label 4',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Time Five',
             'name' => 'department_one_time_five',
              'placeholder' => 'Department One Hours 5',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Label Five',
             'name' => 'department_one_label_five',
             'placeholder' => 'Department One Label 5',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Time Six',
             'name' => 'department_one_time_six',
              'placeholder' => 'Department One Hours 6',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Label Six',
             'name' => 'department_one_label_six',
             'placeholder' => 'Department One Label 6',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Time Seven',
             'name' => 'department_one_time_seven',
              'placeholder' => 'Department One Hours 7',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department One Label Seven',
             'name' => 'department_one_label_seven',
             'placeholder' => 'Department One Label 7',
             'desc'=> 'Hidden if left empty',
           ],
        ],
        'Department Two' => [
           [
             'label' => 'Department Name',
             'name' => 'department_name_two',
             'placeholder' => 'Department Name',
             'desc'=> 'Hidden if left empty'
           ],
           [
             'label' => 'Department Two Custom Alert',
             'name' => 'department_two_alert',
             'placeholder' => 'Department Two Custom Alert',
             'desc'=> 'Hidden if left empty'
           ],
          [
             'label' => 'Department Two Time One',
             'name' => 'department_two_time_one',
              'placeholder' => 'Department Two Hours 1',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Label One',
             'name' => 'department_two_label_one',
             'placeholder' => 'Department Two Label 1',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Time Two',
             'name' => 'department_two_time_two',
              'placeholder' => 'Department Two Hours 2',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Label Two',
             'name' => 'department_two_label_two',
             'placeholder' => 'Department Two Label 2',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Time Three',
             'name' => 'department_two_time_three',
              'placeholder' => 'Department Two Hours 3',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Label Three',
             'name' => 'department_two_label_three',
             'placeholder' => 'Department Two Label 3',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Time Four',
             'name' => 'department_two_time_four',
              'placeholder' => 'Department Two Hours 4',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Label Four',
             'name' => 'department_two_label_four',
             'placeholder' => 'Department Two Label 4',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Time Five',
             'name' => 'department_two_time_five',
              'placeholder' => 'Department Two Hours 5',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Label Five',
             'name' => 'department_two_label_five',
             'placeholder' => 'Department Two Label 5',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Time Six',
             'name' => 'department_two_time_six',
              'placeholder' => 'Department Two Hours 6',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Label Six',
             'name' => 'department_two_label_six',
             'placeholder' => 'Department Two Label 6',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Time Seven',
             'name' => 'department_two_time_seven',
              'placeholder' => 'Department Two Hours 7',
             'desc'=> 'Hidden if left empty',
           ],
           [
             'label' => 'Department Two Label Seven',
             'name' => 'department_two_label_seven',
             'placeholder' => 'Department Two Label 7',
             'desc'=> 'Hidden if left empty',
           ],
           
        ],
        'Holiday Hours' => [
           [
             'type' => 'textarea',
             'label' => 'Holiday Hours',
             'name' => 'company_one_holiday',
              'placeholder' => 'Holiday Hours',
             'desc'=> 'Hidden if left empty'
           ],
          ],
        'Announcements' => [
           [
             'type' => 'textarea',
             'label' => 'Announcement',
             'name' => 'company_announcement',
              'placeholder' => 'Announcement',
             'desc'=> 'Hidden if left empty'
           ],
        ]
      ];

      foreach ($contacts as $title => $trs) { ?>
        <h1 class="title"><?= $title ?></h1>
        <table class="form-table">
          <?php foreach ($trs as $tr) {
            $tr['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', ( str_replace(' ', '-', $tr['label'] ) ) );
            if (!isset($tr['type'])) $tr['type'] = 'text';
            if (!isset($tr['desc'])) $tr['desc'] = ''; ?>
            <tr>
              <th scope="row"><label for="<?= $tr['slug'] ?>"><?= $tr['label'] ?></label></th>
              <td>
                <?php if ($tr['type'] == 'textarea') { ?>
                  <textarea name="cc[company_info_<?= $tr['name'] ?>]" id="<?= $tr['slug'] ?>" class="large-text" rows="5"><?= stripslashes(get_option('company_info_' . $tr['name'])); ?></textarea>
                <?php } else { ?>
                  <input name="cc[company_info_<?= $tr['name'] ?>]" type="text" id="<?= $tr['slug'] ?>" value="<?= stripslashes(get_option('company_info_' . $tr['name'])); ?>" class="regular-text" />
                <?php } ?>
                <p class="description"><?= $tr['desc']; ?></p>
                <!-- <?php if (current_user_can('administrator')) { ?><p><code>&lt;?= get_option('company_info_<?= $tr['name'] ?>'); ?&gt;</code></p><?php } ?> -->
              </td>
            </tr>
          <?php } ?>
        </table>
        <?php 
      }?>
      <div class="department-holder">
        <?php
          foreach ($info as $title => $trs) { ?>
            <div class="department-div">
              <h1 class="title"><?= $title ?></h1>
              <table class="form-table">
                <?php foreach ($trs as $tr) {
                  $tr['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', ( str_replace(' ', '-', $tr['label'] ) ) );
                  if (!isset($tr['type'])) $tr['type'] = 'text';
                  if (!isset($tr['desc'])) $tr['desc'] = ''; ?>
                  <tr>
                    <td>
                      <?php if ($tr['type'] == 'textarea') { ?>
                        <textarea name="cc[company_info_<?= $tr['name'] ?>]" id="<?= $tr['slug'] ?>" class="large-text" rows="5"><?= stripslashes(get_option('company_info_' . $tr['name'])); ?></textarea>
                      <?php } else { ?>
                        <input placeholder="<?= $tr['placeholder'] ?>" name="cc[company_info_<?= $tr['name'] ?>]" type="text" id="<?= $tr['slug'] ?>" value="<?= stripslashes(get_option('company_info_' . $tr['name'])); ?>" class="regular-text" />
                      <?php } ?>
                      <p class="description"><?= $tr['desc']; ?></p>
                      <?php if (current_user_can('administrator')) { ?><p><code>&lt;?= get_option('company_info_<?= $tr['name'] ?>'); ?&gt;</code></p><?php } ?>
                    </td>
                  </tr>
                <?php } ?>
              </table>
            </div>
            <?php 
          } 
        ?>
      </div>

      <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes" />
      </p>
    </form>
  </div>
<?php }

/**
 * settings page
 */
function settings_page() {
  do_action('save_ov', 'cs');
  ?>
  <div class="wrap">
    <h1>Settings</h1>
    <form method="post" action="<?= admin_url('admin.php?page=jac-info-settings'); ?>" novalidate="novalidate">
      <table class="form-table">
        <?php
        $trs = [
          [
            'label' => 'Google Analytics ID',
            'name' => 'ga_id',
            'desc' => 'Get the data you need to make intelligent marketing and business decisions with <a href="https://www.google.com/analytics" target="_blank">Google Analytics</a>.',
          ],
        ];

        if (current_theme_supports('google-api-key')) {
          $trs[] = [
            'label' => 'Google API Key',
            'name' => 'google_api_key',
            'desc' => '<a href="https://developers.google.com/" target="_black">Google APIs</a> is a set of application programming interfaces (APIs) developed by Google which allow communication with Google Services and their integration to other services.',
          ];
        }

        if (current_theme_supports('jac-fb-pixel-events')) {
          $trs[] = [
            'label' => 'Facebook Tracking Pixel',
            'name' => 'fb_pixel',
            'desc' => 'The <a href="https://www.facebook.com/business/help/651294705016616" target="_black">Facebook pixel</a> allows you to place a single pixel across your entire website to report conversions.',
          ];
        }

        if (current_theme_supports('typekit')) {
          $trs[] = [
            'label' => 'Typekit Kit ID',
            'name' => 'typekit_id',
            'desc' => 'Modifying this value will impact the appearance of your website. Get the Kit ID at <a href="https://typekit.com/account/kits" target="_blank">Typekit.com</a>',
          ];
        }

        foreach ($trs as $tr) {
          $tr['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', ( str_replace(' ', '-', $tr['label'] ) ) );
          ?>
          <tr>
            <th scope="row"><label for="<?= $tr['slug'] ?>"><?= $tr['label'] ?></label></th>
            <td>
              <input name="cs[<?= $tr['name'] ?>]" type="text" id="<?= $tr['slug'] ?>" value="<?= get_option($tr['name']); ?>" class="regular-text" />
              <p class="description"><?= $tr['desc']; ?></p>
            </td>
          </tr>
        <?php } ?>

        <tr>
          <th scope="row">
            <input type="hidden" name="cs[fb_messenger_enabled]" value="" />
            <input
              name="cs[fb_messenger_enabled]"
              type="checkbox"
              id="Enable-Facebook-Messenger"
              value="1"
              <?php if(get_option('fb_messenger_enabled')) { echo "checked"; } ?>
            />
            <label for="Enable-Facebook-Messenger">Enable Messenger</label>
          </th>
          <td>

            <p>Enables Facebook Messenger Widget as long as there is a Social Channel post named "Facebook" and the page URL is defined.</p>
          </td>
        </tr>
      </table>
      <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes" />
      </p>
    </form>
  </div>
<?php }

/**
 * add google analytics and facebook tracking pixel to header or footer
 */
if (!WP_DEBUG && !empty(get_option('ga_id'))) {
  add_theme_support('soil-google-analytics', get_option('ga_id'));
}

if( empty(get_option('ga_id')) && WP_ENV !== 'development' ) {
  add_action( 'admin_notices', __NAMESPACE__ . '\\admin_notice_analytics' );
}
function admin_notice_analytics() {
    ?>
    <div class="notice error">
        <p><strong>Warning:</strong> Google Analytics ID not set. <a href="<?= admin_url('admin.php?page=jac-info-settings') ?>">Click here to set.</a></p>
    </div>
    <?php
}

if( empty(get_option('google_api_key')) && WP_ENV == 'production' && current_theme_supports('google-api-key')) {
  add_action( 'admin_notices', __NAMESPACE__ . '\\admin_notice_google_api' );
}
function admin_notice_google_api() {
    ?>
    <div class="notice error">
        <p><strong>Warning:</strong> Google API Key is not set. <a href="<?= admin_url('admin.php?page=jac-info-settings') ?>">Click here to set.</a> Google services, such as maps, will not function unless this is resolved.</p>
    </div>
    <?php
}

function featured_posts() {
?>
  <div class="wrap">
<?php

    $args = [
      'public' => true,
    ];

    $postTypes = get_post_types( $args, 'objects' );
    // echo '<pre>',print_r($postTypes),'</pre>';

    foreach ($postTypes as $postType) {
      $slug = $postType->name;
      $singular_name = $postType->labels->singular_name;
      $name = $postType->labels->name;
      // echo '<pre>',print_r($postType),'</pre>';

      $query = new \WP_Query([
        'post_type'   => $slug,
        'orderby'   => 'date',
        'order'     => 'DESC',
        'posts_per_page' => -1,
        'meta_query' => [
          [
            'key' => '_featured',
            'value' => 1
          ]
        ],
      ]);

      $total = $query->found_posts;
      $currentID = get_the_ID();

      if ( $query->have_posts() ) {
?>
        <ul>
          <li><h4><?= $name ?> (<?= $total ?>)</h4></li>
          <li>
            <ul>
<?php
        while ($query->have_posts()) {
          $query->the_post();
?>
          <li><?php edit_post_link('<i class="fa fa-edit"></i>'); ?> <?php the_title(); ?></li>
<?php
        } // while posts
?>
            </ul>
          </li>
        </ul>
<?php
      } // if posts
      wp_reset_postdata();
    } // foreach post type
?>
  </div><!--/.wrap-->
<?php } // all featured posts
