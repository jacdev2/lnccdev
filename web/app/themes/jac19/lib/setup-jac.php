<?php

namespace JAC\Setup;

use Roots\Sage\Assets;

add_theme_support('featured-posts');

/**
 * subtitle support
 */
// add post subtitle field to DB
if (empty(get_option('subtitle_setup'))) {
  global $wpdb;
  $wpdb->query("ALTER TABLE `{$wpdb->prefix}posts` ADD `post_subtitle` text NOT NULL AFTER `post_title`");
  add_option('subtitle_setup', 1);
}
// add post subtitle field to backend
add_action('edit_form_before_permalink', function ($post) {
  if (post_type_supports($post->post_type, 'subtitle')) { ?>
    <div id="subtitlewrap">
      <label class="screen-reader-text" id="title-prompt-text" for="subtitle">Enter subtitle here</label>
      <input type="text" name="post_subtitle" size="30" value="<?php echo esc_attr( $post->post_subtitle ); ?>" id="subtitle" spellcheck="true" autocomplete="off" placeholder="Enter subtitle here" style="width: 100%;" />
    </div>
  <?php }
}, 10, 1);
// save post subtitle when save post
add_filter('wp_insert_post_data', function ( $data ) {
  if (isset($_POST['post_subtitle'])) $data['post_subtitle'] = $_POST['post_subtitle'];
  return $data;
}, 10);

/**
 * yoast related
 */
// remove yoast added fields https://yoast.com/user-contact-fields-wordpress/
add_filter( 'user_contactmethods', '__return_empty_array', 999 );
// move meta box to the end https://gist.github.com/krogsgard/4707754
add_filter( 'wpseo_metabox_prio', function() { return 'low'; } );

/**
 * show post excerpt box by default
 */
add_filter( 'default_hidden_meta_boxes', function ( $hidden, $screen ) {
  if (in_array('postexcerpt', $hidden)) $hidden = array_diff($hidden, ['postexcerpt']);
  return $hidden;
}, 10, 2 );

/**
 * hide admin bar
 */
add_action('after_setup_theme', function () {
  show_admin_bar(false);
});

/**
 * inject JS variables to all pages
 */
add_action('wp_enqueue_scripts', function () {
  wp_localize_script( 'sage/js', 'templateJS',
    array(
      'templateURL' => get_bloginfo('template_url'),
      'ajaxURL' => admin_url('admin-ajax.php')
    )
  );
}, 101); //right after sage's script

/**
 * Admin assets
 */
add_action( 'admin_enqueue_scripts',  function () {
  wp_enqueue_script('admin/js', Assets\asset_path('scripts/admin.js'), ['jquery', 'jquery-ui-datepicker'], null, true);
  wp_enqueue_style('admin/css', Assets\asset_path('styles/admin.css'), false, null);
});

/**
 * remove comment system
 */
// admin bar
add_action('wp_before_admin_bar_render', function () {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('comments');
});
// backend menu
add_action('admin_menu', function () {
  remove_menu_page( 'edit-comments.php' );
  remove_submenu_page( 'options-general.php', 'options-discussion.php' );
});
// dashboard meta box
add_action( 'wp_dashboard_setup', function () {
  remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
});
// post type
add_action( 'admin_init', function () {
  remove_post_type_support( 'post', 'comments' );
  remove_post_type_support( 'page', 'comments' );
});
// rewrite
add_filter('comments_rewrite_rules', '__return_empty_array');

/**
 * clean up unused theme features
 */
add_action('after_setup_theme', function () {
  add_theme_support('soil-disable-trackbacks');
  remove_theme_support('post-formats');
  remove_theme_support('comments');
});

/**
 * clean up rewrite rule
 */
add_filter('rewrite_rules_array', function ($rules) {
  foreach ($rules as $rule => $rewrite) {
    if (preg_match('/.*(feed)/', $rule)) {
      unset($rules[$rule]);
    }
    if (preg_match('/.*(comment-page)/', $rule)) {
      unset($rules[$rule]);
    }
  }
  return $rules;
});

/**
 * modify default post type
 */
add_filter( 'register_post_type_args', function ( $args, $post_type ) {

  if ($post_type == 'post') {
    $args['menu_position'] = 25;
    //trick to fix url rewrite
    $args['rewrite'] = ['slug' => 'the-latest/news'];
    $args['supports'] = array_merge($args['supports'], ['subtitle', 'excerpt', 'media-gallery', 'video-popup', 'cta-selector']);
    $args['show_in_nav_menus'] = false;
    $args['labels']['menu_name'] = 'News';
    $args['menu_icon'] = 'dashicons-welcome-widgets-menus';
  }

  if ($post_type =='page') {
    $args['supports'] = array_merge($args['supports'], ['subtitle', 'excerpt', 'media-gallery', 'video-popup', 'cta-selector']);
  }

  return $args;
}, 10, 2 );

if(!current_theme_supports('tags')) {
  add_action('init', function() {
    unregister_taxonomy_for_object_type('post_tag', 'post');
  });
}

// fix permalink
/* add_filter( 'post_link', function ($permalink, $post, $leavename) {
  return str_replace(home_url(), home_url('/news'), $permalink);
}, 10, 3 ); */
add_filter( 'pre_post_link', function($permalink, $post, $leavename) {
  if (get_post_type($post) == 'post') // TODO: need double check
    return 'the-latest/news' . $permalink;
  else
    return $permalink;
}, 10, 3);

/**
 * ajax loading animation
 */
//add_action('wp_footer', __NAMESPACE__ . '\\ajax_loading_animation');
add_action('admin_footer', __NAMESPACE__ . '\\ajax_loading_animation');
function ajax_loading_animation() { ?>
  <div class="loading" id="loader">
    <div class="overlay"></div>
    <div class="loader">
      <i class="fa fa-gear fa-spin fa-3x"></i>
    </div><!-- .loader -->
  </div><!-- .loading -->
<?php }

/**
 * remove jquery migrate
 */
//add_filter( 'wp_default_scripts', function (&$scripts) {

//  if (is_admin()) {
//    $scripts->remove('jquery');
//    $scripts->remove('jquery-migrate');
//    $scripts->add('jquery', false, array('jquery-core'), '1.11.3');
//  }

//});

/**
 * register extra nav menus
 */
register_nav_menus([
  'secondary_navigation' => __('Secondary Navigation', 'sage'),
  'bottom_navigation' => __('Bottom Navigation', 'sage'),
  'tertiary_navigation' => __('Tertiary Navigation', 'sage')
]);

/**
 * get some deprecated arguments back
 */
add_action('deprecated_argument_run', function ( $function ) {
  if ($function == 'get_the_excerpt') {
    add_filter('deprecated_argument_trigger_error', '__return_false');
  }
}, 10);

/**
 * allow login with email
 */
add_filter( 'authenticate', function ($user, $email, $password) {

  if ( $user instanceof \WP_User ) {
    return $user;
  }

  if ( empty($email) || empty($password) ) {
    if ( is_wp_error( $user ) )
      return $user;

    $error = new \WP_Error();

    if ( empty($email) )
      $error->add('empty_email', __('<strong>ERROR</strong>: The email field is empty.'));

    if ( empty($password) )
      $error->add('empty_password', __('<strong>ERROR</strong>: The password field is empty.'));

    return $error;
  }

  if ( is_email($email) ) {

    $user = get_user_by('email', $email);

    if (!$user) {
      return new \WP_Error('invalid_email',
        __('<strong>ERROR</strong>: Invalid email.') .
        ' <a href="' . wp_lostpassword_url() . '">' .
        __('Lost your password?') .
        '</a>'
      );
    }

    if (!wp_check_password($password, $user->user_pass, $user->ID)) {
      return new \WP_Error('incorrect_password',
        sprintf(
        /* translators: %s: user name */
          __('<strong>ERROR</strong>: The password you entered for the email %s is incorrect.'),
          '<strong>' . $email . '</strong>'
        ) .
        ' <a href="' . wp_lostpassword_url() . '">' .
        __('Lost your password?') .
        '</a>'
      );
    }
  } else {
    return null;
  }

  return $user;

}, 10, 3 );

/**
 * default contents
 */
add_filter( 'the_title', function ( $title ) {
  if ( WP_ENV == 'development' && empty( $title ) ) {
    return "Development Title";
  } else {
    return $title;
  }
} );
add_filter( 'the_subtitle', function ( $subtitle ) {
  if ( WP_ENV == 'development' && empty( $subtitle ) ) {
    return "Development Subtitle";
  } else {
    return $subtitle;
  }
} );
add_filter( 'the_excerpt', function ( $excerpt ) {
  if ( WP_ENV == 'development' && empty( $excerpt ) ) {
    return "Verte nitentibus oculis ego subinde dilabuntur. Et nunc eget nocte (et nunc necessaria) et magis indigere. Si fuero tantum tenere (si solum) tenere possimus inquirendi et iustum faciens voltis.";
  } else {
    return $excerpt;
  }
} );
add_filter( 'the_content', function ( $content ) {
  if ( WP_ENV == 'development' && empty( $content ) ) {
    return "\n
        <p><strong>Whoops! This page has no content. </strong> Cras mattis consectetur purus sit amet fermentum. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\n

        <p>Cras mattis consectetur purus sit amet fermentum. <a href='#'>Hello, I'm a hyperlink.</a> Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n

        <h2>Heading 2</h2>\n
        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nulla vitae elit libero, a pharetra augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n

        <blockquote><p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. <a href='#'>Hello, I'm a hyperlink.</a> Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p></blockquote>\n

        <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n


        <h3>Heading 3</h3>\n
        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>\n
        <ul>
          <li>Vestibulum id ligula porta felis euismod semper. dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>\n
          <li>Vestibulum id ligula porta felis euismod semper. Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>\n
          <li>Vestibulum id ligula porta felis euismod semper. Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>\n
        </ul>\n

        <p>Vestibulum id ligula porta felis euismod semper. Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Nullam id dolor id nibh ultricies vehicula ut id elit. Nulla vitae elit libero, a pharetra augue.</p>\n

        <h3>Heading 3</h3>\n
        <ol>
          <li>Vestibulum id ligula porta felis euismod semper. dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>\n
          <li>Vestibulum id ligula porta felis euismod semper. Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>\n

        </ol>\n

        <p>Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\n

        <h4>Heading 4</h4>
        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>\n";
  } else {
    return $content;
  }

} );

/**
 * Add the wp-editor back into WordPress after it was removed in 4.2.2.
 * Remove wp-editor from page set as Home in Reading Settings.
 */
add_action('edit_form_after_title', function ($post) {

  if ($post->ID == get_option('page_for_posts')) {
    remove_action('edit_form_after_title', '_wp_posts_page_notice');
    add_post_type_support('page', 'editor');
  }

  if ($post->ID == get_option('page_on_front')) {
    remove_post_type_support('page', 'editor');
  }

});

/**
 * add feedback to footer menu
 */
//add_filter( 'wp_nav_menu_items',  function( $items, $args ) {
//
//  if ( ($args->theme_location == 'bottom_navigation') ) {
//    $items = $items.'<li class="menu-feedback"><a class="js-popup-feedback">Feedback</a></li>';
//  }
//
//  return $items;
//
//}, 10, 2 );

/**
 * add plupload library
 */
//add_action('wp_enqueue_scripts', function() {
//  wp_enqueue_script('plupload', false, [], false, true);
//}, 90); // need before main.js

/**
 * Inject Typekit Script into Head
 */
add_action( 'wp_head', function () {
  if (!empty(get_option('typekit_id'))) {
    ?>
    <script src="https://use.typekit.net/<?= get_option('typekit_id') ?>.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <?php
  }
});

// for wp functions not loaded yet
require_once(ABSPATH . 'wp-admin/includes/plugin.php');

/**
 * remove mailgun plugin and redirect email to mailhog
 * https://github.com/mailhog/MailHog
 */
if (WP_ENV == 'development') {
  deactivate_plugins('mailgun/mailgun.php');
  add_action('phpmailer_init', function ($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = '127.0.0.1';
    $phpmailer->SMTPAuth = false;
    $phpmailer->Port = 1025;
  });
}

/**
 * low priority for Wordpress SEO (Yoast) Meta Box
 * remove yoast advertisement
 */
if(is_plugin_active('wordpress-seo/wp-seo.php')) {
  add_filter('wpseo_metabox_prio', function($html) {
    return 'low';
  });
  add_action('admin_head', function () { ?>
    <style>
      #toplevel_page_wpseo_dashboard ul li:last-of-type,
      .wpseo_content_cell#sidebar-container {
        display: none !important;
      }
    </style>
  <?php });
}

/**
 * add all users to user drop down meta box
 */
add_filter('wp_dropdown_users_args', function ($query_args, $r) {
  if ($r['name'] == 'post_author_override') {
    $query_args['who'] = '';
  }
}, 10, 2);

/**
 * default google api key
 */
if (WP_ENV != 'production') {
  add_filter('pre_option_google_api_key', function () {
    return 'AIzaSyBYA_azqYuqUs2DbnFkZFDUW4zu3lbp0nk';
  });
}

add_filter('excerpt_length', function ($length) {
  return 20;
}, 999);

add_action('pre_get_posts', function ($query) {

  if (!is_admin() && $query->is_main_query() && $query->is_post_type_archive('event')) {

    $query->set('order', 'DESC');
    $query->set('orderby', 'meta_value_num');
    $query->set('meta_key', '_jac_event_end');

    $query->set('meta_query', [
      [
        'key' => '_jac_event_end',
        'value' => time(),
        'compare' => '<=',
      ]
    ]);

  }

}); // end pre_get_posts action
