-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 12, 2020 at 02:15 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lnccdev_wp`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_actions`
--

CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_actions`
--

INSERT INTO `wp_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(1, 'action_scheduler/migration_hook', 'pending', '2020-10-12 01:02:08', '2020-10-12 01:02:08', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1602464528;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1602464528;}', 1, 0, '2020-10-12 02:15:28', '2020-10-12 02:15:28', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_claims`
--

CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_groups`
--

CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_groups`
--

INSERT INTO `wp_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration');

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_logs`
--

CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_logs`
--

INSERT INTO `wp_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 1, 'action created', '2020-10-12 01:02:08', '2020-10-12 01:02:08');

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-05-27 07:16:27', '2019-05-27 07:16:27', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_addon_feed`
--

CREATE TABLE `wp_gf_addon_feed` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `feed_order` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `addon_slug` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_type` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_draft_submissions`
--

CREATE TABLE `wp_gf_draft_submissions` (
  `uuid` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `submission` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_entry`
--

CREATE TABLE `wp_gf_entry` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_status` varchar(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_entry_meta`
--

CREATE TABLE `wp_gf_entry_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `entry_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  `item_index` varchar(60) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_entry_notes`
--

CREATE TABLE `wp_gf_entry_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `entry_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci,
  `note_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sub_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_form`
--

CREATE TABLE `wp_gf_form` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_gf_form`
--

INSERT INTO `wp_gf_form` (`id`, `title`, `date_created`, `date_updated`, `is_active`, `is_trash`) VALUES
(1, 'Contact', '2020-03-11 19:08:00', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_form_meta`
--

CREATE TABLE `wp_gf_form_meta` (
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `entries_grid_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `confirmations` longtext COLLATE utf8mb4_unicode_520_ci,
  `notifications` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_gf_form_meta`
--

INSERT INTO `wp_gf_form_meta` (`form_id`, `display_meta`, `entries_grid_meta`, `confirmations`, `notifications`) VALUES
(1, '{\"title\":\"Contact\",\"description\":\"\",\"labelPlacement\":\"top_label\",\"descriptionPlacement\":\"below\",\"button\":{\"type\":\"text\",\"text\":\"Submit\",\"imageUrl\":\"\"},\"fields\":[{\"type\":\"text\",\"id\":1,\"label\":\"Name\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"name\",\"cssClass\":\"gf_left_half\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"email\",\"id\":2,\"label\":\"Email\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"email\",\"cssClass\":\"gf_right_half\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"emailConfirmEnabled\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"textarea\",\"id\":3,\"label\":\"Message\",\"adminLabel\":\"\",\"isRequired\":true,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"message\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"form_id\":\"\",\"useRichTextEditor\":false,\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"pageNumber\":1}],\"version\":\"2.2.6\",\"id\":1,\"useCurrentUserAsAuthor\":true,\"postContentTemplateEnabled\":false,\"postTitleTemplateEnabled\":false,\"postTitleTemplate\":\"\",\"postContentTemplate\":\"\",\"lastPageButton\":null,\"pagination\":null,\"firstPageCssClass\":null}', NULL, '{\"5e693710c8606\":{\"id\":\"5e693710c8606\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}', '{\"5e693710c81bc\":{\"id\":\"5e693710c81bc\",\"to\":\"{admin_email}\",\"name\":\"Admin Notification\",\"event\":\"form_submission\",\"toType\":\"email\",\"subject\":\"New submission from {form_title}\",\"message\":\"{all_fields}\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_form_revisions`
--

CREATE TABLE `wp_gf_form_revisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_gf_form_view`
--

CREATE TABLE `wp_gf_form_view` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `count` mediumint(8) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_gf_form_view`
--

INSERT INTO `wp_gf_form_view` (`id`, `form_id`, `date_created`, `ip`, `count`) VALUES
(1, 1, '2020-03-11 16:01:05', '', 110),
(2, 1, '2020-03-23 14:26:29', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(3, 'siteurl', 'http://localhost/JAC/lnccdev/web/wp', 'yes'),
(4, 'home', 'http://localhost/JAC/lnccdev/web/wp', 'yes'),
(5, 'blogname', 'B E D R O C K', 'yes'),
(6, 'blogdescription', 'Just another WordPress site', 'yes'),
(7, 'users_can_register', '0', 'yes'),
(8, 'admin_email', 'developers@jac.co', 'yes'),
(9, 'start_of_week', '1', 'yes'),
(10, 'use_balanceTags', '0', 'yes'),
(11, 'use_smilies', '1', 'yes'),
(12, 'require_name_email', '1', 'yes'),
(13, 'comments_notify', '1', 'yes'),
(14, 'posts_per_rss', '10', 'yes'),
(15, 'rss_use_excerpt', '0', 'yes'),
(16, 'mailserver_url', 'mail.example.com', 'yes'),
(17, 'mailserver_login', 'login@example.com', 'yes'),
(18, 'mailserver_pass', 'password', 'yes'),
(19, 'mailserver_port', '110', 'yes'),
(20, 'default_category', '1', 'yes'),
(21, 'default_comment_status', 'open', 'yes'),
(22, 'default_ping_status', 'open', 'yes'),
(23, 'default_pingback_flag', '1', 'yes'),
(24, 'posts_per_page', '10', 'yes'),
(25, 'date_format', 'F j, Y', 'yes'),
(26, 'time_format', 'g:i a', 'yes'),
(27, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(28, 'comment_moderation', '0', 'yes'),
(29, 'moderation_notify', '1', 'yes'),
(30, 'permalink_structure', '/%postname%/', 'yes'),
(32, 'hack_file', '0', 'yes'),
(33, 'blog_charset', 'UTF-8', 'yes'),
(34, 'moderation_keys', '', 'no'),
(35, 'active_plugins', 'a:17:{i:0;s:29:\"gravityforms/gravityforms.php\";i:2;s:30:\"advanced-custom-fields/acf.php\";i:3;s:37:\"ajax-search-lite/ajax-search-lite.php\";i:4;s:33:\"classic-editor/classic-editor.php\";i:6;s:33:\"duplicate-post/duplicate-post.php\";i:7;s:35:\"gravityformsmailchimp/mailchimp.php\";i:8;s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";i:9;s:15:\"mmenu/mmenu.php\";i:10;s:13:\"pods/init.php\";i:11;s:55:\"searchwp-live-ajax-search/searchwp-live-ajax-search.php\";i:12;s:26:\"snazzy-maps/snazzymaps.php\";i:14;s:27:\"the-preloader/preloader.php\";i:15;s:33:\"ultimate-modal/ultimate-modal.php\";i:16;s:71:\"woocommerce-gravityforms-product-addons/gravityforms-product-addons.php\";i:17;s:61:\"woocommerce-variations-table/woocommerce-variations-table.php\";i:18;s:27:\"woocommerce/woocommerce.php\";i:19;s:24:\"wordpress-seo/wp-seo.php\";}', 'yes'),
(36, 'category_base', '', 'yes'),
(37, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(38, 'comment_max_links', '2', 'yes'),
(39, 'gmt_offset', '0', 'yes'),
(40, 'default_email_category', '1', 'yes'),
(41, 'recently_edited', '', 'no'),
(42, 'template', 'jac19', 'yes'),
(43, 'stylesheet', 'jac19', 'yes'),
(44, 'comment_whitelist', '1', 'yes'),
(45, 'blacklist_keys', '', 'no'),
(46, 'comment_registration', '0', 'yes'),
(47, 'html_type', 'text/html', 'yes'),
(48, 'use_trackback', '0', 'yes'),
(49, 'default_role', 'subscriber', 'yes'),
(50, 'db_version', '45805', 'yes'),
(51, 'uploads_use_yearmonth_folders', '1', 'yes'),
(52, 'upload_path', '', 'yes'),
(53, 'blog_public', '1', 'yes'),
(54, 'default_link_category', '2', 'yes'),
(55, 'show_on_front', 'page', 'yes'),
(56, 'tag_base', '', 'yes'),
(57, 'show_avatars', '1', 'yes'),
(58, 'avatar_rating', 'G', 'yes'),
(59, 'upload_url_path', '', 'yes'),
(60, 'thumbnail_size_w', '150', 'yes'),
(61, 'thumbnail_size_h', '150', 'yes'),
(62, 'thumbnail_crop', '1', 'yes'),
(63, 'medium_size_w', '300', 'yes'),
(64, 'medium_size_h', '300', 'yes'),
(65, 'avatar_default', 'mystery', 'yes'),
(66, 'large_size_w', '1024', 'yes'),
(67, 'large_size_h', '1024', 'yes'),
(68, 'image_default_link_type', 'none', 'yes'),
(69, 'image_default_size', '', 'yes'),
(70, 'image_default_align', '', 'yes'),
(71, 'close_comments_for_old_posts', '0', 'yes'),
(72, 'close_comments_days_old', '14', 'yes'),
(73, 'thread_comments', '1', 'yes'),
(74, 'thread_comments_depth', '5', 'yes'),
(75, 'page_comments', '0', 'yes'),
(76, 'comments_per_page', '50', 'yes'),
(77, 'default_comments_page', 'newest', 'yes'),
(78, 'comment_order', 'asc', 'yes'),
(79, 'sticky_posts', 'a:0:{}', 'yes'),
(80, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(82, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(83, 'uninstall_plugins', 'a:2:{s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}s:35:\"cookie-law-info/cookie-law-info.php\";s:25:\"uninstall_cookie_law_info\";}', 'no'),
(84, 'timezone_string', '', 'yes'),
(85, 'page_for_posts', '43', 'yes'),
(86, 'page_on_front', '7', 'yes'),
(87, 'default_post_format', '0', 'yes'),
(88, 'link_manager_enabled', '0', 'yes'),
(89, 'finished_splitting_shared_terms', '1', 'yes'),
(90, 'site_icon', '0', 'yes'),
(91, 'medium_large_size_w', '768', 'yes'),
(92, 'medium_large_size_h', '0', 'yes'),
(93, 'wp_page_for_privacy_policy', '3', 'yes'),
(94, 'show_comments_cookies_opt_in', '1', 'yes'),
(95, 'initial_db_version', '44719', 'yes'),
(96, 'wp_user_roles', 'a:9:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:116:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:20:\"wpseo_manage_options\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:44:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:10:\"edit_users\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:9:\"add_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:38:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(97, 'fresh_site', '0', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"sidebar-primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"sidebar-footer\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'bedrock_autoloader', 'a:2:{s:7:\"plugins\";a:1:{s:50:\"multiple-post-thumbnails/multi-post-thumbnails.php\";a:13:{s:4:\"Name\";s:24:\"Multiple Post Thumbnails\";s:9:\"PluginURI\";s:61:\"http://wordpress.org/extend/plugins/multiple-post-thumbnails/\";s:7:\"Version\";s:3:\"1.7\";s:11:\"Description\";s:64:\"Adds the ability to add multiple post thumbnails to a post type.\";s:6:\"Author\";s:11:\"Chris Scott\";s:9:\"AuthorURI\";s:18:\"http://iamzed.com/\";s:10:\"TextDomain\";s:0:\"\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:10:\"RequiresWP\";s:0:\"\";s:11:\"RequiresPHP\";s:0:\"\";s:5:\"Title\";s:24:\"Multiple Post Thumbnails\";s:10:\"AuthorName\";s:11:\"Chris Scott\";}}s:5:\"count\";i:1;}', 'no'),
(105, 'cron', 'a:21:{i:1602468900;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:0:{}s:8:\"interval\";i:60;}}}i:1602468917;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1602468990;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1602469310;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602470913;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1602471240;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1602471246;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1602480110;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1602486989;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602486990;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1602487014;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602487017;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602512965;a:1:{s:17:\"gravityforms_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602513054;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602514441;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602521435;a:1:{s:14:\"check_post_exp\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602544920;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602547200;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602552180;a:1:{s:29:\"mc4wp_refresh_mailchimp_lists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1603236170;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}s:7:\"version\";i:2;}', 'yes'),
(106, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'nonce_key', ':D]6HHr4o*:V{B|L]~KF.8HC2=d|Qkz.0q7y-|b,<c? p#V#}+-gBa[D4E&=>0 v', 'no'),
(113, 'nonce_salt', ',ga2F?OY5ag`UnicGksK7SuoENX=t_- kc^/C# ULoTeuL,bH<CTW,F0NKtbbg{a', 'no'),
(114, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(118, 'recovery_keys', 'a:0:{}', 'yes'),
(120, 'theme_mods_twentynineteen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1559219490;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}}', 'yes'),
(123, 'auth_key', '4sWgO4V(|iSlsqtD&6py|#R!fqC&8G!UlWvj_h_S3oPWL&AJ]~nts4U(Hy/sWaOY', 'no'),
(124, 'auth_salt', ')EUR@35I%#!Htq8Jx2Y]#8oZ8i,+hI7<?lm[$+o@tB.#Mhi(d-@*uU8K+X(=-CqC', 'no'),
(125, 'logged_in_key', 'zum-BjT ,Q5^a^tcg2jt^mRcow=PB]6W<G*_({;yllH[K[n;_f8g16Il[yJiXi_%', 'no'),
(126, 'logged_in_salt', 'T1Dm,`Eb3R2V-4?_Bh`39/Q`j=)LHee(s*YyfjI(v}VPlWd5_&<Nb$|N+#)$ 7zL', 'no'),
(147, 'current_theme', 'jac19', 'yes'),
(148, 'theme_mods_bedrockSage', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:18:\"primary_navigation\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1558954712;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"sidebar-primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"sidebar-footer\";a:0:{}}}}', 'yes'),
(149, 'theme_switched', '', 'yes'),
(150, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(151, 'category_children', 'a:0:{}', 'yes'),
(163, 'theme_mods_Foundation-for-Sage', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:18:\"primary_navigation\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1558954577;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"sidebar-primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"sidebar-footer\";a:0:{}}}}', 'yes'),
(164, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1602463651;s:7:\"checked\";a:10:{s:5:\"jac19\";s:5:\"8.5.4\";s:12:\"twentyeleven\";s:3:\"3.3\";s:13:\"twentyfifteen\";s:3:\"2.5\";s:14:\"twentyfourteen\";s:3:\"2.7\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";s:9:\"twentyten\";s:3:\"2.9\";s:14:\"twentythirteen\";s:3:\"2.9\";s:12:\"twentytwelve\";s:3:\"3.0\";}s:8:\"response\";a:9:{s:12:\"twentyeleven\";a:6:{s:5:\"theme\";s:12:\"twentyeleven\";s:11:\"new_version\";s:3:\"3.5\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentyeleven/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentyeleven.3.5.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";s:5:\"5.2.4\";}s:13:\"twentyfifteen\";a:6:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"2.7\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.2.7.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";s:5:\"5.2.4\";}s:14:\"twentyfourteen\";a:6:{s:5:\"theme\";s:14:\"twentyfourteen\";s:11:\"new_version\";s:3:\"2.9\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentyfourteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentyfourteen.2.9.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";s:5:\"5.2.4\";}s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.7.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.4.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:13:\"twentysixteen\";a:6:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"2.2\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.2.2.zip\";s:8:\"requires\";s:3:\"4.4\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:9:\"twentyten\";a:6:{s:5:\"theme\";s:9:\"twentyten\";s:11:\"new_version\";s:3:\"3.1\";s:3:\"url\";s:39:\"https://wordpress.org/themes/twentyten/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/theme/twentyten.3.1.zip\";s:8:\"requires\";s:3:\"3.0\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:14:\"twentythirteen\";a:6:{s:5:\"theme\";s:14:\"twentythirteen\";s:11:\"new_version\";s:3:\"3.1\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentythirteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentythirteen.3.1.zip\";s:8:\"requires\";s:3:\"3.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwelve\";a:6:{s:5:\"theme\";s:12:\"twentytwelve\";s:11:\"new_version\";s:3:\"3.2\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwelve/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwelve.3.2.zip\";s:8:\"requires\";s:3:\"3.5\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(167, 'theme_mods_bedrockSageFoundation', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:18:\"primary_navigation\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1558965823;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"sidebar-primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"sidebar-footer\";a:0:{}}}}', 'yes'),
(172, 'theme_mods_sageFund', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:18:\"primary_navigation\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1559219486;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"sidebar-primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"sidebar-footer\";a:0:{}}}}', 'yes'),
(205, 'theme_mods_jac19', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:5:{s:18:\"primary_navigation\";i:17;s:20:\"secondary_navigation\";i:18;s:17:\"bottom_navigation\";i:19;s:9:\"side_menu\";i:20;s:11:\"side_bottom\";i:21;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(220, 'recently_activated', 'a:1:{s:35:\"cookie-law-info/cookie-law-info.php\";i:1602464527;}', 'yes'),
(221, 'acf_version', '5.8.8', 'yes'),
(223, 'widget_gform_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(224, 'gravityformsaddon_gravityformswebapi_version', '1.0', 'yes'),
(225, 'gform_enable_background_updates', '1', 'yes'),
(226, 'gf_db_version', '2.4.17', 'no'),
(227, 'gform_pending_installation', '', 'yes'),
(228, 'rg_form_version', '2.4.17', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(229, 'gform_version_info', 'a:11:{s:12:\"is_valid_key\";b:1;s:6:\"reason\";s:0:\"\";s:7:\"version\";s:6:\"2.4.20\";s:3:\"url\";s:168:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.20.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=gJGBKu35ui%2Fa747EupLpqVW1Mco%3D\";s:15:\"expiration_time\";i:1608654649;s:9:\"offerings\";a:58:{s:12:\"gravityforms\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:6:\"2.4.20\";s:14:\"version_latest\";s:8:\"2.4.20.5\";s:3:\"url\";s:168:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.20.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=gJGBKu35ui%2Fa747EupLpqVW1Mco%3D\";s:10:\"url_latest\";s:170:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.20.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=yXUEIlGpJEZH7uKUE%2FNw5V4PXeE%3D\";}s:21:\"gravityforms2checkout\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:3:\"1.1\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/2checkout/gravityforms2checkout_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=5VhWSHxLHv1vJEZfUZvTyJWtVYo%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/2checkout/gravityforms2checkout_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=5VhWSHxLHv1vJEZfUZvTyJWtVYo%3D\";}s:26:\"gravityformsactivecampaign\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.8\";s:14:\"version_latest\";s:3:\"1.8\";s:3:\"url\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=YMVYQUTqVng04y9AQKYf8i1exNs%3D\";s:10:\"url_latest\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=YMVYQUTqVng04y9AQKYf8i1exNs%3D\";}s:32:\"gravityformsadvancedpostcreation\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"1.0-beta-7\";s:14:\"version_latest\";s:10:\"1.0-beta-7\";s:3:\"url\";s:209:\"https://s3.amazonaws.com/gravityforms/addons/advancedpostcreation/gravityformsadvancedpostcreation_1.0-beta-7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=NJrjGXmOBf2oSAbhA6G820Tp8VU%3D\";s:10:\"url_latest\";s:209:\"https://s3.amazonaws.com/gravityforms/addons/advancedpostcreation/gravityformsadvancedpostcreation_1.0-beta-7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=NJrjGXmOBf2oSAbhA6G820Tp8VU%3D\";}s:20:\"gravityformsagilecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=ShTJxh0NjTW%2BCMiVLazSJMKVlxs%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=ShTJxh0NjTW%2BCMiVLazSJMKVlxs%3D\";}s:24:\"gravityformsauthorizenet\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:5:\"2.8.1\";s:3:\"url\";s:188:\"https://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=9gviSDpWF8bybZug89OD%2BG7jUqE%3D\";s:10:\"url_latest\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.8.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=uYaMZCTEoLTvngb9043%2FamxBlN8%3D\";}s:18:\"gravityformsaweber\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:4:\"2.11\";s:14:\"version_latest\";s:4:\"2.11\";s:3:\"url\";s:179:\"https://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.11.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=YK%2FbgUZwjF8TlH120jZWB09i%2BKs%3D\";s:10:\"url_latest\";s:179:\"https://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.11.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=YK%2FbgUZwjF8TlH120jZWB09i%2BKs%3D\";}s:21:\"gravityformsbatchbook\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=e%2Fr%2FzyW2sgX7HtdJDGUQ0RFsQ%2Bw%3D\";s:10:\"url_latest\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=e%2Fr%2FzyW2sgX7HtdJDGUQ0RFsQ%2Bw%3D\";}s:18:\"gravityformsbreeze\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=%2Bmrj07wKcam0CDDnlbaHf7GU4Kc%3D\";s:10:\"url_latest\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=%2Bmrj07wKcam0CDDnlbaHf7GU4Kc%3D\";}s:27:\"gravityformscampaignmonitor\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.9\";s:14:\"version_latest\";s:3:\"3.9\";s:3:\"url\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=TXwD2EDNBIDktjGRPoWeBATTILY%3D\";s:10:\"url_latest\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=TXwD2EDNBIDktjGRPoWeBATTILY%3D\";}s:20:\"gravityformscampfire\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.2.2\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=iWsdt52V5ARe5NE6EFNHC8KGXFg%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=c6iCzqTzfB3jkKYSfIqzRzloN88%3D\";}s:22:\"gravityformscapsulecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";s:3:\"url\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8lx8tICxYSxVA0xvKEEE%2B49S%2F6Q%3D\";s:10:\"url_latest\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8lx8tICxYSxVA0xvKEEE%2B49S%2F6Q%3D\";}s:26:\"gravityformschainedselects\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";s:3:\"url\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=RBuGPP89m7d6YoVhqGGfp2R2jKY%3D\";s:10:\"url_latest\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=RBuGPP89m7d6YoVhqGGfp2R2jKY%3D\";}s:23:\"gravityformscleverreach\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.7\";s:14:\"version_latest\";s:3:\"1.7\";s:3:\"url\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=aVC291A3mt%2BmY7CGhYxSlr7FMVg%3D\";s:10:\"url_latest\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=aVC291A3mt%2BmY7CGhYxSlr7FMVg%3D\";}s:15:\"gravityformscli\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:0:\"\";s:10:\"url_latest\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/cli/gravityformscli_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=r5uYP1xxdP0%2F%2BTOmP8UTbyhP6GM%3D\";}s:27:\"gravityformsconstantcontact\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";s:3:\"url\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/constantcontact/gravityformsconstantcontact_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=mvAZAc30oZZOSI793kKN2lXtYTo%3D\";s:10:\"url_latest\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/constantcontact/gravityformsconstantcontact_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=mvAZAc30oZZOSI793kKN2lXtYTo%3D\";}s:19:\"gravityformscoupons\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:4:\"2.11\";s:14:\"version_latest\";s:4:\"2.11\";s:3:\"url\";s:179:\"https://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.11.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=Cy1xSY4mXOfiot%2FygcBPxN1UbIk%3D\";s:10:\"url_latest\";s:179:\"https://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.11.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=Cy1xSY4mXOfiot%2FygcBPxN1UbIk%3D\";}s:17:\"gravityformsdebug\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";s:10:\"1.0.beta12\";s:3:\"url\";s:0:\"\";s:10:\"url_latest\";s:179:\"https://s3.amazonaws.com/gravityforms/addons/debug/gravityformsdebug_1.0.beta12.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=229qFqIkc7BDH7v93pCso1Q8OXM%3D\";}s:19:\"gravityformsdropbox\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:3:\"2.8\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=czm%2Fx9YpBrChCYMYT%2FKIn9oVl0U%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=czm%2Fx9YpBrChCYMYT%2FKIn9oVl0U%3D\";}s:24:\"gravityformsemailoctopus\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:188:\"https://s3.amazonaws.com/gravityforms/addons/emailoctopus/gravityformsemailoctopus_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=YjxIQ%2FchoE9mbD2QlLKbPQt4uKo%3D\";s:10:\"url_latest\";s:188:\"https://s3.amazonaws.com/gravityforms/addons/emailoctopus/gravityformsemailoctopus_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=YjxIQ%2FchoE9mbD2QlLKbPQt4uKo%3D\";}s:16:\"gravityformsemma\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:170:\"https://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=B0YKLCBw1nKLFUUGPg6ltnTsoLQ%3D\";s:10:\"url_latest\";s:170:\"https://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=B0YKLCBw1nKLFUUGPg6ltnTsoLQ%3D\";}s:22:\"gravityformsfreshbooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.7\";s:14:\"version_latest\";s:3:\"2.7\";s:3:\"url\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=x3pWb%2F5yJFPwB2ERLk7qqIoxa20%3D\";s:10:\"url_latest\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=x3pWb%2F5yJFPwB2ERLk7qqIoxa20%3D\";}s:23:\"gravityformsgetresponse\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";s:3:\"url\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8PyreqgajF8V5A7XcQxUO0ibVss%3D\";s:10:\"url_latest\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8PyreqgajF8V5A7XcQxUO0ibVss%3D\";}s:21:\"gravityformsgutenberg\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"1.0-rc-1.4\";s:14:\"version_latest\";s:10:\"1.0-rc-1.5\";s:3:\"url\";s:187:\"https://s3.amazonaws.com/gravityforms/addons/gutenberg/gravityformsgutenberg_1.0-rc-1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=7Oy066EwErR6K0FLAIuqxIgE9UI%3D\";s:10:\"url_latest\";s:187:\"https://s3.amazonaws.com/gravityforms/addons/gutenberg/gravityformsgutenberg_1.0-rc-1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=OgMVihwOrsaM6951oAiUFhZnZhU%3D\";}s:21:\"gravityformshelpscout\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:4:\"1.15\";s:14:\"version_latest\";s:4:\"1.15\";s:3:\"url\";s:181:\"https://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.15.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8EN1PptKR2TOMmzMbGlK3khhoqY%3D\";s:10:\"url_latest\";s:181:\"https://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.15.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8EN1PptKR2TOMmzMbGlK3khhoqY%3D\";}s:20:\"gravityformshighrise\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=jQZmfkA5SkKfVDKE16uV1cCrYiw%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=jQZmfkA5SkKfVDKE16uV1cCrYiw%3D\";}s:19:\"gravityformshipchat\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";}s:19:\"gravityformshubspot\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/hubspot/gravityformshubspot_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8q9AJysX8uMMfmJ%2FGKYmiI9KcQQ%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/hubspot/gravityformshubspot_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8q9AJysX8uMMfmJ%2FGKYmiI9KcQQ%3D\";}s:20:\"gravityformsicontact\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=h69%2Bxt2oY3geglZxEeOMz3hHIeI%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=h69%2Bxt2oY3geglZxEeOMz3hHIeI%3D\";}s:19:\"gravityformslogging\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=%2FZX9UOiJHmihNRoro3JgUZKRQ5U%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=aw7PVzsRbeR7lGCYB96nn67frvI%3D\";}s:19:\"gravityformsmadmimi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=ukOD7TOmcicjdxfXCzllMMTgpjQ%3D\";s:10:\"url_latest\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=ukOD7TOmcicjdxfXCzllMMTgpjQ%3D\";}s:21:\"gravityformsmailchimp\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.8\";s:14:\"version_latest\";s:3:\"4.8\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=MRsPMR%2FFCD75gBFdoNROrtzzKl8%3D\";s:10:\"url_latest\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=MRsPMR%2FFCD75gBFdoNROrtzzKl8%3D\";}s:19:\"gravityformsmailgun\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/mailgun/gravityformsmailgun_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=gongIV%2FDizab6XSbHt4SX%2FXdWvQ%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/mailgun/gravityformsmailgun_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=gongIV%2FDizab6XSbHt4SX%2FXdWvQ%3D\";}s:18:\"gravityformsmollie\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:3:\"1.1\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/mollie/gravityformsmollie_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=c1YDqRDoJhIenSeMetFQa5%2Blxcw%3D\";s:10:\"url_latest\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/mollie/gravityformsmollie_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=c1YDqRDoJhIenSeMetFQa5%2Blxcw%3D\";}s:26:\"gravityformspartialentries\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.6\";s:14:\"version_latest\";s:3:\"1.6\";s:3:\"url\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=7ewuFGtDgjhs5W9%2BivkZ3OaEl4A%3D\";s:10:\"url_latest\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=7ewuFGtDgjhs5W9%2BivkZ3OaEl4A%3D\";}s:18:\"gravityformspaypal\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.4\";s:14:\"version_latest\";s:3:\"3.4\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_3.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=w%2BfU5E9L5qY3OMcUHFjd1F%2BtH5U%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_3.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=w%2BfU5E9L5qY3OMcUHFjd1F%2BtH5U%3D\";}s:33:\"gravityformspaypalexpresscheckout\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";N;}s:29:\"gravityformspaypalpaymentspro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.6\";s:14:\"version_latest\";s:3:\"2.6\";s:3:\"url\";s:198:\"https://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=rRNtxjyljIZ0igyNLWNjpO%2Bqsfc%3D\";s:10:\"url_latest\";s:198:\"https://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=rRNtxjyljIZ0igyNLWNjpO%2Bqsfc%3D\";}s:21:\"gravityformspaypalpro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"1.8.1\";s:14:\"version_latest\";s:5:\"1.8.3\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.8.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=1Pcr30giX7sCrZ6AMzQoZTtXFyU%3D\";s:10:\"url_latest\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.8.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=u42zADd9W82603GlB2FjaP3HvmU%3D\";}s:20:\"gravityformspicatcha\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:16:\"gravityformspipe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:170:\"https://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=X4UbcYvXhq92BrRrPtSxSOvL9KY%3D\";s:10:\"url_latest\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=KVXIQAq%2BKZ%2BCZyHnPFwoLn6sFr0%3D\";}s:17:\"gravityformspolls\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.6\";s:14:\"version_latest\";s:3:\"3.6\";s:3:\"url\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=1KuYLSpox2B8VqwEPd4XBsuHHYg%3D\";s:10:\"url_latest\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=1KuYLSpox2B8VqwEPd4XBsuHHYg%3D\";}s:20:\"gravityformspostmark\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/postmark/gravityformspostmark_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=kXPqhkcqUOIv5Fx6M8pIa%2Br%2FaXY%3D\";s:10:\"url_latest\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/postmark/gravityformspostmark_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=kXPqhkcqUOIv5Fx6M8pIa%2Br%2FaXY%3D\";}s:16:\"gravityformsppcp\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/ppcp/gravityformsppcp_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=X0xSZbu9RxCudqKiy0%2Fkgd5%2FyL8%3D\";s:10:\"url_latest\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/ppcp/gravityformsppcp_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=X0xSZbu9RxCudqKiy0%2Fkgd5%2FyL8%3D\";}s:16:\"gravityformsquiz\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.5\";s:14:\"version_latest\";s:3:\"3.5\";s:3:\"url\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=3c0tfeBtbBf2h2Fl%2FtVmGctaZns%3D\";s:10:\"url_latest\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=3c0tfeBtbBf2h2Fl%2FtVmGctaZns%3D\";}s:19:\"gravityformsrestapi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"2.0-beta-2\";s:14:\"version_latest\";s:10:\"2.0-beta-2\";s:3:\"url\";s:183:\"https://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=2zhffJnNv280T0pSY8yUAnUIny8%3D\";s:10:\"url_latest\";s:183:\"https://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=2zhffJnNv280T0pSY8yUAnUIny8%3D\";}s:20:\"gravityformssendgrid\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/sendgrid/gravityformssendgrid_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8YnHE7xEKkPnv6se1dWwtR10%2BRw%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/sendgrid/gravityformssendgrid_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=8YnHE7xEKkPnv6se1dWwtR10%2BRw%3D\";}s:21:\"gravityformssignature\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.0\";s:14:\"version_latest\";s:3:\"4.0\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_4.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=NFpbSqSLeOEmvs1%2B8mK3JWfb6JA%3D\";s:10:\"url_latest\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_4.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=NFpbSqSLeOEmvs1%2B8mK3JWfb6JA%3D\";}s:17:\"gravityformsslack\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:4:\"1.12\";s:14:\"version_latest\";s:4:\"1.12\";s:3:\"url\";s:175:\"https://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.12.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=sVICf08N0%2BefQyRzcsi1vTMT0iM%3D\";s:10:\"url_latest\";s:175:\"https://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.12.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=sVICf08N0%2BefQyRzcsi1vTMT0iM%3D\";}s:18:\"gravityformssquare\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/square/gravityformssquare_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=l0Ip2RYw%2Fh6aavp%2FFUnWBPFX32Q%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/square/gravityformssquare_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=l0Ip2RYw%2Fh6aavp%2FFUnWBPFX32Q%3D\";}s:18:\"gravityformsstripe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.8\";s:14:\"version_latest\";s:5:\"3.8.3\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_3.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=DgOhZD5ZKuU36TFmzEZH%2FDi%2FhEU%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_3.8.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=sUR63OwP4%2FpKumstNntfcmYuZ%2Bs%3D\";}s:18:\"gravityformssurvey\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.6\";s:14:\"version_latest\";s:5:\"3.6.1\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=uZ8oW0aaO%2BxuobtYfvcHd6miQNY%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.6.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=6HNI67%2FqnQnOsMQZia1ZFF8kxZY%3D\";}s:18:\"gravityformstrello\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=yBUUNU0tlxcDNgDhA8sBaZVaUmk%3D\";s:10:\"url_latest\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=yBUUNU0tlxcDNgDhA8sBaZVaUmk%3D\";}s:18:\"gravityformstwilio\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:3:\"2.8\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=vpTPR9DtXtc444lQnwK%2BXrvEIMg%3D\";s:10:\"url_latest\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=vpTPR9DtXtc444lQnwK%2BXrvEIMg%3D\";}s:28:\"gravityformsuserregistration\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.6\";s:14:\"version_latest\";s:3:\"4.6\";s:3:\"url\";s:196:\"https://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_4.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=9LyZ971fjW%2BDfSDSYlFl8fPYB6w%3D\";s:10:\"url_latest\";s:196:\"https://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_4.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=9LyZ971fjW%2BDfSDSYlFl8fPYB6w%3D\";}s:20:\"gravityformswebhooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=MGx3qJKkWrrpN7Roa%2B6F81GsFvA%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=MGx3qJKkWrrpN7Roa%2B6F81GsFvA%3D\";}s:18:\"gravityformszapier\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.3\";s:14:\"version_latest\";s:3:\"3.3\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_3.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=3CbNYPfaZ%2B55lcw%2FwDnARn2VT%2Fk%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_3.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=3CbNYPfaZ%2B55lcw%2FwDnARn2VT%2Fk%3D\";}s:19:\"gravityformszohocrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:4:\"1.12\";s:14:\"version_latest\";s:4:\"1.12\";s:3:\"url\";s:177:\"https://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.12.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=Fg9bcY26DjjsPIMzsE31GnYB3fk%3D\";s:10:\"url_latest\";s:177:\"https://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.12.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=Fg9bcY26DjjsPIMzsE31GnYB3fk%3D\";}}s:9:\"is_active\";s:1:\"1\";s:12:\"product_code\";s:7:\"GFELITE\";s:14:\"version_latest\";s:8:\"2.4.20.5\";s:10:\"url_latest\";s:170:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.20.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1602636450&Signature=yXUEIlGpJEZH7uKUE%2FNw5V4PXeE%3D\";s:9:\"timestamp\";i:1602463650;}', 'no'),
(231, 'widget_mc4wp_form_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(233, 'mc4wp_version', '4.7.5', 'yes'),
(234, 'mc4wp_flash_messages', 'a:0:{}', 'no'),
(235, 'mm_setup', 'a:1:{s:14:\"plugin_version\";s:5:\"2.8.1\";}', 'yes'),
(246, 'pods_component_settings', '{\"components\":{\"templates\":[]}}', 'yes'),
(260, 'widget_pods_widget_single', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(261, 'widget_pods_widget_list', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(262, 'widget_pods_widget_field', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(263, 'widget_pods_widget_form', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(264, 'widget_pods_widget_view', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(272, 'woocommerce_store_address', '', 'yes'),
(273, 'woocommerce_store_address_2', '', 'yes'),
(274, 'woocommerce_store_city', '', 'yes'),
(275, 'woocommerce_default_country', 'GB', 'yes'),
(276, 'woocommerce_store_postcode', '', 'yes'),
(277, 'woocommerce_allowed_countries', 'all', 'yes'),
(278, 'woocommerce_all_except_countries', '', 'yes'),
(279, 'woocommerce_specific_allowed_countries', '', 'yes'),
(280, 'woocommerce_ship_to_countries', '', 'yes'),
(281, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(282, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(283, 'woocommerce_calc_taxes', 'no', 'yes'),
(284, 'woocommerce_enable_coupons', 'yes', 'yes'),
(285, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(286, 'woocommerce_currency', 'GBP', 'yes'),
(287, 'woocommerce_currency_pos', 'left', 'yes'),
(288, 'woocommerce_price_thousand_sep', ',', 'yes'),
(289, 'woocommerce_price_decimal_sep', '.', 'yes'),
(290, 'woocommerce_price_num_decimals', '2', 'yes'),
(291, 'woocommerce_shop_page_id', '', 'yes'),
(292, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(293, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(294, 'woocommerce_placeholder_image', '11', 'yes'),
(295, 'woocommerce_weight_unit', 'kg', 'yes'),
(296, 'woocommerce_dimension_unit', 'cm', 'yes'),
(297, 'woocommerce_enable_reviews', 'yes', 'yes'),
(298, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(299, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(300, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(301, 'woocommerce_review_rating_required', 'yes', 'no'),
(302, 'woocommerce_manage_stock', 'yes', 'yes'),
(303, 'woocommerce_hold_stock_minutes', '60', 'no'),
(304, 'woocommerce_notify_low_stock', 'yes', 'no'),
(305, 'woocommerce_notify_no_stock', 'yes', 'no'),
(306, 'woocommerce_stock_email_recipient', 'developers@jac.co', 'no'),
(307, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(308, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(309, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(310, 'woocommerce_stock_format', '', 'yes'),
(311, 'woocommerce_file_download_method', 'force', 'no'),
(312, 'woocommerce_downloads_require_login', 'no', 'no'),
(313, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(314, 'woocommerce_prices_include_tax', 'no', 'yes'),
(315, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(316, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(317, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(318, 'woocommerce_tax_classes', 'Reduced rate\r\nZero rate', 'yes'),
(319, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(320, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(321, 'woocommerce_price_display_suffix', '', 'yes'),
(322, 'woocommerce_tax_total_display', 'itemized', 'no'),
(323, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(324, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(325, 'woocommerce_ship_to_destination', 'billing', 'no'),
(326, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(327, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(328, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(329, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(330, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(331, 'woocommerce_registration_generate_username', 'yes', 'no'),
(332, 'woocommerce_registration_generate_password', 'yes', 'no'),
(333, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(334, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(335, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(336, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(337, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(338, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(339, 'woocommerce_trash_pending_orders', '', 'no'),
(340, 'woocommerce_trash_failed_orders', '', 'no'),
(341, 'woocommerce_trash_cancelled_orders', '', 'no'),
(342, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(343, 'woocommerce_email_from_name', 'B E D R O C K', 'no'),
(344, 'woocommerce_email_from_address', 'developers@jac.co', 'no'),
(345, 'woocommerce_email_header_image', '', 'no'),
(346, 'woocommerce_email_footer_text', '{site_title}<br/>Built with <a href=\"https://woocommerce.com/\">WooCommerce</a>', 'no'),
(347, 'woocommerce_email_base_color', '#96588a', 'no'),
(348, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(349, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(350, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(351, 'woocommerce_cart_page_id', '', 'yes'),
(352, 'woocommerce_checkout_page_id', '', 'yes'),
(353, 'woocommerce_myaccount_page_id', '', 'yes'),
(354, 'woocommerce_terms_page_id', '', 'no'),
(355, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(356, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(357, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(358, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(359, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(360, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(361, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(362, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(363, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(364, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(365, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(366, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(367, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(368, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(369, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(370, 'woocommerce_api_enabled', 'no', 'yes'),
(371, 'woocommerce_allow_tracking', 'no', 'no'),
(372, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(373, 'woocommerce_single_image_width', '600', 'yes'),
(374, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(375, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(376, 'woocommerce_demo_store', 'no', 'no'),
(377, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:8:\"/product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(378, 'current_theme_supports_woocommerce', 'no', 'yes'),
(379, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(381, 'product_cat_children', 'a:0:{}', 'yes'),
(382, 'default_product_cat', '16', 'yes'),
(386, 'woocommerce_db_version', '3.6.4', 'yes'),
(387, 'woocommerce_admin_notices', 'a:1:{i:0;s:6:\"update\";}', 'yes'),
(388, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(389, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(390, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(391, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(392, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(393, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(394, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(395, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(396, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(397, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(398, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(399, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(400, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(402, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(404, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:8:\"approved\";s:1:\"1\";s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(405, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(419, 'wpseo', 'a:20:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:4:\"13.3\";s:20:\"disableadvanced_meta\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1559226652;s:13:\"myyoast-oauth\";b:0;}', 'yes'),
(420, 'wpseo_titles', 'a:130:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";i:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";i:0;s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;s:13:\"title-product\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-product\";s:0:\"\";s:15:\"noindex-product\";b:0;s:16:\"showdate-product\";b:0;s:26:\"display-metabox-pt-product\";b:1;s:26:\"post_types-product-maintax\";i:0;s:20:\"title-social_channel\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:23:\"metadesc-social_channel\";s:0:\"\";s:22:\"noindex-social_channel\";b:1;s:23:\"showdate-social_channel\";b:0;s:33:\"display-metabox-pt-social_channel\";b:1;s:33:\"post_types-social_channel-maintax\";i:0;s:11:\"title-slide\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-slide\";s:0:\"\";s:13:\"noindex-slide\";b:1;s:14:\"showdate-slide\";b:0;s:24:\"display-metabox-pt-slide\";b:1;s:24:\"post_types-slide-maintax\";i:0;s:11:\"title-event\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-event\";s:0:\"\";s:13:\"noindex-event\";b:0;s:14:\"showdate-event\";b:0;s:24:\"display-metabox-pt-event\";b:1;s:24:\"post_types-event-maintax\";i:0;s:21:\"title-ptarchive-event\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-ptarchive-event\";s:0:\"\";s:23:\"bctitle-ptarchive-event\";s:0:\"\";s:23:\"noindex-ptarchive-event\";b:0;s:20:\"title-call_to_action\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:23:\"metadesc-call_to_action\";s:0:\"\";s:22:\"noindex-call_to_action\";b:1;s:23:\"showdate-call_to_action\";b:0;s:33:\"display-metabox-pt-call_to_action\";b:1;s:33:\"post_types-call_to_action-maintax\";i:0;s:21:\"title-tax-product_cat\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-product_cat\";s:0:\"\";s:31:\"display-metabox-tax-product_cat\";b:1;s:23:\"noindex-tax-product_cat\";b:0;s:29:\"taxonomy-product_cat-ptparent\";i:0;s:21:\"title-tax-product_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-product_tag\";s:0:\"\";s:31:\"display-metabox-tax-product_tag\";b:1;s:23:\"noindex-tax-product_tag\";b:0;s:29:\"taxonomy-product_tag-ptparent\";i:0;s:32:\"title-tax-product_shipping_class\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:35:\"metadesc-tax-product_shipping_class\";s:0:\"\";s:42:\"display-metabox-tax-product_shipping_class\";b:1;s:34:\"noindex-tax-product_shipping_class\";b:0;s:40:\"taxonomy-product_shipping_class-ptparent\";i:0;s:26:\"title-tax-slide_categories\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:29:\"metadesc-tax-slide_categories\";s:0:\"\";s:36:\"display-metabox-tax-slide_categories\";b:1;s:28:\"noindex-tax-slide_categories\";b:0;s:34:\"taxonomy-slide_categories-ptparent\";i:0;s:26:\"title-tax-event_categories\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:29:\"metadesc-tax-event_categories\";s:0:\"\";s:36:\"display-metabox-tax-event_categories\";b:1;s:28:\"noindex-tax-event_categories\";b:0;s:34:\"taxonomy-event_categories-ptparent\";i:0;}', 'yes'),
(421, 'wpseo_social', 'a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(422, 'wpseo_flush_rewrite', '1', 'yes'),
(479, '_transient_product_query-transient-version', '1602467841', 'yes'),
(516, 'editor_cap_setup', '1', 'yes'),
(517, 'subtitle_setup', '1', 'yes'),
(529, 'recovery_mode_email_last_sent', '1583951887', 'yes'),
(1288, 'company_info_company', 'JAC » We Create', 'yes'),
(1289, 'company_info_address1', '673 Topsail Road', 'yes'),
(1290, 'company_info_address2', 'PO Box 12345', 'yes'),
(1291, 'company_info_city', 'St. John\\\'s', 'yes'),
(1292, 'company_info_province', 'NL', 'yes'),
(1293, 'company_info_country', 'Canada', 'yes'),
(1294, 'company_info_postal', 'A1E 2E3', 'yes'),
(1295, 'company_info_phone', '709 754 0555', 'yes'),
(1296, 'company_info_tollfree', '800 555 0555', 'yes'),
(1297, 'company_info_fax', '709 555 0555', 'yes'),
(1298, 'company_info_email', 'contact@jac.co', 'yes'),
(1299, 'company_info_weekday', '8am - 5pm', 'yes'),
(1300, 'company_info_sunday', 'Closed', 'yes'),
(1305, 'rg_gforms_key', '9d912829bb2522e8bcb724fbef903dca', 'yes'),
(1306, 'rg_gforms_enable_akismet', '1', 'yes'),
(1307, 'rg_gforms_currency', 'CAD', 'yes'),
(1308, 'gform_enable_toolbar_menu', '1', 'yes'),
(1354, 'duplicate_post_copytitle', '1', 'yes'),
(1355, 'duplicate_post_copydate', '', 'yes'),
(1356, 'duplicate_post_copystatus', '', 'yes'),
(1357, 'duplicate_post_copyslug', '', 'yes'),
(1358, 'duplicate_post_copyexcerpt', '1', 'yes'),
(1359, 'duplicate_post_copycontent', '1', 'yes'),
(1360, 'duplicate_post_copythumbnail', '1', 'yes'),
(1361, 'duplicate_post_copytemplate', '1', 'yes'),
(1362, 'duplicate_post_copyformat', '1', 'yes'),
(1363, 'duplicate_post_copyauthor', '', 'yes'),
(1364, 'duplicate_post_copypassword', '', 'yes'),
(1365, 'duplicate_post_copyattachments', '', 'yes'),
(1366, 'duplicate_post_copychildren', '', 'yes'),
(1367, 'duplicate_post_copycomments', '', 'yes'),
(1368, 'duplicate_post_copymenuorder', '1', 'yes'),
(1369, 'duplicate_post_taxonomies_blacklist', '', 'yes'),
(1370, 'duplicate_post_blacklist', '', 'yes'),
(1371, 'duplicate_post_types_enabled', 'a:4:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:16:\"notification_bar\";i:3;s:11:\"video_popup\";}', 'yes'),
(1372, 'duplicate_post_show_row', '1', 'yes'),
(1373, 'duplicate_post_show_adminbar', '1', 'yes'),
(1374, 'duplicate_post_show_submitbox', '1', 'yes'),
(1375, 'duplicate_post_show_bulkactions', '1', 'yes'),
(1376, 'duplicate_post_show_original_column', '', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1377, 'duplicate_post_show_original_in_post_states', '', 'yes'),
(1378, 'duplicate_post_show_original_meta_box', '', 'yes'),
(1379, 'duplicate_post_version', '3.2.4', 'yes'),
(1380, 'duplicate_post_show_notice', '', 'no'),
(1384, 'gf_previous_db_version', '2.2.6', 'yes'),
(1385, 'gf_upgrade_lock', '', 'yes'),
(1386, 'gform_sticky_admin_messages', 'a:0:{}', 'yes'),
(1390, 'gf_submissions_block', '', 'yes'),
(1392, 'SnazzyMapStyles', 'a:3:{i:0;a:12:{s:2:\"id\";i:72543;s:4:\"name\";s:19:\"Assassin\'s Creed IV\";s:11:\"description\";s:56:\"Themed map matching the colors from Assassin\'s Creed IV.\";s:3:\"url\";s:53:\"https://snazzymaps.com/style/72543/assassins-creed-iv\";s:8:\"imageUrl\";s:90:\"https://snazzy-maps-cdn.azureedge.net/assets/72543-assassins-creed-iv.png?v=00010101120000\";s:4:\"json\";s:3302:\"[{\"featureType\":\"all\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"all\",\"elementType\":\"labels\",\"stylers\":[{\"visibility\":\"off\"},{\"saturation\":\"-100\"}]},{\"featureType\":\"all\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"saturation\":36},{\"color\":\"#000000\"},{\"lightness\":40},{\"visibility\":\"off\"}]},{\"featureType\":\"all\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"off\"},{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"all\",\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":17},{\"weight\":1.2}]},{\"featureType\":\"landscape\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"landscape\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#4d6059\"}]},{\"featureType\":\"landscape\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#4d6059\"}]},{\"featureType\":\"landscape.natural\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#4d6059\"}]},{\"featureType\":\"poi\",\"elementType\":\"geometry\",\"stylers\":[{\"lightness\":21}]},{\"featureType\":\"poi\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#4d6059\"}]},{\"featureType\":\"poi\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#4d6059\"}]},{\"featureType\":\"road\",\"elementType\":\"geometry\",\"stylers\":[{\"visibility\":\"on\"},{\"color\":\"#7f8d89\"}]},{\"featureType\":\"road\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#7f8d89\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#7f8d89\"},{\"lightness\":17}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#7f8d89\"},{\"lightness\":29},{\"weight\":0.2}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":18}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#7f8d89\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#7f8d89\"}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#7f8d89\"}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#7f8d89\"}]},{\"featureType\":\"transit\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":19}]},{\"featureType\":\"water\",\"elementType\":\"all\",\"stylers\":[{\"color\":\"#2b3638\"},{\"visibility\":\"on\"}]},{\"featureType\":\"water\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#2b3638\"},{\"lightness\":17}]},{\"featureType\":\"water\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#24282b\"}]},{\"featureType\":\"water\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#24282b\"}]},{\"featureType\":\"water\",\"elementType\":\"labels\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"water\",\"elementType\":\"labels.text\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"water\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"water\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"water\",\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]}]\";s:5:\"views\";i:427872;s:9:\"favorites\";i:1646;s:9:\"createdBy\";a:2:{s:4:\"name\";s:11:\"Sarah Frisk\";s:3:\"url\";N;}s:9:\"createdOn\";s:23:\"2016-08-20T00:16:43.153\";s:4:\"tags\";a:1:{i:0;s:4:\"dark\";}s:6:\"colors\";a:2:{i:0;s:4:\"gray\";i:1;s:5:\"green\";}}i:1;a:12:{s:2:\"id\";i:109014;s:4:\"name\";s:19:\"Brantley Cleaners \";s:11:\"description\";s:384:\"We specialize in Dry cleaning and we also provide services such as wedding gown preservation, leather treatments and cleaning of household items in Highland Park. We provide quality cleaning, definite satisfaction, and exquisite customer service for our clients while maintaining a focus on environmental consciousness.\r\nFor more details please visit at https://brantleycleaners.com\r\n\";s:3:\"url\";s:59:\"https://snazzymaps.com/style/109014/brantley-cleaners%C2%A0\";s:8:\"imageUrl\";s:92:\"https://snazzy-maps-cdn.azureedge.net/assets/109014-brantley-cleaners .png?v=00010101120000\";s:4:\"json\";s:1415:\"[{\"featureType\":\"all\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"saturation\":36},{\"color\":\"#000000\"},{\"lightness\":40}]},{\"featureType\":\"all\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"on\"},{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"all\",\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":17},{\"weight\":1.2}]},{\"featureType\":\"landscape\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"poi\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":21}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":17}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":29},{\"weight\":0.2}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":18}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"transit\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":19}]},{\"featureType\":\"water\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#0f252e\"},{\"lightness\":17}]}]\";s:5:\"views\";i:29;s:9:\"favorites\";i:1;s:9:\"createdBy\";a:2:{s:4:\"name\";s:16:\"Brantleycleaners\";s:3:\"url\";N;}s:9:\"createdOn\";s:23:\"2017-06-14T11:57:26.507\";s:4:\"tags\";a:1:{i:0;s:4:\"dark\";}s:6:\"colors\";a:1:{i:0;s:4:\"blue\";}}i:2;a:12:{s:2:\"id\";i:26527;s:4:\"name\";s:8:\"Sin City\";s:11:\"description\";s:20:\"Intense and elegant.\";s:3:\"url\";s:43:\"https://snazzymaps.com/style/26527/sin-city\";s:8:\"imageUrl\";s:80:\"https://snazzy-maps-cdn.azureedge.net/assets/26527-sin-city.png?v=00010101120000\";s:4:\"json\";s:2503:\"[{\"featureType\":\"all\",\"elementType\":\"labels\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"all\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"saturation\":36},{\"color\":\"#000000\"},{\"lightness\":40}]},{\"featureType\":\"all\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"on\"},{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"all\",\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":17},{\"weight\":1.2}]},{\"featureType\":\"administrative.locality\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#c4c4c4\"}]},{\"featureType\":\"administrative.neighborhood\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#707070\"}]},{\"featureType\":\"landscape\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"poi\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":21},{\"visibility\":\"on\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"geometry\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#be2026\"},{\"lightness\":\"0\"},{\"visibility\":\"on\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"off\"},{\"hue\":\"#ff000a\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":18}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#575757\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#ffffff\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"color\":\"#2c2c2c\"}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"road.local\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#999999\"}]},{\"featureType\":\"road.local\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"saturation\":\"-52\"}]},{\"featureType\":\"transit\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":19}]},{\"featureType\":\"water\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":17}]}]\";s:5:\"views\";i:35192;s:9:\"favorites\";i:352;s:9:\"createdBy\";a:2:{s:4:\"name\";s:14:\"Luca Castorani\";s:3:\"url\";N;}s:9:\"createdOn\";s:23:\"2015-07-02T17:40:35.033\";s:4:\"tags\";a:3:{i:0;s:4:\"dark\";i:1;s:9:\"greyscale\";i:2;s:8:\"two-tone\";}s:6:\"colors\";a:2:{i:0;s:4:\"gray\";i:1;s:3:\"red\";}}}', 'yes'),
(1393, 'SnazzyMapDefaultStyle', 'a:12:{s:2:\"id\";i:26527;s:4:\"name\";s:8:\"Sin City\";s:11:\"description\";s:20:\"Intense and elegant.\";s:3:\"url\";s:43:\"https://snazzymaps.com/style/26527/sin-city\";s:8:\"imageUrl\";s:80:\"https://snazzy-maps-cdn.azureedge.net/assets/26527-sin-city.png?v=00010101120000\";s:4:\"json\";s:2503:\"[{\"featureType\":\"all\",\"elementType\":\"labels\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"all\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"saturation\":36},{\"color\":\"#000000\"},{\"lightness\":40}]},{\"featureType\":\"all\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"on\"},{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"all\",\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":17},{\"weight\":1.2}]},{\"featureType\":\"administrative.locality\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#c4c4c4\"}]},{\"featureType\":\"administrative.neighborhood\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#707070\"}]},{\"featureType\":\"landscape\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":20}]},{\"featureType\":\"poi\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":21},{\"visibility\":\"on\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"geometry\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#be2026\"},{\"lightness\":\"0\"},{\"visibility\":\"on\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"off\"},{\"hue\":\"#ff000a\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":18}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#575757\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#ffffff\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"color\":\"#2c2c2c\"}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":16}]},{\"featureType\":\"road.local\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#999999\"}]},{\"featureType\":\"road.local\",\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"saturation\":\"-52\"}]},{\"featureType\":\"transit\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":19}]},{\"featureType\":\"water\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#000000\"},{\"lightness\":17}]}]\";s:5:\"views\";i:35192;s:9:\"favorites\";i:352;s:9:\"createdBy\";a:2:{s:4:\"name\";s:14:\"Luca Castorani\";s:3:\"url\";N;}s:9:\"createdOn\";s:23:\"2015-07-02T17:40:35.033\";s:4:\"tags\";a:3:{i:0;s:4:\"dark\";i:1;s:9:\"greyscale\";i:2;s:8:\"two-tone\";}s:6:\"colors\";a:2:{i:0;s:4:\"gray\";i:1;s:3:\"red\";}}', 'yes'),
(1408, 'wptpreloader_screen', 'full', 'yes'),
(1409, 'wptpreloader_bg_color', '#FFFFFF', 'yes'),
(1410, 'wptpreloader_image', '//localhost:9090/foundation-bedrock-theme/web/app/plugins/the-preloader/images/preloader.GIF', 'yes'),
(1496, 'company_info_st_johns_address1', '673 Topsail Road', 'yes'),
(1497, 'company_info_st_johns_city', 'St. John\\\'s', 'yes'),
(1498, 'company_info_st_johns_province', 'NL', 'yes'),
(1499, 'company_info_st_johns_country', 'Canada', 'yes'),
(1500, 'company_info_st_johns_postal', 'A1E 2E3', 'yes'),
(1501, 'company_info_st_johns_phone', '709 754 0555', 'yes'),
(1502, 'company_info_st_johns_tollfree', '800 555 0555', 'yes'),
(1503, 'company_info_st_johns_fax', '709 555 0555', 'yes'),
(1504, 'company_info_st_johns_email', 'contact@jac.co', 'yes'),
(1505, 'company_info_sales_time_one', 'Monday - Saturday', 'yes'),
(1506, 'company_info_sales_label_one', '8am - 5pm', 'yes'),
(1507, 'company_info_sales_time_two', 'Sunday', 'yes'),
(1508, 'company_info_sales_label_two', 'CLOSED', 'yes'),
(1509, 'company_info_sales_announcement', 'We are currently closed due to bad weather', 'yes'),
(1515, 'company_info_company_one_address1', '673 Topsail Road', 'yes'),
(1516, 'company_info_company_one_city', 'St. John\\\'s', 'yes'),
(1517, 'company_info_company_one_province', 'NL', 'yes'),
(1518, 'company_info_company_one_country', 'Canada', 'yes'),
(1519, 'company_info_company_one_postal', 'A1E 2E3', 'yes'),
(1520, 'company_info_company_one_phone', '709 754 0555', 'yes'),
(1521, 'company_info_company_one_tollfree', '800 555 0555', 'yes'),
(1522, 'company_info_company_one_fax', '709 754 5555', 'yes'),
(1523, 'company_info_company_one_email', 'contact@jac.co', 'yes'),
(1524, 'company_info_department_one_time_one', '8AM - 9PM', 'yes'),
(1525, 'company_info_department_one_label_one', 'Monday - Saturday', 'yes'),
(1526, 'company_info_department_one_time_two', 'CLOSED', 'yes'),
(1527, 'company_info_department_one_label_two', 'Sunday', 'yes'),
(1562, 'company_info_company_announcement', 'We are closed due to bad weather', 'yes'),
(1603, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"rQDYN20lNjJBNjUvodiYuMxeuzsD7iXk\";}', 'yes'),
(1604, 'action_scheduler_hybrid_store_demarkation', '79', 'yes'),
(1605, 'schema-ActionScheduler_StoreSchema', '3.0.1584024797', 'yes'),
(1606, 'schema-ActionScheduler_LoggerSchema', '2.0.1584024797', 'yes'),
(1609, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(1617, 'action_scheduler_lock_async-request-runner', '1602468926', 'yes'),
(1627, 'admin_email_lifespan', '1617525462', 'yes'),
(1628, 'db_upgraded', '', 'yes'),
(1630, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.4.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.4.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.4-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.3.4-partial-2.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.4-rollback-2.zip\";}s:7:\"current\";s:5:\"5.3.4\";s:7:\"version\";s:5:\"5.3.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:5:\"5.3.2\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1602463649;s:15:\"version_checked\";s:5:\"5.3.2\";s:12:\"translations\";a:0:{}}', 'no'),
(1631, 'woocommerce_onboarding_opt_in', 'no', 'yes'),
(1635, 'woocommerce_admin_install_timestamp', '1585092111', 'yes'),
(1636, 'can_compress_scripts', '1', 'no'),
(1641, 'woocommerce_admin_last_orders_milestone', '0', 'yes'),
(1642, 'woocommerce_onboarding_profile', 'a:0:{}', 'yes'),
(1697, 'pods_framework_version_last', '2.7.12', 'yes'),
(1698, 'pods_framework_version', '2.7.16.2', 'yes'),
(1699, 'pods_framework_db_version', '2.3.5', 'yes'),
(1700, '_transient_pods_pods_flush_rewrites-2.7.16.2', '0', 'yes'),
(1703, '_transient_pods_pods_components-2.7.16.2', 'a:12:{s:22:\"advanced-content-types\";a:25:{s:2:\"ID\";s:22:\"advanced-content-types\";s:4:\"Name\";s:22:\"Advanced Content Types\";s:9:\"ShortName\";s:22:\"Advanced Content Types\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:22:\"Advanced Content Types\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:98:\"A content type that exists outside of the WordPress post and postmeta table and uses custom tables\";s:7:\"Version\";s:3:\"2.3\";s:8:\"Category\";s:8:\"Advanced\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:27:\"Pods_Advanced_Content_Types\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:26:\"Advanced-Content-Types.php\";}s:22:\"advanced-relationships\";a:25:{s:2:\"ID\";s:22:\"advanced-relationships\";s:4:\"Name\";s:22:\"Advanced Relationships\";s:9:\"ShortName\";s:22:\"Advanced Relationships\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:22:\"Advanced Relationships\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:96:\"Add advanced relationship objects for relating to including Database Tables, Multisite Networks,\";s:7:\"Version\";s:3:\"2.3\";s:8:\"Category\";s:8:\"Advanced\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:27:\"Pods_Advanced_Relationships\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:26:\"Advanced-Relationships.php\";}s:19:\"builder-integration\";a:25:{s:2:\"ID\";s:19:\"builder-integration\";s:4:\"Name\";s:19:\"Builder Integration\";s:9:\"ShortName\";s:19:\"Builder Integration\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:19:\"Builder Integration\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:91:\"Integration with the <a href=\"http://ithemes.com/\">Builder</a> theme / child themes from <a\";s:7:\"Version\";s:3:\"1.0\";s:8:\"Category\";s:11:\"Integration\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:12:\"Pods_Builder\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:29:\"pods-builder/pods-builder.php\";s:8:\"External\";b:0;s:4:\"File\";s:19:\"Builder/Builder.php\";}s:7:\"helpers\";a:25:{s:2:\"ID\";s:7:\"helpers\";s:4:\"Name\";s:7:\"Helpers\";s:9:\"ShortName\";s:7:\"Helpers\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:7:\"Helpers\";s:8:\"MenuPage\";s:31:\"edit.php?post_type=_pods_helper\";s:11:\"MenuAddPage\";s:35:\"post-new.php?post_type=_pods_helper\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:102:\"A holdover from Pods 1.x for backwards compatibility purposes, you most likely don\'t need these and we\";s:7:\"Version\";s:3:\"2.3\";s:8:\"Category\";s:8:\"Advanced\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:12:\"Pods_Helpers\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:11:\"Helpers.php\";}s:15:\"markdown-syntax\";a:25:{s:2:\"ID\";s:15:\"markdown-syntax\";s:4:\"Name\";s:15:\"Markdown Syntax\";s:9:\"ShortName\";s:15:\"Markdown Syntax\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:15:\"Markdown Syntax\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:95:\"Integration with Markdown (http://michelf.com/projects/php-markdown/); Adds an option to enable\";s:7:\"Version\";s:3:\"1.0\";s:8:\"Category\";s:11:\"Field Types\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:13:\"Pods_Markdown\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:12:\"Markdown.php\";}s:50:\"migrate-import-from-the-custom-post-type-ui-plugin\";a:25:{s:2:\"ID\";s:50:\"migrate-import-from-the-custom-post-type-ui-plugin\";s:4:\"Name\";s:51:\"Migrate: Import from the Custom Post Type UI plugin\";s:9:\"ShortName\";s:51:\"Migrate: Import from the Custom Post Type UI plugin\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:14:\"Migrate CPT UI\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:68:\"Import Custom Post Types and Taxonomies from Custom Post Type UI (<a\";s:7:\"Version\";s:3:\"1.0\";s:8:\"Category\";s:9:\"Migration\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:18:\"Pods_Migrate_CPTUI\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:69:\"pods-migrate-custom-post-type-ui/pods-migrate-custom-post-type-ui.php\";s:8:\"External\";b:0;s:4:\"File\";s:31:\"Migrate-CPTUI/Migrate-CPTUI.php\";}s:16:\"migrate-packages\";a:25:{s:2:\"ID\";s:16:\"migrate-packages\";s:4:\"Name\";s:17:\"Migrate: Packages\";s:9:\"ShortName\";s:17:\"Migrate: Packages\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:16:\"Migrate Packages\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:90:\"Import/Export your Pods, Fields, and other settings from any Pods site; Includes an API to\";s:7:\"Version\";s:3:\"2.0\";s:8:\"Category\";s:9:\"Migration\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:21:\"Pods_Migrate_Packages\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:47:\"pods-migrate-packages/pods-migrate-packages.php\";s:8:\"External\";b:0;s:4:\"File\";s:37:\"Migrate-Packages/Migrate-Packages.php\";}s:5:\"pages\";a:25:{s:2:\"ID\";s:5:\"pages\";s:4:\"Name\";s:5:\"Pages\";s:9:\"ShortName\";s:5:\"Pages\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:5:\"Pages\";s:8:\"MenuPage\";s:29:\"edit.php?post_type=_pods_page\";s:11:\"MenuAddPage\";s:33:\"post-new.php?post_type=_pods_page\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:96:\"Creates advanced URL structures using wildcards in order to enable the front-end display of Pods\";s:7:\"Version\";s:3:\"2.3\";s:8:\"Category\";s:8:\"Advanced\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:10:\"Pods_Pages\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:9:\"Pages.php\";}s:22:\"roles-and-capabilities\";a:25:{s:2:\"ID\";s:22:\"roles-and-capabilities\";s:4:\"Name\";s:22:\"Roles and Capabilities\";s:9:\"ShortName\";s:22:\"Roles and Capabilities\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:24:\"Roles &amp; Capabilities\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:69:\"Create and Manage WordPress User Roles and Capabilities; Uses the \'<a\";s:7:\"Version\";s:3:\"1.0\";s:8:\"Category\";s:5:\"Tools\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:10:\"Pods_Roles\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:15:\"Roles/Roles.php\";}s:13:\"table-storage\";a:25:{s:2:\"ID\";s:13:\"table-storage\";s:4:\"Name\";s:13:\"Table Storage\";s:9:\"ShortName\";s:13:\"Table Storage\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:13:\"Table Storage\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:98:\"Enable a custom database table for your custom fields on Post Types, Media, Taxonomies, Users, and\";s:7:\"Version\";s:3:\"2.3\";s:8:\"Category\";s:8:\"Advanced\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:18:\"Pods_Table_Storage\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:17:\"Table-Storage.php\";}s:9:\"templates\";a:25:{s:2:\"ID\";s:9:\"templates\";s:4:\"Name\";s:9:\"Templates\";s:9:\"ShortName\";s:9:\"Templates\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:9:\"Templates\";s:8:\"MenuPage\";s:33:\"edit.php?post_type=_pods_template\";s:11:\"MenuAddPage\";s:37:\"post-new.php?post_type=_pods_template\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:101:\"An easy to use templating engine for Pods. Use {@field_name} magic tags to output values, within your\";s:7:\"Version\";s:3:\"2.3\";s:8:\"Category\";s:8:\"Advanced\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:14:\"Pods_Templates\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:23:\"Templates/Templates.php\";}s:20:\"translate-pods-admin\";a:25:{s:2:\"ID\";s:20:\"translate-pods-admin\";s:4:\"Name\";s:20:\"Translate Pods Admin\";s:9:\"ShortName\";s:20:\"Translate Pods Admin\";s:10:\"PluginName\";s:0:\"\";s:13:\"ComponentName\";s:0:\"\";s:3:\"URI\";s:0:\"\";s:8:\"MenuName\";s:14:\"Translate Pods\";s:8:\"MenuPage\";s:0:\"\";s:11:\"MenuAddPage\";s:0:\"\";s:7:\"MustUse\";b:0;s:11:\"Description\";s:44:\"Allow UI of Pods and fields to be translated\";s:7:\"Version\";s:3:\"0.2\";s:8:\"Category\";s:4:\"I18n\";s:6:\"Author\";s:0:\"\";s:9:\"AuthorURI\";s:0:\"\";s:5:\"Class\";s:19:\"Pods_Component_I18n\";s:4:\"Hide\";s:0:\"\";s:16:\"PluginDependency\";s:0:\"\";s:15:\"ThemeDependency\";s:0:\"\";s:13:\"DeveloperMode\";b:0;s:13:\"TablelessMode\";b:0;s:10:\"Capability\";s:0:\"\";s:6:\"Plugin\";s:0:\"\";s:8:\"External\";b:0;s:4:\"File\";s:13:\"I18n/I18n.php\";}}', 'yes'),
(1704, '_transient_pods_pods_get_type_pod-2.7.16.2', 'none', 'yes'),
(1705, '_transient_pods_pods_get_type_post_type-2.7.16.2', 'none', 'yes'),
(1706, '_transient_pods_pods_get_type_taxonomy-2.7.16.2', 'none', 'yes'),
(1707, '_transient_pods_pods_get_type_media-2.7.16.2', 'none', 'yes'),
(1708, '_transient_pods_pods_get_type_user-2.7.16.2', 'none', 'yes'),
(1709, '_transient_pods_pods_get_type_comment-2.7.16.2', 'none', 'yes'),
(1710, '_transient_pods_pods_get_type_settings-2.7.16.2', 'none', 'yes'),
(1712, '_transient_pods_pods_wp_cpt_ct-2.7.16.2', 'a:3:{s:10:\"post_types\";a:0:{}s:10:\"taxonomies\";a:0:{}s:22:\"post_format_post_types\";a:0:{}}', 'yes'),
(1713, '_transient_pods_pods_field_types-2.7.16.2', 'a:20:{s:4:\"text\";a:7:{s:5:\"group\";s:4:\"Text\";s:4:\"type\";s:4:\"text\";s:5:\"label\";s:10:\"Plain Text\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:7:\"website\";a:7:{s:5:\"group\";s:4:\"Text\";s:4:\"type\";s:7:\"website\";s:5:\"label\";s:7:\"Website\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:5:\"phone\";a:7:{s:5:\"group\";s:4:\"Text\";s:4:\"type\";s:5:\"phone\";s:5:\"label\";s:5:\"Phone\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:5:\"email\";a:7:{s:5:\"group\";s:4:\"Text\";s:4:\"type\";s:5:\"email\";s:5:\"label\";s:6:\"E-mail\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:8:\"password\";a:7:{s:5:\"group\";s:4:\"Text\";s:4:\"type\";s:8:\"password\";s:5:\"label\";s:8:\"Password\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:9:\"paragraph\";a:7:{s:5:\"group\";s:9:\"Paragraph\";s:4:\"type\";s:9:\"paragraph\";s:5:\"label\";s:20:\"Plain Paragraph Text\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:7:\"wysiwyg\";a:7:{s:5:\"group\";s:9:\"Paragraph\";s:4:\"type\";s:7:\"wysiwyg\";s:5:\"label\";s:23:\"WYSIWYG (Visual Editor)\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:4:\"code\";a:7:{s:5:\"group\";s:9:\"Paragraph\";s:4:\"type\";s:4:\"code\";s:5:\"label\";s:26:\"Code (Syntax Highlighting)\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:8:\"datetime\";a:9:{s:5:\"group\";s:11:\"Date / Time\";s:4:\"type\";s:8:\"datetime\";s:5:\"label\";s:11:\"Date / Time\";s:7:\"prepare\";s:2:\"%s\";s:14:\"storage_format\";s:11:\"Y-m-d H:i:s\";s:11:\"empty_value\";s:19:\"0000-00-00 00:00:00\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:4:\"date\";a:9:{s:5:\"group\";s:11:\"Date / Time\";s:4:\"type\";s:4:\"date\";s:5:\"label\";s:4:\"Date\";s:7:\"prepare\";s:2:\"%s\";s:14:\"storage_format\";s:5:\"Y-m-d\";s:11:\"empty_value\";s:10:\"0000-00-00\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:4:\"time\";a:9:{s:5:\"group\";s:11:\"Date / Time\";s:4:\"type\";s:4:\"time\";s:5:\"label\";s:4:\"Time\";s:7:\"prepare\";s:2:\"%s\";s:14:\"storage_format\";s:5:\"H:i:s\";s:11:\"empty_value\";s:0:\"\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:6:\"number\";a:7:{s:5:\"group\";s:6:\"Number\";s:4:\"type\";s:6:\"number\";s:5:\"label\";s:12:\"Plain Number\";s:7:\"prepare\";s:2:\"%d\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:8:\"currency\";a:8:{s:5:\"group\";s:6:\"Number\";s:4:\"type\";s:8:\"currency\";s:5:\"label\";s:8:\"Currency\";s:7:\"prepare\";s:2:\"%d\";s:10:\"currencies\";a:34:{s:3:\"aud\";a:4:{s:5:\"label\";s:3:\"AUD\";s:4:\"name\";s:17:\"Australian Dollar\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:3:\"brl\";a:4:{s:5:\"label\";s:3:\"BRL\";s:4:\"name\";s:14:\"Brazilian Real\";s:4:\"sign\";s:2:\"R$\";s:6:\"entity\";s:6:\"R&#36;\";}s:3:\"cad\";a:4:{s:5:\"label\";s:3:\"CAD\";s:4:\"name\";s:15:\"Canadian Dollar\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:3:\"chf\";a:4:{s:5:\"label\";s:3:\"CHF\";s:4:\"name\";s:11:\"Swiss Franc\";s:4:\"sign\";s:2:\"Fr\";s:6:\"entity\";s:2:\"Fr\";}s:3:\"cny\";a:4:{s:5:\"label\";s:3:\"CNY\";s:4:\"name\";s:16:\"Chinese Yen (¥)\";s:4:\"sign\";s:2:\"¥\";s:6:\"entity\";s:5:\"&yen;\";}s:4:\"cny2\";a:4:{s:5:\"label\";s:3:\"CNY\";s:4:\"name\";s:18:\"Chinese Yuan (元)\";s:4:\"sign\";s:3:\"元\";s:6:\"entity\";s:8:\"&#20803;\";}s:3:\"czk\";a:4:{s:5:\"label\";s:3:\"CZK\";s:4:\"name\";s:12:\"Czech Koruna\";s:4:\"sign\";s:3:\"Kč\";s:6:\"entity\";s:8:\"K&#x10D;\";}s:3:\"dkk\";a:4:{s:5:\"label\";s:3:\"DKK\";s:4:\"name\";s:12:\"Danish Krone\";s:4:\"sign\";s:3:\"kr.\";s:6:\"entity\";s:3:\"kr.\";}s:4:\"euro\";a:4:{s:5:\"label\";s:3:\"EUR\";s:4:\"name\";s:4:\"Euro\";s:4:\"sign\";s:3:\"€\";s:6:\"entity\";s:6:\"&euro;\";}s:3:\"gbp\";a:4:{s:5:\"label\";s:3:\"GBP\";s:4:\"name\";s:13:\"British Pound\";s:4:\"sign\";s:2:\"£\";s:6:\"entity\";s:7:\"&pound;\";}s:3:\"hkd\";a:4:{s:5:\"label\";s:3:\"HKD\";s:4:\"name\";s:16:\"Hong Kong Dollar\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:3:\"huf\";a:4:{s:5:\"label\";s:3:\"HUF\";s:4:\"name\";s:16:\"Hungarian Forint\";s:4:\"sign\";s:2:\"Ft\";s:6:\"entity\";s:2:\"Ft\";}s:3:\"idr\";a:4:{s:5:\"label\";s:3:\"IDR\";s:4:\"name\";s:17:\"Indonesian Rupiah\";s:4:\"sign\";s:2:\"Rp\";s:6:\"entity\";s:2:\"Rp\";}s:3:\"ils\";a:4:{s:5:\"label\";s:3:\"ILS\";s:4:\"name\";s:18:\"Israeli New Sheqel\";s:4:\"sign\";s:3:\"₪\";s:6:\"entity\";s:8:\"&#x20AA;\";}s:3:\"inr\";a:4:{s:5:\"label\";s:3:\"INR\";s:4:\"name\";s:12:\"Indian Rupee\";s:4:\"sign\";s:3:\"₹\";s:6:\"entity\";s:8:\"&#x20B9;\";}s:3:\"jpy\";a:4:{s:5:\"label\";s:3:\"JPY\";s:4:\"name\";s:12:\"Japanese Yen\";s:4:\"sign\";s:2:\"¥\";s:6:\"entity\";s:5:\"&yen;\";}s:3:\"krw\";a:4:{s:5:\"label\";s:3:\"KRW\";s:4:\"name\";s:10:\"Korean Won\";s:4:\"sign\";s:3:\"₩\";s:6:\"entity\";s:7:\"&#8361;\";}s:3:\"mxn\";a:4:{s:5:\"label\";s:3:\"MXN\";s:4:\"name\";s:12:\"Mexican Peso\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:3:\"myr\";a:4:{s:5:\"label\";s:3:\"MYR\";s:4:\"name\";s:17:\"Malaysian Ringgit\";s:4:\"sign\";s:2:\"RM\";s:6:\"entity\";s:2:\"RM\";}s:3:\"ngn\";a:4:{s:5:\"label\";s:3:\"NGN\";s:4:\"name\";s:14:\"Nigerian Naira\";s:4:\"sign\";s:3:\"₦\";s:6:\"entity\";s:7:\"&#8358;\";}s:3:\"nok\";a:4:{s:5:\"label\";s:3:\"NOK\";s:4:\"name\";s:15:\"Norwegian Krone\";s:4:\"sign\";s:2:\"kr\";s:6:\"entity\";s:2:\"kr\";}s:3:\"nzd\";a:4:{s:5:\"label\";s:3:\"NZD\";s:4:\"name\";s:18:\"New Zealand Dollar\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:3:\"php\";a:4:{s:5:\"label\";s:3:\"PHP\";s:4:\"name\";s:15:\"Philippine Peso\";s:4:\"sign\";s:3:\"₱\";s:6:\"entity\";s:8:\"&#x20B1;\";}s:3:\"pln\";a:4:{s:5:\"label\";s:3:\"PLN\";s:4:\"name\";s:13:\"Polish Złoty\";s:4:\"sign\";s:3:\"zł\";s:6:\"entity\";s:8:\"z&#x142;\";}s:3:\"rub\";a:4:{s:5:\"label\";s:3:\"RUB\";s:4:\"name\";s:13:\"Russian Ruble\";s:4:\"sign\";s:3:\"₽\";s:6:\"entity\";s:7:\"&#8381;\";}s:3:\"sek\";a:4:{s:5:\"label\";s:3:\"SEK\";s:4:\"name\";s:13:\"Swedish Krona\";s:4:\"sign\";s:2:\"kr\";s:6:\"entity\";s:2:\"kr\";}s:3:\"sgd\";a:4:{s:5:\"label\";s:3:\"SGD\";s:4:\"name\";s:16:\"Singapore Dollar\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:3:\"thb\";a:4:{s:5:\"label\";s:3:\"THB\";s:4:\"name\";s:9:\"Thai Baht\";s:4:\"sign\";s:3:\"฿\";s:6:\"entity\";s:8:\"&#x0E3F;\";}s:3:\"trl\";a:4:{s:5:\"label\";s:3:\"TRL\";s:4:\"name\";s:12:\"Turkish Lira\";s:4:\"sign\";s:3:\"₺\";s:6:\"entity\";s:7:\"&#8378;\";}s:3:\"twd\";a:4:{s:5:\"label\";s:3:\"TWD\";s:4:\"name\";s:17:\"Taiwan New Dollar\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:3:\"usd\";a:4:{s:5:\"label\";s:3:\"USD\";s:4:\"name\";s:9:\"US Dollar\";s:4:\"sign\";s:1:\"$\";s:6:\"entity\";s:5:\"&#36;\";}s:7:\"usdcent\";a:4:{s:5:\"label\";s:7:\"USDCENT\";s:4:\"name\";s:14:\"US Dollar Cent\";s:4:\"sign\";s:2:\"¢\";s:6:\"entity\";s:6:\"&cent;\";}s:3:\"vnd\";a:4:{s:5:\"label\";s:3:\"VND\";s:4:\"name\";s:15:\"Vietnamese Dong\";s:4:\"sign\";s:3:\"₫\";s:6:\"entity\";s:7:\"&#8363;\";}s:3:\"zar\";a:4:{s:5:\"label\";s:3:\"ZAR\";s:4:\"name\";s:18:\"South African Rand\";s:4:\"sign\";s:1:\"R\";s:6:\"entity\";s:1:\"R\";}}s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:4:\"file\";a:7:{s:5:\"group\";s:21:\"Relationships / Media\";s:4:\"type\";s:4:\"file\";s:5:\"label\";s:20:\"File / Image / Video\";s:10:\"deprecated\";b:0;s:7:\"prepare\";s:2:\"%s\";s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:6:\"avatar\";a:7:{s:5:\"group\";s:21:\"Relationships / Media\";s:4:\"type\";s:6:\"avatar\";s:5:\"label\";s:6:\"Avatar\";s:9:\"pod_types\";a:1:{i:0;s:4:\"user\";}s:10:\"deprecated\";b:0;s:7:\"prepare\";s:2:\"%s\";s:4:\"file\";N;}s:6:\"oembed\";a:7:{s:5:\"group\";s:21:\"Relationships / Media\";s:4:\"type\";s:6:\"oembed\";s:5:\"label\";s:6:\"oEmbed\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:4:\"pick\";a:11:{s:5:\"group\";s:21:\"Relationships / Media\";s:4:\"type\";s:4:\"pick\";s:5:\"label\";s:12:\"Relationship\";s:15:\"related_objects\";a:0:{}s:22:\"custom_related_objects\";a:0:{}s:12:\"related_data\";a:0:{}s:10:\"field_data\";a:0:{}s:10:\"deprecated\";b:0;s:7:\"prepare\";s:2:\"%s\";s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:7:\"boolean\";a:6:{s:4:\"type\";s:7:\"boolean\";s:5:\"label\";s:8:\"Yes / No\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:5:\"color\";a:6:{s:4:\"type\";s:5:\"color\";s:5:\"label\";s:12:\"Color Picker\";s:7:\"prepare\";s:2:\"%s\";s:10:\"deprecated\";b:0;s:9:\"pod_types\";b:1;s:4:\"file\";N;}s:4:\"slug\";a:6:{s:4:\"type\";s:4:\"slug\";s:5:\"label\";s:24:\"Permalink (url-friendly)\";s:7:\"prepare\";s:2:\"%s\";s:9:\"pod_types\";a:2:{i:0;s:3:\"pod\";i:1;s:5:\"table\";}s:10:\"deprecated\";b:0;s:4:\"file\";N;}}', 'yes'),
(1714, '_transient_pods_70149562a3d7bf47ee7da5360fc5cd3a', 'a:20:{i:0;s:4:\"text\";i:1;s:7:\"website\";i:2;s:5:\"phone\";i:3;s:5:\"email\";i:4;s:8:\"password\";i:5;s:9:\"paragraph\";i:6;s:7:\"wysiwyg\";i:7;s:4:\"code\";i:8;s:8:\"datetime\";i:9;s:4:\"date\";i:10;s:4:\"time\";i:11;s:6:\"number\";i:12;s:8:\"currency\";i:13;s:4:\"file\";i:14;s:6:\"avatar\";i:15;s:6:\"oembed\";i:16;s:4:\"pick\";i:17;s:7:\"boolean\";i:18;s:5:\"color\";i:19;s:4:\"slug\";}', 'yes'),
(1716, '_transient_pods_bb16f5e3f4f10d925e2a6cf856ce94b5', 'none', 'yes'),
(1717, '_transient_pods__pods_pfat_the_pods-2.7.16.2', 'a:0:{}', 'yes'),
(1718, '_transient_pods__pods_pfat_auto_pods-2.7.16.2', 'a:0:{}', 'yes'),
(1766, '_transient_pods_pods_count_get_all-2.7.16.2', '0', 'yes'),
(1771, '_transient_pods_pods_nofields_get_type_pod-2.7.16.2', 'none', 'yes'),
(1803, '_transient_pods_pods_names_get_all-2.7.16.2', 'none', 'yes'),
(1856, '_transient_shipping-transient-version', '1585089442', 'yes'),
(1920, 'gravityformsaddon_feed-base_version', '0.14', 'yes'),
(1921, 'gravityformsaddon_gravityformsmailchimp_version', '4.7', 'yes'),
(1928, 'woocommerce_version', '4.0.1', 'yes'),
(1931, 'woocommerce_admin_version', '1.0.3', 'yes'),
(1933, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1937, 'rewrite_rules', 'a:151:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:20:\"the-latest/events/?$\";s:25:\"index.php?post_type=event\";s:37:\"the-latest/events/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=event&paged=$matches[1]\";s:44:\"the-latest/news/([^/]+)/page/?([0-9]{1,})/?$\";s:59:\"index.php?post_type=post&name=$matches[1]&paged=$matches[2]\";s:41:\"the-latest/news/([^/]+)/wc-api(/(.*))?/?$\";s:60:\"index.php?post_type=post&name=$matches[1]&wc-api=$matches[3]\";s:26:\"the-latest/news/([^/]+)/?$\";s:41:\"index.php?post_type=post&name=$matches[1]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"cookielawinfo/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"cookielawinfo/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:47:\"cookielawinfo/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"cookielawinfo/([^/]+)/embed/?$\";s:46:\"index.php?cookielawinfo=$matches[1]&embed=true\";s:34:\"cookielawinfo/([^/]+)/trackback/?$\";s:40:\"index.php?cookielawinfo=$matches[1]&tb=1\";s:42:\"cookielawinfo/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?cookielawinfo=$matches[1]&paged=$matches[2]\";s:39:\"cookielawinfo/([^/]+)/wc-api(/(.*))?/?$\";s:54:\"index.php?cookielawinfo=$matches[1]&wc-api=$matches[3]\";s:45:\"cookielawinfo/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:56:\"cookielawinfo/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:38:\"cookielawinfo/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?cookielawinfo=$matches[1]&page=$matches[2]\";s:30:\"cookielawinfo/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"cookielawinfo/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:36:\"cookielawinfo/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:38:\"mc4wp-form/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"mc4wp-form/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:44:\"mc4wp-form/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"mc4wp-form/([^/]+)/embed/?$\";s:43:\"index.php?mc4wp-form=$matches[1]&embed=true\";s:31:\"mc4wp-form/([^/]+)/trackback/?$\";s:37:\"index.php?mc4wp-form=$matches[1]&tb=1\";s:39:\"mc4wp-form/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?mc4wp-form=$matches[1]&paged=$matches[2]\";s:36:\"mc4wp-form/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?mc4wp-form=$matches[1]&wc-api=$matches[3]\";s:42:\"mc4wp-form/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:53:\"mc4wp-form/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:35:\"mc4wp-form/([^/]+)(?:/([0-9]+))?/?$\";s:49:\"index.php?mc4wp-form=$matches[1]&page=$matches[2]\";s:27:\"mc4wp-form/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"mc4wp-form/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:33:\"mc4wp-form/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"slide_categories/([^/]+)/embed/?$\";s:49:\"index.php?slide_categories=$matches[1]&embed=true\";s:45:\"slide_categories/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?slide_categories=$matches[1]&paged=$matches[2]\";s:27:\"slide_categories/([^/]+)/?$\";s:38:\"index.php?slide_categories=$matches[1]\";s:33:\"event_categories/([^/]+)/embed/?$\";s:49:\"index.php?event_categories=$matches[1]&embed=true\";s:45:\"event_categories/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?event_categories=$matches[1]&paged=$matches[2]\";s:27:\"event_categories/([^/]+)/?$\";s:38:\"index.php?event_categories=$matches[1]\";s:45:\"the-latest/events/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:55:\"the-latest/events/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"the-latest/events/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"the-latest/events/([^/]+)/embed/?$\";s:53:\"index.php?post_type=event&name=$matches[1]&embed=true\";s:38:\"the-latest/events/([^/]+)/trackback/?$\";s:47:\"index.php?post_type=event&name=$matches[1]&tb=1\";s:46:\"the-latest/events/([^/]+)/page/?([0-9]{1,})/?$\";s:60:\"index.php?post_type=event&name=$matches[1]&paged=$matches[2]\";s:43:\"the-latest/events/([^/]+)/wc-api(/(.*))?/?$\";s:61:\"index.php?post_type=event&name=$matches[1]&wc-api=$matches[3]\";s:49:\"the-latest/events/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:60:\"the-latest/events/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"the-latest/events/([^/]+)(?:/([0-9]+))?/?$\";s:59:\"index.php?post_type=event&name=$matches[1]&page=$matches[2]\";s:34:\"the-latest/events/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"the-latest/events/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:40:\"the-latest/events/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(1940, 'woocommerce_setup_ab_wc_admin_onboarding', 'b', 'yes'),
(2286, 'ultimatemodal_settings', 'a:8:{s:6:\"active\";s:1:\"0\";s:4:\"time\";s:1:\"1\";s:9:\"only_home\";s:1:\"0\";s:10:\"background\";s:7:\"#000000\";s:5:\"width\";s:3:\"500\";s:6:\"height\";s:3:\"300\";s:5:\"delay\";s:3:\"300\";s:7:\"content\";s:0:\"\";}', 'yes'),
(2291, 'widget_searchwp_live_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2350, 'asl_options', 'a:163:{s:5:\"theme\";s:10:\"simple-red\";s:20:\"override_search_form\";s:1:\"0\";s:24:\"override_woo_search_form\";s:1:\"0\";s:13:\"keyword_logic\";s:3:\"and\";s:23:\"trigger_on_facet_change\";s:1:\"1\";s:17:\"redirect_click_to\";s:12:\"results_page\";s:17:\"redirect_enter_to\";s:12:\"results_page\";s:21:\"click_action_location\";s:4:\"same\";s:22:\"return_action_location\";s:4:\"same\";s:19:\"custom_redirect_url\";s:11:\"?s={phrase}\";s:16:\"results_per_page\";s:2:\"10\";s:13:\"triggerontype\";s:1:\"1\";s:11:\"customtypes\";s:28:\"_decode_WyJwb3N0IiwicGFnZSJd\";s:13:\"searchintitle\";s:1:\"1\";s:15:\"searchincontent\";s:1:\"1\";s:15:\"searchinexcerpt\";s:1:\"1\";s:20:\"search_in_permalinks\";s:1:\"0\";s:13:\"search_in_ids\";s:1:\"0\";s:13:\"search_all_cf\";s:1:\"0\";s:12:\"customfields\";s:0:\"\";s:11:\"post_status\";s:7:\"publish\";s:24:\"override_default_results\";s:1:\"0\";s:15:\"override_method\";s:3:\"get\";s:9:\"exactonly\";s:1:\"0\";s:20:\"exact_match_location\";s:8:\"anywhere\";s:13:\"searchinterms\";s:1:\"1\";s:9:\"charcount\";s:1:\"0\";s:10:\"maxresults\";s:2:\"10\";s:10:\"itemscount\";s:1:\"4\";s:16:\"resultitemheight\";s:4:\"70px\";s:15:\"orderby_primary\";s:14:\"relevance DESC\";s:17:\"orderby_secondary\";s:9:\"date DESC\";s:11:\"show_images\";s:1:\"1\";s:18:\"image_transparency\";i:1;s:14:\"image_bg_color\";s:7:\"#FFFFFF\";s:11:\"image_width\";s:2:\"70\";s:12:\"image_height\";s:2:\"70\";s:25:\"image_parser_image_number\";s:1:\"1\";s:19:\"image_crop_location\";s:1:\"c\";s:27:\"image_crop_location_selects\";a:9:{i:0;a:2:{s:6:\"option\";s:13:\"In the center\";s:5:\"value\";s:1:\"c\";}i:1;a:2:{s:6:\"option\";s:9:\"Align top\";s:5:\"value\";s:1:\"t\";}i:2;a:2:{s:6:\"option\";s:15:\"Align top right\";s:5:\"value\";s:2:\"tr\";}i:3;a:2:{s:6:\"option\";s:14:\"Align top left\";s:5:\"value\";s:2:\"tl\";}i:4;a:2:{s:6:\"option\";s:12:\"Align bottom\";s:5:\"value\";s:1:\"b\";}i:5;a:2:{s:6:\"option\";s:18:\"Align bottom right\";s:5:\"value\";s:2:\"br\";}i:6;a:2:{s:6:\"option\";s:17:\"Align bottom left\";s:5:\"value\";s:2:\"bl\";}i:7;a:2:{s:6:\"option\";s:10:\"Align left\";s:5:\"value\";s:1:\"l\";}i:8;a:2:{s:6:\"option\";s:11:\"Align right\";s:5:\"value\";s:1:\"r\";}}s:13:\"image_sources\";a:7:{i:0;a:2:{s:6:\"option\";s:14:\"Featured image\";s:5:\"value\";s:8:\"featured\";}i:1;a:2:{s:6:\"option\";s:12:\"Post Content\";s:5:\"value\";s:7:\"content\";}i:2;a:2:{s:6:\"option\";s:12:\"Post Excerpt\";s:5:\"value\";s:7:\"excerpt\";}i:3;a:2:{s:6:\"option\";s:12:\"Custom field\";s:5:\"value\";s:6:\"custom\";}i:4;a:2:{s:6:\"option\";s:15:\"Page Screenshot\";s:5:\"value\";s:10:\"screenshot\";}i:5;a:2:{s:6:\"option\";s:13:\"Default image\";s:5:\"value\";s:7:\"default\";}i:6;a:2:{s:6:\"option\";s:8:\"Disabled\";s:5:\"value\";s:8:\"disabled\";}}s:13:\"image_source1\";s:8:\"featured\";s:13:\"image_source2\";s:7:\"content\";s:13:\"image_source3\";s:7:\"excerpt\";s:13:\"image_source4\";s:6:\"custom\";s:13:\"image_source5\";s:7:\"default\";s:13:\"image_default\";s:95:\"http://localhost:8888/foundation-bedrock-theme/web/app/plugins/ajax-search-lite/img/default.jpg\";s:21:\"image_source_featured\";s:8:\"original\";s:18:\"image_custom_field\";s:0:\"\";s:12:\"use_timthumb\";i:1;s:29:\"show_frontend_search_settings\";s:1:\"0\";s:16:\"showexactmatches\";s:1:\"1\";s:17:\"showsearchinposts\";s:1:\"1\";s:17:\"showsearchinpages\";s:1:\"1\";s:17:\"showsearchintitle\";s:1:\"1\";s:19:\"showsearchincontent\";s:1:\"1\";s:15:\"showcustomtypes\";s:0:\"\";s:20:\"showsearchincomments\";i:1;s:19:\"showsearchinexcerpt\";i:1;s:19:\"showsearchinbpusers\";i:0;s:20:\"showsearchinbpgroups\";i:0;s:20:\"showsearchinbpforums\";i:0;s:16:\"exactmatchestext\";s:18:\"Exact matches only\";s:17:\"searchinpoststext\";s:15:\"Search in posts\";s:17:\"searchinpagestext\";s:15:\"Search in pages\";s:17:\"searchintitletext\";s:15:\"Search in title\";s:19:\"searchincontenttext\";s:17:\"Search in content\";s:20:\"searchincommentstext\";s:18:\"Search in comments\";s:19:\"searchinexcerpttext\";s:17:\"Search in excerpt\";s:19:\"searchinbpuserstext\";s:15:\"Search in users\";s:20:\"searchinbpgroupstext\";s:16:\"Search in groups\";s:20:\"searchinbpforumstext\";s:16:\"Search in forums\";s:22:\"showsearchincategories\";s:1:\"0\";s:17:\"showuncategorised\";s:1:\"0\";s:20:\"exsearchincategories\";s:0:\"\";s:26:\"exsearchincategoriesheight\";i:200;s:22:\"showsearchintaxonomies\";i:1;s:9:\"showterms\";s:0:\"\";s:23:\"showseparatefilterboxes\";i:1;s:24:\"exsearchintaxonomiestext\";s:9:\"Filter by\";s:24:\"exsearchincategoriestext\";s:20:\"Filter by Categories\";s:9:\"box_width\";s:4:\"100%\";s:16:\"box_width_tablet\";s:4:\"100%\";s:15:\"box_width_phone\";s:4:\"100%\";s:10:\"box_margin\";s:22:\"||0px||0px||0px||0px||\";s:8:\"box_font\";s:9:\"Open Sans\";s:11:\"override_bg\";s:1:\"0\";s:17:\"override_bg_color\";s:22:\"rgba(255, 255, 255, 1)\";s:13:\"override_icon\";s:1:\"0\";s:22:\"override_icon_bg_color\";s:22:\"rgba(255, 255, 255, 1)\";s:19:\"override_icon_color\";s:16:\"rgba(0, 0, 0, 1)\";s:15:\"override_border\";s:1:\"0\";s:21:\"override_border_style\";s:59:\"border:1px none rgb(0, 0, 0);border-radius:0px 0px 0px 0px;\";s:15:\"resultstype_def\";a:4:{i:0;a:2:{s:6:\"option\";s:16:\"Vertical Results\";s:5:\"value\";s:8:\"vertical\";}i:1;a:2:{s:6:\"option\";s:18:\"Horizontal Results\";s:5:\"value\";s:10:\"horizontal\";}i:2;a:2:{s:6:\"option\";s:16:\"Isotopic Results\";s:5:\"value\";s:8:\"isotopic\";}i:3;a:2:{s:6:\"option\";s:22:\"Polaroid style Results\";s:5:\"value\";s:8:\"polaroid\";}}s:11:\"resultstype\";s:8:\"vertical\";s:19:\"resultsposition_def\";a:2:{i:0;a:2:{s:6:\"option\";s:20:\"Hover - over content\";s:5:\"value\";s:5:\"hover\";}i:1;a:2:{s:6:\"option\";s:22:\"Block - pushes content\";s:5:\"value\";s:5:\"block\";}}s:15:\"resultsposition\";s:5:\"hover\";s:16:\"resultsmargintop\";s:4:\"12px\";s:16:\"v_res_max_height\";s:4:\"none\";s:18:\"v_res_column_count\";s:1:\"1\";s:22:\"v_res_column_min_width\";s:5:\"200px\";s:29:\"v_res_column_min_width_tablet\";s:5:\"200px\";s:28:\"v_res_column_min_width_phone\";s:5:\"200px\";s:17:\"defaultsearchtext\";s:13:\"Search here..\";s:15:\"showmoreresults\";s:1:\"0\";s:19:\"showmoreresultstext\";s:15:\"More results...\";s:19:\"results_click_blank\";s:1:\"0\";s:17:\"scroll_to_results\";s:1:\"0\";s:19:\"resultareaclickable\";s:1:\"1\";s:23:\"close_on_document_click\";s:1:\"1\";s:15:\"show_close_icon\";s:1:\"1\";s:10:\"showauthor\";s:1:\"0\";s:8:\"showdate\";s:1:\"0\";s:15:\"showdescription\";s:1:\"1\";s:17:\"descriptionlength\";s:3:\"100\";s:19:\"description_context\";s:1:\"0\";s:13:\"noresultstext\";s:11:\"No results!\";s:14:\"didyoumeantext\";s:13:\"Did you mean:\";s:12:\"kw_highlight\";s:1:\"0\";s:24:\"kw_highlight_whole_words\";s:1:\"1\";s:15:\"highlight_color\";s:20:\"rgba(217, 49, 43, 1)\";s:18:\"highlight_bg_color\";s:22:\"rgba(238, 238, 238, 1)\";s:10:\"custom_css\";s:0:\"\";s:12:\"autocomplete\";s:1:\"1\";s:14:\"kw_suggestions\";s:1:\"1\";s:9:\"kw_length\";s:2:\"60\";s:8:\"kw_count\";s:2:\"10\";s:14:\"kw_google_lang\";s:2:\"en\";s:13:\"kw_exceptions\";s:0:\"\";s:12:\"shortcode_op\";s:6:\"remove\";s:16:\"striptagsexclude\";s:0:\"\";s:12:\"runshortcode\";i:1;s:14:\"stripshortcode\";i:0;s:19:\"pageswithcategories\";i:0;s:10:\"titlefield\";s:1:\"0\";s:13:\"titlefield_cf\";s:0:\"\";s:16:\"descriptionfield\";s:1:\"0\";s:19:\"descriptionfield_cf\";s:0:\"\";s:22:\"woo_exclude_outofstock\";s:1:\"0\";s:18:\"exclude_woo_hidden\";s:1:\"1\";s:17:\"excludecategories\";s:0:\"\";s:12:\"excludeposts\";s:0:\"\";s:18:\"wpml_compatibility\";s:1:\"1\";s:22:\"polylang_compatibility\";s:1:\"1\";s:21:\"classname-customtypes\";s:23:\"wpdreamsCustomPostTypes\";s:8:\"wdcfs_10\";s:0:\"\";s:22:\"classname-customfields\";s:20:\"wpdreamsCustomFields\";s:10:\"asl_submit\";s:1:\"1\";s:25:\"classname-showcustomtypes\";s:31:\"wpdreamsCustomPostTypesEditable\";s:30:\"classname-exsearchincategories\";s:18:\"wpdreamsCategories\";s:10:\"submit_asl\";s:13:\"Save options!\";s:7:\"topleft\";s:3:\"0px\";s:11:\"bottomright\";s:3:\"0px\";s:8:\"topright\";s:3:\"0px\";s:10:\"bottomleft\";s:3:\"0px\";s:12:\"_colorpicker\";s:12:\"rgb(0, 0, 0)\";s:27:\"classname-excludecategories\";s:18:\"wpdreamsCategories\";s:10:\"sett_tabid\";s:1:\"3\";s:20:\"selected-customtypes\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:21:\"selected-customfields\";N;s:24:\"selected-showcustomtypes\";N;s:29:\"selected-exsearchincategories\";N;s:26:\"selected-excludecategories\";N;}', 'yes'),
(2351, 'asl_version', '4738', 'yes'),
(2352, 'widget_ajaxsearchlitewidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2355, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1602464503;s:7:\"checked\";a:18:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.8.8\";s:37:\"ajax-search-lite/ajax-search-lite.php\";s:5:\"4.8.1\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.5\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.4\";s:35:\"cookie-law-info/cookie-law-info.php\";s:5:\"1.8.7\";s:29:\"gravityforms/gravityforms.php\";s:6:\"2.4.17\";s:35:\"gravityformsmailchimp/mailchimp.php\";s:3:\"4.7\";s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";s:5:\"4.7.5\";s:15:\"mmenu/mmenu.php\";s:5:\"2.8.1\";s:13:\"pods/init.php\";s:8:\"2.7.16.2\";s:27:\"the-preloader/preloader.php\";s:5:\"1.0.9\";s:55:\"searchwp-live-ajax-search/searchwp-live-ajax-search.php\";s:5:\"1.4.6\";s:26:\"snazzy-maps/snazzymaps.php\";s:5:\"1.2.1\";s:33:\"ultimate-modal/ultimate-modal.php\";s:5:\"1.4.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"4.0.1\";s:71:\"woocommerce-gravityforms-product-addons/gravityforms-product-addons.php\";s:5:\"3.3.9\";s:61:\"woocommerce-variations-table/woocommerce-variations-table.php\";s:6:\"1.2.14\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"13.3\";}s:8:\"response\";a:10:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.1\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"ajax-search-lite/ajax-search-lite.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:30:\"w.org/plugins/ajax-search-lite\";s:4:\"slug\";s:16:\"ajax-search-lite\";s:6:\"plugin\";s:37:\"ajax-search-lite/ajax-search-lite.php\";s:11:\"new_version\";s:5:\"4.8.4\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/ajax-search-lite/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/ajax-search-lite.4.8.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/ajax-search-lite/assets/icon-256x256.png?rev=970684\";s:2:\"1x\";s:68:\"https://ps.w.org/ajax-search-lite/assets/icon-128x128.png?rev=970684\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/ajax-search-lite/assets/banner-772x250.png?rev=963999\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=2336666\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=2336666\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/duplicate-post/assets/banner-1544x500.png?rev=2336666\";s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=2336666\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:48:\"<p>Compatibility with WP 5.5 + various fixes</p>\";s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:35:\"cookie-law-info/cookie-law-info.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:29:\"w.org/plugins/cookie-law-info\";s:4:\"slug\";s:15:\"cookie-law-info\";s:6:\"plugin\";s:35:\"cookie-law-info/cookie-law-info.php\";s:11:\"new_version\";s:5:\"1.9.1\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/cookie-law-info/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/cookie-law-info.1.9.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/cookie-law-info/assets/icon-256x256.png?rev=1879809\";s:2:\"1x\";s:68:\"https://ps.w.org/cookie-law-info/assets/icon-256x256.png?rev=1879809\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/cookie-law-info/assets/banner-1544x500.jpg?rev=2289277\";s:2:\"1x\";s:70:\"https://ps.w.org/cookie-law-info/assets/banner-772x250.jpg?rev=2289277\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:61:\"<ul>\n<li>Fix - Conflict with the Divi page builder</li>\n</ul>\";s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:30:\"w.org/plugins/mailchimp-for-wp\";s:4:\"slug\";s:16:\"mailchimp-for-wp\";s:6:\"plugin\";s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";s:11:\"new_version\";s:5:\"4.8.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/mailchimp-for-wp/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/mailchimp-for-wp.4.8.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/mailchimp-for-wp/assets/icon-256x256.png?rev=1224577\";s:2:\"1x\";s:69:\"https://ps.w.org/mailchimp-for-wp/assets/icon-128x128.png?rev=1224577\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:71:\"https://ps.w.org/mailchimp-for-wp/assets/banner-772x250.png?rev=1184706\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:13:\"pods/init.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:18:\"w.org/plugins/pods\";s:4:\"slug\";s:4:\"pods\";s:6:\"plugin\";s:13:\"pods/init.php\";s:11:\"new_version\";s:6:\"2.7.22\";s:3:\"url\";s:35:\"https://wordpress.org/plugins/pods/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/pods.2.7.22.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:57:\"https://ps.w.org/pods/assets/icon-256x256.png?rev=1667333\";s:2:\"1x\";s:49:\"https://ps.w.org/pods/assets/icon.svg?rev=1667333\";s:3:\"svg\";s:49:\"https://ps.w.org/pods/assets/icon.svg?rev=1667333\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:59:\"https://ps.w.org/pods/assets/banner-772x250.png?rev=1667333\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:55:\"searchwp-live-ajax-search/searchwp-live-ajax-search.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:39:\"w.org/plugins/searchwp-live-ajax-search\";s:4:\"slug\";s:25:\"searchwp-live-ajax-search\";s:6:\"plugin\";s:55:\"searchwp-live-ajax-search/searchwp-live-ajax-search.php\";s:11:\"new_version\";s:7:\"1.4.6.1\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/searchwp-live-ajax-search/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/searchwp-live-ajax-search.1.4.6.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/searchwp-live-ajax-search/assets/icon-256x256.png?rev=2113131\";s:2:\"1x\";s:78:\"https://ps.w.org/searchwp-live-ajax-search/assets/icon-128x128.png?rev=2113131\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/searchwp-live-ajax-search/assets/banner-1544x500.png?rev=2113131\";s:2:\"1x\";s:80:\"https://ps.w.org/searchwp-live-ajax-search/assets/banner-772x250.png?rev=2113131\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.4\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:26:\"snazzy-maps/snazzymaps.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/snazzy-maps\";s:4:\"slug\";s:11:\"snazzy-maps\";s:6:\"plugin\";s:26:\"snazzy-maps/snazzymaps.php\";s:11:\"new_version\";s:5:\"1.3.0\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/snazzy-maps/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/snazzy-maps.1.3.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/snazzy-maps/assets/icon-256x256.png?rev=1130667\";s:2:\"1x\";s:64:\"https://ps.w.org/snazzy-maps/assets/icon-128x128.png?rev=1130680\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/snazzy-maps/assets/banner-1544x500.png?rev=1102757\";s:2:\"1x\";s:66:\"https://ps.w.org/snazzy-maps/assets/banner-772x250.png?rev=1102757\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"4.5.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.4.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2366418\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2366418\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2366418\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2366418\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:27:\"the-preloader/preloader.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/the-preloader\";s:4:\"slug\";s:13:\"the-preloader\";s:6:\"plugin\";s:27:\"the-preloader/preloader.php\";s:11:\"new_version\";s:5:\"1.0.9\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/the-preloader/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/the-preloader.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/the-preloader/assets/icon-128x128.png?rev=1192678\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:33:\"ultimate-modal/ultimate-modal.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/ultimate-modal\";s:4:\"slug\";s:14:\"ultimate-modal\";s:6:\"plugin\";s:33:\"ultimate-modal/ultimate-modal.php\";s:11:\"new_version\";s:5:\"1.4.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/ultimate-modal/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/ultimate-modal.1.4.2.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:58:\"https://s.w.org/plugins/geopattern-icon/ultimate-modal.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"15.0\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.15.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:6:\"5.6.20\";s:13:\"compatibility\";a:0:{}}}}', 'no'),
(2356, 'CookieLawInfo-0.9', 'a:78:{s:18:\"animate_speed_hide\";s:3:\"500\";s:18:\"animate_speed_show\";s:3:\"500\";s:10:\"background\";s:4:\"#FFF\";s:14:\"background_url\";s:0:\"\";s:6:\"border\";s:9:\"#b1a6a6c2\";s:9:\"border_on\";b:1;s:9:\"bar_style\";s:0:\"\";s:13:\"button_1_text\";s:6:\"ACCEPT\";s:12:\"button_1_url\";s:1:\"#\";s:15:\"button_1_action\";s:27:\"#cookie_action_close_header\";s:20:\"button_1_link_colour\";s:4:\"#fff\";s:16:\"button_1_new_win\";b:0;s:18:\"button_1_as_button\";b:1;s:22:\"button_1_button_colour\";s:4:\"#000\";s:20:\"button_1_button_size\";s:6:\"medium\";s:14:\"button_1_style\";s:0:\"\";s:13:\"button_2_text\";s:9:\"Read More\";s:12:\"button_2_url\";s:53:\"http://localhost:8888/foundation-bedrock-theme/web/wp\";s:15:\"button_2_action\";s:17:\"CONSTANT_OPEN_URL\";s:20:\"button_2_link_colour\";s:4:\"#444\";s:16:\"button_2_new_win\";b:1;s:18:\"button_2_as_button\";b:0;s:22:\"button_2_button_colour\";s:4:\"#333\";s:20:\"button_2_button_size\";s:6:\"medium\";s:17:\"button_2_url_type\";s:3:\"url\";s:13:\"button_2_page\";s:1:\"3\";s:16:\"button_2_hidebar\";b:0;s:14:\"button_2_style\";s:0:\"\";s:13:\"button_3_text\";s:6:\"Reject\";s:12:\"button_3_url\";s:1:\"#\";s:15:\"button_3_action\";s:34:\"#cookie_action_close_header_reject\";s:20:\"button_3_link_colour\";s:4:\"#fff\";s:16:\"button_3_new_win\";b:0;s:18:\"button_3_as_button\";b:1;s:22:\"button_3_button_colour\";s:4:\"#000\";s:20:\"button_3_button_size\";s:6:\"medium\";s:14:\"button_3_style\";s:0:\"\";s:13:\"button_4_text\";s:15:\"Cookie settings\";s:12:\"button_4_url\";s:1:\"#\";s:15:\"button_4_action\";s:23:\"#cookie_action_settings\";s:20:\"button_4_link_colour\";s:7:\"#62a329\";s:16:\"button_4_new_win\";b:0;s:18:\"button_4_as_button\";b:0;s:22:\"button_4_button_colour\";s:4:\"#000\";s:20:\"button_4_button_size\";s:6:\"medium\";s:14:\"button_4_style\";s:0:\"\";s:11:\"font_family\";s:7:\"inherit\";s:10:\"header_fix\";b:0;s:5:\"is_on\";b:1;s:8:\"is_eu_on\";b:0;s:10:\"logging_on\";b:0;s:19:\"notify_animate_hide\";b:1;s:19:\"notify_animate_show\";b:0;s:13:\"notify_div_id\";s:20:\"#cookie-law-info-bar\";s:26:\"notify_position_horizontal\";s:5:\"right\";s:24:\"notify_position_vertical\";s:6:\"bottom\";s:14:\"notify_message\";s:199:\"This website uses cookies to improve your experience. We\\\'ll assume you\\\'re ok with this, but you can opt-out if you wish. [cookie_settings margin=\\\"5px 20px 5px 20px\\\"][cookie_button margin=\\\"5px\\\"]\";s:12:\"scroll_close\";b:0;s:19:\"scroll_close_reload\";b:0;s:19:\"accept_close_reload\";b:0;s:19:\"reject_close_reload\";b:0;s:20:\"showagain_background\";s:4:\"#fff\";s:16:\"showagain_border\";s:4:\"#000\";s:14:\"showagain_text\";s:24:\"Privacy & Cookies Policy\";s:16:\"showagain_div_id\";s:22:\"#cookie-law-info-again\";s:13:\"showagain_tab\";b:1;s:20:\"showagain_x_position\";s:5:\"100px\";s:4:\"text\";s:4:\"#000\";s:17:\"use_colour_picker\";b:1;s:12:\"show_once_yn\";b:0;s:9:\"show_once\";s:5:\"10000\";s:9:\"is_GMT_on\";b:1;s:8:\"as_popup\";b:0;s:13:\"popup_overlay\";b:1;s:16:\"bar_heading_text\";s:0:\"\";s:13:\"cookie_bar_as\";s:6:\"banner\";s:24:\"popup_showagain_position\";s:12:\"bottom-right\";s:15:\"widget_position\";s:4:\"left\";}', 'yes'),
(2357, 'cookielawinfo_privacy_overview_content_settings', 'a:2:{s:24:\"privacy_overview_content\";s:571:\"This website uses cookies to improve your experience while you navigate through the website. Out of these cookies, the cookies that are categorized as necessary are stored on your browser as they are essential for the working of basic functionalities of the website. We also use third-party cookies that help us analyze and understand how you use this website. These cookies will be stored in your browser only with your consent. You also have the option to opt-out of these cookies. But opting out of some of these cookies may have an effect on your browsing experience.\";s:22:\"privacy_overview_title\";s:16:\"Privacy Overview\";}', 'yes'),
(2358, 'cookielawinfo_necessary_settings', 'a:1:{s:21:\"necessary_description\";s:242:\"Necessary cookies are absolutely essential for the website to function properly. This category only includes cookies that ensures basic functionalities and security features of the website. These cookies do not store any personal information.\";}', 'yes'),
(2359, 'cookielawinfo_thirdparty_settings', 'a:5:{s:19:\"thirdparty_on_field\";b:1;s:25:\"third_party_default_state\";b:1;s:22:\"thirdparty_description\";s:302:\"Any cookies that may not be particularly necessary for the website to function and is used specifically to collect user personal data via analytics, ads, other embedded contents are termed as non-necessary cookies. It is mandatory to procure user consent prior to running these cookies on your website.\";s:23:\"thirdparty_head_section\";s:0:\"\";s:23:\"thirdparty_body_section\";s:0:\"\";}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2363, 'asl_debug_data', 'a:2:{s:11:\"asl_options\";a:163:{s:5:\"theme\";s:10:\"simple-red\";s:20:\"override_search_form\";s:1:\"0\";s:24:\"override_woo_search_form\";s:1:\"0\";s:13:\"keyword_logic\";s:3:\"and\";s:23:\"trigger_on_facet_change\";s:1:\"1\";s:17:\"redirect_click_to\";s:12:\"results_page\";s:17:\"redirect_enter_to\";s:12:\"results_page\";s:21:\"click_action_location\";s:4:\"same\";s:22:\"return_action_location\";s:4:\"same\";s:19:\"custom_redirect_url\";s:11:\"?s={phrase}\";s:16:\"results_per_page\";s:2:\"10\";s:13:\"triggerontype\";s:1:\"1\";s:11:\"customtypes\";s:28:\"_decode_WyJwb3N0IiwicGFnZSJd\";s:13:\"searchintitle\";s:1:\"1\";s:15:\"searchincontent\";s:1:\"1\";s:15:\"searchinexcerpt\";s:1:\"1\";s:20:\"search_in_permalinks\";s:1:\"0\";s:13:\"search_in_ids\";s:1:\"0\";s:13:\"search_all_cf\";s:1:\"0\";s:12:\"customfields\";s:0:\"\";s:11:\"post_status\";s:7:\"publish\";s:24:\"override_default_results\";s:1:\"0\";s:15:\"override_method\";s:3:\"get\";s:9:\"exactonly\";s:1:\"0\";s:20:\"exact_match_location\";s:8:\"anywhere\";s:13:\"searchinterms\";s:1:\"1\";s:9:\"charcount\";s:1:\"0\";s:10:\"maxresults\";s:2:\"10\";s:10:\"itemscount\";s:1:\"4\";s:16:\"resultitemheight\";s:4:\"70px\";s:15:\"orderby_primary\";s:14:\"relevance DESC\";s:17:\"orderby_secondary\";s:9:\"date DESC\";s:11:\"show_images\";s:1:\"1\";s:18:\"image_transparency\";i:1;s:14:\"image_bg_color\";s:7:\"#FFFFFF\";s:11:\"image_width\";s:2:\"70\";s:12:\"image_height\";s:2:\"70\";s:25:\"image_parser_image_number\";s:1:\"1\";s:19:\"image_crop_location\";s:1:\"c\";s:27:\"image_crop_location_selects\";a:9:{i:0;a:2:{s:6:\"option\";s:13:\"In the center\";s:5:\"value\";s:1:\"c\";}i:1;a:2:{s:6:\"option\";s:9:\"Align top\";s:5:\"value\";s:1:\"t\";}i:2;a:2:{s:6:\"option\";s:15:\"Align top right\";s:5:\"value\";s:2:\"tr\";}i:3;a:2:{s:6:\"option\";s:14:\"Align top left\";s:5:\"value\";s:2:\"tl\";}i:4;a:2:{s:6:\"option\";s:12:\"Align bottom\";s:5:\"value\";s:1:\"b\";}i:5;a:2:{s:6:\"option\";s:18:\"Align bottom right\";s:5:\"value\";s:2:\"br\";}i:6;a:2:{s:6:\"option\";s:17:\"Align bottom left\";s:5:\"value\";s:2:\"bl\";}i:7;a:2:{s:6:\"option\";s:10:\"Align left\";s:5:\"value\";s:1:\"l\";}i:8;a:2:{s:6:\"option\";s:11:\"Align right\";s:5:\"value\";s:1:\"r\";}}s:13:\"image_sources\";a:7:{i:0;a:2:{s:6:\"option\";s:14:\"Featured image\";s:5:\"value\";s:8:\"featured\";}i:1;a:2:{s:6:\"option\";s:12:\"Post Content\";s:5:\"value\";s:7:\"content\";}i:2;a:2:{s:6:\"option\";s:12:\"Post Excerpt\";s:5:\"value\";s:7:\"excerpt\";}i:3;a:2:{s:6:\"option\";s:12:\"Custom field\";s:5:\"value\";s:6:\"custom\";}i:4;a:2:{s:6:\"option\";s:15:\"Page Screenshot\";s:5:\"value\";s:10:\"screenshot\";}i:5;a:2:{s:6:\"option\";s:13:\"Default image\";s:5:\"value\";s:7:\"default\";}i:6;a:2:{s:6:\"option\";s:8:\"Disabled\";s:5:\"value\";s:8:\"disabled\";}}s:13:\"image_source1\";s:8:\"featured\";s:13:\"image_source2\";s:7:\"content\";s:13:\"image_source3\";s:7:\"excerpt\";s:13:\"image_source4\";s:6:\"custom\";s:13:\"image_source5\";s:7:\"default\";s:13:\"image_default\";s:95:\"http://localhost:8888/foundation-bedrock-theme/web/app/plugins/ajax-search-lite/img/default.jpg\";s:21:\"image_source_featured\";s:8:\"original\";s:18:\"image_custom_field\";s:0:\"\";s:12:\"use_timthumb\";i:1;s:29:\"show_frontend_search_settings\";s:1:\"0\";s:16:\"showexactmatches\";s:1:\"1\";s:17:\"showsearchinposts\";s:1:\"1\";s:17:\"showsearchinpages\";s:1:\"1\";s:17:\"showsearchintitle\";s:1:\"1\";s:19:\"showsearchincontent\";s:1:\"1\";s:15:\"showcustomtypes\";s:0:\"\";s:20:\"showsearchincomments\";i:1;s:19:\"showsearchinexcerpt\";i:1;s:19:\"showsearchinbpusers\";i:0;s:20:\"showsearchinbpgroups\";i:0;s:20:\"showsearchinbpforums\";i:0;s:16:\"exactmatchestext\";s:18:\"Exact matches only\";s:17:\"searchinpoststext\";s:15:\"Search in posts\";s:17:\"searchinpagestext\";s:15:\"Search in pages\";s:17:\"searchintitletext\";s:15:\"Search in title\";s:19:\"searchincontenttext\";s:17:\"Search in content\";s:20:\"searchincommentstext\";s:18:\"Search in comments\";s:19:\"searchinexcerpttext\";s:17:\"Search in excerpt\";s:19:\"searchinbpuserstext\";s:15:\"Search in users\";s:20:\"searchinbpgroupstext\";s:16:\"Search in groups\";s:20:\"searchinbpforumstext\";s:16:\"Search in forums\";s:22:\"showsearchincategories\";s:1:\"0\";s:17:\"showuncategorised\";s:1:\"0\";s:20:\"exsearchincategories\";s:0:\"\";s:26:\"exsearchincategoriesheight\";i:200;s:22:\"showsearchintaxonomies\";i:1;s:9:\"showterms\";s:0:\"\";s:23:\"showseparatefilterboxes\";i:1;s:24:\"exsearchintaxonomiestext\";s:9:\"Filter by\";s:24:\"exsearchincategoriestext\";s:20:\"Filter by Categories\";s:9:\"box_width\";s:4:\"100%\";s:16:\"box_width_tablet\";s:4:\"100%\";s:15:\"box_width_phone\";s:4:\"100%\";s:10:\"box_margin\";s:22:\"||0px||0px||0px||0px||\";s:8:\"box_font\";s:9:\"Open Sans\";s:11:\"override_bg\";s:1:\"0\";s:17:\"override_bg_color\";s:22:\"rgba(255, 255, 255, 1)\";s:13:\"override_icon\";s:1:\"0\";s:22:\"override_icon_bg_color\";s:22:\"rgba(255, 255, 255, 1)\";s:19:\"override_icon_color\";s:16:\"rgba(0, 0, 0, 1)\";s:15:\"override_border\";s:1:\"0\";s:21:\"override_border_style\";s:59:\"border:1px none rgb(0, 0, 0);border-radius:0px 0px 0px 0px;\";s:15:\"resultstype_def\";a:4:{i:0;a:2:{s:6:\"option\";s:16:\"Vertical Results\";s:5:\"value\";s:8:\"vertical\";}i:1;a:2:{s:6:\"option\";s:18:\"Horizontal Results\";s:5:\"value\";s:10:\"horizontal\";}i:2;a:2:{s:6:\"option\";s:16:\"Isotopic Results\";s:5:\"value\";s:8:\"isotopic\";}i:3;a:2:{s:6:\"option\";s:22:\"Polaroid style Results\";s:5:\"value\";s:8:\"polaroid\";}}s:11:\"resultstype\";s:8:\"vertical\";s:19:\"resultsposition_def\";a:2:{i:0;a:2:{s:6:\"option\";s:20:\"Hover - over content\";s:5:\"value\";s:5:\"hover\";}i:1;a:2:{s:6:\"option\";s:22:\"Block - pushes content\";s:5:\"value\";s:5:\"block\";}}s:15:\"resultsposition\";s:5:\"hover\";s:16:\"resultsmargintop\";s:4:\"12px\";s:16:\"v_res_max_height\";s:4:\"none\";s:18:\"v_res_column_count\";s:1:\"1\";s:22:\"v_res_column_min_width\";s:5:\"200px\";s:29:\"v_res_column_min_width_tablet\";s:5:\"200px\";s:28:\"v_res_column_min_width_phone\";s:5:\"200px\";s:17:\"defaultsearchtext\";s:13:\"Search here..\";s:15:\"showmoreresults\";s:1:\"0\";s:19:\"showmoreresultstext\";s:15:\"More results...\";s:19:\"results_click_blank\";s:1:\"0\";s:17:\"scroll_to_results\";s:1:\"0\";s:19:\"resultareaclickable\";s:1:\"1\";s:23:\"close_on_document_click\";s:1:\"1\";s:15:\"show_close_icon\";s:1:\"1\";s:10:\"showauthor\";s:1:\"0\";s:8:\"showdate\";s:1:\"0\";s:15:\"showdescription\";s:1:\"1\";s:17:\"descriptionlength\";s:3:\"100\";s:19:\"description_context\";s:1:\"0\";s:13:\"noresultstext\";s:11:\"No results!\";s:14:\"didyoumeantext\";s:13:\"Did you mean:\";s:12:\"kw_highlight\";s:1:\"0\";s:24:\"kw_highlight_whole_words\";s:1:\"1\";s:15:\"highlight_color\";s:20:\"rgba(217, 49, 43, 1)\";s:18:\"highlight_bg_color\";s:22:\"rgba(238, 238, 238, 1)\";s:10:\"custom_css\";s:0:\"\";s:12:\"autocomplete\";s:1:\"1\";s:14:\"kw_suggestions\";s:1:\"1\";s:9:\"kw_length\";s:2:\"60\";s:8:\"kw_count\";s:2:\"10\";s:14:\"kw_google_lang\";s:2:\"en\";s:13:\"kw_exceptions\";s:0:\"\";s:12:\"shortcode_op\";s:6:\"remove\";s:16:\"striptagsexclude\";s:0:\"\";s:12:\"runshortcode\";i:1;s:14:\"stripshortcode\";i:0;s:19:\"pageswithcategories\";i:0;s:10:\"titlefield\";s:1:\"0\";s:13:\"titlefield_cf\";s:0:\"\";s:16:\"descriptionfield\";s:1:\"0\";s:19:\"descriptionfield_cf\";s:0:\"\";s:22:\"woo_exclude_outofstock\";s:1:\"0\";s:18:\"exclude_woo_hidden\";s:1:\"1\";s:17:\"excludecategories\";s:0:\"\";s:12:\"excludeposts\";s:0:\"\";s:18:\"wpml_compatibility\";s:1:\"1\";s:22:\"polylang_compatibility\";s:1:\"1\";s:21:\"classname-customtypes\";s:23:\"wpdreamsCustomPostTypes\";s:8:\"wdcfs_10\";s:0:\"\";s:22:\"classname-customfields\";s:20:\"wpdreamsCustomFields\";s:10:\"asl_submit\";s:1:\"1\";s:25:\"classname-showcustomtypes\";s:31:\"wpdreamsCustomPostTypesEditable\";s:30:\"classname-exsearchincategories\";s:18:\"wpdreamsCategories\";s:10:\"submit_asl\";s:13:\"Save options!\";s:7:\"topleft\";s:3:\"0px\";s:11:\"bottomright\";s:3:\"0px\";s:8:\"topright\";s:3:\"0px\";s:10:\"bottomleft\";s:3:\"0px\";s:12:\"_colorpicker\";s:12:\"rgb(0, 0, 0)\";s:27:\"classname-excludecategories\";s:18:\"wpdreamsCategories\";s:10:\"sett_tabid\";s:1:\"3\";s:20:\"selected-customtypes\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:21:\"selected-customfields\";N;s:24:\"selected-showcustomtypes\";N;s:29:\"selected-exsearchincategories\";N;s:26:\"selected-excludecategories\";N;}s:7:\"queries\";a:1:{i:0;a:4:{s:6:\"phrase\";s:1:\"s\";s:7:\"options\";a:16:{s:15:\"qtranslate_lang\";s:1:\"0\";s:11:\"set_intitle\";b:1;s:13:\"set_incontent\";b:1;s:13:\"set_inexcerpt\";b:1;s:11:\"set_inposts\";b:1;s:11:\"set_inpages\";b:1;s:13:\"set_exactonly\";b:0;s:14:\"set_incomments\";b:0;s:13:\"searchinterms\";b:1;s:13:\"set_inbpusers\";b:0;s:14:\"set_inbpgroups\";b:0;s:14:\"set_inbpforums\";b:0;s:10:\"maxresults\";s:2:\"10\";s:8:\"do_group\";b:1;s:11:\"categoryset\";a:0:{}s:7:\"termset\";a:0:{}}s:5:\"query\";s:3611:\"\r\n    	SELECT\r\n			\r\n			wp_posts.post_title as title,\r\n			wp_posts.ID as id,\r\n			wp_posts.post_date as date,\r\n			wp_posts.post_content as content,\r\n			wp_posts.post_excerpt as excerpt,\r\n			\'pagepost\' as content_type,\r\n			(SELECT\r\n				wp_users.display_name as author\r\n				FROM wp_users\r\n				WHERE wp_users.ID = wp_posts.post_author\r\n			) as author,\r\n			\'\' as ttid,\r\n			wp_posts.post_type as post_type,\r\n			((case when\r\n                    (wp_posts.post_title LIKE \'s%\')\r\n                     then 20 else 0 end) + (case when\r\n                    (wp_posts.post_title LIKE \'%s%\')\r\n                     then 10 else 0 end) + (case when\r\n                      (wp_posts.post_title LIKE \'%s%\')\r\n                       then 10 else 0 end) + (case when\r\n                            (wp_posts.post_content LIKE \'%s%\')\r\n                             then 10 else 0 end) + (case when\r\n                    (wp_posts.post_content LIKE \'%s%\')\r\n                     then 10 else 0 end) + (case when\r\n                            (wp_posts.post_excerpt LIKE \'%s%\')\r\n                             then 10 else 0 end) + (case when\r\n                    (wp_posts.post_excerpt LIKE \'%s%\')\r\n                     then 10 else 0 end) + (case when\r\n                    (wp_terms.name = \'s\')\r\n                     then 10 else 0 end)) as relevance\r\n    	FROM wp_posts\r\n			\r\n			\r\n                LEFT JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id\r\n                LEFT JOIN wp_term_taxonomy ON wp_term_taxonomy.term_taxonomy_id = wp_term_relationships.term_taxonomy_id\r\n                LEFT JOIN wp_terms ON wp_term_taxonomy.term_id = wp_terms.term_id\r\n			\r\n    	WHERE\r\n                ( wp_posts.post_type IN (\'post\',\'page\') )\r\n                 AND (\r\n                        \r\n                        NOT EXISTS (\r\n                            SELECT *\r\n                            FROM wp_term_relationships as xt\r\n                            INNER JOIN wp_term_taxonomy as tt ON ( xt.term_taxonomy_id = tt.term_taxonomy_id AND tt.taxonomy = \'product_visibility\')\r\n                            WHERE\r\n                                xt.object_id = wp_posts.ID\r\n                        ) OR \r\n        \r\n                        wp_posts.ID IN (\r\n                        SELECT DISTINCT(tr.object_id)\r\n                            FROM wp_term_relationships AS tr\r\n                            LEFT JOIN wp_term_taxonomy as tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id AND tt.taxonomy = \'product_visibility\')\r\n                                WHERE\r\n                                    tt.term_id NOT IN (7)\r\n                                    AND tr.object_id NOT IN (\r\n                                        SELECT DISTINCT(trs.object_id)\r\n                                        FROM wp_term_relationships AS trs\r\n                                        LEFT JOIN wp_term_taxonomy as tts ON (trs.term_taxonomy_id = tts.term_taxonomy_id AND tts.taxonomy = \'product_visibility\')\r\n                                        WHERE tts.term_id IN (7)\r\n                                    )\r\n                                )\r\n                            )\r\n                \r\n            AND ( wp_posts.post_status IN (\'publish\') )\r\n            AND (1)\r\n            AND ( (( wp_posts.post_title LIKE \'%s%\' ) OR ( wp_posts.post_content LIKE \'%s%\' ) OR ( wp_posts.post_excerpt LIKE \'%s%\' ) OR ( wp_terms.name LIKE \'%s%\' )) )\r\n            \r\n            AND ( (1) )\r\n            \r\n            \r\n        GROUP BY\r\n          	 wp_posts.ID\r\n        ORDER BY\r\n        	 relevance DESC, date DESC, id DESC\r\n        LIMIT 10\";s:7:\"results\";i:10;}}}', 'yes'),
(2549, '_transient_timeout_wpseo_link_table_inaccessible', '1624989815', 'no'),
(2550, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(2551, '_transient_timeout_wpseo_meta_table_inaccessible', '1624989815', 'no'),
(2552, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(2635, 'duplicate_post_title_prefix', '', 'yes'),
(2636, 'duplicate_post_title_suffix', '', 'yes'),
(2637, 'duplicate_post_increase_menu_order_by', '', 'yes'),
(2638, 'duplicate_post_roles', 'a:2:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";}', 'yes'),
(2674, '_transient_timeout_external_ip_address_::1', '1602526805', 'no'),
(2675, '_transient_external_ip_address_::1', '103.82.80.162', 'no'),
(2707, '_site_transient_timeout_browser_5a5b8cd389519fb04c28d6c5e9f4d727', '1602578135', 'no'),
(2708, '_site_transient_browser_5a5b8cd389519fb04c28d6c5e9f4d727', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"85.0.4183.121\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(2709, '_site_transient_timeout_php_check_baf661bc1e7958f3dc9ec8c07cccafb4', '1602578136', 'no'),
(2710, '_site_transient_php_check_baf661bc1e7958f3dc9ec8c07cccafb4', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(2711, '_transient_timeout_wc_low_stock_count', '1604565337', 'no'),
(2712, '_transient_wc_low_stock_count', '0', 'no'),
(2713, '_transient_timeout_wc_outofstock_count', '1604565337', 'no'),
(2714, '_transient_wc_outofstock_count', '0', 'no'),
(2782, '_transient_timeout_wc_shipping_method_count_legacy', '1604568185', 'no'),
(2783, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1585089442\";s:5:\"value\";i:0;}', 'no'),
(3299, '_transient_timeout_external_ip_address_142.176.47.76', '1602703920', 'no'),
(3300, '_transient_external_ip_address_142.176.47.76', '15.222.219.63', 'no'),
(3302, 'theme_mods_jac', 'a:2:{s:18:\"nav_menu_locations\";a:3:{s:20:\"secondary_navigation\";i:21;s:17:\"bottom_navigation\";i:19;s:18:\"primary_navigation\";i:17;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(3303, 'woocommerce_maybe_regenerate_images_hash', '991b1ca641921cf0f5baf7a2fe85861b', 'yes'),
(3305, '_transient_timeout_external_ip_address_15.222.219.63', '1602703920', 'no'),
(3306, '_transient_external_ip_address_15.222.219.63', '15.222.219.63', 'no'),
(3340, '_transient_timeout_external_ip_address_159.89.112.83', '1602712499', 'no'),
(3341, '_transient_external_ip_address_159.89.112.83', '15.222.219.63', 'no'),
(3349, '_transient_timeout_external_ip_address_120.79.139.196', '1602735534', 'no'),
(3350, '_transient_external_ip_address_120.79.139.196', '15.222.219.63', 'no'),
(3355, '_transient_timeout_external_ip_address_148.72.210.140', '1602735988', 'no'),
(3356, '_transient_external_ip_address_148.72.210.140', '15.222.219.63', 'no'),
(3360, '_transient_timeout_external_ip_address_103.209.9.2', '1602737372', 'no'),
(3361, '_transient_external_ip_address_103.209.9.2', '15.222.219.63', 'no'),
(3363, '_transient_timeout_external_ip_address_92.222.92.237', '1602737962', 'no'),
(3364, '_transient_external_ip_address_92.222.92.237', '15.222.219.63', 'no'),
(3368, '_transient_timeout_external_ip_address_161.35.232.146', '1602739326', 'no'),
(3369, '_transient_external_ip_address_161.35.232.146', '15.222.219.63', 'no'),
(3371, '_transient_timeout_external_ip_address_137.74.167.133', '1602739934', 'no'),
(3372, '_transient_external_ip_address_137.74.167.133', '15.222.219.63', 'no'),
(3376, '_transient_timeout_external_ip_address_52.143.52.199', '1602741978', 'no'),
(3377, '_transient_external_ip_address_52.143.52.199', '15.222.219.63', 'no'),
(3385, '_transient_timeout_external_ip_address_159.253.46.18', '1602743409', 'no'),
(3386, '_transient_external_ip_address_159.253.46.18', '15.222.219.63', 'no'),
(3396, '_transient_timeout_external_ip_address_198.211.117.96', '1602744040', 'no'),
(3397, '_transient_external_ip_address_198.211.117.96', '15.222.219.63', 'no'),
(3399, '_transient_timeout_external_ip_address_138.68.233.112', '1602744864', 'no'),
(3400, '_transient_external_ip_address_138.68.233.112', '15.222.219.63', 'no'),
(3402, '_transient_timeout_external_ip_address_193.70.89.118', '1602745498', 'no'),
(3403, '_transient_external_ip_address_193.70.89.118', '15.222.219.63', 'no'),
(3408, '_transient_timeout_external_ip_address_46.101.139.73', '1602747042', 'no'),
(3409, '_transient_external_ip_address_46.101.139.73', '15.222.219.63', 'no'),
(3415, '_transient_timeout_external_ip_address_35.246.214.111', '1602747685', 'no'),
(3416, '_transient_external_ip_address_35.246.214.111', '15.222.219.63', 'no'),
(3420, '_transient_timeout_external_ip_address_213.149.103.132', '1602749171', 'no'),
(3421, '_transient_external_ip_address_213.149.103.132', '15.222.219.63', 'no'),
(3425, '_transient_timeout_external_ip_address_142.93.73.89', '1602751400', 'no'),
(3426, '_transient_external_ip_address_142.93.73.89', '15.222.219.63', 'no'),
(3428, '_site_transient_timeout_browser_d62a1a3bb908ffa3361920bc8dc2be5a', '1602751401', 'no'),
(3429, '_site_transient_browser_d62a1a3bb908ffa3361920bc8dc2be5a', 'a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"62.0\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:32:\"https://www.mozilla.org/firefox/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(3430, '_site_transient_timeout_php_check_9c1053d8f82668af273e4a26e955e566', '1602751401', 'no'),
(3431, '_site_transient_php_check_9c1053d8f82668af273e4a26e955e566', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(3432, '_transient_timeout_external_ip_address_185.27.36.140', '1602752049', 'no'),
(3433, '_transient_external_ip_address_185.27.36.140', '15.222.219.63', 'no'),
(3437, '_transient_timeout_external_ip_address_51.77.140.110', '1602753654', 'no'),
(3438, '_transient_external_ip_address_51.77.140.110', '15.222.219.63', 'no'),
(3440, '_transient_timeout_external_ip_address_93.123.36.231', '1602754353', 'no'),
(3441, '_transient_external_ip_address_93.123.36.231', '15.222.219.63', 'no'),
(3445, '_transient_timeout_external_ip_address_13.94.245.44', '1602755720', 'no'),
(3446, '_transient_external_ip_address_13.94.245.44', '15.222.219.63', 'no'),
(3448, '_transient_timeout_external_ip_address_178.62.50.212', '1602756485', 'no'),
(3449, '_transient_external_ip_address_178.62.50.212', '15.222.219.63', 'no'),
(3453, '_transient_timeout_external_ip_address_192.99.11.48', '1602758119', 'no'),
(3454, '_transient_external_ip_address_192.99.11.48', '15.222.219.63', 'no'),
(3456, '_transient_timeout_external_ip_address_138.68.45.164', '1602758888', 'no'),
(3457, '_transient_external_ip_address_138.68.45.164', '15.222.219.63', 'no'),
(3466, 'secure_auth_key', '9;)RhpNK>kZxE*bOOLB xt7ex)y};>c[Up!_qtT5XdR`doVWXX<VW9x2HDw]f*/,', 'no'),
(3467, 'secure_auth_salt', 'D0H%rD>Kt7*FtjWYjA!K_(u.5;55lu]c){JA25pLjANGwN^ALT/irYY=WrTN[n}U', 'no'),
(3468, '_site_transient_timeout_browser_5930764e8cc4abdd272dd999d487641c', '1602762631', 'no'),
(3469, '_site_transient_browser_5930764e8cc4abdd272dd999d487641c', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"85.0.4183.121\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(3492, '_transient_timeout_external_ip_address_139.59.141.196', '1602763045', 'no'),
(3493, '_transient_external_ip_address_139.59.141.196', '15.222.219.63', 'no'),
(3495, '_transient_timeout_external_ip_address_127.0.0.1', '1602763372', 'no'),
(3496, '_transient_external_ip_address_127.0.0.1', '142.176.47.76', 'no'),
(3500, '_site_transient_timeout_browser_195f77c180c23e3f5a8a8f38e4290186', '1602763544', 'no'),
(3501, '_site_transient_browser_195f77c180c23e3f5a8a8f38e4290186', 'a:10:{s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:6:\"13.0.3\";s:8:\"platform\";s:6:\"iPhone\";s:10:\"update_url\";s:0:\"\";s:7:\"img_src\";s:0:\"\";s:11:\"img_src_ssl\";s:0:\"\";s:15:\"current_version\";s:0:\"\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:1;}', 'no'),
(3502, '_site_transient_timeout_php_check_90e738eca301c4d89366b1a4d15fe37f', '1602763545', 'no'),
(3503, '_site_transient_php_check_90e738eca301c4d89366b1a4d15fe37f', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(3544, '_transient_timeout__woocommerce_helper_updates', '1602506850', 'no'),
(3545, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"98d544e60afebf2d4e3fbf223d68077d\";s:7:\"updated\";i:1602463650;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(3547, '_transient_timeout_mc4wp_mailchimp_lists', '1602552227', 'no'),
(3548, '_transient_mc4wp_mailchimp_lists', 'a:0:{}', 'no'),
(3559, '_site_transient_timeout_available_translations', '1602474753', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(3560, '_site_transient_available_translations', 'a:121:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-05 08:33:42\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-29 17:14:25\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2019-10-29 07:54:22\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.15/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-22 10:57:09\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.8.14/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-20 05:00:26\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:33:\"མུ་མཐུད་དུ།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-18 19:20:00\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-15 19:05:13\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-27 14:39:02\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-13 11:39:25\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-15 20:45:17\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:5:\"de_AT\";a:8:{s:8:\"language\";s:5:\"de_AT\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-16 07:58:55\";s:12:\"english_name\";s:16:\"German (Austria)\";s:11:\"native_name\";s:21:\"Deutsch (Österreich)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/de_AT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-24 17:26:42\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.3.2/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-12 08:02:56\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/5.3.2/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-12 08:02:09\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-24 17:29:56\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-10 10:14:21\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-14 09:40:29\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-23 21:50:21\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-11-28 17:04:17\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-04-23 14:51:04\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-14 09:42:12\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-14 12:06:57\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-06 05:52:53\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-10 15:47:49\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-23 23:02:03\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-04-19 06:57:02\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-04 17:46:33\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"5.1.6\";s:7:\"updated\";s:19:\"2019-03-02 06:35:01\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.6/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_UY\";a:8:{s:8:\"language\";s:5:\"es_UY\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-11-12 04:43:11\";s:12:\"english_name\";s:17:\"Spanish (Uruguay)\";s:11:\"native_name\";s:19:\"Español de Uruguay\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_UY.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-11 04:39:12\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-04-17 07:21:39\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:9:\"5.0-beta3\";s:7:\"updated\";s:19:\"2018-11-28 16:04:33\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.0-beta3/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-08 17:55:03\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-03 22:42:09\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-02 09:08:55\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-04-16 22:07:17\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-04-17 14:28:30\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2020-07-04 16:43:09\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.15/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-01 17:58:42\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-20 16:09:08\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-24 15:34:57\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:3:\"hsb\";a:8:{s:8:\"language\";s:3:\"hsb\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-11-28 17:22:10\";s:12:\"english_name\";s:13:\"Upper Sorbian\";s:11:\"native_name\";s:17:\"Hornjoserbšćina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.3.2/hsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"hsb\";i:3;s:3:\"hsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:4:\"Dale\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-19 14:36:40\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-04 22:54:51\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.7.11\";s:7:\"updated\";s:19:\"2018-09-20 11:13:37\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.7.11/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-20 17:02:39\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-02 04:22:13\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"次へ\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-25 11:03:02\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-28 21:59:12\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.3.2/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 07:34:10\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.0.3/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2020-09-30 14:08:59\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.15/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರಿಸು\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"5.2.7\";s:7:\"updated\";s:19:\"2020-07-29 06:48:26\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.7/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2018-12-18 14:32:44\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.15/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"5.2.7\";s:7:\"updated\";s:19:\"2020-07-14 08:45:32\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.7/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:6:\"4.7.18\";s:7:\"updated\";s:19:\"2020-07-14 09:04:42\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.18/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"5.2.3\";s:7:\"updated\";s:19:\"2019-09-08 12:57:25\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.3/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.14/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2018-08-31 11:57:07\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.15/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-26 13:54:23\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-15 20:22:22\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.3.2/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-12 09:57:15\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-17 19:42:41\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-01 08:53:00\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-18 18:30:50\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-04 11:20:43\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_AO\";a:8:{s:8:\"language\";s:5:\"pt_AO\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-14 08:20:00\";s:12:\"english_name\";s:19:\"Portuguese (Angola)\";s:11:\"native_name\";s:20:\"Português de Angola\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/pt_AO.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-08 13:01:50\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/5.3.2/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-09 12:17:54\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-04-28 10:28:56\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-01-17 16:11:46\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:3:\"snd\";a:8:{s:8:\"language\";s:3:\"snd\";s:7:\"version\";s:3:\"5.3\";s:7:\"updated\";s:19:\"2019-11-12 04:37:38\";s:12:\"english_name\";s:6:\"Sindhi\";s:11:\"native_name\";s:8:\"سنڌي\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/5.3/snd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"sd\";i:2;s:3:\"snd\";i:3;s:3:\"snd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"اڳتي هلو\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-02 07:46:23\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:3:\"skr\";a:8:{s:8:\"language\";s:3:\"skr\";s:7:\"version\";s:5:\"5.2.3\";s:7:\"updated\";s:19:\"2019-06-26 11:40:37\";s:12:\"english_name\";s:7:\"Saraiki\";s:11:\"native_name\";s:14:\"سرائیکی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2.3/skr.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"skr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"جاری رکھو\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-14 22:44:44\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-15 22:50:02\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-05 10:15:34\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:2:\"sw\";a:8:{s:8:\"language\";s:2:\"sw\";s:7:\"version\";s:5:\"5.2.6\";s:7:\"updated\";s:19:\"2019-10-22 00:19:41\";s:12:\"english_name\";s:7:\"Swahili\";s:11:\"native_name\";s:9:\"Kiswahili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.6/sw.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sw\";i:2;s:3:\"swa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Endelea\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"5.2.7\";s:7:\"updated\";s:19:\"2020-08-22 08:24:09\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.7/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-02-21 13:52:32\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-08 20:31:05\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"5.1.6\";s:7:\"updated\";s:19:\"2020-04-09 10:48:08\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.6/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:6:\"5.0.10\";s:7:\"updated\";s:19:\"2019-01-23 12:32:40\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.0.10/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-07 15:52:24\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-08 12:12:22\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-12-08 21:26:25\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2020-03-17 15:32:45\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}}', 'no'),
(3594, '_transient_timeout_plugin_slugs', '1602554296', 'no'),
(3595, '_transient_plugin_slugs', 'a:18:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:37:\"ajax-search-lite/ajax-search-lite.php\";i:2;s:33:\"classic-editor/classic-editor.php\";i:3;s:33:\"duplicate-post/duplicate-post.php\";i:4;s:35:\"cookie-law-info/cookie-law-info.php\";i:5;s:29:\"gravityforms/gravityforms.php\";i:6;s:35:\"gravityformsmailchimp/mailchimp.php\";i:7;s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";i:8;s:15:\"mmenu/mmenu.php\";i:9;s:13:\"pods/init.php\";i:10;s:27:\"the-preloader/preloader.php\";i:11;s:55:\"searchwp-live-ajax-search/searchwp-live-ajax-search.php\";i:12;s:26:\"snazzy-maps/snazzymaps.php\";i:13;s:33:\"ultimate-modal/ultimate-modal.php\";i:14;s:27:\"woocommerce/woocommerce.php\";i:15;s:71:\"woocommerce-gravityforms-product-addons/gravityforms-product-addons.php\";i:16;s:61:\"woocommerce-variations-table/woocommerce-variations-table.php\";i:17;s:24:\"wordpress-seo/wp-seo.php\";}', 'no'),
(3597, '_transient_timeout_as-post-store-dependencies-met', '1602550928', 'no'),
(3598, '_transient_as-post-store-dependencies-met', 'yes', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(3749, '_site_transient_timeout_theme_roots', '1602469110', 'no'),
(3750, '_site_transient_theme_roots', 'a:10:{s:5:\"jac19\";s:7:\"/themes\";s:12:\"twentyeleven\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:13:\"twentyfifteen\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:14:\"twentyfourteen\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:14:\"twentynineteen\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:15:\"twentyseventeen\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:13:\"twentysixteen\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:9:\"twentyten\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:14:\"twentythirteen\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";s:12:\"twentytwelve\";s:58:\"/Applications/MAMP/htdocs/lnccdev/web/wp/wp-content/themes\";}', 'no'),
(3842, '_transient_timeout_GFCache_18015fe3120b1d58ccccfa486b3501ec', '1602468732', 'no'),
(3843, '_transient_GFCache_18015fe3120b1d58ccccfa486b3501ec', 'a:0:{}', 'no'),
(3850, '_transient_timeout_GFCache_477252443fead72aeffef6fdf437d140', '1602468719', 'no'),
(3851, '_transient_GFCache_477252443fead72aeffef6fdf437d140', 'a:1:{i:0;O:8:\"stdClass\":2:{s:7:\"form_id\";s:1:\"1\";s:10:\"view_count\";s:3:\"111\";}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_podsrel`
--

CREATE TABLE `wp_podsrel` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pod_id` int(10) UNSIGNED DEFAULT NULL,
  `field_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `related_pod_id` int(10) UNSIGNED DEFAULT NULL,
  `related_field_id` int(10) UNSIGNED DEFAULT NULL,
  `related_item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `weight` smallint(5) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'overview.php'),
(3, 5, '_menu_item_type', 'custom'),
(4, 5, '_menu_item_menu_item_parent', '0'),
(5, 5, '_menu_item_object_id', '5'),
(6, 5, '_menu_item_object', 'custom'),
(7, 5, '_menu_item_target', ''),
(8, 5, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(9, 5, '_menu_item_xfn', ''),
(10, 5, '_menu_item_url', 'http://bedrock.loc/'),
(12, 6, '_menu_item_type', 'post_type'),
(13, 6, '_menu_item_menu_item_parent', '0'),
(14, 6, '_menu_item_object_id', '2'),
(15, 6, '_menu_item_object', 'page'),
(16, 6, '_menu_item_target', ''),
(17, 6, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(18, 6, '_menu_item_xfn', ''),
(19, 6, '_menu_item_url', ''),
(20, 7, '_edit_lock', '1602464650:1'),
(21, 7, '_wp_page_template', 'template-home.php'),
(22, 9, '_menu_item_type', 'post_type'),
(23, 9, '_menu_item_menu_item_parent', '0'),
(24, 9, '_menu_item_object_id', '7'),
(25, 9, '_menu_item_object', 'page'),
(26, 9, '_menu_item_target', ''),
(27, 9, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(28, 9, '_menu_item_xfn', ''),
(29, 9, '_menu_item_url', ''),
(32, 11, '_wp_attached_file', 'woocommerce-placeholder.png'),
(33, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(34, 1, '_edit_lock', '1583860913:1'),
(35, 2, '_edit_lock', '1583862633:1'),
(36, 7, '_edit_last', '1'),
(57, 29, '_edit_last', '1'),
(58, 29, '_wp_page_template', 'default'),
(59, 29, 'extra-content', ''),
(60, 29, '_yoast_wpseo_content_score', '30'),
(61, 29, '_edit_lock', '1583947944:1'),
(62, 31, '_edit_last', '1'),
(63, 31, '_wp_page_template', 'default'),
(64, 31, 'extra-content', ''),
(65, 31, '_yoast_wpseo_content_score', '30'),
(66, 31, '_edit_lock', '1583867577:1'),
(67, 33, '_edit_last', '1'),
(68, 33, '_wp_page_template', 'default'),
(69, 33, 'extra-content', ''),
(70, 33, '_yoast_wpseo_content_score', '30'),
(71, 33, '_edit_lock', '1602468512:1'),
(99, 38, '_edit_last', '1'),
(100, 38, '_wp_page_template', 'default'),
(101, 38, 'extra-content', ''),
(102, 38, '_yoast_wpseo_content_score', '30'),
(103, 38, '_edit_lock', '1583867978:1'),
(104, 39, '_edit_last', '1'),
(105, 39, '_wp_page_template', 'default'),
(106, 39, 'extra-content', ''),
(107, 39, '_edit_lock', '1586106586:1'),
(108, 39, '_yoast_wpseo_content_score', '30'),
(109, 43, '_edit_last', '1'),
(110, 43, '_wp_page_template', 'default'),
(111, 43, 'extra-content', ''),
(112, 43, '_yoast_wpseo_content_score', '30'),
(113, 43, '_edit_lock', '1586106220:1'),
(143, 49, '_menu_item_type', 'post_type'),
(144, 49, '_menu_item_menu_item_parent', '0'),
(145, 49, '_menu_item_object_id', '39'),
(146, 49, '_menu_item_object', 'page'),
(147, 49, '_menu_item_target', ''),
(148, 49, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(149, 49, '_menu_item_xfn', ''),
(150, 49, '_menu_item_url', ''),
(170, 54, '_menu_item_type', 'post_type'),
(171, 54, '_menu_item_menu_item_parent', '0'),
(172, 54, '_menu_item_object_id', '43'),
(173, 54, '_menu_item_object', 'page'),
(174, 54, '_menu_item_target', ''),
(175, 54, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(176, 54, '_menu_item_xfn', ''),
(177, 54, '_menu_item_url', ''),
(179, 3, '_edit_lock', '1583952776:1'),
(180, 55, '_edit_last', '1'),
(181, 55, '_edit_lock', '1583946239:1'),
(182, 55, '_linkage_option', 'new'),
(183, 55, '_color_controls_background', 'gray-dark'),
(184, 55, '_color_controls_text', 'knockout'),
(185, 55, '_featured', '1'),
(186, 56, '_wp_attached_file', '2020/03/jac-we-create.png'),
(187, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:203;s:4:\"file\";s:25:\"2020/03/jac-we-create.png\";s:5:\"sizes\";a:6:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"jac-we-create-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"jac-we-create-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"jac-we-create-300x203.png\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"jac-we-create-300x203.png\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:25:\"jac-we-create-300x203.png\";s:5:\"width\";i:300;s:6:\"height\";i:203;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"jac-we-create-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(188, 57, '_wp_attached_file', '2020/03/login-background-scaled.jpg'),
(189, 57, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1707;s:4:\"file\";s:35:\"2020/03/login-background-scaled.jpg\";s:5:\"sizes\";a:11:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"login-background-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"login-background-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:28:\"login-background-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"login-background-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"login-background-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"login-background-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"login-background-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:28:\"login-background-320x240.jpg\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:28:\"login-background-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:28:\"login-background-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:28:\"login-background-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1436286097\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:20:\"login-background.jpg\";}'),
(190, 55, '_thumbnail_id', '57'),
(191, 55, '_linkage_link', 'http://jac.co/'),
(192, 55, '_linkage_text', 'LEARN MORE'),
(193, 59, '_edit_last', '1'),
(194, 59, '_wp_page_template', 'default'),
(195, 59, 'extra-content', ''),
(196, 59, '_yoast_wpseo_content_score', '30'),
(197, 59, '_edit_lock', '1583942389:1'),
(198, 3, '_edit_last', '1'),
(199, 3, 'extra-content', ''),
(200, 3, '_yoast_wpseo_content_score', '30'),
(201, 65, '_menu_item_type', 'post_type'),
(202, 65, '_menu_item_menu_item_parent', '0'),
(203, 65, '_menu_item_object_id', '3'),
(204, 65, '_menu_item_object', 'page'),
(205, 65, '_menu_item_target', ''),
(206, 65, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(207, 65, '_menu_item_xfn', ''),
(208, 65, '_menu_item_url', ''),
(210, 66, '_menu_item_type', 'post_type'),
(211, 66, '_menu_item_menu_item_parent', '0'),
(212, 66, '_menu_item_object_id', '59'),
(213, 66, '_menu_item_object', 'page'),
(214, 66, '_menu_item_target', ''),
(215, 66, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(216, 66, '_menu_item_xfn', ''),
(217, 66, '_menu_item_url', ''),
(219, 7, 'extra-content', ''),
(220, 67, '_edit_last', '1'),
(221, 67, '_edit_lock', '1602467599:1'),
(222, 67, '_linkage_link', 'https://jac.co/'),
(223, 67, '_linkage_text', 'MORE'),
(224, 67, '_linkage_option', 'same'),
(225, 68, '_edit_last', '1'),
(226, 68, '_linkage_link', 'https://jac.co/'),
(227, 68, '_linkage_text', 'READ MORE'),
(228, 68, '_linkage_option', 'same'),
(229, 68, '_edit_lock', '1602467790:1'),
(232, 69, '_edit_last', '1'),
(233, 69, '_wp_page_template', 'default'),
(234, 69, 'extra-content', ''),
(235, 69, '_yoast_wpseo_content_score', '30'),
(236, 69, '_edit_lock', '1583954039:1'),
(237, 72, '_edit_last', '1'),
(238, 72, '_edit_lock', '1583948164:1'),
(239, 72, '_social_icon', 'facebook-f'),
(240, 72, '_social_handle', 'john.atkins.co'),
(241, 72, '_social_url', 'http://facebook.com/john.atkins.co/'),
(242, 73, '_edit_last', '1'),
(243, 73, '_social_icon', 'twitter-squre'),
(244, 73, '_social_handle', 'meet_jac'),
(245, 73, '_social_url', 'https://twitter.com/meet_jac'),
(246, 73, '_edit_lock', '1583956190:1'),
(247, 74, '_edit_last', '1'),
(248, 74, '_edit_lock', '1583948234:1'),
(249, 75, '_edit_last', '1'),
(250, 74, '_social_icon', 'instagram'),
(251, 75, '_social_icon', 'youtube'),
(252, 74, '_social_handle', 'meet_jac'),
(253, 75, '_social_handle', 'HeresJAC'),
(254, 74, '_social_url', 'http://instagram.com/meet_jac/'),
(255, 75, '_social_url', 'https://www.youtube.com/user/HeresJAC'),
(256, 75, '_edit_lock', '1583948415:1'),
(257, 75, '_wp_old_slug', 'instagram'),
(258, 67, '_thumbnail_id', '101'),
(269, 39, '_thumbnail_id', '57'),
(270, 82, '_edit_last', '1'),
(271, 82, '_edit_lock', '1585089240:1'),
(272, 82, '_wp_page_template', 'default'),
(273, 82, 'extra-content', ''),
(274, 84, '_edit_last', '1'),
(275, 84, '_wp_page_template', 'default'),
(276, 84, 'extra-content', ''),
(277, 84, '_yoast_wpseo_content_score', '30'),
(278, 84, '_edit_lock', '1585089273:1'),
(279, 86, '_edit_last', '1'),
(280, 86, '_edit_lock', '1585089295:1'),
(281, 86, '_wp_page_template', 'default'),
(282, 86, 'extra-content', ''),
(283, 89, '_edit_last', '1'),
(284, 89, '_wp_page_template', 'default'),
(285, 89, 'extra-content', ''),
(286, 89, '_yoast_wpseo_content_score', '90'),
(287, 89, '_edit_lock', '1585089623:1'),
(297, 92, '_menu_item_type', 'post_type'),
(298, 92, '_menu_item_menu_item_parent', '0'),
(299, 92, '_menu_item_object_id', '86'),
(300, 92, '_menu_item_object', 'page'),
(301, 92, '_menu_item_target', ''),
(302, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(303, 92, '_menu_item_xfn', ''),
(304, 92, '_menu_item_url', ''),
(306, 93, '_menu_item_type', 'post_type'),
(307, 93, '_menu_item_menu_item_parent', '0'),
(308, 93, '_menu_item_object_id', '69'),
(309, 93, '_menu_item_object', 'page'),
(310, 93, '_menu_item_target', ''),
(311, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(312, 93, '_menu_item_xfn', ''),
(313, 93, '_menu_item_url', ''),
(354, 98, '_edit_last', '1'),
(355, 98, '_edit_lock', '1594144750:1'),
(356, 98, '_linkage_link', 'https://jac.co/'),
(357, 98, '_linkage_text', 'Notification Bar'),
(358, 98, '_linkage_option', 'new'),
(359, 98, 'management_team-13', '7'),
(360, 98, 'management_team-8', '69'),
(369, 100, '_edit_last', '1'),
(370, 100, '_edit_lock', '1594144568:1'),
(371, 100, '_video_popup_link', 'https://www.youtube.com/embed/VDfmOZeJF5k'),
(372, 100, 'video-13', '7'),
(373, 101, '_wp_attached_file', '2020/03/hong-kong-city-2434627-min-scaled.jpg'),
(374, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2560;s:6:\"height\";i:1707;s:4:\"file\";s:45:\"2020/03/hong-kong-city-2434627-min-scaled.jpg\";s:5:\"sizes\";a:13:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:45:\"hong-kong-city-2434627-min-scaled-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:45:\"hong-kong-city-2434627-min-scaled-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:45:\"hong-kong-city-2434627-min-scaled-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"hong-kong-city-2434627-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:39:\"hong-kong-city-2434627-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"hong-kong-city-2434627-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"hong-kong-city-2434627-min-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:40:\"hong-kong-city-2434627-min-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:40:\"hong-kong-city-2434627-min-2048x1365.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1365;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:38:\"hong-kong-city-2434627-min-320x240.jpg\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:45:\"hong-kong-city-2434627-min-scaled-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:45:\"hong-kong-city-2434627-min-scaled-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:45:\"hong-kong-city-2434627-min-scaled-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(375, 103, '_edit_last', '1'),
(376, 103, '_edit_lock', '1602468352:1'),
(377, 103, '_wp_page_template', 'default'),
(378, 103, 'extra-content', ''),
(379, 107, '_edit_last', '1'),
(380, 107, '_edit_lock', '1602067244:1'),
(381, 123, '_wp_attached_file', '2020/10/1229106_landscape-16.jpg'),
(382, 123, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1200;s:4:\"file\";s:32:\"2020/10/1229106_landscape-16.jpg\";s:5:\"sizes\";a:12:{s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-300x188.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:188;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1229106_landscape-16-1024x640.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:640;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-768x480.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:480;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:33:\"1229106_landscape-16-1536x960.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-320x240.jpg\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"1229106_landscape-16-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-600x375.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:375;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-600x375.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:375;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"1229106_landscape-16-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(383, 124, '_wp_attached_file', '2020/10/matterhorn-shutterstock_1118486243.jpg'),
(384, 124, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1339;s:4:\"file\";s:46:\"2020/10/matterhorn-shutterstock_1118486243.jpg\";s:5:\"sizes\";a:12:{s:6:\"medium\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:47:\"matterhorn-shutterstock_1118486243-1024x714.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:714;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-768x536.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:48:\"matterhorn-shutterstock_1118486243-1536x1071.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1071;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-320x240.jpg\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-600x418.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:418;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-600x418.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:418;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:46:\"matterhorn-shutterstock_1118486243-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(385, 125, '_wp_attached_file', '2020/10/pink_landscape.jpg'),
(386, 125, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1340;s:4:\"file\";s:26:\"2020/10/pink_landscape.jpg\";s:5:\"sizes\";a:12:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"pink_landscape-300x201.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:201;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"pink_landscape-1024x686.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:686;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"pink_landscape-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"pink_landscape-768x515.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:28:\"pink_landscape-1536x1029.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1029;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:26:\"pink_landscape-320x240.jpg\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"pink_landscape-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:26:\"pink_landscape-600x402.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:402;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"pink_landscape-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"pink_landscape-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:26:\"pink_landscape-600x402.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:402;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"pink_landscape-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:2:\"11\";s:6:\"credit\";s:13:\"Sander Grefte\";s:6:\"camera\";s:9:\"NIKON Z 6\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1564062510\";s:9:\"copyright\";s:30:\"Copyright ©2018 Sander Grefte\";s:12:\"focal_length\";s:3:\"280\";s:3:\"iso\";s:3:\"200\";s:13:\"shutter_speed\";s:5:\"0.004\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(387, 7, 'first_slide_slider_image', ''),
(388, 7, '_first_slide_slider_image', 'field_5f7d915f5b7ba'),
(389, 7, 'first_slide_slider_title', ''),
(390, 7, '_first_slide_slider_title', 'field_5f7d918e5b7bb'),
(391, 7, 'first_slide_slider_subtitle', ''),
(392, 7, '_first_slide_slider_subtitle', 'field_5f7d91a85b7bc'),
(393, 7, 'first_slide_link_to_open', ''),
(394, 7, '_first_slide_link_to_open', 'field_5f7d91b25b7bd'),
(395, 7, 'first_slide', ''),
(396, 7, '_first_slide', 'field_5f7d91405b7b9'),
(397, 7, 'second_slider_slider_image', ''),
(398, 7, '_second_slider_slider_image', 'field_5f7d91d6b8c43'),
(399, 7, 'second_slider_slider_title', ''),
(400, 7, '_second_slider_slider_title', 'field_5f7d91d6b8c44'),
(401, 7, 'second_slider_slider_subtitle', ''),
(402, 7, '_second_slider_slider_subtitle', 'field_5f7d91d6b8c45'),
(403, 7, 'second_slider_link_to_open', ''),
(404, 7, '_second_slider_link_to_open', 'field_5f7d91d6b8c46'),
(405, 7, 'second_slider', ''),
(406, 7, '_second_slider', 'field_5f7d91d6b8c42'),
(407, 7, 'third_slider_slider_image', ''),
(408, 7, '_third_slider_slider_image', 'field_5f7d91fd7ea7f'),
(409, 7, 'third_slider_slider_title', ''),
(410, 7, '_third_slider_slider_title', 'field_5f7d91fd7ea80'),
(411, 7, 'third_slider_slider_subtitle', ''),
(412, 7, '_third_slider_slider_subtitle', 'field_5f7d91fd7ea81'),
(413, 7, 'third_slider_link_to_open', ''),
(414, 7, '_third_slider_link_to_open', 'field_5f7d91fd7ea82'),
(415, 7, 'third_slider', ''),
(416, 7, '_third_slider', 'field_5f7d91fd7ea7e'),
(417, 126, 'first_slide_slider_image', '123'),
(418, 126, '_first_slide_slider_image', 'field_5f7d915f5b7ba'),
(419, 126, 'first_slide_slider_title', 'Leading the region.<br>Influencing the country.'),
(420, 126, '_first_slide_slider_title', 'field_5f7d918e5b7bb'),
(421, 126, 'first_slide_slider_subtitle', 'Over 50 Years of Commitment.'),
(422, 126, '_first_slide_slider_subtitle', 'field_5f7d91a85b7bc'),
(423, 126, 'first_slide_link_to_open', 'https://www.netflix.com/browse'),
(424, 126, '_first_slide_link_to_open', 'field_5f7d91b25b7bd'),
(425, 126, 'first_slide', ''),
(426, 126, '_first_slide', 'field_5f7d91405b7b9'),
(427, 126, 'second_slider_slider_image', '124'),
(428, 126, '_second_slider_slider_image', 'field_5f7d91d6b8c43'),
(429, 126, 'second_slider_slider_title', 'This is a text.<br>for the next slider.'),
(430, 126, '_second_slider_slider_title', 'field_5f7d91d6b8c44'),
(431, 126, 'second_slider_slider_subtitle', 'This is a paragraph.'),
(432, 126, '_second_slider_slider_subtitle', 'field_5f7d91d6b8c45'),
(433, 126, 'second_slider_link_to_open', 'https://www.netflix.com/browse'),
(434, 126, '_second_slider_link_to_open', 'field_5f7d91d6b8c46'),
(435, 126, 'second_slider', ''),
(436, 126, '_second_slider', 'field_5f7d91d6b8c42'),
(437, 126, 'third_slider_slider_image', ''),
(438, 126, '_third_slider_slider_image', 'field_5f7d91fd7ea7f'),
(439, 126, 'third_slider_slider_title', 'This is another.<br>text for a slider.'),
(440, 126, '_third_slider_slider_title', 'field_5f7d91fd7ea80'),
(441, 126, 'third_slider_slider_subtitle', 'This is just a subtitle.'),
(442, 126, '_third_slider_slider_subtitle', 'field_5f7d91fd7ea81'),
(443, 126, 'third_slider_link_to_open', 'https://www.netflix.com/browse'),
(444, 126, '_third_slider_link_to_open', 'field_5f7d91fd7ea82'),
(445, 126, 'third_slider', ''),
(446, 126, '_third_slider', 'field_5f7d91fd7ea7e'),
(447, 127, 'first_slide_slider_image', '123'),
(448, 127, '_first_slide_slider_image', 'field_5f7d915f5b7ba'),
(449, 127, 'first_slide_slider_title', 'Leading the region.<br>Influencing the country.'),
(450, 127, '_first_slide_slider_title', 'field_5f7d918e5b7bb'),
(451, 127, 'first_slide_slider_subtitle', 'Over 50 Years of Commitment.'),
(452, 127, '_first_slide_slider_subtitle', 'field_5f7d91a85b7bc'),
(453, 127, 'first_slide_link_to_open', 'https://www.netflix.com/browse'),
(454, 127, '_first_slide_link_to_open', 'field_5f7d91b25b7bd'),
(455, 127, 'first_slide', ''),
(456, 127, '_first_slide', 'field_5f7d91405b7b9'),
(457, 127, 'second_slider_slider_image', '124'),
(458, 127, '_second_slider_slider_image', 'field_5f7d91d6b8c43'),
(459, 127, 'second_slider_slider_title', 'This is a text.<br>for the next slider.'),
(460, 127, '_second_slider_slider_title', 'field_5f7d91d6b8c44'),
(461, 127, 'second_slider_slider_subtitle', 'This is a paragraph.'),
(462, 127, '_second_slider_slider_subtitle', 'field_5f7d91d6b8c45'),
(463, 127, 'second_slider_link_to_open', 'https://www.netflix.com/browse'),
(464, 127, '_second_slider_link_to_open', 'field_5f7d91d6b8c46'),
(465, 127, 'second_slider', ''),
(466, 127, '_second_slider', 'field_5f7d91d6b8c42'),
(467, 127, 'third_slider_slider_image', '125'),
(468, 127, '_third_slider_slider_image', 'field_5f7d91fd7ea7f'),
(469, 127, 'third_slider_slider_title', 'This is another.<br>text for a slider.'),
(470, 127, '_third_slider_slider_title', 'field_5f7d91fd7ea80'),
(471, 127, 'third_slider_slider_subtitle', 'This is just a subtitle.'),
(472, 127, '_third_slider_slider_subtitle', 'field_5f7d91fd7ea81'),
(473, 127, 'third_slider_link_to_open', 'https://www.netflix.com/browse'),
(474, 127, '_third_slider_link_to_open', 'field_5f7d91fd7ea82'),
(475, 127, 'third_slider', ''),
(476, 127, '_third_slider', 'field_5f7d91fd7ea7e'),
(477, 128, 'first_slide_slider_image', ''),
(478, 128, '_first_slide_slider_image', 'field_5f7d915f5b7ba'),
(479, 128, 'first_slide_slider_title', ''),
(480, 128, '_first_slide_slider_title', 'field_5f7d918e5b7bb'),
(481, 128, 'first_slide_slider_subtitle', ''),
(482, 128, '_first_slide_slider_subtitle', 'field_5f7d91a85b7bc'),
(483, 128, 'first_slide_link_to_open', ''),
(484, 128, '_first_slide_link_to_open', 'field_5f7d91b25b7bd'),
(485, 128, 'first_slide', ''),
(486, 128, '_first_slide', 'field_5f7d91405b7b9'),
(487, 128, 'second_slider_slider_image', ''),
(488, 128, '_second_slider_slider_image', 'field_5f7d91d6b8c43'),
(489, 128, 'second_slider_slider_title', ''),
(490, 128, '_second_slider_slider_title', 'field_5f7d91d6b8c44'),
(491, 128, 'second_slider_slider_subtitle', ''),
(492, 128, '_second_slider_slider_subtitle', 'field_5f7d91d6b8c45'),
(493, 128, 'second_slider_link_to_open', ''),
(494, 128, '_second_slider_link_to_open', 'field_5f7d91d6b8c46'),
(495, 128, 'second_slider', ''),
(496, 128, '_second_slider', 'field_5f7d91d6b8c42'),
(497, 128, 'third_slider_slider_image', ''),
(498, 128, '_third_slider_slider_image', 'field_5f7d91fd7ea7f'),
(499, 128, 'third_slider_slider_title', ''),
(500, 128, '_third_slider_slider_title', 'field_5f7d91fd7ea80'),
(501, 128, 'third_slider_slider_subtitle', ''),
(502, 128, '_third_slider_slider_subtitle', 'field_5f7d91fd7ea81'),
(503, 128, 'third_slider_link_to_open', ''),
(504, 128, '_third_slider_link_to_open', 'field_5f7d91fd7ea82'),
(505, 128, 'third_slider', ''),
(506, 128, '_third_slider', 'field_5f7d91fd7ea7e'),
(507, 7, 'home_slider_0_slider_image', '123'),
(508, 7, '_home_slider_0_slider_image', 'field_5f7d9724b5c39'),
(509, 7, 'home_slider_0_slider_title', 'Leading the region.<br>Influencing the country.'),
(510, 7, '_home_slider_0_slider_title', 'field_5f7d973eb5c3a'),
(511, 7, 'home_slider_0_slider_subtitle', 'Over 50 Years of Commitment.'),
(512, 7, '_home_slider_0_slider_subtitle', 'field_5f7d974eb5c3b'),
(513, 7, 'home_slider_0_link_to_open', 'https://www.netflix.com/browse'),
(514, 7, '_home_slider_0_link_to_open', 'field_5f7d9758b5c3c'),
(515, 7, 'home_slider_1_slider_image', '124'),
(516, 7, '_home_slider_1_slider_image', 'field_5f7d9724b5c39'),
(517, 7, 'home_slider_1_slider_title', 'This is a text.<br>for the next slider.'),
(518, 7, '_home_slider_1_slider_title', 'field_5f7d973eb5c3a'),
(519, 7, 'home_slider_1_slider_subtitle', 'This is a paragraph.'),
(520, 7, '_home_slider_1_slider_subtitle', 'field_5f7d974eb5c3b'),
(521, 7, 'home_slider_1_link_to_open', 'https://www.netflix.com/browse'),
(522, 7, '_home_slider_1_link_to_open', 'field_5f7d9758b5c3c'),
(523, 7, 'home_slider_2_slider_image', '125'),
(524, 7, '_home_slider_2_slider_image', 'field_5f7d9724b5c39'),
(525, 7, 'home_slider_2_slider_title', 'This is another.<br>text for a slider.'),
(526, 7, '_home_slider_2_slider_title', 'field_5f7d973eb5c3a'),
(527, 7, 'home_slider_2_slider_subtitle', 'This is just a subtitle'),
(528, 7, '_home_slider_2_slider_subtitle', 'field_5f7d974eb5c3b'),
(529, 7, 'home_slider_2_link_to_open', 'https://www.netflix.com/browse'),
(530, 7, '_home_slider_2_link_to_open', 'field_5f7d9758b5c3c'),
(531, 7, 'home_slider', '3'),
(532, 7, '_home_slider', 'field_5f7d9700b5c38'),
(533, 134, 'first_slide_slider_image', ''),
(534, 134, '_first_slide_slider_image', 'field_5f7d915f5b7ba'),
(535, 134, 'first_slide_slider_title', ''),
(536, 134, '_first_slide_slider_title', 'field_5f7d918e5b7bb'),
(537, 134, 'first_slide_slider_subtitle', ''),
(538, 134, '_first_slide_slider_subtitle', 'field_5f7d91a85b7bc'),
(539, 134, 'first_slide_link_to_open', ''),
(540, 134, '_first_slide_link_to_open', 'field_5f7d91b25b7bd'),
(541, 134, 'first_slide', ''),
(542, 134, '_first_slide', 'field_5f7d91405b7b9'),
(543, 134, 'second_slider_slider_image', ''),
(544, 134, '_second_slider_slider_image', 'field_5f7d91d6b8c43'),
(545, 134, 'second_slider_slider_title', ''),
(546, 134, '_second_slider_slider_title', 'field_5f7d91d6b8c44'),
(547, 134, 'second_slider_slider_subtitle', ''),
(548, 134, '_second_slider_slider_subtitle', 'field_5f7d91d6b8c45'),
(549, 134, 'second_slider_link_to_open', ''),
(550, 134, '_second_slider_link_to_open', 'field_5f7d91d6b8c46'),
(551, 134, 'second_slider', ''),
(552, 134, '_second_slider', 'field_5f7d91d6b8c42'),
(553, 134, 'third_slider_slider_image', ''),
(554, 134, '_third_slider_slider_image', 'field_5f7d91fd7ea7f'),
(555, 134, 'third_slider_slider_title', ''),
(556, 134, '_third_slider_slider_title', 'field_5f7d91fd7ea80'),
(557, 134, 'third_slider_slider_subtitle', ''),
(558, 134, '_third_slider_slider_subtitle', 'field_5f7d91fd7ea81'),
(559, 134, 'third_slider_link_to_open', ''),
(560, 134, '_third_slider_link_to_open', 'field_5f7d91fd7ea82'),
(561, 134, 'third_slider', ''),
(562, 134, '_third_slider', 'field_5f7d91fd7ea7e'),
(563, 134, 'home_slider_0_slider_image', '123'),
(564, 134, '_home_slider_0_slider_image', 'field_5f7d9724b5c39'),
(565, 134, 'home_slider_0_slider_title', 'Leading the region.<br>Influencing the country.'),
(566, 134, '_home_slider_0_slider_title', 'field_5f7d973eb5c3a'),
(567, 134, 'home_slider_0_slider_subtitle', 'Over 50 Years of Commitment.'),
(568, 134, '_home_slider_0_slider_subtitle', 'field_5f7d974eb5c3b'),
(569, 134, 'home_slider_0_link_to_open', 'https://www.netflix.com/browse'),
(570, 134, '_home_slider_0_link_to_open', 'field_5f7d9758b5c3c'),
(571, 134, 'home_slider_1_slider_image', '124'),
(572, 134, '_home_slider_1_slider_image', 'field_5f7d9724b5c39'),
(573, 134, 'home_slider_1_slider_title', 'This is a text.<br>for the next slider.'),
(574, 134, '_home_slider_1_slider_title', 'field_5f7d973eb5c3a'),
(575, 134, 'home_slider_1_slider_subtitle', 'This is a paragraph.'),
(576, 134, '_home_slider_1_slider_subtitle', 'field_5f7d974eb5c3b'),
(577, 134, 'home_slider_1_link_to_open', 'https://www.netflix.com/browse'),
(578, 134, '_home_slider_1_link_to_open', 'field_5f7d9758b5c3c'),
(579, 134, 'home_slider_2_slider_image', '125'),
(580, 134, '_home_slider_2_slider_image', 'field_5f7d9724b5c39'),
(581, 134, 'home_slider_2_slider_title', 'This is another.<br>text for a slider.'),
(582, 134, '_home_slider_2_slider_title', 'field_5f7d973eb5c3a'),
(583, 134, 'home_slider_2_slider_subtitle', 'This is just a subtitle'),
(584, 134, '_home_slider_2_slider_subtitle', 'field_5f7d974eb5c3b'),
(585, 134, 'home_slider_2_link_to_open', 'https://www.netflix.com/browse'),
(586, 134, '_home_slider_2_link_to_open', 'field_5f7d9758b5c3c'),
(587, 134, 'home_slider', '3'),
(588, 134, '_home_slider', 'field_5f7d9700b5c38'),
(589, 135, '_edit_last', '1'),
(590, 135, '_edit_lock', '1602072184:1'),
(591, 150, '_wp_attached_file', '2020/10/business_icon.png'),
(592, 150, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:911;s:6:\"height\";i:792;s:4:\"file\";s:25:\"2020/10/business_icon.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"business_icon-300x261.png\";s:5:\"width\";i:300;s:6:\"height\";i:261;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"business_icon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"business_icon-768x668.png\";s:5:\"width\";i:768;s:6:\"height\";i:668;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:25:\"business_icon-320x240.png\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"business_icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"business_icon-600x522.png\";s:5:\"width\";i:600;s:6:\"height\";i:522;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"business_icon-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"business_icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"business_icon-600x522.png\";s:5:\"width\";i:600;s:6:\"height\";i:522;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"business_icon-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(593, 151, '_wp_attached_file', '2020/10/networking_icon.png'),
(594, 151, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:911;s:6:\"height\";i:792;s:4:\"file\";s:27:\"2020/10/networking_icon.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"networking_icon-300x261.png\";s:5:\"width\";i:300;s:6:\"height\";i:261;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"networking_icon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"networking_icon-768x668.png\";s:5:\"width\";i:768;s:6:\"height\";i:668;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:27:\"networking_icon-320x240.png\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"networking_icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"networking_icon-600x522.png\";s:5:\"width\";i:600;s:6:\"height\";i:522;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"networking_icon-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"networking_icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"networking_icon-600x522.png\";s:5:\"width\";i:600;s:6:\"height\";i:522;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"networking_icon-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(595, 152, '_wp_attached_file', '2020/10/saving_icon.png'),
(596, 152, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:911;s:6:\"height\";i:792;s:4:\"file\";s:23:\"2020/10/saving_icon.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"saving_icon-300x261.png\";s:5:\"width\";i:300;s:6:\"height\";i:261;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"saving_icon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"saving_icon-768x668.png\";s:5:\"width\";i:768;s:6:\"height\";i:668;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"gallery-thumbnail\";a:4:{s:4:\"file\";s:23:\"saving_icon-320x240.png\";s:5:\"width\";i:320;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"saving_icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"saving_icon-600x522.png\";s:5:\"width\";i:600;s:6:\"height\";i:522;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"saving_icon-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"saving_icon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"saving_icon-600x522.png\";s:5:\"width\";i:600;s:6:\"height\";i:522;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"saving_icon-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(597, 103, 'first_section_title', 'Member Benefits'),
(598, 103, '_first_section_title', 'field_5f7d9c996c3c5'),
(599, 103, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(600, 103, '_first_section_description', 'field_5f7d9ca26c3c6'),
(601, 103, 'first_section_button_text', 'Become a Member'),
(602, 103, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(603, 103, 'first_section_button_link', 'https://www.netflix.com/'),
(604, 103, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(605, 103, 'first_section_benefit_icon_1_icon_image', '152'),
(606, 103, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(607, 103, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(608, 103, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(609, 103, 'first_section_benefit_icon_1', ''),
(610, 103, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(611, 103, 'first_section_benefit_icon_2_icon_image', '150'),
(612, 103, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(613, 103, 'first_section_benefit_icon_2_icon_text', 'GROW YOUR BUSINESS'),
(614, 103, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(615, 103, 'first_section_benefit_icon_2', ''),
(616, 103, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(617, 103, 'first_section_benefit_icon_3_icon_image', '151'),
(618, 103, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(619, 103, 'first_section_benefit_icon_3_icon_text', 'INCREASE NETWORKING'),
(620, 103, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(621, 103, 'first_section_benefit_icon_3', ''),
(622, 103, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(623, 103, 'first_section', ''),
(624, 103, '_first_section', 'field_5f7d9c2c6c3c4'),
(625, 153, 'first_section_title', 'Member Benefits'),
(626, 153, '_first_section_title', 'field_5f7d9c996c3c5'),
(627, 153, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(628, 153, '_first_section_description', 'field_5f7d9ca26c3c6'),
(629, 153, 'first_section_button_text', 'Become a Member'),
(630, 153, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(631, 153, 'first_section_button_link', 'https://www.netflix.com/'),
(632, 153, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(633, 153, 'first_section_benefit_icon_1_icon_image', '152'),
(634, 153, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(635, 153, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(636, 153, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(637, 153, 'first_section_benefit_icon_1', ''),
(638, 153, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(639, 153, 'first_section_benefit_icon_2_icon_image', '150'),
(640, 153, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(641, 153, 'first_section_benefit_icon_2_icon_text', 'GROW YOUR BUSINESS'),
(642, 153, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(643, 153, 'first_section_benefit_icon_2', ''),
(644, 153, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(645, 153, 'first_section_benefit_icon_3_icon_image', '151'),
(646, 153, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(647, 153, 'first_section_benefit_icon_3_icon_text', 'INCREASE NETWORKING'),
(648, 153, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(649, 153, 'first_section_benefit_icon_3', ''),
(650, 153, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(651, 153, 'first_section', ''),
(652, 153, '_first_section', 'field_5f7d9c2c6c3c4'),
(653, 161, '_wp_attached_file', '2020/10/SHIPPING.png'),
(654, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:203;s:6:\"height\";i:203;s:4:\"file\";s:20:\"2020/10/SHIPPING.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"SHIPPING-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"SHIPPING-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"SHIPPING-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(655, 162, '_wp_attached_file', '2020/10/ACCOMMODATIONS.png'),
(656, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:203;s:6:\"height\";i:203;s:4:\"file\";s:26:\"2020/10/ACCOMMODATIONS.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"ACCOMMODATIONS-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"ACCOMMODATIONS-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"ACCOMMODATIONS-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(657, 163, '_wp_attached_file', '2020/10/AUTOMOTIVE.png'),
(658, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:203;s:6:\"height\";i:203;s:4:\"file\";s:22:\"2020/10/AUTOMOTIVE.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"AUTOMOTIVE-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"AUTOMOTIVE-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"AUTOMOTIVE-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(659, 164, '_wp_attached_file', '2020/10/INSURANCE.png');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(660, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:203;s:6:\"height\";i:203;s:4:\"file\";s:21:\"2020/10/INSURANCE.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"INSURANCE-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"INSURANCE-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"INSURANCE-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(661, 165, '_wp_attached_file', '2020/10/phone.png'),
(662, 165, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:203;s:6:\"height\";i:203;s:4:\"file\";s:17:\"2020/10/phone.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"phone-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"phone-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"phone-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(663, 103, 'second_section_list_0_list_item', '<i class=\"fa fa-heart\" aria-hidden=\"true\"></i> AFFINITY PROGRAM'),
(664, 103, '_second_section_list_0_list_item', 'field_5f7da111ff9f4'),
(665, 103, 'second_section_list_1_list_item', '<i class=\"fa fa-star\" aria-hidden=\"true\"></i> GENERAL MEMBER DISCOUNTS'),
(666, 103, '_second_section_list_1_list_item', 'field_5f7da111ff9f4'),
(667, 103, 'second_section_list_2_list_item', '<i class=\"fa fa-bullseye\" aria-hidden=\"true\"></i> LATEST PROMOTIONS'),
(668, 103, '_second_section_list_2_list_item', 'field_5f7da111ff9f4'),
(669, 103, 'second_section_list', '3'),
(670, 103, '_second_section_list', 'field_5f7da0eaff9f3'),
(671, 103, 'second_section_view_all_link', 'https://www.netflix.com/'),
(672, 103, '_second_section_view_all_link', 'field_5f7da12e099b1'),
(673, 103, 'second_section_benefit_list_0_benefit_image', '165'),
(674, 103, '_second_section_benefit_list_0_benefit_image', 'field_5f7da173099b3'),
(675, 103, 'second_section_benefit_list_0_benefit_text', 'GROW YOUR BUSINESS'),
(676, 103, '_second_section_benefit_list_0_benefit_text', 'field_5f7da184099b4'),
(677, 103, 'second_section_benefit_list_1_benefit_image', '162'),
(678, 103, '_second_section_benefit_list_1_benefit_image', 'field_5f7da173099b3'),
(679, 103, 'second_section_benefit_list_1_benefit_text', 'ACCOMMODATIONS'),
(680, 103, '_second_section_benefit_list_1_benefit_text', 'field_5f7da184099b4'),
(681, 103, 'second_section_benefit_list_2_benefit_image', '164'),
(682, 103, '_second_section_benefit_list_2_benefit_image', 'field_5f7da173099b3'),
(683, 103, 'second_section_benefit_list_2_benefit_text', 'INSURANCE'),
(684, 103, '_second_section_benefit_list_2_benefit_text', 'field_5f7da184099b4'),
(685, 103, 'second_section_benefit_list_3_benefit_image', '161'),
(686, 103, '_second_section_benefit_list_3_benefit_image', 'field_5f7da173099b3'),
(687, 103, 'second_section_benefit_list_3_benefit_text', 'SHIPPING'),
(688, 103, '_second_section_benefit_list_3_benefit_text', 'field_5f7da184099b4'),
(689, 103, 'second_section_benefit_list_4_benefit_image', '163'),
(690, 103, '_second_section_benefit_list_4_benefit_image', 'field_5f7da173099b3'),
(691, 103, 'second_section_benefit_list_4_benefit_text', 'AUTOMOTIVE'),
(692, 103, '_second_section_benefit_list_4_benefit_text', 'field_5f7da184099b4'),
(693, 103, 'second_section_benefit_list', '5'),
(694, 103, '_second_section_benefit_list', 'field_5f7da168099b2'),
(695, 103, 'second_section', ''),
(696, 103, '_second_section', 'field_5f7da0ddff9f2'),
(697, 166, 'first_section_title', 'Member Benefits'),
(698, 166, '_first_section_title', 'field_5f7d9c996c3c5'),
(699, 166, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(700, 166, '_first_section_description', 'field_5f7d9ca26c3c6'),
(701, 166, 'first_section_button_text', 'Become a Member'),
(702, 166, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(703, 166, 'first_section_button_link', 'https://www.netflix.com/'),
(704, 166, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(705, 166, 'first_section_benefit_icon_1_icon_image', '152'),
(706, 166, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(707, 166, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(708, 166, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(709, 166, 'first_section_benefit_icon_1', ''),
(710, 166, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(711, 166, 'first_section_benefit_icon_2_icon_image', '150'),
(712, 166, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(713, 166, 'first_section_benefit_icon_2_icon_text', 'GROW YOUR BUSINESS'),
(714, 166, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(715, 166, 'first_section_benefit_icon_2', ''),
(716, 166, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(717, 166, 'first_section_benefit_icon_3_icon_image', '151'),
(718, 166, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(719, 166, 'first_section_benefit_icon_3_icon_text', 'INCREASE NETWORKING'),
(720, 166, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(721, 166, 'first_section_benefit_icon_3', ''),
(722, 166, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(723, 166, 'first_section', ''),
(724, 166, '_first_section', 'field_5f7d9c2c6c3c4'),
(725, 166, 'second_section_list_0_list_item', '<i class=\"fa fa-heart\" aria-hidden=\"true\"></i> AFFINITY PROGRAM'),
(726, 166, '_second_section_list_0_list_item', 'field_5f7da111ff9f4'),
(727, 166, 'second_section_list_1_list_item', '<i class=\"fa fa-star\" aria-hidden=\"true\"></i> GENERAL MEMBER DISCOUNTS'),
(728, 166, '_second_section_list_1_list_item', 'field_5f7da111ff9f4'),
(729, 166, 'second_section_list_2_list_item', '<i class=\"fa fa-bullseye\" aria-hidden=\"true\"></i> LATEST PROMOTIONS'),
(730, 166, '_second_section_list_2_list_item', 'field_5f7da111ff9f4'),
(731, 166, 'second_section_list', '3'),
(732, 166, '_second_section_list', 'field_5f7da0eaff9f3'),
(733, 166, 'second_section_view_all_link', 'https://www.netflix.com/'),
(734, 166, '_second_section_view_all_link', 'field_5f7da12e099b1'),
(735, 166, 'second_section_benefit_list_0_benefit_image', '165'),
(736, 166, '_second_section_benefit_list_0_benefit_image', 'field_5f7da173099b3'),
(737, 166, 'second_section_benefit_list_0_benefit_text', 'GROW YOUR BUSINESS'),
(738, 166, '_second_section_benefit_list_0_benefit_text', 'field_5f7da184099b4'),
(739, 166, 'second_section_benefit_list_1_benefit_image', '162'),
(740, 166, '_second_section_benefit_list_1_benefit_image', 'field_5f7da173099b3'),
(741, 166, 'second_section_benefit_list_1_benefit_text', 'ACCOMMODATIONS'),
(742, 166, '_second_section_benefit_list_1_benefit_text', 'field_5f7da184099b4'),
(743, 166, 'second_section_benefit_list_2_benefit_image', '164'),
(744, 166, '_second_section_benefit_list_2_benefit_image', 'field_5f7da173099b3'),
(745, 166, 'second_section_benefit_list_2_benefit_text', 'INSURANCE'),
(746, 166, '_second_section_benefit_list_2_benefit_text', 'field_5f7da184099b4'),
(747, 166, 'second_section_benefit_list_3_benefit_image', '161'),
(748, 166, '_second_section_benefit_list_3_benefit_image', 'field_5f7da173099b3'),
(749, 166, 'second_section_benefit_list_3_benefit_text', 'SHIPPING'),
(750, 166, '_second_section_benefit_list_3_benefit_text', 'field_5f7da184099b4'),
(751, 166, 'second_section_benefit_list_4_benefit_image', '163'),
(752, 166, '_second_section_benefit_list_4_benefit_image', 'field_5f7da173099b3'),
(753, 166, 'second_section_benefit_list_4_benefit_text', 'AUTOMOTIVE'),
(754, 166, '_second_section_benefit_list_4_benefit_text', 'field_5f7da184099b4'),
(755, 166, 'second_section_benefit_list', '5'),
(756, 166, '_second_section_benefit_list', 'field_5f7da168099b2'),
(757, 166, 'second_section', ''),
(758, 166, '_second_section', 'field_5f7da0ddff9f2'),
(759, 176, '_wp_attached_file', '2020/10/LeGrow.png'),
(760, 176, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:227;s:6:\"height\";i:38;s:4:\"file\";s:18:\"2020/10/LeGrow.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"LeGrow-150x38.png\";s:5:\"width\";i:150;s:6:\"height\";i:38;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"LeGrow-100x38.png\";s:5:\"width\";i:100;s:6:\"height\";i:38;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"LeGrow-100x38.png\";s:5:\"width\";i:100;s:6:\"height\";i:38;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(761, 103, 'third_section_0_companys_logo', '176'),
(762, 103, '_third_section_0_companys_logo', 'field_5f7da570885db'),
(763, 103, 'third_section_0_company_name', 'LEGROW’S TRAVEL'),
(764, 103, '_third_section_0_company_name', 'field_5f7da585885dc'),
(765, 103, 'third_section_0_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(766, 103, '_third_section_0_description', 'field_5f7da595885dd'),
(767, 103, 'third_section_0_join_now_link', 'https://www.netflix.com/'),
(768, 103, '_third_section_0_join_now_link', 'field_5f7da5a5885de'),
(769, 103, 'third_section_0_view_button_link', 'https://www.netflix.com/'),
(770, 103, '_third_section_0_view_button_link', 'field_5f7da5e9885df'),
(771, 103, 'third_section_0_backside_offer_text', '$50.00 off a vacation package booking'),
(772, 103, '_third_section_0_backside_offer_text', 'field_5f7da5f7885e0'),
(773, 103, 'third_section_0_offer_validity', 'July 2022'),
(774, 103, '_third_section_0_offer_validity', 'field_5f7da616885e1'),
(775, 103, 'third_section_0_view_offer_link', 'https://www.netflix.com/'),
(776, 103, '_third_section_0_view_offer_link', 'field_5f7da61e885e2'),
(777, 103, 'third_section_1_companys_logo', '176'),
(778, 103, '_third_section_1_companys_logo', 'field_5f7da570885db'),
(779, 103, 'third_section_1_company_name', 'LEGROW’S TRAVEL'),
(780, 103, '_third_section_1_company_name', 'field_5f7da585885dc'),
(781, 103, 'third_section_1_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(782, 103, '_third_section_1_description', 'field_5f7da595885dd'),
(783, 103, 'third_section_1_join_now_link', 'https://www.netflix.com/'),
(784, 103, '_third_section_1_join_now_link', 'field_5f7da5a5885de'),
(785, 103, 'third_section_1_view_button_link', 'https://www.netflix.com/'),
(786, 103, '_third_section_1_view_button_link', 'field_5f7da5e9885df'),
(787, 103, 'third_section_1_backside_offer_text', '$50.00 off a vacation package booking'),
(788, 103, '_third_section_1_backside_offer_text', 'field_5f7da5f7885e0'),
(789, 103, 'third_section_1_offer_validity', 'July 2022'),
(790, 103, '_third_section_1_offer_validity', 'field_5f7da616885e1'),
(791, 103, 'third_section_1_view_offer_link', 'https://www.netflix.com/'),
(792, 103, '_third_section_1_view_offer_link', 'field_5f7da61e885e2'),
(793, 103, 'third_section', '2'),
(794, 103, '_third_section', 'field_5f7da553885da'),
(795, 177, 'first_section_title', 'Member Benefits'),
(796, 177, '_first_section_title', 'field_5f7d9c996c3c5'),
(797, 177, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(798, 177, '_first_section_description', 'field_5f7d9ca26c3c6'),
(799, 177, 'first_section_button_text', 'Become a Member'),
(800, 177, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(801, 177, 'first_section_button_link', 'https://www.netflix.com/'),
(802, 177, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(803, 177, 'first_section_benefit_icon_1_icon_image', '152'),
(804, 177, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(805, 177, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(806, 177, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(807, 177, 'first_section_benefit_icon_1', ''),
(808, 177, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(809, 177, 'first_section_benefit_icon_2_icon_image', '150'),
(810, 177, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(811, 177, 'first_section_benefit_icon_2_icon_text', 'GROW YOUR BUSINESS'),
(812, 177, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(813, 177, 'first_section_benefit_icon_2', ''),
(814, 177, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(815, 177, 'first_section_benefit_icon_3_icon_image', '151'),
(816, 177, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(817, 177, 'first_section_benefit_icon_3_icon_text', 'INCREASE NETWORKING'),
(818, 177, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(819, 177, 'first_section_benefit_icon_3', ''),
(820, 177, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(821, 177, 'first_section', ''),
(822, 177, '_first_section', 'field_5f7d9c2c6c3c4'),
(823, 177, 'second_section_list_0_list_item', '<i class=\"fa fa-heart\" aria-hidden=\"true\"></i> AFFINITY PROGRAM'),
(824, 177, '_second_section_list_0_list_item', 'field_5f7da111ff9f4'),
(825, 177, 'second_section_list_1_list_item', '<i class=\"fa fa-star\" aria-hidden=\"true\"></i> GENERAL MEMBER DISCOUNTS'),
(826, 177, '_second_section_list_1_list_item', 'field_5f7da111ff9f4'),
(827, 177, 'second_section_list_2_list_item', '<i class=\"fa fa-bullseye\" aria-hidden=\"true\"></i> LATEST PROMOTIONS'),
(828, 177, '_second_section_list_2_list_item', 'field_5f7da111ff9f4'),
(829, 177, 'second_section_list', '3'),
(830, 177, '_second_section_list', 'field_5f7da0eaff9f3'),
(831, 177, 'second_section_view_all_link', 'https://www.netflix.com/'),
(832, 177, '_second_section_view_all_link', 'field_5f7da12e099b1'),
(833, 177, 'second_section_benefit_list_0_benefit_image', '165'),
(834, 177, '_second_section_benefit_list_0_benefit_image', 'field_5f7da173099b3'),
(835, 177, 'second_section_benefit_list_0_benefit_text', 'GROW YOUR BUSINESS'),
(836, 177, '_second_section_benefit_list_0_benefit_text', 'field_5f7da184099b4'),
(837, 177, 'second_section_benefit_list_1_benefit_image', '162'),
(838, 177, '_second_section_benefit_list_1_benefit_image', 'field_5f7da173099b3'),
(839, 177, 'second_section_benefit_list_1_benefit_text', 'ACCOMMODATIONS'),
(840, 177, '_second_section_benefit_list_1_benefit_text', 'field_5f7da184099b4'),
(841, 177, 'second_section_benefit_list_2_benefit_image', '164'),
(842, 177, '_second_section_benefit_list_2_benefit_image', 'field_5f7da173099b3'),
(843, 177, 'second_section_benefit_list_2_benefit_text', 'INSURANCE'),
(844, 177, '_second_section_benefit_list_2_benefit_text', 'field_5f7da184099b4'),
(845, 177, 'second_section_benefit_list_3_benefit_image', '161'),
(846, 177, '_second_section_benefit_list_3_benefit_image', 'field_5f7da173099b3'),
(847, 177, 'second_section_benefit_list_3_benefit_text', 'SHIPPING'),
(848, 177, '_second_section_benefit_list_3_benefit_text', 'field_5f7da184099b4'),
(849, 177, 'second_section_benefit_list_4_benefit_image', '163'),
(850, 177, '_second_section_benefit_list_4_benefit_image', 'field_5f7da173099b3'),
(851, 177, 'second_section_benefit_list_4_benefit_text', 'AUTOMOTIVE'),
(852, 177, '_second_section_benefit_list_4_benefit_text', 'field_5f7da184099b4'),
(853, 177, 'second_section_benefit_list', '5'),
(854, 177, '_second_section_benefit_list', 'field_5f7da168099b2'),
(855, 177, 'second_section', ''),
(856, 177, '_second_section', 'field_5f7da0ddff9f2'),
(857, 177, 'third_section_0_companys_logo', '176'),
(858, 177, '_third_section_0_companys_logo', 'field_5f7da570885db'),
(859, 177, 'third_section_0_company_name', 'LEGROW’S TRAVEL'),
(860, 177, '_third_section_0_company_name', 'field_5f7da585885dc'),
(861, 177, 'third_section_0_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(862, 177, '_third_section_0_description', 'field_5f7da595885dd'),
(863, 177, 'third_section_0_join_now_link', 'https://www.netflix.com/'),
(864, 177, '_third_section_0_join_now_link', 'field_5f7da5a5885de'),
(865, 177, 'third_section_0_view_button_link', 'https://www.netflix.com/'),
(866, 177, '_third_section_0_view_button_link', 'field_5f7da5e9885df'),
(867, 177, 'third_section_0_backside_offer_text', '$50.00 off a vacation package booking'),
(868, 177, '_third_section_0_backside_offer_text', 'field_5f7da5f7885e0'),
(869, 177, 'third_section_0_offer_validity', 'July 2022'),
(870, 177, '_third_section_0_offer_validity', 'field_5f7da616885e1'),
(871, 177, 'third_section_0_view_offer_link', 'https://www.netflix.com/'),
(872, 177, '_third_section_0_view_offer_link', 'field_5f7da61e885e2'),
(873, 177, 'third_section_1_companys_logo', '176'),
(874, 177, '_third_section_1_companys_logo', 'field_5f7da570885db'),
(875, 177, 'third_section_1_company_name', 'LEGROW’S TRAVEL'),
(876, 177, '_third_section_1_company_name', 'field_5f7da585885dc'),
(877, 177, 'third_section_1_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(878, 177, '_third_section_1_description', 'field_5f7da595885dd'),
(879, 177, 'third_section_1_join_now_link', 'https://www.netflix.com/'),
(880, 177, '_third_section_1_join_now_link', 'field_5f7da5a5885de'),
(881, 177, 'third_section_1_view_button_link', 'https://www.netflix.com/'),
(882, 177, '_third_section_1_view_button_link', 'field_5f7da5e9885df'),
(883, 177, 'third_section_1_backside_offer_text', '$50.00 off a vacation package booking'),
(884, 177, '_third_section_1_backside_offer_text', 'field_5f7da5f7885e0'),
(885, 177, 'third_section_1_offer_validity', 'July 2022'),
(886, 177, '_third_section_1_offer_validity', 'field_5f7da616885e1'),
(887, 177, 'third_section_1_view_offer_link', 'https://www.netflix.com/'),
(888, 177, '_third_section_1_view_offer_link', 'field_5f7da61e885e2'),
(889, 177, 'third_section', '2'),
(890, 177, '_third_section', 'field_5f7da553885da'),
(891, 103, 'fourth_section_0_companys_logo', '176'),
(892, 103, '_fourth_section_0_companys_logo', 'field_5f7dac59665c7'),
(893, 103, 'fourth_section_0_company_name', 'LEGROW’S TRAVEL'),
(894, 103, '_fourth_section_0_company_name', 'field_5f7dac59665c8'),
(895, 103, 'fourth_section_0_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(896, 103, '_fourth_section_0_description', 'field_5f7dac59665c9'),
(897, 103, 'fourth_section_0_join_now_link', 'https://www.netflix.com/'),
(898, 103, '_fourth_section_0_join_now_link', 'field_5f7dac59665ca'),
(899, 103, 'fourth_section_0_view_button_link', 'https://www.netflix.com/'),
(900, 103, '_fourth_section_0_view_button_link', 'field_5f7dac59665cb'),
(901, 103, 'fourth_section_0_backside_offer_text', '$75.00 off a vacation package booking'),
(902, 103, '_fourth_section_0_backside_offer_text', 'field_5f7dac59665cc'),
(903, 103, 'fourth_section_0_offer_validity', 'June 2022'),
(904, 103, '_fourth_section_0_offer_validity', 'field_5f7dac59665cd'),
(905, 103, 'fourth_section_0_view_offer_link', 'https://www.netflix.com/'),
(906, 103, '_fourth_section_0_view_offer_link', 'field_5f7dac59665ce'),
(907, 103, 'fourth_section', '4'),
(908, 103, '_fourth_section', 'field_5f7dac59665c6'),
(909, 187, 'first_section_title', 'Member Benefits'),
(910, 187, '_first_section_title', 'field_5f7d9c996c3c5'),
(911, 187, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(912, 187, '_first_section_description', 'field_5f7d9ca26c3c6'),
(913, 187, 'first_section_button_text', 'Become a Member'),
(914, 187, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(915, 187, 'first_section_button_link', 'https://www.netflix.com/'),
(916, 187, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(917, 187, 'first_section_benefit_icon_1_icon_image', '152'),
(918, 187, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(919, 187, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(920, 187, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(921, 187, 'first_section_benefit_icon_1', ''),
(922, 187, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(923, 187, 'first_section_benefit_icon_2_icon_image', '150'),
(924, 187, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(925, 187, 'first_section_benefit_icon_2_icon_text', 'GROW YOUR BUSINESS'),
(926, 187, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(927, 187, 'first_section_benefit_icon_2', ''),
(928, 187, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(929, 187, 'first_section_benefit_icon_3_icon_image', '151'),
(930, 187, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(931, 187, 'first_section_benefit_icon_3_icon_text', 'INCREASE NETWORKING'),
(932, 187, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(933, 187, 'first_section_benefit_icon_3', ''),
(934, 187, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(935, 187, 'first_section', ''),
(936, 187, '_first_section', 'field_5f7d9c2c6c3c4'),
(937, 187, 'second_section_list_0_list_item', '<i class=\"fa fa-heart\" aria-hidden=\"true\"></i> AFFINITY PROGRAM'),
(938, 187, '_second_section_list_0_list_item', 'field_5f7da111ff9f4'),
(939, 187, 'second_section_list_1_list_item', '<i class=\"fa fa-star\" aria-hidden=\"true\"></i> GENERAL MEMBER DISCOUNTS'),
(940, 187, '_second_section_list_1_list_item', 'field_5f7da111ff9f4'),
(941, 187, 'second_section_list_2_list_item', '<i class=\"fa fa-bullseye\" aria-hidden=\"true\"></i> LATEST PROMOTIONS'),
(942, 187, '_second_section_list_2_list_item', 'field_5f7da111ff9f4'),
(943, 187, 'second_section_list', '3'),
(944, 187, '_second_section_list', 'field_5f7da0eaff9f3'),
(945, 187, 'second_section_view_all_link', 'https://www.netflix.com/'),
(946, 187, '_second_section_view_all_link', 'field_5f7da12e099b1'),
(947, 187, 'second_section_benefit_list_0_benefit_image', '165'),
(948, 187, '_second_section_benefit_list_0_benefit_image', 'field_5f7da173099b3'),
(949, 187, 'second_section_benefit_list_0_benefit_text', 'GROW YOUR BUSINESS'),
(950, 187, '_second_section_benefit_list_0_benefit_text', 'field_5f7da184099b4'),
(951, 187, 'second_section_benefit_list_1_benefit_image', '162'),
(952, 187, '_second_section_benefit_list_1_benefit_image', 'field_5f7da173099b3'),
(953, 187, 'second_section_benefit_list_1_benefit_text', 'ACCOMMODATIONS'),
(954, 187, '_second_section_benefit_list_1_benefit_text', 'field_5f7da184099b4'),
(955, 187, 'second_section_benefit_list_2_benefit_image', '164'),
(956, 187, '_second_section_benefit_list_2_benefit_image', 'field_5f7da173099b3'),
(957, 187, 'second_section_benefit_list_2_benefit_text', 'INSURANCE'),
(958, 187, '_second_section_benefit_list_2_benefit_text', 'field_5f7da184099b4'),
(959, 187, 'second_section_benefit_list_3_benefit_image', '161'),
(960, 187, '_second_section_benefit_list_3_benefit_image', 'field_5f7da173099b3'),
(961, 187, 'second_section_benefit_list_3_benefit_text', 'SHIPPING'),
(962, 187, '_second_section_benefit_list_3_benefit_text', 'field_5f7da184099b4'),
(963, 187, 'second_section_benefit_list_4_benefit_image', '163'),
(964, 187, '_second_section_benefit_list_4_benefit_image', 'field_5f7da173099b3'),
(965, 187, 'second_section_benefit_list_4_benefit_text', 'AUTOMOTIVE'),
(966, 187, '_second_section_benefit_list_4_benefit_text', 'field_5f7da184099b4'),
(967, 187, 'second_section_benefit_list', '5'),
(968, 187, '_second_section_benefit_list', 'field_5f7da168099b2'),
(969, 187, 'second_section', ''),
(970, 187, '_second_section', 'field_5f7da0ddff9f2'),
(971, 187, 'third_section_0_companys_logo', '176'),
(972, 187, '_third_section_0_companys_logo', 'field_5f7da570885db'),
(973, 187, 'third_section_0_company_name', 'LEGROW’S TRAVEL'),
(974, 187, '_third_section_0_company_name', 'field_5f7da585885dc'),
(975, 187, 'third_section_0_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(976, 187, '_third_section_0_description', 'field_5f7da595885dd'),
(977, 187, 'third_section_0_join_now_link', 'https://www.netflix.com/'),
(978, 187, '_third_section_0_join_now_link', 'field_5f7da5a5885de'),
(979, 187, 'third_section_0_view_button_link', 'https://www.netflix.com/'),
(980, 187, '_third_section_0_view_button_link', 'field_5f7da5e9885df'),
(981, 187, 'third_section_0_backside_offer_text', '$50.00 off a vacation package booking'),
(982, 187, '_third_section_0_backside_offer_text', 'field_5f7da5f7885e0'),
(983, 187, 'third_section_0_offer_validity', 'July 2022'),
(984, 187, '_third_section_0_offer_validity', 'field_5f7da616885e1'),
(985, 187, 'third_section_0_view_offer_link', 'https://www.netflix.com/'),
(986, 187, '_third_section_0_view_offer_link', 'field_5f7da61e885e2'),
(987, 187, 'third_section_1_companys_logo', '176'),
(988, 187, '_third_section_1_companys_logo', 'field_5f7da570885db'),
(989, 187, 'third_section_1_company_name', 'LEGROW’S TRAVEL'),
(990, 187, '_third_section_1_company_name', 'field_5f7da585885dc'),
(991, 187, 'third_section_1_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(992, 187, '_third_section_1_description', 'field_5f7da595885dd'),
(993, 187, 'third_section_1_join_now_link', 'https://www.netflix.com/'),
(994, 187, '_third_section_1_join_now_link', 'field_5f7da5a5885de'),
(995, 187, 'third_section_1_view_button_link', 'https://www.netflix.com/'),
(996, 187, '_third_section_1_view_button_link', 'field_5f7da5e9885df'),
(997, 187, 'third_section_1_backside_offer_text', '$50.00 off a vacation package booking'),
(998, 187, '_third_section_1_backside_offer_text', 'field_5f7da5f7885e0'),
(999, 187, 'third_section_1_offer_validity', 'July 2022'),
(1000, 187, '_third_section_1_offer_validity', 'field_5f7da616885e1'),
(1001, 187, 'third_section_1_view_offer_link', 'https://www.netflix.com/'),
(1002, 187, '_third_section_1_view_offer_link', 'field_5f7da61e885e2'),
(1003, 187, 'third_section', '2'),
(1004, 187, '_third_section', 'field_5f7da553885da'),
(1005, 187, 'fourth_section_0_companys_logo', '176'),
(1006, 187, '_fourth_section_0_companys_logo', 'field_5f7dac59665c7'),
(1007, 187, 'fourth_section_0_company_name', 'LEGROW’S TRAVEL'),
(1008, 187, '_fourth_section_0_company_name', 'field_5f7dac59665c8'),
(1009, 187, 'fourth_section_0_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1010, 187, '_fourth_section_0_description', 'field_5f7dac59665c9'),
(1011, 187, 'fourth_section_0_join_now_link', 'https://www.netflix.com/'),
(1012, 187, '_fourth_section_0_join_now_link', 'field_5f7dac59665ca'),
(1013, 187, 'fourth_section_0_view_button_link', 'https://www.netflix.com/'),
(1014, 187, '_fourth_section_0_view_button_link', 'field_5f7dac59665cb'),
(1015, 187, 'fourth_section_0_backside_offer_text', '$75.00 off a vacation package booking'),
(1016, 187, '_fourth_section_0_backside_offer_text', 'field_5f7dac59665cc'),
(1017, 187, 'fourth_section_0_offer_validity', 'June 2022'),
(1018, 187, '_fourth_section_0_offer_validity', 'field_5f7dac59665cd'),
(1019, 187, 'fourth_section_0_view_offer_link', 'https://www.netflix.com/'),
(1020, 187, '_fourth_section_0_view_offer_link', 'field_5f7dac59665ce'),
(1021, 187, 'fourth_section', '1'),
(1022, 187, '_fourth_section', 'field_5f7dac59665c6'),
(1023, 103, 'fourth_section_1_companys_logo', '176'),
(1024, 103, '_fourth_section_1_companys_logo', 'field_5f7dac59665c7'),
(1025, 103, 'fourth_section_1_company_name', 'LEGROW’S TRAVEL'),
(1026, 103, '_fourth_section_1_company_name', 'field_5f7dac59665c8'),
(1027, 103, 'fourth_section_1_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1028, 103, '_fourth_section_1_description', 'field_5f7dac59665c9'),
(1029, 103, 'fourth_section_1_join_now_link', 'https://www.netflix.com/'),
(1030, 103, '_fourth_section_1_join_now_link', 'field_5f7dac59665ca'),
(1031, 103, 'fourth_section_1_view_button_link', 'https://www.netflix.com/'),
(1032, 103, '_fourth_section_1_view_button_link', 'field_5f7dac59665cb'),
(1033, 103, 'fourth_section_1_backside_offer_text', '$75.00 off a vacation package booking'),
(1034, 103, '_fourth_section_1_backside_offer_text', 'field_5f7dac59665cc'),
(1035, 103, 'fourth_section_1_offer_validity', 'June 2021'),
(1036, 103, '_fourth_section_1_offer_validity', 'field_5f7dac59665cd'),
(1037, 103, 'fourth_section_1_view_offer_link', 'https://www.netflix.com/'),
(1038, 103, '_fourth_section_1_view_offer_link', 'field_5f7dac59665ce'),
(1039, 103, 'fourth_section_2_companys_logo', '176'),
(1040, 103, '_fourth_section_2_companys_logo', 'field_5f7dac59665c7'),
(1041, 103, 'fourth_section_2_company_name', 'LEGROW’S TRAVEL'),
(1042, 103, '_fourth_section_2_company_name', 'field_5f7dac59665c8'),
(1043, 103, 'fourth_section_2_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1044, 103, '_fourth_section_2_description', 'field_5f7dac59665c9'),
(1045, 103, 'fourth_section_2_join_now_link', 'https://www.netflix.com/'),
(1046, 103, '_fourth_section_2_join_now_link', 'field_5f7dac59665ca'),
(1047, 103, 'fourth_section_2_view_button_link', 'https://www.netflix.com/'),
(1048, 103, '_fourth_section_2_view_button_link', 'field_5f7dac59665cb'),
(1049, 103, 'fourth_section_2_backside_offer_text', '$75.00 off a vacation package booking'),
(1050, 103, '_fourth_section_2_backside_offer_text', 'field_5f7dac59665cc'),
(1051, 103, 'fourth_section_2_offer_validity', 'June 2021'),
(1052, 103, '_fourth_section_2_offer_validity', 'field_5f7dac59665cd'),
(1053, 103, 'fourth_section_2_view_offer_link', 'https://www.netflix.com/'),
(1054, 103, '_fourth_section_2_view_offer_link', 'field_5f7dac59665ce'),
(1055, 103, 'fourth_section_3_companys_logo', '176'),
(1056, 103, '_fourth_section_3_companys_logo', 'field_5f7dac59665c7'),
(1057, 103, 'fourth_section_3_company_name', 'LEGROW’S TRAVEL'),
(1058, 103, '_fourth_section_3_company_name', 'field_5f7dac59665c8'),
(1059, 103, 'fourth_section_3_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1060, 103, '_fourth_section_3_description', 'field_5f7dac59665c9'),
(1061, 103, 'fourth_section_3_join_now_link', 'https://www.netflix.com/'),
(1062, 103, '_fourth_section_3_join_now_link', 'field_5f7dac59665ca'),
(1063, 103, 'fourth_section_3_view_button_link', 'https://www.netflix.com/'),
(1064, 103, '_fourth_section_3_view_button_link', 'field_5f7dac59665cb'),
(1065, 103, 'fourth_section_3_backside_offer_text', '$75.00 off a vacation package booking'),
(1066, 103, '_fourth_section_3_backside_offer_text', 'field_5f7dac59665cc'),
(1067, 103, 'fourth_section_3_offer_validity', 'June 2021'),
(1068, 103, '_fourth_section_3_offer_validity', 'field_5f7dac59665cd'),
(1069, 103, 'fourth_section_3_view_offer_link', 'https://www.netflix.com/'),
(1070, 103, '_fourth_section_3_view_offer_link', 'field_5f7dac59665ce'),
(1071, 188, 'first_section_title', 'Member Benefits'),
(1072, 188, '_first_section_title', 'field_5f7d9c996c3c5'),
(1073, 188, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(1074, 188, '_first_section_description', 'field_5f7d9ca26c3c6'),
(1075, 188, 'first_section_button_text', 'Become a Member'),
(1076, 188, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(1077, 188, 'first_section_button_link', 'https://www.netflix.com/'),
(1078, 188, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(1079, 188, 'first_section_benefit_icon_1_icon_image', '152'),
(1080, 188, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(1081, 188, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(1082, 188, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(1083, 188, 'first_section_benefit_icon_1', ''),
(1084, 188, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(1085, 188, 'first_section_benefit_icon_2_icon_image', '150'),
(1086, 188, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(1087, 188, 'first_section_benefit_icon_2_icon_text', 'GROW YOUR BUSINESS'),
(1088, 188, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(1089, 188, 'first_section_benefit_icon_2', ''),
(1090, 188, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(1091, 188, 'first_section_benefit_icon_3_icon_image', '151'),
(1092, 188, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(1093, 188, 'first_section_benefit_icon_3_icon_text', 'INCREASE NETWORKING'),
(1094, 188, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(1095, 188, 'first_section_benefit_icon_3', ''),
(1096, 188, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(1097, 188, 'first_section', ''),
(1098, 188, '_first_section', 'field_5f7d9c2c6c3c4'),
(1099, 188, 'second_section_list_0_list_item', '<i class=\"fa fa-heart\" aria-hidden=\"true\"></i> AFFINITY PROGRAM'),
(1100, 188, '_second_section_list_0_list_item', 'field_5f7da111ff9f4'),
(1101, 188, 'second_section_list_1_list_item', '<i class=\"fa fa-star\" aria-hidden=\"true\"></i> GENERAL MEMBER DISCOUNTS'),
(1102, 188, '_second_section_list_1_list_item', 'field_5f7da111ff9f4'),
(1103, 188, 'second_section_list_2_list_item', '<i class=\"fa fa-bullseye\" aria-hidden=\"true\"></i> LATEST PROMOTIONS'),
(1104, 188, '_second_section_list_2_list_item', 'field_5f7da111ff9f4'),
(1105, 188, 'second_section_list', '3'),
(1106, 188, '_second_section_list', 'field_5f7da0eaff9f3'),
(1107, 188, 'second_section_view_all_link', 'https://www.netflix.com/'),
(1108, 188, '_second_section_view_all_link', 'field_5f7da12e099b1'),
(1109, 188, 'second_section_benefit_list_0_benefit_image', '165'),
(1110, 188, '_second_section_benefit_list_0_benefit_image', 'field_5f7da173099b3'),
(1111, 188, 'second_section_benefit_list_0_benefit_text', 'GROW YOUR BUSINESS'),
(1112, 188, '_second_section_benefit_list_0_benefit_text', 'field_5f7da184099b4'),
(1113, 188, 'second_section_benefit_list_1_benefit_image', '162'),
(1114, 188, '_second_section_benefit_list_1_benefit_image', 'field_5f7da173099b3'),
(1115, 188, 'second_section_benefit_list_1_benefit_text', 'ACCOMMODATIONS'),
(1116, 188, '_second_section_benefit_list_1_benefit_text', 'field_5f7da184099b4'),
(1117, 188, 'second_section_benefit_list_2_benefit_image', '164'),
(1118, 188, '_second_section_benefit_list_2_benefit_image', 'field_5f7da173099b3'),
(1119, 188, 'second_section_benefit_list_2_benefit_text', 'INSURANCE'),
(1120, 188, '_second_section_benefit_list_2_benefit_text', 'field_5f7da184099b4'),
(1121, 188, 'second_section_benefit_list_3_benefit_image', '161'),
(1122, 188, '_second_section_benefit_list_3_benefit_image', 'field_5f7da173099b3'),
(1123, 188, 'second_section_benefit_list_3_benefit_text', 'SHIPPING'),
(1124, 188, '_second_section_benefit_list_3_benefit_text', 'field_5f7da184099b4'),
(1125, 188, 'second_section_benefit_list_4_benefit_image', '163'),
(1126, 188, '_second_section_benefit_list_4_benefit_image', 'field_5f7da173099b3'),
(1127, 188, 'second_section_benefit_list_4_benefit_text', 'AUTOMOTIVE'),
(1128, 188, '_second_section_benefit_list_4_benefit_text', 'field_5f7da184099b4'),
(1129, 188, 'second_section_benefit_list', '5'),
(1130, 188, '_second_section_benefit_list', 'field_5f7da168099b2'),
(1131, 188, 'second_section', ''),
(1132, 188, '_second_section', 'field_5f7da0ddff9f2'),
(1133, 188, 'third_section_0_companys_logo', '176'),
(1134, 188, '_third_section_0_companys_logo', 'field_5f7da570885db'),
(1135, 188, 'third_section_0_company_name', 'LEGROW’S TRAVEL'),
(1136, 188, '_third_section_0_company_name', 'field_5f7da585885dc'),
(1137, 188, 'third_section_0_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1138, 188, '_third_section_0_description', 'field_5f7da595885dd'),
(1139, 188, 'third_section_0_join_now_link', 'https://www.netflix.com/'),
(1140, 188, '_third_section_0_join_now_link', 'field_5f7da5a5885de'),
(1141, 188, 'third_section_0_view_button_link', 'https://www.netflix.com/'),
(1142, 188, '_third_section_0_view_button_link', 'field_5f7da5e9885df'),
(1143, 188, 'third_section_0_backside_offer_text', '$50.00 off a vacation package booking'),
(1144, 188, '_third_section_0_backside_offer_text', 'field_5f7da5f7885e0'),
(1145, 188, 'third_section_0_offer_validity', 'July 2022'),
(1146, 188, '_third_section_0_offer_validity', 'field_5f7da616885e1'),
(1147, 188, 'third_section_0_view_offer_link', 'https://www.netflix.com/'),
(1148, 188, '_third_section_0_view_offer_link', 'field_5f7da61e885e2'),
(1149, 188, 'third_section_1_companys_logo', '176'),
(1150, 188, '_third_section_1_companys_logo', 'field_5f7da570885db'),
(1151, 188, 'third_section_1_company_name', 'LEGROW’S TRAVEL'),
(1152, 188, '_third_section_1_company_name', 'field_5f7da585885dc'),
(1153, 188, 'third_section_1_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1154, 188, '_third_section_1_description', 'field_5f7da595885dd'),
(1155, 188, 'third_section_1_join_now_link', 'https://www.netflix.com/'),
(1156, 188, '_third_section_1_join_now_link', 'field_5f7da5a5885de'),
(1157, 188, 'third_section_1_view_button_link', 'https://www.netflix.com/'),
(1158, 188, '_third_section_1_view_button_link', 'field_5f7da5e9885df'),
(1159, 188, 'third_section_1_backside_offer_text', '$50.00 off a vacation package booking'),
(1160, 188, '_third_section_1_backside_offer_text', 'field_5f7da5f7885e0'),
(1161, 188, 'third_section_1_offer_validity', 'July 2022'),
(1162, 188, '_third_section_1_offer_validity', 'field_5f7da616885e1'),
(1163, 188, 'third_section_1_view_offer_link', 'https://www.netflix.com/'),
(1164, 188, '_third_section_1_view_offer_link', 'field_5f7da61e885e2'),
(1165, 188, 'third_section', '2'),
(1166, 188, '_third_section', 'field_5f7da553885da'),
(1167, 188, 'fourth_section_0_companys_logo', '176'),
(1168, 188, '_fourth_section_0_companys_logo', 'field_5f7dac59665c7'),
(1169, 188, 'fourth_section_0_company_name', 'LEGROW’S TRAVEL'),
(1170, 188, '_fourth_section_0_company_name', 'field_5f7dac59665c8'),
(1171, 188, 'fourth_section_0_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1172, 188, '_fourth_section_0_description', 'field_5f7dac59665c9'),
(1173, 188, 'fourth_section_0_join_now_link', 'https://www.netflix.com/'),
(1174, 188, '_fourth_section_0_join_now_link', 'field_5f7dac59665ca'),
(1175, 188, 'fourth_section_0_view_button_link', 'https://www.netflix.com/'),
(1176, 188, '_fourth_section_0_view_button_link', 'field_5f7dac59665cb'),
(1177, 188, 'fourth_section_0_backside_offer_text', '$75.00 off a vacation package booking'),
(1178, 188, '_fourth_section_0_backside_offer_text', 'field_5f7dac59665cc'),
(1179, 188, 'fourth_section_0_offer_validity', 'June 2022'),
(1180, 188, '_fourth_section_0_offer_validity', 'field_5f7dac59665cd'),
(1181, 188, 'fourth_section_0_view_offer_link', 'https://www.netflix.com/'),
(1182, 188, '_fourth_section_0_view_offer_link', 'field_5f7dac59665ce'),
(1183, 188, 'fourth_section', '4'),
(1184, 188, '_fourth_section', 'field_5f7dac59665c6'),
(1185, 188, 'fourth_section_1_companys_logo', '176'),
(1186, 188, '_fourth_section_1_companys_logo', 'field_5f7dac59665c7'),
(1187, 188, 'fourth_section_1_company_name', 'LEGROW’S TRAVEL'),
(1188, 188, '_fourth_section_1_company_name', 'field_5f7dac59665c8'),
(1189, 188, 'fourth_section_1_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1190, 188, '_fourth_section_1_description', 'field_5f7dac59665c9'),
(1191, 188, 'fourth_section_1_join_now_link', 'https://www.netflix.com/'),
(1192, 188, '_fourth_section_1_join_now_link', 'field_5f7dac59665ca'),
(1193, 188, 'fourth_section_1_view_button_link', 'https://www.netflix.com/'),
(1194, 188, '_fourth_section_1_view_button_link', 'field_5f7dac59665cb'),
(1195, 188, 'fourth_section_1_backside_offer_text', '$75.00 off a vacation package booking'),
(1196, 188, '_fourth_section_1_backside_offer_text', 'field_5f7dac59665cc'),
(1197, 188, 'fourth_section_1_offer_validity', 'June 2021'),
(1198, 188, '_fourth_section_1_offer_validity', 'field_5f7dac59665cd'),
(1199, 188, 'fourth_section_1_view_offer_link', 'https://www.netflix.com/'),
(1200, 188, '_fourth_section_1_view_offer_link', 'field_5f7dac59665ce'),
(1201, 188, 'fourth_section_2_companys_logo', '176'),
(1202, 188, '_fourth_section_2_companys_logo', 'field_5f7dac59665c7'),
(1203, 188, 'fourth_section_2_company_name', 'LEGROW’S TRAVEL'),
(1204, 188, '_fourth_section_2_company_name', 'field_5f7dac59665c8'),
(1205, 188, 'fourth_section_2_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1206, 188, '_fourth_section_2_description', 'field_5f7dac59665c9'),
(1207, 188, 'fourth_section_2_join_now_link', 'https://www.netflix.com/'),
(1208, 188, '_fourth_section_2_join_now_link', 'field_5f7dac59665ca'),
(1209, 188, 'fourth_section_2_view_button_link', 'https://www.netflix.com/'),
(1210, 188, '_fourth_section_2_view_button_link', 'field_5f7dac59665cb'),
(1211, 188, 'fourth_section_2_backside_offer_text', '$75.00 off a vacation package booking'),
(1212, 188, '_fourth_section_2_backside_offer_text', 'field_5f7dac59665cc'),
(1213, 188, 'fourth_section_2_offer_validity', 'June 2021'),
(1214, 188, '_fourth_section_2_offer_validity', 'field_5f7dac59665cd'),
(1215, 188, 'fourth_section_2_view_offer_link', 'https://www.netflix.com/'),
(1216, 188, '_fourth_section_2_view_offer_link', 'field_5f7dac59665ce'),
(1217, 188, 'fourth_section_3_companys_logo', '176'),
(1218, 188, '_fourth_section_3_companys_logo', 'field_5f7dac59665c7'),
(1219, 188, 'fourth_section_3_company_name', 'LEGROW’S TRAVEL'),
(1220, 188, '_fourth_section_3_company_name', 'field_5f7dac59665c8'),
(1221, 188, 'fourth_section_3_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus'),
(1222, 188, '_fourth_section_3_description', 'field_5f7dac59665c9'),
(1223, 188, 'fourth_section_3_join_now_link', 'https://www.netflix.com/'),
(1224, 188, '_fourth_section_3_join_now_link', 'field_5f7dac59665ca'),
(1225, 188, 'fourth_section_3_view_button_link', 'https://www.netflix.com/'),
(1226, 188, '_fourth_section_3_view_button_link', 'field_5f7dac59665cb'),
(1227, 188, 'fourth_section_3_backside_offer_text', '$75.00 off a vacation package booking'),
(1228, 188, '_fourth_section_3_backside_offer_text', 'field_5f7dac59665cc'),
(1229, 188, 'fourth_section_3_offer_validity', 'June 2021'),
(1230, 188, '_fourth_section_3_offer_validity', 'field_5f7dac59665cd'),
(1231, 188, 'fourth_section_3_view_offer_link', 'https://www.netflix.com/'),
(1232, 188, '_fourth_section_3_view_offer_link', 'field_5f7dac59665ce'),
(1233, 189, '_menu_item_type', 'post_type'),
(1234, 189, '_menu_item_menu_item_parent', '0'),
(1235, 189, '_menu_item_object_id', '103'),
(1236, 189, '_menu_item_object', 'page'),
(1237, 189, '_menu_item_target', ''),
(1238, 189, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1239, 189, '_menu_item_xfn', ''),
(1240, 189, '_menu_item_url', ''),
(1242, 190, '_menu_item_type', 'post_type'),
(1243, 190, '_menu_item_menu_item_parent', '0'),
(1244, 190, '_menu_item_object_id', '7'),
(1245, 190, '_menu_item_object', 'page'),
(1246, 190, '_menu_item_target', ''),
(1247, 190, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1248, 190, '_menu_item_xfn', ''),
(1249, 190, '_menu_item_url', ''),
(1251, 191, '_menu_item_type', 'custom'),
(1252, 191, '_menu_item_menu_item_parent', '189'),
(1253, 191, '_menu_item_object_id', '191'),
(1254, 191, '_menu_item_object', 'custom'),
(1255, 191, '_menu_item_target', ''),
(1256, 191, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1257, 191, '_menu_item_xfn', ''),
(1258, 191, '_menu_item_url', '#'),
(1260, 192, '_menu_item_type', 'custom'),
(1261, 192, '_menu_item_menu_item_parent', '189'),
(1262, 192, '_menu_item_object_id', '192'),
(1263, 192, '_menu_item_object', 'custom'),
(1264, 192, '_menu_item_target', ''),
(1265, 192, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1266, 192, '_menu_item_xfn', ''),
(1267, 192, '_menu_item_url', '#'),
(1269, 193, '_menu_item_type', 'custom'),
(1270, 193, '_menu_item_menu_item_parent', '189'),
(1271, 193, '_menu_item_object_id', '193'),
(1272, 193, '_menu_item_object', 'custom'),
(1273, 193, '_menu_item_target', ''),
(1274, 193, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1275, 193, '_menu_item_xfn', ''),
(1276, 193, '_menu_item_url', '#'),
(1278, 194, '_menu_item_type', 'custom'),
(1279, 194, '_menu_item_menu_item_parent', '0'),
(1280, 194, '_menu_item_object_id', '194'),
(1281, 194, '_menu_item_object', 'custom'),
(1282, 194, '_menu_item_target', ''),
(1283, 194, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1284, 194, '_menu_item_xfn', ''),
(1285, 194, '_menu_item_url', '#'),
(1287, 195, '_menu_item_type', 'custom'),
(1288, 195, '_menu_item_menu_item_parent', '194'),
(1289, 195, '_menu_item_object_id', '195'),
(1290, 195, '_menu_item_object', 'custom'),
(1291, 195, '_menu_item_target', ''),
(1292, 195, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1293, 195, '_menu_item_xfn', ''),
(1294, 195, '_menu_item_url', '#'),
(1296, 196, '_menu_item_type', 'custom'),
(1297, 196, '_menu_item_menu_item_parent', '194'),
(1298, 196, '_menu_item_object_id', '196'),
(1299, 196, '_menu_item_object', 'custom'),
(1300, 196, '_menu_item_target', ''),
(1301, 196, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1302, 196, '_menu_item_xfn', ''),
(1303, 196, '_menu_item_url', '#'),
(1305, 197, '_menu_item_type', 'post_type'),
(1306, 197, '_menu_item_menu_item_parent', '0'),
(1307, 197, '_menu_item_object_id', '69'),
(1308, 197, '_menu_item_object', 'page'),
(1309, 197, '_menu_item_target', ''),
(1310, 197, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1311, 197, '_menu_item_xfn', ''),
(1312, 197, '_menu_item_url', ''),
(1314, 198, '_menu_item_type', 'post_type'),
(1315, 198, '_menu_item_menu_item_parent', '0'),
(1316, 198, '_menu_item_object_id', '43'),
(1317, 198, '_menu_item_object', 'page'),
(1318, 198, '_menu_item_target', ''),
(1319, 198, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1320, 198, '_menu_item_xfn', ''),
(1321, 198, '_menu_item_url', ''),
(1323, 199, '_menu_item_type', 'custom'),
(1324, 199, '_menu_item_menu_item_parent', '0'),
(1325, 199, '_menu_item_object_id', '199'),
(1326, 199, '_menu_item_object', 'custom'),
(1327, 199, '_menu_item_target', ''),
(1328, 199, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1329, 199, '_menu_item_xfn', ''),
(1330, 199, '_menu_item_url', '#'),
(1332, 200, '_menu_item_type', 'custom'),
(1333, 200, '_menu_item_menu_item_parent', '0'),
(1334, 200, '_menu_item_object_id', '200'),
(1335, 200, '_menu_item_object', 'custom'),
(1336, 200, '_menu_item_target', ''),
(1337, 200, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1338, 200, '_menu_item_xfn', ''),
(1339, 200, '_menu_item_url', '#'),
(1341, 7, '_cta', 'none'),
(1342, 67, 'management_team-14', '7'),
(1343, 68, 'management_team-14', '7'),
(1344, 201, '_edit_last', '1'),
(1345, 201, '_wp_page_template', 'default'),
(1346, 201, 'extra-content', ''),
(1347, 201, '_edit_lock', '1602466834:1'),
(1348, 29, '_wp_trash_meta_status', 'publish'),
(1349, 29, '_wp_trash_meta_time', '1602466982'),
(1350, 29, '_wp_desired_post_slug', 'sponsors'),
(1351, 31, '_wp_trash_meta_status', 'publish'),
(1352, 31, '_wp_trash_meta_time', '1602466987'),
(1353, 31, '_wp_desired_post_slug', 'test-2'),
(1354, 38, '_wp_trash_meta_status', 'publish'),
(1355, 38, '_wp_trash_meta_time', '1602466993'),
(1356, 38, '_wp_desired_post_slug', 'test-page'),
(1357, 2, '_wp_trash_meta_status', 'publish'),
(1358, 2, '_wp_trash_meta_time', '1602467011');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1359, 2, '_wp_desired_post_slug', 'sample-page'),
(1360, 204, '_edit_last', '1'),
(1361, 204, '_edit_lock', '1602468210:1'),
(1362, 204, '_wp_page_template', 'template-members.php'),
(1363, 204, 'extra-content', '<del datetime=\"2020-10-12T02:00:40+00:00\"><ol>\r\n'),
(1364, 67, 'management_team-1', '7'),
(1365, 67, 'management_team-3', '103'),
(1366, 68, 'management_team-1', '7'),
(1367, 68, 'management_team-3', '103'),
(1368, 204, 'first_section_title', 'Member Benefits'),
(1369, 204, '_first_section_title', 'field_5f7d9c996c3c5'),
(1370, 204, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(1371, 204, '_first_section_description', 'field_5f7d9ca26c3c6'),
(1372, 204, 'first_section_button_text', 'Become a Member'),
(1373, 204, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(1374, 204, 'first_section_button_link', ''),
(1375, 204, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(1376, 204, 'first_section_benefit_icon_1_icon_image', ''),
(1377, 204, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(1378, 204, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(1379, 204, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(1380, 204, 'first_section_benefit_icon_1', ''),
(1381, 204, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(1382, 204, 'first_section_benefit_icon_2_icon_image', ''),
(1383, 204, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(1384, 204, 'first_section_benefit_icon_2_icon_text', ''),
(1385, 204, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(1386, 204, 'first_section_benefit_icon_2', ''),
(1387, 204, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(1388, 204, 'first_section_benefit_icon_3_icon_image', ''),
(1389, 204, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(1390, 204, 'first_section_benefit_icon_3_icon_text', ''),
(1391, 204, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(1392, 204, 'first_section_benefit_icon_3', ''),
(1393, 204, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(1394, 204, 'first_section', ''),
(1395, 204, '_first_section', 'field_5f7d9c2c6c3c4'),
(1396, 204, 'second_section_view_all_link', 'https://www.netflix.com/'),
(1397, 204, '_second_section_view_all_link', 'field_5f7da12e099b1'),
(1398, 204, 'second_section', ''),
(1399, 204, '_second_section', 'field_5f7da0ddff9f2'),
(1432, 205, 'first_section_title', 'Member Benefits'),
(1433, 205, '_first_section_title', 'field_5f7d9c996c3c5'),
(1434, 205, 'first_section_description', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.'),
(1435, 205, '_first_section_description', 'field_5f7d9ca26c3c6'),
(1436, 205, 'first_section_button_text', 'Become a Member'),
(1437, 205, '_first_section_button_text', 'field_5f7d9cb26c3c7'),
(1438, 205, 'first_section_button_link', ''),
(1439, 205, '_first_section_button_link', 'field_5f7d9cbe6c3c8'),
(1440, 205, 'first_section_benefit_icon_1_icon_image', ''),
(1441, 205, '_first_section_benefit_icon_1_icon_image', 'field_5f7d9d256c3ca'),
(1442, 205, 'first_section_benefit_icon_1_icon_text', '30% IN SAVINGS'),
(1443, 205, '_first_section_benefit_icon_1_icon_text', 'field_5f7d9d5a6c3cb'),
(1444, 205, 'first_section_benefit_icon_1', ''),
(1445, 205, '_first_section_benefit_icon_1', 'field_5f7d9ce46c3c9'),
(1446, 205, 'first_section_benefit_icon_2_icon_image', ''),
(1447, 205, '_first_section_benefit_icon_2_icon_image', 'field_5f7d9d7d00427'),
(1448, 205, 'first_section_benefit_icon_2_icon_text', ''),
(1449, 205, '_first_section_benefit_icon_2_icon_text', 'field_5f7d9d7d00428'),
(1450, 205, 'first_section_benefit_icon_2', ''),
(1451, 205, '_first_section_benefit_icon_2', 'field_5f7d9d7d00426'),
(1452, 205, 'first_section_benefit_icon_3_icon_image', ''),
(1453, 205, '_first_section_benefit_icon_3_icon_image', 'field_5f7d9d8e0042a'),
(1454, 205, 'first_section_benefit_icon_3_icon_text', ''),
(1455, 205, '_first_section_benefit_icon_3_icon_text', 'field_5f7d9d8e0042b'),
(1456, 205, 'first_section_benefit_icon_3', ''),
(1457, 205, '_first_section_benefit_icon_3', 'field_5f7d9d8e00429'),
(1458, 205, 'first_section', ''),
(1459, 205, '_first_section', 'field_5f7d9c2c6c3c4'),
(1460, 205, 'second_section_view_all_link', 'https://www.netflix.com/'),
(1461, 205, '_second_section_view_all_link', 'field_5f7da12e099b1'),
(1462, 205, 'second_section', ''),
(1463, 205, '_second_section', 'field_5f7da0ddff9f2'),
(1464, 209, '_edit_last', '1'),
(1465, 209, '_edit_lock', '1602468318:1'),
(1466, 209, '_wp_page_template', 'default'),
(1467, 209, 'extra-content', ''),
(1468, 211, '_edit_last', '1'),
(1469, 211, '_edit_lock', '1602468486:1'),
(1470, 211, '_wp_page_template', 'default'),
(1471, 211, 'extra-content', ''),
(1472, 211, '_yoast_wpseo_content_score', '30'),
(1473, 213, '_edit_last', '1'),
(1474, 213, '_edit_lock', '1602468530:1'),
(1475, 213, '_wp_page_template', 'default'),
(1476, 213, 'extra-content', ''),
(1477, 213, '_yoast_wpseo_content_score', '60'),
(1478, 215, '_edit_last', '1'),
(1479, 215, '_edit_lock', '1602468662:1'),
(1480, 215, '_wp_page_template', 'default'),
(1481, 215, 'extra-content', ''),
(1482, 215, '_yoast_wpseo_content_score', '30');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_subtitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_subtitle`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-05-27 07:16:27', '2019-05-27 07:16:27', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-05-27 07:16:27', '2019-05-27 07:16:27', '', 0, 'http://bedrock.loc/?p=1', 0, 'post', '', 1),
(2, 1, '2019-05-27 07:16:27', '2019-05-27 07:16:27', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://bedrock.loc/wp/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2020-10-12 01:43:31', '2020-10-12 01:43:31', '', 0, 'http://bedrock.loc/?page_id=2', 2, 'page', '', 0),
(3, 1, '2019-05-27 07:16:27', '2019-05-27 07:16:27', '<!-- wp:heading -->\r\n<h2>Who we are</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Our website address is: http://bedrock.loc.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>What personal data we collect and why we collect it</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Comments</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Media</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Contact forms</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Cookies</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select \"Remember Me\", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Embedded content from other websites</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Analytics</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Who we share your data with</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading -->\r\n<h2>How long we retain your data</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>What rights you have over your data</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Where we send your data</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Visitor comments may be checked through an automated spam detection service.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Your contact information</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Additional information</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>How we protect your data</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>What data breach procedures we have in place</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>What third parties we receive data from</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>What automated decision making and/or profiling we do with user data</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Industry regulatory disclosure requirements</h3>\r\n<!-- /wp:heading -->', 'Privacy Policy', '', '', 'publish', 'closed', 'open', '', 'privacy-policy', '', '', '2020-10-12 01:43:44', '2020-10-12 01:43:44', '', 0, 'http://bedrock.loc/?page_id=3', 100, 'page', '', 0),
(5, 1, '2019-05-27 07:51:23', '2019-05-27 07:51:23', '', 'Others', '', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-05-29 11:05:59', '2019-05-29 11:05:59', '', 0, 'http://bedrock.loc/?p=5', 2, 'nav_menu_item', '', 0),
(6, 1, '2019-05-27 07:51:23', '2019-05-27 07:51:23', ' ', '', '', '', 'publish', 'closed', 'closed', '', '6', '', '', '2019-05-29 11:05:59', '2019-05-29 11:05:59', '', 0, 'http://bedrock.loc/?p=6', 3, 'nav_menu_item', '', 0),
(7, 1, '2019-05-29 11:05:06', '2019-05-29 11:05:06', '', 'Home', '', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2020-10-12 01:43:31', '2020-10-12 01:43:31', '', 0, 'http://bedrock.loc/?page_id=7', -1, 'page', '', 0),
(8, 1, '2019-05-29 11:05:06', '2019-05-29 11:05:06', '', 'Home', '', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-05-29 11:05:06', '2019-05-29 11:05:06', '', 7, 'http://bedrock.loc/2019/05/29/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2019-05-29 11:05:59', '2019-05-29 11:05:59', ' ', '', '', '', 'publish', 'closed', 'closed', '', '9', '', '', '2019-05-29 11:05:59', '2019-05-29 11:05:59', '', 0, 'http://bedrock.loc/?p=9', 1, 'nav_menu_item', '', 0),
(11, 1, '2019-05-30 14:30:24', '2019-05-30 14:30:24', '', 'woocommerce-placeholder', '', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2020-03-10 17:42:25', '2020-03-10 17:42:25', '', 0, 'http://bedrock.loc/app/uploads/2019/05/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2020-03-10 16:27:04', '2020-03-10 16:27:04', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://bedrock.loc/wp/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-03-10 16:27:04', '2020-03-10 16:27:04', '', 2, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/2-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2020-03-10 16:27:05', '2020-03-10 16:27:05', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://bedrock.loc.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2020-03-10 16:27:05', '2020-03-10 16:27:05', '', 3, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/3-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2020-03-10 19:08:59', '2020-03-10 19:08:59', '', 'Sponsors', '', '', 'trash', 'closed', 'closed', '', 'sponsors__trashed', '', '', '2020-10-12 01:43:02', '2020-10-12 01:43:02', '', 0, 'http://localhost:8888/wp-custom-theme/web/?page_id=29', 0, 'page', '', 0),
(30, 1, '2020-03-10 19:08:59', '2020-03-10 19:08:59', '', 'Test', '', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-03-10 19:08:59', '2020-03-10 19:08:59', '', 29, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/29-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2020-03-10 19:09:11', '2020-03-10 19:09:11', '', 'Test 2', '', '', 'trash', 'closed', 'closed', '', 'test-2__trashed', '', '', '2020-10-12 01:43:07', '2020-10-12 01:43:07', '', 33, 'http://localhost:8888/wp-custom-theme/web/?page_id=31', 0, 'page', '', 0),
(32, 1, '2020-03-10 19:09:11', '2020-03-10 19:09:11', '', 'Test 2', '', '', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2020-03-10 19:09:11', '2020-03-10 19:09:11', '', 31, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/31-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2020-03-10 19:09:28', '2020-03-10 19:09:28', '', 'Overview', '', '', 'publish', 'closed', 'closed', '', 'overview', '', '', '2020-10-12 02:08:32', '2020-10-12 02:08:32', '', 103, 'http://localhost:8888/wp-custom-theme/web/?page_id=33', 1, 'page', '', 0),
(34, 1, '2020-03-10 19:09:28', '2020-03-10 19:09:28', '', 'Overview', '', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2020-03-10 19:09:28', '2020-03-10 19:09:28', '', 33, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/33-revision-v1/', 0, 'revision', '', 0),
(38, 1, '2020-03-10 19:19:45', '2020-03-10 19:19:45', '', 'Test Page', '', '', 'trash', 'closed', 'closed', '', 'test-page__trashed', '', '', '2020-10-12 01:43:13', '2020-10-12 01:43:13', '', 0, 'http://localhost:8888/wp-custom-theme/web/?page_id=38', 0, 'page', '', 0),
(39, 1, '2020-03-10 19:19:45', '2020-03-10 19:19:45', '', 'The Latest', '', '', 'publish', 'closed', 'closed', '', 'the-latest', '', '', '2020-10-12 01:43:48', '2020-10-12 01:43:48', '', 0, 'http://localhost:8888/wp-custom-theme/web/?page_id=39', 50, 'page', '', 0),
(40, 1, '2020-03-10 19:19:45', '2020-03-10 19:19:45', '', 'Test Page', '', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2020-03-10 19:19:45', '2020-03-10 19:19:45', '', 38, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/38-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2020-03-10 19:19:45', '2020-03-10 19:19:45', '', 'Test Page', '', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-03-10 19:19:45', '2020-03-10 19:19:45', '', 39, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/39-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2020-03-10 19:20:14', '2020-03-10 19:20:14', '', 'The Latest', '', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-03-10 19:20:14', '2020-03-10 19:20:14', '', 39, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/39-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2020-03-10 19:20:40', '2020-03-10 19:20:40', '', 'News', '', '', 'publish', 'closed', 'closed', '', 'news', '', '', '2020-10-12 01:43:53', '2020-10-12 01:43:53', '', 0, 'http://localhost:8888/wp-custom-theme/web/?page_id=43', 60, 'page', '', 0),
(45, 1, '2020-03-10 19:20:40', '2020-03-10 19:20:40', '', 'News', '', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2020-03-10 19:20:40', '2020-03-10 19:20:40', '', 43, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/43-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2020-03-10 19:22:11', '2020-03-10 19:22:11', '', 'News', '', '', 'publish', 'closed', 'closed', '', '49', '', '', '2020-10-12 01:42:34', '2020-10-12 01:42:34', '', 0, 'http://localhost:8888/wp-custom-theme/web/?p=49', 1, 'nav_menu_item', '', 0),
(51, 1, '2020-03-10 19:23:32', '2020-03-10 19:23:32', '', 'Research and About', '', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2020-03-10 19:23:32', '2020-03-10 19:23:32', '', 33, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/33-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2020-03-10 19:23:33', '2020-03-10 19:23:33', '', 'Research and About', '', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-03-10 19:23:33', '2020-03-10 19:23:33', '', 29, 'http://localhost:8888/wp-custom-theme/web/2020/03/10/29-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2020-03-11 15:39:00', '2020-03-11 15:39:00', ' ', '', '', '', 'publish', 'closed', 'closed', '', '54', '', '', '2020-03-24 23:15:47', '2020-03-24 23:15:47', '', 0, 'http://localhost:8888/wp-custom-theme/web/?p=54', 1, 'nav_menu_item', '', 0),
(55, 1, '2020-03-11 15:42:36', '2020-03-11 15:42:36', '', 'What is a Call to Action', '', 'Click the button to find out', 'publish', 'closed', 'closed', '', 'call-to-action', '', '', '2020-03-11 16:21:02', '2020-03-11 16:21:02', '', 0, 'http://localhost:8888/wp-custom-theme/web/?post_type=call_to_action&#038;p=55', 0, 'call_to_action', '', 0),
(56, 1, '2020-03-11 15:49:17', '2020-03-11 15:49:17', '', 'jac-we-create', '', '', 'inherit', 'open', 'closed', '', 'jac-we-create', '', '', '2020-03-11 15:49:17', '2020-03-11 15:49:17', '', 55, 'http://localhost:8888/wp-custom-theme/web/app/uploads/2020/03/jac-we-create.png', 0, 'attachment', 'image/png', 0),
(57, 1, '2020-03-11 15:49:18', '2020-03-11 15:49:18', '', 'login-background', '', '', 'inherit', 'open', 'closed', '', 'login-background', '', '', '2020-03-11 15:49:18', '2020-03-11 15:49:18', '', 55, 'http://localhost:8888/wp-custom-theme/web/app/uploads/2020/03/login-background.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2020-03-11 15:54:18', '2020-03-11 15:54:18', '', 'Call to action', '', 'Receive regular updates events, and other ways you can get involved.', 'inherit', 'closed', 'closed', '', '55-autosave-v1', '', '', '2020-03-11 15:54:18', '2020-03-11 15:54:18', '', 55, 'http://localhost:8888/wp-custom-theme/web/2020/03/11/55-autosave-v1/', 0, 'revision', '', 0),
(59, 1, '2020-03-11 16:02:09', '2020-03-11 16:02:09', '', 'Sitemap', '', '', 'publish', 'closed', 'closed', '', 'sitemap', '', '', '2020-10-12 01:43:59', '2020-10-12 01:43:59', '', 0, 'http://localhost:8888/wp-custom-theme/web/?page_id=59', 70, 'page', '', 0),
(60, 1, '2020-03-11 16:02:09', '2020-03-11 16:02:09', '', 'Sitemap', '', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-03-11 16:02:09', '2020-03-11 16:02:09', '', 59, 'http://localhost:8888/wp-custom-theme/web/2020/03/11/59-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2020-03-11 16:03:15', '2020-03-11 16:03:15', '', 'Sponsors', '', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-03-11 16:03:15', '2020-03-11 16:03:15', '', 29, 'http://localhost:8888/wp-custom-theme/web/2020/03/11/29-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2020-03-11 16:03:31', '2020-03-11 16:03:31', '<!-- wp:heading -->\r\n<h2>Who we are</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Our website address is: http://bedrock.loc.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>What personal data we collect and why we collect it</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Comments</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Media</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Contact forms</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Cookies</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select \"Remember Me\", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Embedded content from other websites</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Analytics</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Who we share your data with</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading -->\r\n<h2>How long we retain your data</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>What rights you have over your data</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Where we send your data</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Visitor comments may be checked through an automated spam detection service.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Your contact information</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading -->\r\n<h2>Additional information</h2>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>How we protect your data</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>What data breach procedures we have in place</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>What third parties we receive data from</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>What automated decision making and/or profiling we do with user data</h3>\r\n<!-- /wp:heading -->\r\n\r\n<!-- wp:heading {\"level\":3} -->\r\n<h3>Industry regulatory disclosure requirements</h3>\r\n<!-- /wp:heading -->', 'Privacy Policy', '', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2020-03-11 16:03:31', '2020-03-11 16:03:31', '', 3, 'http://localhost:8888/wp-custom-theme/web/2020/03/11/3-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2020-03-11 16:03:48', '2020-03-11 16:03:48', ' ', '', '', '', 'publish', 'closed', 'closed', '', '65', '', '', '2020-03-11 16:03:48', '2020-03-11 16:03:48', '', 0, 'http://localhost:8888/wp-custom-theme/web/?p=65', 2, 'nav_menu_item', '', 0),
(66, 1, '2020-03-11 16:03:48', '2020-03-11 16:03:48', ' ', '', '', '', 'publish', 'closed', 'closed', '', '66', '', '', '2020-03-11 16:03:48', '2020-03-11 16:03:48', '', 0, 'http://localhost:8888/wp-custom-theme/web/?p=66', 1, 'nav_menu_item', '', 0),
(67, 1, '2020-03-11 16:38:48', '2020-03-11 16:38:48', '', 'LEADING THE REGION.  INFLUENCING THE COUNTRY.', 'OVER 50 YEARS OF COMMITMENT', '', 'publish', 'closed', 'closed', '', 'test-slide', '', '', '2020-10-12 01:54:42', '2020-10-12 01:54:42', '', 0, 'http://localhost:8888/wp-custom-theme/web/?post_type=slide&#038;p=67', 0, 'slide', '', 0),
(68, 1, '2020-03-11 16:38:49', '2020-03-11 16:38:49', '', 'Join one of the most influential business networks in the country', 'benefits | directory | advocacy | join', '', 'publish', 'closed', 'closed', '', 'test-slide-2', '', '', '2020-10-12 01:57:21', '2020-10-12 01:57:21', '', 0, 'http://localhost:8888/wp-custom-theme/web/?post_type=slide&#038;p=68', 0, 'slide', '', 0),
(69, 1, '2020-03-11 17:36:30', '2020-03-11 17:36:30', '[gravityform id=\"1\" title=\"false\" description=\"false\"]', 'Contact', 'Get In Touch', 'Please fill the form below and send to us!', 'publish', 'closed', 'closed', '', 'contact', '', '', '2020-10-12 01:44:37', '2020-10-12 01:44:37', '', 0, 'http://localhost:8888/wp-custom-theme/web/?page_id=69', 30, 'page', '', 0),
(71, 1, '2020-03-11 17:36:30', '2020-03-11 17:36:30', '', 'Contact', '', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2020-03-11 17:36:30', '2020-03-11 17:36:30', '', 69, 'http://localhost:8888/wp-custom-theme/web/2020/03/11/69-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2020-03-11 17:38:24', '2020-03-11 17:38:24', '', 'Facebook', '', '', 'publish', 'closed', 'closed', '', 'facebook', '', '', '2020-03-11 17:38:24', '2020-03-11 17:38:24', '', 0, 'http://localhost:8888/wp-custom-theme/web/?post_type=social_channel&#038;p=72', 0, 'social_channel', '', 0),
(73, 1, '2020-03-11 17:38:25', '2020-03-11 17:38:25', '', 'Twitter', '', '', 'publish', 'closed', 'closed', '', 'facebook-2', '', '', '2020-03-11 17:39:04', '2020-03-11 17:39:04', '', 0, 'http://localhost:8888/wp-custom-theme/web/?post_type=social_channel&#038;p=73', 0, 'social_channel', '', 0),
(74, 1, '2020-03-11 17:39:34', '2020-03-11 17:39:34', '', 'Instagram', '', '', 'publish', 'closed', 'closed', '', 'instagram', '', '', '2020-03-11 17:39:34', '2020-03-11 17:39:34', '', 0, 'http://localhost:8888/wp-custom-theme/web/?post_type=social_channel&#038;p=74', 0, 'social_channel', '', 0),
(75, 1, '2020-03-11 17:39:34', '2020-03-11 17:39:34', '', 'Instagram', '', '', 'publish', 'closed', 'closed', '', 'instagram-2', '', '', '2020-03-11 17:40:14', '2020-03-11 17:40:14', '', 0, 'http://localhost:8888/wp-custom-theme/web/?post_type=social_channel&#038;p=75', 0, 'social_channel', '', 0),
(77, 1, '2020-03-11 19:10:39', '2020-03-11 19:10:39', '[gravityform id=\"1\" title=\"false\" description=\"false\"]', 'Contact', 'Get In Touch', 'Please fill the form below and send to us!', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2020-03-11 19:10:39', '2020-03-11 19:10:39', '', 69, 'http://localhost:8888/wp-custom-theme/web/2020/03/11/69-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2020-03-23 13:44:53', '2020-03-23 13:44:53', '', 'Research and About', '', '', 'inherit', 'closed', 'closed', '', '33-autosave-v1', '', '', '2020-03-23 13:44:53', '2020-03-23 13:44:53', '', 33, 'http://localhost:8888/foundation-bedrock-theme/web/2020/03/23/33-autosave-v1/', 0, 'revision', '', 0),
(81, 1, '2020-03-23 13:52:47', '2020-03-23 13:52:47', '', 'Overview', '', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2020-03-23 13:52:47', '2020-03-23 13:52:47', '', 33, 'http://localhost:8888/foundation-bedrock-theme/web/2020/03/23/33-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2020-03-24 22:36:13', '2020-03-24 22:36:13', '', 'Account Dashboard', '', '', 'publish', 'closed', 'closed', '', 'account-dashboard', '', '', '2020-10-12 01:44:11', '2020-10-12 01:44:11', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?page_id=82', 80, 'page', '', 0),
(83, 1, '2020-03-24 22:36:13', '2020-03-24 22:36:13', '', 'Account Dashboard', '', '', 'inherit', 'closed', 'closed', '', '82-revision-v1', '', '', '2020-03-24 22:36:13', '2020-03-24 22:36:13', '', 82, 'http://localhost:8888/foundation-bedrock-theme/web/2020/03/24/82-revision-v1/', 0, 'revision', '', 0),
(84, 1, '2020-03-24 22:36:42', '2020-03-24 22:36:42', '[woocommerce_checkout]', 'Checkout', '', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2020-10-12 01:44:17', '2020-10-12 01:44:17', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?page_id=84', 80, 'page', '', 0),
(85, 1, '2020-03-24 22:36:42', '2020-03-24 22:36:42', '[woocommerce_checkout]', 'Checkout', '', '', 'inherit', 'closed', 'closed', '', '84-revision-v1', '', '', '2020-03-24 22:36:42', '2020-03-24 22:36:42', '', 84, 'http://localhost:8888/foundation-bedrock-theme/web/2020/03/24/84-revision-v1/', 0, 'revision', '', 0),
(86, 1, '2020-03-24 22:37:13', '2020-03-24 22:37:13', '[woocommerce_cart]', 'Cart', '', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2020-10-12 01:44:18', '2020-10-12 01:44:18', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?page_id=86', 80, 'page', '', 0),
(87, 1, '2020-03-24 22:37:13', '2020-03-24 22:37:13', '[woocommerce_cart]', 'Cart', '', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-03-24 22:37:13', '2020-03-24 22:37:13', '', 86, 'http://localhost:8888/foundation-bedrock-theme/web/2020/03/24/86-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2020-03-24 22:37:16', '2020-03-24 22:37:16', '[woocommerce_cart]', 'Cart', '', '', 'inherit', 'closed', 'closed', '', '86-autosave-v1', '', '', '2020-03-24 22:37:16', '2020-03-24 22:37:16', '', 86, 'http://localhost:8888/foundation-bedrock-theme/web/2020/03/24/86-autosave-v1/', 0, 'revision', '', 0),
(89, 1, '2020-03-24 22:37:25', '2020-03-24 22:37:25', '[woocommerce_my_account]', 'Account', '', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2020-10-12 01:44:21', '2020-10-12 01:44:21', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?page_id=89', 80, 'page', '', 0),
(90, 1, '2020-03-24 22:37:25', '2020-03-24 22:37:25', '[woocommerce_my_account]', 'Account', '', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-03-24 22:37:25', '2020-03-24 22:37:25', '', 89, 'http://localhost:8888/foundation-bedrock-theme/web/2020/03/24/89-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2020-03-24 23:15:47', '2020-03-24 23:15:47', ' ', '', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2020-03-24 23:15:47', '2020-03-24 23:15:47', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?p=92', 2, 'nav_menu_item', '', 0),
(93, 1, '2020-03-24 23:16:06', '2020-03-24 23:16:06', ' ', '', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2020-10-12 01:42:34', '2020-10-12 01:42:34', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?p=93', 2, 'nav_menu_item', '', 0),
(98, 1, '2020-07-07 17:35:05', '2020-07-07 17:35:05', '', 'Notification Bar', '', '', 'publish', 'closed', 'closed', '', 'notification-bar', '', '', '2020-07-07 17:49:03', '2020-07-07 17:49:03', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?post_type=notification_bar&#038;p=98', 0, 'notification_bar', '', 0),
(100, 1, '2020-07-07 17:55:37', '2020-07-07 17:55:37', '', 'Video Popup', '', '', 'publish', 'closed', 'closed', '', 'video-popup', '', '', '2020-07-07 17:55:58', '2020-07-07 17:55:58', '', 0, 'http://localhost:8888/foundation-bedrock-theme/web/?post_type=video_popup&#038;p=100', 0, 'video_popup', '', 0),
(101, 1, '2020-07-07 18:01:45', '2020-07-07 18:01:45', '', 'hong-kong-city-2434627-min', '', '', 'inherit', 'open', 'closed', '', 'hong-kong-city-2434627-min', '', '', '2020-07-07 18:01:45', '2020-07-07 18:01:45', '', 67, 'http://localhost:8888/foundation-bedrock-theme/web/app/uploads/2020/03/hong-kong-city-2434627-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2020-10-06 08:35:37', '0000-00-00 00:00:00', '', 'Auto Draft', '', '', 'auto-draft', 'closed', 'open', '', '', '', '', '2020-10-06 08:35:37', '0000-00-00 00:00:00', '', 0, 'http://localhost/JAC/lnccdev/web/?p=102', 0, 'post', '', 0),
(103, 1, '2020-10-06 11:27:03', '2020-10-06 11:27:03', '', 'Members', '', '', 'publish', 'closed', 'closed', '', 'members', '', '', '2020-10-12 02:02:44', '2020-10-12 02:02:44', '', 0, 'http://localhost/JAC/lnccdev/web/?page_id=103', 10, 'page', '', 0),
(104, 1, '2020-10-06 11:27:03', '2020-10-06 11:27:03', '', 'Members', '', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2020-10-06 11:27:03', '2020-10-06 11:27:03', '', 103, 'http://localhost/JAC/lnccdev/web/2020/10/06/103-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2020-10-06 11:29:09', '0000-00-00 00:00:00', '', 'Auto Draft', '', '', 'auto-draft', 'closed', 'open', '', '', '', '', '2020-10-06 11:29:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/JAC/lnccdev/web/?p=105', 0, 'post', '', 0),
(107, 1, '2020-10-07 10:00:47', '2020-10-07 10:00:47', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"template-home.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Home Slider', '', 'home-slider', 'publish', 'closed', 'closed', '', 'group_5f7d91256a182', '', '', '2020-10-07 10:25:12', '2020-10-07 10:25:12', '', 0, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field-group&#038;p=107', 0, 'acf-field-group', '', 0),
(123, 1, '2020-10-07 10:06:48', '2020-10-07 10:06:48', '', '1229106_landscape-16', '', '', 'inherit', 'open', 'closed', '', '1229106_landscape-16', '', '', '2020-10-07 10:09:59', '2020-10-07 10:09:59', '', 7, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/1229106_landscape-16.jpg', 0, 'attachment', 'image/jpeg', 0),
(124, 1, '2020-10-07 10:07:10', '2020-10-07 10:07:10', '', 'matterhorn-shutterstock_1118486243', '', '', 'inherit', 'open', 'closed', '', 'matterhorn-shutterstock_1118486243', '', '', '2020-10-07 10:10:00', '2020-10-07 10:10:00', '', 7, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/matterhorn-shutterstock_1118486243.jpg', 0, 'attachment', 'image/jpeg', 0),
(125, 1, '2020-10-07 10:07:51', '2020-10-07 10:07:51', '', 'pink_landscape', '', '', 'inherit', 'open', 'closed', '', 'pink_landscape', '', '', '2020-10-07 10:15:40', '2020-10-07 10:15:40', '', 7, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/pink_landscape.jpg', 0, 'attachment', 'image/jpeg', 0),
(126, 1, '2020-10-07 10:09:59', '2020-10-07 10:09:59', '', 'Home', '', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2020-10-07 10:09:59', '2020-10-07 10:09:59', '', 7, 'http://localhost/JAC/lnccdev/web/7-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2020-10-07 10:15:40', '2020-10-07 10:15:40', '', 'Home', '', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2020-10-07 10:15:40', '2020-10-07 10:15:40', '', 7, 'http://localhost/JAC/lnccdev/web/7-revision-v1/', 0, 'revision', '', 0),
(128, 1, '2020-10-07 10:22:38', '2020-10-07 10:22:38', '', 'Home', '', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2020-10-07 10:22:38', '2020-10-07 10:22:38', '', 7, 'http://localhost/JAC/lnccdev/web/7-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2020-10-07 10:25:09', '2020-10-07 10:25:09', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Home Slider', '', 'home_slider', 'publish', 'closed', 'closed', '', 'field_5f7d9700b5c38', '', '', '2020-10-07 10:25:09', '2020-10-07 10:25:09', '', 107, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=129', 0, 'acf-field', '', 0),
(130, 1, '2020-10-07 10:25:09', '2020-10-07 10:25:09', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Slider Image', '', 'slider_image', 'publish', 'closed', 'closed', '', 'field_5f7d9724b5c39', '', '', '2020-10-07 10:25:09', '2020-10-07 10:25:09', '', 129, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=130', 0, 'acf-field', '', 0),
(131, 1, '2020-10-07 10:25:10', '2020-10-07 10:25:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Slider Title', '', 'slider_title', 'publish', 'closed', 'closed', '', 'field_5f7d973eb5c3a', '', '', '2020-10-07 10:25:10', '2020-10-07 10:25:10', '', 129, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=131', 1, 'acf-field', '', 0),
(132, 1, '2020-10-07 10:25:10', '2020-10-07 10:25:10', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Slider Subtitle', '', 'slider_subtitle', 'publish', 'closed', 'closed', '', 'field_5f7d974eb5c3b', '', '', '2020-10-07 10:25:10', '2020-10-07 10:25:10', '', 129, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=132', 2, 'acf-field', '', 0),
(133, 1, '2020-10-07 10:25:10', '2020-10-07 10:25:10', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Link to open', '', 'link_to_open', 'publish', 'closed', 'closed', '', 'field_5f7d9758b5c3c', '', '', '2020-10-07 10:25:10', '2020-10-07 10:25:10', '', 129, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=133', 3, 'acf-field', '', 0),
(134, 1, '2020-10-07 10:27:05', '2020-10-07 10:27:05', '', 'Home', '', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2020-10-07 10:27:05', '2020-10-07 10:27:05', '', 7, 'http://localhost/JAC/lnccdev/web/7-revision-v1/', 0, 'revision', '', 0),
(135, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:20:\"template-members.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:1:{i:0;s:11:\"the_content\";}s:11:\"description\";s:0:\"\";}', 'Members Page', '', 'members-page', 'publish', 'closed', 'closed', '', 'group_5f7d9c21135d9', '', '', '2020-10-07 11:54:21', '2020-10-07 11:54:21', '', 0, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field-group&#038;p=135', 0, 'acf-field-group', '', 0),
(136, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'First Section', '', 'first_section', 'publish', 'closed', 'closed', '', 'field_5f7d9c2c6c3c4', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 135, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=136', 0, 'acf-field', '', 0),
(137, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', '', 'title', 'publish', 'closed', 'closed', '', 'field_5f7d9c996c3c5', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 136, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=137', 0, 'acf-field', '', 0),
(138, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description', '', 'description', 'publish', 'closed', 'closed', '', 'field_5f7d9ca26c3c6', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 136, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=138', 1, 'acf-field', '', 0),
(139, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Button Text', '', 'button_text', 'publish', 'closed', 'closed', '', 'field_5f7d9cb26c3c7', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 136, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=139', 2, 'acf-field', '', 0),
(140, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Button Link', '', 'button_link', 'publish', 'closed', 'closed', '', 'field_5f7d9cbe6c3c8', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 136, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=140', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_subtitle`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(141, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Benefit Icon 1', '', 'benefit_icon_1', 'publish', 'closed', 'closed', '', 'field_5f7d9ce46c3c9', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 136, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=141', 4, 'acf-field', '', 0),
(142, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon Image', '', 'icon_image', 'publish', 'closed', 'closed', '', 'field_5f7d9d256c3ca', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 141, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=142', 0, 'acf-field', '', 0),
(143, 1, '2020-10-07 10:50:26', '2020-10-07 10:50:26', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Icon Text', '', 'icon_text', 'publish', 'closed', 'closed', '', 'field_5f7d9d5a6c3cb', '', '', '2020-10-07 10:50:26', '2020-10-07 10:50:26', '', 141, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=143', 1, 'acf-field', '', 0),
(144, 1, '2020-10-07 10:50:59', '2020-10-07 10:50:59', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Benefit Icon 2', '', 'benefit_icon_2', 'publish', 'closed', 'closed', '', 'field_5f7d9d7d00426', '', '', '2020-10-07 10:50:59', '2020-10-07 10:50:59', '', 136, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=144', 5, 'acf-field', '', 0),
(145, 1, '2020-10-07 10:50:59', '2020-10-07 10:50:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon Image', '', 'icon_image', 'publish', 'closed', 'closed', '', 'field_5f7d9d7d00427', '', '', '2020-10-07 11:04:04', '2020-10-07 11:04:04', '', 144, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&#038;p=145', 0, 'acf-field', '', 0),
(146, 1, '2020-10-07 10:51:00', '2020-10-07 10:51:00', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Icon Text', '', 'icon_text', 'publish', 'closed', 'closed', '', 'field_5f7d9d7d00428', '', '', '2020-10-07 10:51:00', '2020-10-07 10:51:00', '', 144, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=146', 1, 'acf-field', '', 0),
(147, 1, '2020-10-07 10:51:00', '2020-10-07 10:51:00', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Benefit Icon 3', '', 'benefit_icon_3', 'publish', 'closed', 'closed', '', 'field_5f7d9d8e00429', '', '', '2020-10-07 10:51:00', '2020-10-07 10:51:00', '', 136, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=147', 6, 'acf-field', '', 0),
(148, 1, '2020-10-07 10:51:00', '2020-10-07 10:51:00', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon Image', '', 'icon_image', 'publish', 'closed', 'closed', '', 'field_5f7d9d8e0042a', '', '', '2020-10-07 10:51:00', '2020-10-07 10:51:00', '', 147, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=148', 0, 'acf-field', '', 0),
(149, 1, '2020-10-07 10:51:00', '2020-10-07 10:51:00', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Icon Text', '', 'icon_text', 'publish', 'closed', 'closed', '', 'field_5f7d9d8e0042b', '', '', '2020-10-07 10:51:00', '2020-10-07 10:51:00', '', 147, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=149', 1, 'acf-field', '', 0),
(150, 1, '2020-10-07 10:55:08', '2020-10-07 10:55:08', '', 'business_icon', '', '', 'inherit', 'open', 'closed', '', 'business_icon', '', '', '2020-10-07 10:55:08', '2020-10-07 10:55:08', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/business_icon.png', 0, 'attachment', 'image/png', 0),
(151, 1, '2020-10-07 10:55:12', '2020-10-07 10:55:12', '', 'networking_icon', '', '', 'inherit', 'open', 'closed', '', 'networking_icon', '', '', '2020-10-07 10:55:12', '2020-10-07 10:55:12', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/networking_icon.png', 0, 'attachment', 'image/png', 0),
(152, 1, '2020-10-07 10:55:15', '2020-10-07 10:55:15', '', 'saving_icon', '', '', 'inherit', 'open', 'closed', '', 'saving_icon', '', '', '2020-10-07 10:55:15', '2020-10-07 10:55:15', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/saving_icon.png', 0, 'attachment', 'image/png', 0),
(153, 1, '2020-10-07 10:56:05', '2020-10-07 10:56:05', '', 'Members', '', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2020-10-07 10:56:05', '2020-10-07 10:56:05', '', 103, 'http://localhost/JAC/lnccdev/web/103-revision-v1/', 0, 'revision', '', 0),
(154, 1, '2020-10-07 11:06:07', '2020-10-07 11:06:07', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Second Section', '', 'second_section', 'publish', 'closed', 'closed', '', 'field_5f7da0ddff9f2', '', '', '2020-10-07 11:06:07', '2020-10-07 11:06:07', '', 135, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=154', 1, 'acf-field', '', 0),
(155, 1, '2020-10-07 11:06:07', '2020-10-07 11:06:07', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Icon List', '', 'list', 'publish', 'closed', 'closed', '', 'field_5f7da0eaff9f3', '', '', '2020-10-07 11:06:07', '2020-10-07 11:06:07', '', 154, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=155', 0, 'acf-field', '', 0),
(156, 1, '2020-10-07 11:06:07', '2020-10-07 11:06:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'List Item', '', 'list_item', 'publish', 'closed', 'closed', '', 'field_5f7da111ff9f4', '', '', '2020-10-07 11:06:07', '2020-10-07 11:06:07', '', 155, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=156', 0, 'acf-field', '', 0),
(157, 1, '2020-10-07 11:08:02', '2020-10-07 11:08:02', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'View All Link', '', 'view_all_link', 'publish', 'closed', 'closed', '', 'field_5f7da12e099b1', '', '', '2020-10-07 11:08:02', '2020-10-07 11:08:02', '', 154, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=157', 1, 'acf-field', '', 0),
(158, 1, '2020-10-07 11:08:03', '2020-10-07 11:08:03', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Benefit List', '', 'benefit_list', 'publish', 'closed', 'closed', '', 'field_5f7da168099b2', '', '', '2020-10-07 11:08:03', '2020-10-07 11:08:03', '', 154, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=158', 2, 'acf-field', '', 0),
(159, 1, '2020-10-07 11:08:03', '2020-10-07 11:08:03', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Benefit Image', '', 'benefit_image', 'publish', 'closed', 'closed', '', 'field_5f7da173099b3', '', '', '2020-10-07 11:08:03', '2020-10-07 11:08:03', '', 158, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=159', 0, 'acf-field', '', 0),
(160, 1, '2020-10-07 11:08:03', '2020-10-07 11:08:03', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Benefit Text', '', 'benefit_text', 'publish', 'closed', 'closed', '', 'field_5f7da184099b4', '', '', '2020-10-07 11:08:03', '2020-10-07 11:08:03', '', 158, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=160', 1, 'acf-field', '', 0),
(161, 1, '2020-10-07 11:09:35', '2020-10-07 11:09:35', '', 'SHIPPING', '', '', 'inherit', 'open', 'closed', '', 'shipping', '', '', '2020-10-07 11:09:35', '2020-10-07 11:09:35', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/SHIPPING.png', 0, 'attachment', 'image/png', 0),
(162, 1, '2020-10-07 11:09:37', '2020-10-07 11:09:37', '', 'ACCOMMODATIONS', '', '', 'inherit', 'open', 'closed', '', 'accommodations', '', '', '2020-10-07 11:09:37', '2020-10-07 11:09:37', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/ACCOMMODATIONS.png', 0, 'attachment', 'image/png', 0),
(163, 1, '2020-10-07 11:09:40', '2020-10-07 11:09:40', '', 'AUTOMOTIVE', '', '', 'inherit', 'open', 'closed', '', 'automotive', '', '', '2020-10-07 11:09:40', '2020-10-07 11:09:40', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/AUTOMOTIVE.png', 0, 'attachment', 'image/png', 0),
(164, 1, '2020-10-07 11:09:42', '2020-10-07 11:09:42', '', 'INSURANCE', '', '', 'inherit', 'open', 'closed', '', 'insurance', '', '', '2020-10-07 11:09:42', '2020-10-07 11:09:42', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/INSURANCE.png', 0, 'attachment', 'image/png', 0),
(165, 1, '2020-10-07 11:09:43', '2020-10-07 11:09:43', '', 'phone', '', '', 'inherit', 'open', 'closed', '', 'phone', '', '', '2020-10-07 11:09:43', '2020-10-07 11:09:43', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/phone.png', 0, 'attachment', 'image/png', 0),
(166, 1, '2020-10-07 11:10:57', '2020-10-07 11:10:57', '', 'Members', '', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2020-10-07 11:10:57', '2020-10-07 11:10:57', '', 103, 'http://localhost/JAC/lnccdev/web/103-revision-v1/', 0, 'revision', '', 0),
(167, 1, '2020-10-07 11:27:51', '2020-10-07 11:27:51', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:3:\"row\";s:12:\"button_label\";s:0:\"\";}', 'Third Section', '', 'third_section', 'publish', 'closed', 'closed', '', 'field_5f7da553885da', '', '', '2020-10-07 11:30:41', '2020-10-07 11:30:41', '', 135, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&#038;p=167', 2, 'acf-field', '', 0),
(168, 1, '2020-10-07 11:27:51', '2020-10-07 11:27:51', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Company\'s Logo', '', 'companys_logo', 'publish', 'closed', 'closed', '', 'field_5f7da570885db', '', '', '2020-10-07 11:27:51', '2020-10-07 11:27:51', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=168', 0, 'acf-field', '', 0),
(169, 1, '2020-10-07 11:27:52', '2020-10-07 11:27:52', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Company Name', '', 'company_name', 'publish', 'closed', 'closed', '', 'field_5f7da585885dc', '', '', '2020-10-07 11:27:52', '2020-10-07 11:27:52', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=169', 1, 'acf-field', '', 0),
(170, 1, '2020-10-07 11:27:52', '2020-10-07 11:27:52', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:3;s:9:\"new_lines\";s:0:\"\";}', 'Description', '', 'description', 'publish', 'closed', 'closed', '', 'field_5f7da595885dd', '', '', '2020-10-07 11:29:41', '2020-10-07 11:29:41', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&#038;p=170', 2, 'acf-field', '', 0),
(171, 1, '2020-10-07 11:27:52', '2020-10-07 11:27:52', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Join Now Link', '', 'join_now_link', 'publish', 'closed', 'closed', '', 'field_5f7da5a5885de', '', '', '2020-10-07 11:27:52', '2020-10-07 11:27:52', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=171', 3, 'acf-field', '', 0),
(172, 1, '2020-10-07 11:27:52', '2020-10-07 11:27:52', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'View Button Link', '', 'view_button_link', 'publish', 'closed', 'closed', '', 'field_5f7da5e9885df', '', '', '2020-10-07 11:27:52', '2020-10-07 11:27:52', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=172', 4, 'acf-field', '', 0),
(173, 1, '2020-10-07 11:27:52', '2020-10-07 11:27:52', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Backside Offer Text', '', 'backside_offer_text', 'publish', 'closed', 'closed', '', 'field_5f7da5f7885e0', '', '', '2020-10-07 11:27:52', '2020-10-07 11:27:52', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=173', 5, 'acf-field', '', 0),
(174, 1, '2020-10-07 11:27:52', '2020-10-07 11:27:52', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Offer Validity', '', 'offer_validity', 'publish', 'closed', 'closed', '', 'field_5f7da616885e1', '', '', '2020-10-07 11:27:52', '2020-10-07 11:27:52', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=174', 6, 'acf-field', '', 0),
(175, 1, '2020-10-07 11:27:52', '2020-10-07 11:27:52', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'View Offer Link', '', 'view_offer_link', 'publish', 'closed', 'closed', '', 'field_5f7da61e885e2', '', '', '2020-10-07 11:28:55', '2020-10-07 11:28:55', '', 167, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&#038;p=175', 7, 'acf-field', '', 0),
(176, 1, '2020-10-07 11:31:24', '2020-10-07 11:31:24', '', 'LeGrow', '', '', 'inherit', 'open', 'closed', '', 'legrow', '', '', '2020-10-07 11:31:24', '2020-10-07 11:31:24', '', 103, 'http://localhost/JAC/lnccdev/web/app/uploads/2020/10/LeGrow.png', 0, 'attachment', 'image/png', 0),
(177, 1, '2020-10-07 11:33:26', '2020-10-07 11:33:26', '', 'Members', '', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2020-10-07 11:33:26', '2020-10-07 11:33:26', '', 103, 'http://localhost/JAC/lnccdev/web/103-revision-v1/', 0, 'revision', '', 0),
(178, 1, '2020-10-07 11:54:20', '2020-10-07 11:54:20', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:3:\"row\";s:12:\"button_label\";s:0:\"\";}', 'Fourth Section', '', 'fourth_section', 'publish', 'closed', 'closed', '', 'field_5f7dac59665c6', '', '', '2020-10-07 11:54:20', '2020-10-07 11:54:20', '', 135, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=178', 3, 'acf-field', '', 0),
(179, 1, '2020-10-07 11:54:20', '2020-10-07 11:54:20', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Company\'s Logo', '', 'companys_logo', 'publish', 'closed', 'closed', '', 'field_5f7dac59665c7', '', '', '2020-10-07 11:54:20', '2020-10-07 11:54:20', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=179', 0, 'acf-field', '', 0),
(180, 1, '2020-10-07 11:54:20', '2020-10-07 11:54:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Company Name', '', 'company_name', 'publish', 'closed', 'closed', '', 'field_5f7dac59665c8', '', '', '2020-10-07 11:54:20', '2020-10-07 11:54:20', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=180', 1, 'acf-field', '', 0),
(181, 1, '2020-10-07 11:54:21', '2020-10-07 11:54:21', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:3;s:9:\"new_lines\";s:0:\"\";}', 'Description', '', 'description', 'publish', 'closed', 'closed', '', 'field_5f7dac59665c9', '', '', '2020-10-07 11:54:21', '2020-10-07 11:54:21', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=181', 2, 'acf-field', '', 0),
(182, 1, '2020-10-07 11:54:21', '2020-10-07 11:54:21', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Join Now Link', '', 'join_now_link', 'publish', 'closed', 'closed', '', 'field_5f7dac59665ca', '', '', '2020-10-07 11:54:21', '2020-10-07 11:54:21', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=182', 3, 'acf-field', '', 0),
(183, 1, '2020-10-07 11:54:21', '2020-10-07 11:54:21', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'View Button Link', '', 'view_button_link', 'publish', 'closed', 'closed', '', 'field_5f7dac59665cb', '', '', '2020-10-07 11:54:21', '2020-10-07 11:54:21', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=183', 4, 'acf-field', '', 0),
(184, 1, '2020-10-07 11:54:21', '2020-10-07 11:54:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Backside Offer Text', '', 'backside_offer_text', 'publish', 'closed', 'closed', '', 'field_5f7dac59665cc', '', '', '2020-10-07 11:54:21', '2020-10-07 11:54:21', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=184', 5, 'acf-field', '', 0),
(185, 1, '2020-10-07 11:54:21', '2020-10-07 11:54:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Offer Validity', '', 'offer_validity', 'publish', 'closed', 'closed', '', 'field_5f7dac59665cd', '', '', '2020-10-07 11:54:21', '2020-10-07 11:54:21', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=185', 6, 'acf-field', '', 0),
(186, 1, '2020-10-07 11:54:21', '2020-10-07 11:54:21', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'View Offer Link', '', 'view_offer_link', 'publish', 'closed', 'closed', '', 'field_5f7dac59665ce', '', '', '2020-10-07 11:54:21', '2020-10-07 11:54:21', '', 178, 'http://localhost/JAC/lnccdev/web/?post_type=acf-field&p=186', 7, 'acf-field', '', 0),
(187, 1, '2020-10-07 11:57:47', '2020-10-07 11:57:47', '', 'Members', '', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2020-10-07 11:57:47', '2020-10-07 11:57:47', '', 103, 'http://localhost/JAC/lnccdev/web/103-revision-v1/', 0, 'revision', '', 0),
(188, 1, '2020-10-07 11:59:45', '2020-10-07 11:59:45', '', 'Members', '', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2020-10-07 11:59:45', '2020-10-07 11:59:45', '', 103, 'http://localhost/JAC/lnccdev/web/103-revision-v1/', 0, 'revision', '', 0),
(189, 1, '2020-10-07 18:48:21', '2020-10-07 18:48:21', ' ', '', '', '', 'publish', 'closed', 'closed', '', '189', '', '', '2020-10-07 18:50:49', '2020-10-07 18:50:49', '', 0, 'http://localhost/JAC/lnccdev/web/?p=189', 2, 'nav_menu_item', '', 0),
(190, 1, '2020-10-07 18:48:21', '2020-10-07 18:48:21', ' ', '', '', '', 'publish', 'closed', 'closed', '', '190', '', '', '2020-10-07 18:50:49', '2020-10-07 18:50:49', '', 0, 'http://localhost/JAC/lnccdev/web/?p=190', 1, 'nav_menu_item', '', 0),
(191, 1, '2020-10-07 18:50:49', '2020-10-07 18:50:49', '', '<i class=\"fa fa-user\" aria-hidden=\"true\"></i> Member Benefits', '', '', 'publish', 'closed', 'closed', '', 'member-benefits', '', '', '2020-10-07 18:50:49', '2020-10-07 18:50:49', '', 0, 'http://localhost/JAC/lnccdev/web/?p=191', 3, 'nav_menu_item', '', 0),
(192, 1, '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', '<i class=\"fa fa-users\" aria-hidden=\"true\"></i> Member Directory', '', '', 'publish', 'closed', 'closed', '', 'member-directory', '', '', '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', 0, 'http://localhost/JAC/lnccdev/web/?p=192', 4, 'nav_menu_item', '', 0),
(193, 1, '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', '<i class=\"fa fa-volume-up\" aria-hidden=\"true\"></i> Advocacy', '', '', 'publish', 'closed', 'closed', '', 'advocacy', '', '', '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', 0, 'http://localhost/JAC/lnccdev/web/?p=193', 5, 'nav_menu_item', '', 0),
(194, 1, '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', 'Events', '', '', 'publish', 'closed', 'closed', '', 'events', '', '', '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', 0, 'http://localhost/JAC/lnccdev/web/?p=194', 6, 'nav_menu_item', '', 0),
(195, 1, '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', '<i class=\"fa fa-volume-up\" aria-hidden=\"true\"></i> Advocacy', '', '', 'publish', 'closed', 'closed', '', 'advocacy-2', '', '', '2020-10-07 18:50:50', '2020-10-07 18:50:50', '', 0, 'http://localhost/JAC/lnccdev/web/?p=195', 7, 'nav_menu_item', '', 0),
(196, 1, '2020-10-07 18:50:51', '2020-10-07 18:50:51', '', '<i class=\"fa fa-user\" aria-hidden=\"true\"></i> Member Benefits', '', '', 'publish', 'closed', 'closed', '', 'member-benefits-2', '', '', '2020-10-07 18:50:51', '2020-10-07 18:50:51', '', 0, 'http://localhost/JAC/lnccdev/web/?p=196', 8, 'nav_menu_item', '', 0),
(197, 1, '2020-10-07 19:06:39', '2020-10-07 19:06:39', ' ', '', '', '', 'publish', 'closed', 'closed', '', '197', '', '', '2020-10-07 19:06:39', '2020-10-07 19:06:39', '', 0, 'http://localhost/JAC/lnccdev/web/?p=197', 4, 'nav_menu_item', '', 0),
(198, 1, '2020-10-07 19:06:39', '2020-10-07 19:06:39', ' ', '', '', '', 'publish', 'closed', 'closed', '', '198', '', '', '2020-10-07 19:06:39', '2020-10-07 19:06:39', '', 0, 'http://localhost/JAC/lnccdev/web/?p=198', 3, 'nav_menu_item', '', 0),
(199, 1, '2020-10-07 19:06:38', '2020-10-07 19:06:38', '', 'About', '', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2020-10-07 19:06:38', '2020-10-07 19:06:38', '', 0, 'http://localhost/JAC/lnccdev/web/?p=199', 1, 'nav_menu_item', '', 0),
(200, 1, '2020-10-07 19:06:39', '2020-10-07 19:06:39', '', 'Resources', '', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2020-10-07 19:06:39', '2020-10-07 19:06:39', '', 0, 'http://localhost/JAC/lnccdev/web/?p=200', 2, 'nav_menu_item', '', 0),
(201, 1, '2020-10-12 01:42:51', '2020-10-12 01:42:51', '', 'About', '', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2020-10-12 01:44:25', '2020-10-12 01:44:25', '', 0, 'http://localhost:8888/lnccdev/web/?page_id=201', 20, 'page', '', 0),
(202, 1, '2020-10-12 01:42:51', '2020-10-12 01:42:51', '', 'About', '', '', 'inherit', 'closed', 'closed', '', '201-revision-v1', '', '', '2020-10-12 01:42:51', '2020-10-12 01:42:51', '', 201, 'http://localhost:8888/lnccdev/web/201-revision-v1/', 0, 'revision', '', 0),
(204, 1, '2020-10-12 01:51:34', '2020-10-12 01:51:34', '', 'Member Benefits', '', '', 'publish', 'closed', 'closed', '', 'member-benefits', '', '', '2020-10-12 02:08:43', '2020-10-12 02:08:43', '', 103, 'http://localhost:8888/lnccdev/web/?page_id=204', 2, 'page', '', 0),
(205, 1, '2020-10-12 01:51:34', '2020-10-12 01:51:34', '', 'Member Benefits', '', '', 'inherit', 'closed', 'closed', '', '204-revision-v1', '', '', '2020-10-12 01:51:34', '2020-10-12 01:51:34', '', 204, 'http://localhost:8888/lnccdev/web/204-revision-v1/', 0, 'revision', '', 0),
(206, 1, '2020-10-12 01:52:33', '0000-00-00 00:00:00', '', 'Auto Draft', '', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-10-12 01:52:33', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/lnccdev/web/?page_id=206', 0, 'page', '', 0),
(207, 1, '2020-10-12 01:52:38', '0000-00-00 00:00:00', '', 'Auto Draft', '', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-10-12 01:52:38', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/lnccdev/web/?page_id=207', 0, 'page', '', 0),
(209, 1, '2020-10-12 02:06:09', '2020-10-12 02:06:09', '', 'Resources', '', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2020-10-12 02:06:09', '2020-10-12 02:06:09', '', 0, 'http://localhost:8888/lnccdev/web/?page_id=209', 40, 'page', '', 0),
(210, 1, '2020-10-12 02:06:09', '2020-10-12 02:06:09', '', 'Resources', '', '', 'inherit', 'closed', 'closed', '', '209-revision-v1', '', '', '2020-10-12 02:06:09', '2020-10-12 02:06:09', '', 209, 'http://localhost:8888/lnccdev/web/209-revision-v1/', 0, 'revision', '', 0),
(211, 1, '2020-10-12 02:10:14', '2020-10-12 02:10:14', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.\r\n\r\n', 'MEMBERSHIP DIRECTORY', '', '', 'publish', 'closed', 'closed', '', 'membership-directory', '', '', '2020-10-12 02:10:14', '2020-10-12 02:10:14', '', 103, 'http://localhost:8888/lnccdev/web/?page_id=211', 3, 'page', '', 0),
(212, 1, '2020-10-12 02:10:14', '2020-10-12 02:10:14', 'An advantage to your Labrador North Chamber of Commerce membership is the access you get to many benefit programs that can save you or your company money. As a member, you can recover the cost of your membership many times over just by taking advantage of these time and cost-saving benefits. For more information, we would be happy to discuss the full benefits of joining our Chamber. Please feel free to contact us.\r\n\r\n', 'MEMBERSHIP DIRECTORY', '', '', 'inherit', 'closed', 'closed', '', '211-revision-v1', '', '', '2020-10-12 02:10:14', '2020-10-12 02:10:14', '', 211, 'http://localhost:8888/lnccdev/web/211-revision-v1/', 0, 'revision', '', 0),
(213, 1, '2020-10-12 02:10:48', '2020-10-12 02:10:48', 'The chamber represents 150+ members and works as the voice of business to influence federal, provincial and municipal legislation. These are issues that affect how our members operate their businesses.\r\n\r\n', 'ADVOCACY', '', '', 'publish', 'closed', 'closed', '', 'advocacy', '', '', '2020-10-12 02:10:48', '2020-10-12 02:10:48', '', 103, 'http://localhost:8888/lnccdev/web/?page_id=213', 4, 'page', '', 0),
(214, 1, '2020-10-12 02:10:48', '2020-10-12 02:10:48', 'The chamber represents 150+ members and works as the voice of business to influence federal, provincial and municipal legislation. These are issues that affect how our members operate their businesses.\r\n\r\n', 'ADVOCACY', '', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2020-10-12 02:10:48', '2020-10-12 02:10:48', '', 213, 'http://localhost:8888/lnccdev/web/213-revision-v1/', 0, 'revision', '', 0),
(215, 1, '2020-10-12 02:11:28', '2020-10-12 02:11:28', 'To take advantage of the many benefits of being a member of the Labrador North Chamber of Commerce, please complete the form below and send to Events & Member Services Manager Darian Bird at events@chamberlabrador.com.\r\n\r\n', 'BECOME A MEMBER', '', '', 'publish', 'closed', 'closed', '', 'become-a-member', '', '', '2020-10-12 02:11:40', '2020-10-12 02:11:40', '', 103, 'http://localhost:8888/lnccdev/web/?page_id=215', 5, 'page', '', 0),
(216, 1, '2020-10-12 02:11:28', '2020-10-12 02:11:28', 'To take advantage of the many benefits of being a member of the Labrador North Chamber of Commerce, please complete the form below and send to Events & Member Services Manager Darian Bird at events@chamberlabrador.com.\r\n\r\n', 'BECOME A MEMBER', '', '', 'inherit', 'closed', 'closed', '', '215-revision-v1', '', '', '2020-10-12 02:11:28', '2020-10-12 02:11:28', '', 215, 'http://localhost:8888/lnccdev/web/215-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_form`
--

CREATE TABLE `wp_rg_form` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rg_form`
--

INSERT INTO `wp_rg_form` (`id`, `title`, `date_created`, `is_active`, `is_trash`) VALUES
(1, 'Contact', '2020-03-11 19:08:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_form_meta`
--

CREATE TABLE `wp_rg_form_meta` (
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_ci,
  `entries_grid_meta` longtext COLLATE utf8mb4_unicode_ci,
  `confirmations` longtext COLLATE utf8mb4_unicode_ci,
  `notifications` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rg_form_meta`
--

INSERT INTO `wp_rg_form_meta` (`form_id`, `display_meta`, `entries_grid_meta`, `confirmations`, `notifications`) VALUES
(1, '{\"title\":\"Contact\",\"description\":\"\",\"labelPlacement\":\"top_label\",\"descriptionPlacement\":\"below\",\"button\":{\"type\":\"text\",\"text\":\"Submit\",\"imageUrl\":\"\"},\"fields\":[{\"type\":\"text\",\"id\":1,\"label\":\"Name\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"name\",\"cssClass\":\"gf_left_half\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"email\",\"id\":2,\"label\":\"Email\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"email\",\"cssClass\":\"gf_right_half\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"emailConfirmEnabled\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"textarea\",\"id\":3,\"label\":\"Message\",\"adminLabel\":\"\",\"isRequired\":true,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"message\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"form_id\":\"\",\"useRichTextEditor\":false,\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"pageNumber\":1}],\"version\":\"2.2.6\",\"id\":1,\"useCurrentUserAsAuthor\":true,\"postContentTemplateEnabled\":false,\"postTitleTemplateEnabled\":false,\"postTitleTemplate\":\"\",\"postContentTemplate\":\"\",\"lastPageButton\":null,\"pagination\":null,\"firstPageCssClass\":null}', NULL, '{\"5e693710c8606\":{\"id\":\"5e693710c8606\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}', '{\"5e693710c81bc\":{\"id\":\"5e693710c81bc\",\"to\":\"{admin_email}\",\"name\":\"Admin Notification\",\"event\":\"form_submission\",\"toType\":\"email\",\"subject\":\"New submission from {form_title}\",\"message\":\"{all_fields}\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_form_view`
--

CREATE TABLE `wp_rg_form_view` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` mediumint(8) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rg_form_view`
--

INSERT INTO `wp_rg_form_view` (`id`, `form_id`, `date_created`, `ip`, `count`) VALUES
(1, 1, '2020-03-11 16:01:05', '', 107);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_incomplete_submissions`
--

CREATE TABLE `wp_rg_incomplete_submissions` (
  `uuid` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `submission` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead`
--

CREATE TABLE `wp_rg_lead` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_detail`
--

CREATE TABLE `wp_rg_lead_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lead_id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `field_number` float NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_detail_long`
--

CREATE TABLE `wp_rg_lead_detail_long` (
  `lead_detail_id` bigint(20) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_meta`
--

CREATE TABLE `wp_rg_lead_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `lead_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_notes`
--

CREATE TABLE `wp_rg_lead_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `lead_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `note_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Menu', 'menu', 0),
(3, 'simple', 'simple', 0),
(4, 'grouped', 'grouped', 0),
(5, 'variable', 'variable', 0),
(6, 'external', 'external', 0),
(7, 'exclude-from-search', 'exclude-from-search', 0),
(8, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(9, 'featured', 'featured', 0),
(10, 'outofstock', 'outofstock', 0),
(11, 'rated-1', 'rated-1', 0),
(12, 'rated-2', 'rated-2', 0),
(13, 'rated-3', 'rated-3', 0),
(14, 'rated-4', 'rated-4', 0),
(15, 'rated-5', 'rated-5', 0),
(16, 'Uncategorized', 'uncategorized', 0),
(17, 'Primary', 'primary', 0),
(18, 'Secondary', 'secondary', 0),
(19, 'Legal', 'legal', 0),
(20, 'Side Menu', 'side-menu', 0),
(21, 'Side Bottom', 'side-bottom', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(5, 2, 0),
(6, 2, 0),
(9, 2, 0),
(49, 17, 0),
(54, 18, 0),
(65, 19, 0),
(66, 19, 0),
(92, 18, 0),
(93, 17, 0),
(189, 20, 0),
(190, 20, 0),
(191, 20, 0),
(192, 20, 0),
(193, 20, 0),
(194, 20, 0),
(195, 20, 0),
(196, 20, 0),
(197, 21, 0),
(198, 21, 0),
(199, 21, 0),
(200, 21, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 3),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_type', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_visibility', '', 0, 0),
(16, 16, 'product_cat', '', 0, 0),
(17, 17, 'nav_menu', '', 0, 2),
(18, 18, 'nav_menu', '', 0, 2),
(19, 19, 'nav_menu', '', 0, 2),
(20, 20, 'nav_menu', '', 0, 8),
(21, 21, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(18, 1, 'wp_user-settings-time', '1585089429'),
(19, 1, 'wp_dashboard_quick_press_last_post_id', '102'),
(20, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(24, 1, 'nav_menu_recently_edited', '20'),
(26, 1, '_woocommerce_tracks_anon_id', 'woo:Qe+7OweL9WbI0iiTJUOh47fx'),
(28, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(29, 1, 'default_password_nag', ''),
(30, 1, 'wc_last_active', '1602460800'),
(41, 1, 'meta-box-order_call_to_action', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:52:\"submitdiv,pageparentdiv,featured_toggle,postimagediv\";s:6:\"normal\";s:70:\"postexcerpt,image-gallery,video-gallery,slugdiv,linkage,color_controls\";s:8:\"advanced\";s:7:\"details\";}'),
(42, 1, 'screen_layout_call_to_action', '2'),
(43, 1, 'gform_recent_forms', 'a:1:{i:0;s:1:\"1\";}'),
(50, 1, '_order_count', '0'),
(52, 1, 'session_tokens', 'a:2:{s:64:\"960b5ac9e8d74cb231d4096c09cdcfdef0d1252d4f8a6956888004a34dc3453d\";a:4:{s:10:\"expiration\";i:1603183052;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36\";s:5:\"login\";i:1601973452;}s:64:\"82785d2a3122cf82aa43a17de61beed995a145f19731f820a239af06aeeff640\";a:4:{s:10:\"expiration\";i:1602636565;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36\";s:5:\"login\";i:1602463765;}}'),
(54, 1, 'dismissed_install_notice', '1'),
(55, 1, 'dismissed_no_secure_connection_notice', '1'),
(56, 1, 'dismissed_template_files_notice', '1'),
(57, 1, 'dismissed_maxmind_license_key_notice', '1'),
(58, 1, 'wp_yoast_notifications', 'a:2:{i:0;a:2:{s:7:\"message\";s:430:\"Yoast SEO and WooCommerce can work together a lot better by adding a helper plugin. Please install Yoast WooCommerce SEO to make your life better. <a href=\"https://yoa.st/1o0?php_version=7.3&platform=wordpress&platform_version=5.3.2&software=free&software_version=13.3&days_active=30plus&user_language=en_US\" aria-label=\"More information about Yoast WooCommerce SEO\" target=\"_blank\" rel=\"noopener noreferrer\">More information</a>.\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:44:\"wpseo-suggested-plugin-yoast-woocommerce-seo\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:5:\"admin\";s:9:\"user_pass\";s:60:\"$2y$10$Jyb0TkDAPleFnMr3HHaeZO2QStqF3iS2Feo257dz0WP8/pg6eu8y2\";s:13:\"user_nicename\";s:5:\"admin\";s:10:\"user_email\";s:17:\"developers@jac.co\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2019-05-27 07:16:26\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:5:\"admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:15:\"wp_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:117:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:20:\"wpseo_manage_options\";b:1;s:10:\"copy_posts\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";a:1:{i:0;s:15:\"install_plugins\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:381:\"Yoast SEO and Advanced Custom Fields can work together a lot better by adding a helper plugin. Please install ACF Content Analysis for Yoast SEO to make your life better. <a href=\"https://wordpress.org/plugins/acf-content-analysis-for-yoast-seo/\" aria-label=\"More information about ACF Content Analysis for Yoast SEO\" target=\"_blank\" rel=\"noopener noreferrer\">More information</a>.\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:41:\"wpseo-suggested-plugin-yoast-acf-analysis\";s:4:\"user\";r:7;s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";a:1:{i:0;s:15:\"install_plugins\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(59, 1, '_yoast_wpseo_profile_updated', '1601973413'),
(60, 1, 'wpseo_title', ''),
(61, 1, 'wpseo_metadesc', ''),
(62, 1, 'wpseo_noindex_author', ''),
(63, 1, 'wpseo_content_analysis_disable', ''),
(64, 1, 'wpseo_keyword_analysis_disable', ''),
(65, 1, 'billing_first_name', ''),
(66, 1, 'billing_last_name', ''),
(67, 1, 'billing_company', ''),
(68, 1, 'billing_address_1', ''),
(69, 1, 'billing_address_2', ''),
(70, 1, 'billing_city', ''),
(71, 1, 'billing_postcode', ''),
(72, 1, 'billing_country', ''),
(73, 1, 'billing_state', ''),
(74, 1, 'billing_phone', ''),
(75, 1, 'billing_email', 'developers@jac.co'),
(76, 1, 'shipping_first_name', ''),
(77, 1, 'shipping_last_name', ''),
(78, 1, 'shipping_company', ''),
(79, 1, 'shipping_address_1', ''),
(80, 1, 'shipping_address_2', ''),
(81, 1, 'shipping_city', ''),
(82, 1, 'shipping_postcode', ''),
(83, 1, 'shipping_country', ''),
(84, 1, 'shipping_state', ''),
(85, 1, 'last_update', '1601973414'),
(87, 1, 'closedpostboxes_page', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(88, 1, 'metaboxhidden_page', 'a:2:{i:0;s:7:\"slugdiv\";i:1;s:9:\"authordiv\";}'),
(89, 1, 'meta-box-order_page', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:69:\"submitdiv,pageparentdiv,video_popup,postimagediv,page-secondary-image\";s:6:\"normal\";s:126:\"acf-group_5f7d91256a182,image-gallery,video-gallery,revisionsdiv,postexcerpt,slugdiv,authordiv,wysiwyg,cta_selector,wpseo_meta\";s:8:\"advanced\";s:0:\"\";}'),
(90, 1, 'screen_layout_page', '2');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$2y$10$Jyb0TkDAPleFnMr3HHaeZO2QStqF3iS2Feo257dz0WP8/pg6eu8y2', 'admin', 'developers@jac.co', '', '2019-05-27 07:16:26', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_notes`
--

CREATE TABLE `wp_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_admin_notes`
--

INSERT INTO `wp_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `icon`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`) VALUES
(1, 'wc-admin-welcome-note', 'info', 'en_US', 'New feature(s)', 'Welcome to the new WooCommerce experience! In this new release you\'ll be able to have a glimpse of how your store is doing in the Dashboard, manage important aspects of your business (such as managing orders, stock, reviews) from anywhere in the interface, dive into your store data with a completely new Analytics section and more!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-12 14:54:01', NULL, 0),
(2, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-12 14:54:01', NULL, 0),
(3, 'wc-update-db-reminder', 'update', 'en_US', 'WooCommerce database update required', 'WooCommerce has been updated! To keep things running smoothly, we have to update your database to the newest version. The database update process runs in the background and may take a little while, so please be patient. Advanced users can alternatively update via <a href=\"https://github.com/woocommerce/woocommerce/wiki/Upgrading-the-database-using-WP-CLI\">WP CLI</a>.', 'info', '{}', 'unactioned', 'woocommerce-core', '2020-03-12 14:54:01', NULL, 0),
(4, 'wc-admin-add-first-product', 'info', 'en_US', 'Add your first product', 'Grow your revenue by adding products to your store. Add products manually, import from a sheet, or migrate from another platform.', 'product', '{}', 'unactioned', 'woocommerce-admin', '2020-03-12 14:54:54', NULL, 0),
(5, 'wc-admin-store-notice-giving-feedback', 'info', 'en_US', 'Review your experience', 'If you like WooCommerce Admin please leave us a 5 star rating. A huge thanks in advance!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-23 13:31:25', NULL, 0),
(6, 'wc-admin-mobile-app', 'info', 'en_US', 'Install Woo mobile app', 'Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.', 'phone', '{}', 'unactioned', 'woocommerce-admin', '2020-03-23 13:31:25', NULL, 0),
(7, 'wc-admin-usage-tracking-opt-in', 'info', 'en_US', 'Help WooCommerce improve with usage tracking', 'Gathering usage data allows us to improve WooCommerce. Your store will be considered as we evaluate new features, judge the quality of an update, or determine if an improvement makes sense. You can always visit the <a href=\"http://localhost:8888/foundation-bedrock-theme/web/wp/wp-admin/admin.php?page=wc-settings&#038;tab=advanced&#038;section=woocommerce_com\" target=\"_blank\">Settings</a> and choose to stop sharing data. <a href=\"https://woocommerce.com/usage-tracking\" target=\"_blank\">Read more</a> about what data we collect.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-23 13:31:25', NULL, 0),
(8, 'wc-admin-store-notice-setting-moved', 'info', 'en_US', 'Looking for the Store Notice setting?', 'It can now be found in the Customizer.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-24 23:21:51', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_note_actions`
--

CREATE TABLE `wp_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_admin_note_actions`
--

INSERT INTO `wp_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`) VALUES
(1, 1, 'learn-more', 'Learn more', 'https://woocommerce.wordpress.com/', 'actioned', 0),
(2, 2, 'connect', 'Connect', '?page=wc-addons&section=helper', 'actioned', 0),
(5, 4, 'add-a-product', 'Add a product', 'http://localhost:8888/foundation-bedrock-theme/web/wp/wp-admin/post-new.php?post_type=product', 'actioned', 1),
(6, 5, 'share-feedback', 'Review', 'https://wordpress.org/support/plugin/woocommerce-admin/reviews/?rate=5#new-post', 'actioned', 0),
(7, 6, 'learn-more', 'Learn more', 'https://woocommerce.com/mobile/', 'actioned', 0),
(8, 7, 'tracking-dismiss', 'Dismiss', '', 'actioned', 0),
(9, 7, 'tracking-opt-in', 'Activate usage tracking', '', 'actioned', 1),
(16, 8, 'open-customizer', 'Open Customizer', 'customize.php', 'actioned', 0),
(53, 3, 'update-db_run', 'Update WooCommerce Database', 'http://localhost:8888/lnccdev/web/wp/wp-admin/admin.php?page=wc-settings&do_update_woocommerce=true&wc_db_update_nonce=748590c520', 'unactioned', 1),
(54, 3, 'update-db_learn-more', 'Learn more about updates', 'https://docs.woocommerce.com/document/how-to-update-woocommerce/', 'unactioned', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_category_lookup`
--

CREATE TABLE `wp_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_category_lookup`
--

INSERT INTO `wp_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(16, 16);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_customer_lookup`
--

CREATE TABLE `wp_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_coupon_lookup`
--

CREATE TABLE `wp_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_product_lookup`
--

CREATE TABLE `wp_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_stats`
--

CREATE TABLE `wp_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_tax_lookup`
--

CREATE TABLE `wp_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_product_meta_lookup`
--

CREATE TABLE `wp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_tax_rate_classes`
--

CREATE TABLE `wp_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_tax_rate_classes`
--

INSERT INTO `wp_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(2, '1', 'a:7:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:731:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2020-10-06T08:36:54+00:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"GB\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"GB\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:17:\"developers@jac.co\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1602636644);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_links`
--

CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`) VALUES
(7, 'http://bedrock.loc/wp/wp-admin/', 2, 0, 'external'),
(8, 'http://localhost:8888/lnccdev/web/account-dashboard', 89, 82, 'internal'),
(9, 'http://localhost:8888/lnccdev/web/account/orders', 89, 89, 'internal'),
(10, 'http://localhost:8888/lnccdev/web/account/edit-address', 89, 89, 'internal'),
(11, 'http://localhost:8888/lnccdev/web/account/edit-account', 89, 89, 'internal'),
(12, 'http://localhost:8888/lnccdev/web/wp/wp-login.php?action=logout&amp;_wpnonce=a03c045016', 89, 0, 'internal'),
(13, 'http://localhost:8888/lnccdev/web/customer-logout/?_wpnonce=0b04cea9bf', 89, 0, 'internal'),
(14, '/orders/', 89, 0, 'internal'),
(15, '/edit-address/', 89, 0, 'internal'),
(16, '/edit-account/', 89, 0, 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_meta`
--

CREATE TABLE `wp_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yoast_seo_meta`
--

INSERT INTO `wp_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(2, 0, 0),
(3, 0, 0),
(4, 0, 0),
(7, 0, 0),
(12, 0, 0),
(15, 0, 0),
(16, 0, 0),
(17, 0, 0),
(18, 0, 0),
(19, 0, 0),
(20, 0, 0),
(21, 0, 0),
(22, 0, 0),
(23, 0, 0),
(24, 0, 0),
(25, 0, 0),
(26, 0, 0),
(27, 0, 0),
(28, 0, 0),
(29, 0, 0),
(31, 0, 0),
(33, 0, 0),
(35, 0, 0),
(36, 0, 0),
(37, 0, 0),
(38, 0, 0),
(39, 0, 0),
(43, 0, 0),
(44, 0, 0),
(46, 0, 0),
(47, 0, 0),
(48, 0, 0),
(50, 0, 0),
(53, 0, 0),
(55, 0, 0),
(59, 0, 0),
(61, 0, 0),
(62, 0, 0),
(67, 0, 0),
(68, 0, 0),
(69, 0, 0),
(70, 0, 0),
(72, 0, 0),
(73, 0, 0),
(74, 0, 0),
(75, 0, 0),
(76, 0, 0),
(78, 0, 0),
(79, 0, 0),
(82, 0, 1),
(84, 0, 0),
(86, 0, 0),
(89, 9, 3),
(91, 0, 0),
(94, 0, 0),
(95, 0, 0),
(96, 0, 0),
(97, 0, 0),
(98, 0, 0),
(99, 0, 0),
(100, 0, 0),
(103, 0, 0),
(106, 0, 0),
(108, 0, 0),
(109, 0, 0),
(110, 0, 0),
(111, 0, 0),
(112, 0, 0),
(113, 0, 0),
(114, 0, 0),
(115, 0, 0),
(116, 0, 0),
(117, 0, 0),
(118, 0, 0),
(119, 0, 0),
(120, 0, 0),
(121, 0, 0),
(122, 0, 0),
(201, 0, 0),
(203, 0, 0),
(204, 0, 0),
(208, 0, 0),
(209, 0, 0),
(211, 0, 0),
(213, 0, 0),
(215, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_gf_addon_feed`
--
ALTER TABLE `wp_gf_addon_feed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addon_form` (`addon_slug`,`form_id`);

--
-- Indexes for table `wp_gf_draft_submissions`
--
ALTER TABLE `wp_gf_draft_submissions`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_gf_entry`
--
ALTER TABLE `wp_gf_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `form_id_status` (`form_id`,`status`);

--
-- Indexes for table `wp_gf_entry_meta`
--
ALTER TABLE `wp_gf_entry_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `meta_value` (`meta_value`(191));

--
-- Indexes for table `wp_gf_entry_notes`
--
ALTER TABLE `wp_gf_entry_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `entry_user_key` (`entry_id`,`user_id`);

--
-- Indexes for table `wp_gf_form`
--
ALTER TABLE `wp_gf_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_gf_form_meta`
--
ALTER TABLE `wp_gf_form_meta`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `wp_gf_form_revisions`
--
ALTER TABLE `wp_gf_form_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_gf_form_view`
--
ALTER TABLE `wp_gf_form_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_podsrel`
--
ALTER TABLE `wp_podsrel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_item_idx` (`field_id`,`item_id`),
  ADD KEY `rel_field_rel_item_idx` (`related_field_id`,`related_item_id`),
  ADD KEY `field_rel_item_idx` (`field_id`,`related_item_id`),
  ADD KEY `rel_field_item_idx` (`related_field_id`,`item_id`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_rg_form`
--
ALTER TABLE `wp_rg_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rg_form_meta`
--
ALTER TABLE `wp_rg_form_meta`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `wp_rg_form_view`
--
ALTER TABLE `wp_rg_form_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_rg_incomplete_submissions`
--
ALTER TABLE `wp_rg_incomplete_submissions`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_rg_lead`
--
ALTER TABLE `wp_rg_lead`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `wp_rg_lead_detail`
--
ALTER TABLE `wp_rg_lead_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `lead_field_number` (`lead_id`,`field_number`),
  ADD KEY `lead_field_value` (`value`(191));

--
-- Indexes for table `wp_rg_lead_detail_long`
--
ALTER TABLE `wp_rg_lead_detail_long`
  ADD PRIMARY KEY (`lead_detail_id`);

--
-- Indexes for table `wp_rg_lead_meta`
--
ALTER TABLE `wp_rg_lead_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `form_id_meta_key` (`form_id`,`meta_key`(191));

--
-- Indexes for table `wp_rg_lead_notes`
--
ALTER TABLE `wp_rg_lead_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `lead_user_key` (`lead_id`,`user_id`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `wp_wc_category_lookup`
--
ALTER TABLE `wp_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_order_coupon_lookup`
--
ALTER TABLE `wp_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_product_lookup`
--
ALTER TABLE `wp_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_stats`
--
ALTER TABLE `wp_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `wp_wc_order_tax_lookup`
--
ALTER TABLE `wp_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_product_meta_lookup`
--
ALTER TABLE `wp_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `wp_yoast_seo_meta`
--
ALTER TABLE `wp_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=276087;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_gf_addon_feed`
--
ALTER TABLE `wp_gf_addon_feed`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_gf_entry`
--
ALTER TABLE `wp_gf_entry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_gf_entry_meta`
--
ALTER TABLE `wp_gf_entry_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_gf_entry_notes`
--
ALTER TABLE `wp_gf_entry_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_gf_form`
--
ALTER TABLE `wp_gf_form`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_gf_form_revisions`
--
ALTER TABLE `wp_gf_form_revisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_gf_form_view`
--
ALTER TABLE `wp_gf_form_view`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3857;

--
-- AUTO_INCREMENT for table `wp_podsrel`
--
ALTER TABLE `wp_podsrel`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1483;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `wp_rg_form`
--
ALTER TABLE `wp_rg_form`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_rg_form_view`
--
ALTER TABLE `wp_rg_form_view`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_rg_lead`
--
ALTER TABLE `wp_rg_lead`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rg_lead_detail`
--
ALTER TABLE `wp_rg_lead_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rg_lead_meta`
--
ALTER TABLE `wp_rg_lead_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rg_lead_notes`
--
ALTER TABLE `wp_rg_lead_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
